find_package(Catch2 CONFIG REQUIRED)
include(CTest)
include(Catch)

macro(valo_add_test target)
    set(test_target ${target}-tests)
    add_executable(${test_target})
    target_compile_options(${test_target} PRIVATE ${VALO_COMPILE_OPTIONS})

    # Link catch2 unit test library
    target_link_libraries(${test_target} PRIVATE Catch2::Catch2WithMain)
    catch_discover_tests(${test_target} WORKING_DIRECTORY $<TARGET_FILE_DIR:${test_target}>)
endmacro()
