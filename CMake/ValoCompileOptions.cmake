if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU" OR CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(VALO_GNU_AND_CLANG_COMMON_COMPILE_OPTIONS
        -pedantic
        -pedantic-errors
        -fno-common
        -fno-exceptions
        -fno-rtti
        -Wall
        -Wextra
        -Werror
        -Werror=return-type
        -Wcast-align
        -Wcast-qual
        -Wconversion
        -Wctor-dtor-privacy
        -Wdisabled-optimization
        -Wdouble-promotion
        -Weffc++
        -Wextra-semi
        -Wformat=2
        -Wformat-security
        -Wfloat-equal
        -Winit-self
        -Wmisleading-indentation
        -Wmissing-declarations
        -Wmissing-include-dirs
        -Wnon-virtual-dtor
        -Wnull-dereference
        -Wold-style-cast
        -Woverloaded-virtual
        -Wpointer-arith
        -Wredundant-decls
        -Wshadow
        -Wsign-promo
        -Wsuggest-override
        -Wswitch-default
        -Wundef
        -Wuninitialized
        -Wunknown-pragmas
        -Wunreachable-code
        -Wzero-as-null-pointer-constant

        -Wno-sign-conversion
        -Wno-deprecated-declarations #TODO: REMOVE
        -Wno-return-type-c-linkage

        # DXC requires MS extensions
        -fms-extensions #TODO: REMOVE

        #-fdata-sections
        #-ffunction-sections
        #-Wl,--gc-sections

        #-Wsuggest-final-methods
        #-Wsuggest-final-types

        #-fstack-usage
        #-Wstack-usage=512
    )

endif()

if(CMAKE_CXX_COMPILER_ID STREQUAL "Intel")
    message(FATAL "Intel C++ is not supported")
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(VALO_DEBUG_COMPILE_OPTIONS -g -O0)
    set(VALO_RELEASE_COMPILE_OPTIONS -O3 -DNDEBUG)
    set(VALO_COMMON_COMPILE_OPTIONS
        -Wduplicated-branches
        -Wduplicated-cond
        -Wformat-overflow
        -Wformat-truncation
        -Wimplicit-fallthrough=5
        -Wlogical-op
        -Wmaybe-uninitialized
        -Wstrict-null-sentinel
        ${VALO_GNU_AND_CLANG_COMMON_COMPILE_OPTIONS}
    )
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "Clang")
    set(VALO_DEBUG_COMPILE_OPTIONS -g -O0)
    set(VALO_RELEASE_COMPILE_OPTIONS -O3 -DNDEBUG)
    set(VALO_COMMON_COMPILE_OPTIONS
        -Wdocumentation
        -Wimplicit-fallthrough
        ${VALO_GNU_AND_CLANG_COMMON_COMPILE_OPTIONS}
    )
elseif(CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
    set(VALO_DEBUG_COMPILE_OPTIONS /MP /Od /DEBUG)
    set(VALO_RELEASE_COMPILE_OPTIONS /MP /O2 /DNDEBUG)
    set(VALO_COMMON_COMPILE_OPTIONS
        /GR-
        /W4
        /wd4201 # Disable GLM warnings
    )
endif()

set(VALO_COMPILE_OPTIONS
    "$<$<CONFIG:DEBUG>:${VALO_DEBUG_COMPILE_OPTIONS}>"
    "$<$<CONFIG:RELEASE>:${VALO_RELEASE_COMPILE_OPTIONS}>"
    ${VALO_COMMON_COMPILE_OPTIONS}
)
