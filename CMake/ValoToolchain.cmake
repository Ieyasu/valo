# Set C++ Language version

set(CMAKE_CXX_STANDARD 23)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CXX_SCAN_FOR_MODULES ON)

# Enable "import std" support
#set(CMAKE_EXPERIMENTAL_CXX_IMPORT_STD "0e5b6991-d74f-4b3d-a41c-cf096e0b2508")
#set(CMAKE_CXX_MODULE_STD ON)

# Configure vcpkg

set(VCPKG_RELATIVE_PATH "scripts/buildsystems/vcpkg.cmake")
set(VCPKG_SUBMODULE_ROOT "${CMAKE_CURRENT_LIST_DIR}/../Modules/vcpkg")
set(VCPKG_SUBMODULE_TOOLCHAIN_FILE "${VCPKG_SUBMODULE_ROOT}/${VCPKG_RELATIVE_PATH}")

# Try to find vcpkg from submodule
if(NOT DEFINED VCPKG_TOOLCHAIN_FILE AND EXISTS ${VCPKG_SUBMODULE_TOOLCHAIN_FILE})
    set(VCPKG_TOOLCHAIN_FILE ${VCPKG_SUBMODULE_TOOLCHAIN_FILE})
    set(VCPKG_ROOT ${VCPKG_SUBMODULE_ROOT})

    message(STATUS "Using vcpkg from submodule")
endif()

# Try to find vcpkg from environment variable
if(NOT DEFINED VCPKG_TOOLCHAIN_FILE AND DEFINED ENV{VCPKG_ROOT})
    set(VCPKG_TOOLCHAIN_FILE "$ENV{VCPKG_ROOT}/${VCPKG_RELATIVE_PATH}")

    message(STATUS "Using vcpkg from environment variable: VCPKG_ROOT=${VCPKG_TOOLCHAIN_FILE}")
endif()

# Try to find vcpkg from PATH
if(NOT DEFINED VCPKG_TOOLCHAIN_FILE)
    if(WIN32)
        execute_process(COMMAND where vcpkg OUTPUT_VARIABLE VCPKG_EXECUTABLE)
    elseif(UNIX)
        execute_process(COMMAND which vcpkg OUTPUT_VARIABLE VCPKG_EXECUTABLE)
    endif()

    if (DEFINED VCPKG_EXECUTABLE)
        get_filename_component(VCPKG_ROOT ${VCPKG_EXECUTABLE} DIRECTORY)
        set(VCPKG_TOOLCHAIN_FILE ${VCPKG_ROOT}/${VCPKG_RELATIVE_PATH})

        message(STATUS "Using vcpkg from path: ${VCPKG_TOOLCHAIN_FILE}")
    endif()
endif()

# Make sure vcpkg was found
if(NOT DEFINED VCPKG_TOOLCHAIN_FILE)
    if(WIN32)
        set(VCPKG_BOOSTRAP_CMD "Modules/vcpkg/boostrap-vcpkg.bat -disableMetrics")
    else()
        set(VCPKG_BOOSTRAP_CMD "Modules/vcpkg/boostrap-vcpkg.sh -disableMetrics")
    endif()

    message(FATAL_ERROR "Failed to find vcpkg. Run 'git submodule update --init --recursive' followed by '${VCPKG_BOOTSTRAP_CMD}' before using CMake")
endif()

include(${VCPKG_TOOLCHAIN_FILE})
