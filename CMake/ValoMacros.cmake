set(VALO_GLTF_SAMPLE_ASSETS_DIRECTORY ${CMAKE_SOURCE_DIR}/Modules/glTF-Sample-Assets/Models)

set(VALO_ASSET_DIRECTORY_NAME "Assets")
set(VALO_SHADER_DIRECTORY_NAME "Shaders")
set(VALO_ENGINE_ASSET_DIRECTORY ${CMAKE_BINARY_DIR}/Engine/${VALO_ASSET_DIRECTORY_NAME})
set(VALO_ENGINE_SHADER_DIRECTORY ${CMAKE_BINARY_DIR}/Engine/${VALO_SHADER_DIRECTORY_NAME})

add_custom_target(ValoRemoveEngineAssetDirectory
    COMMAND ${CMAKE_COMMAND} -E make_directory
    ${VALO_ENGINE_ASSET_DIRECTORY}
)
add_custom_target(ValoCreateEngineAssetDirectory
    COMMAND ${CMAKE_COMMAND} -E make_directory
    ${VALO_ENGINE_ASSET_DIRECTORY}
)
add_dependencies(ValoCreateEngineAssetDirectory ValoRemoveEngineAssetDirectory)

add_custom_target(ValoRemoveEngineShaderDirectory
    COMMAND ${CMAKE_COMMAND} -E make_directory
    ${VALO_ENGINE_SHADER_DIRECTORY}
)
add_custom_target(ValoCreateEngineShaderDirectory
    COMMAND ${CMAKE_COMMAND} -E make_directory
    ${VALO_ENGINE_SHADER_DIRECTORY}
)
add_dependencies(ValoCreateEngineShaderDirectory ValoRemoveEngineShaderDirectory)

macro(valo_add_library _in_library_type _in_target_name _in_target_var_name)
    # Final target name is of form {PROJECT_NAME}{_in_target_name}, which is saved to _in_target_var_name
    set(${_in_target_var_name} ${PROJECT_NAME}${_in_target_name})
    set(${_in_target_var_name} ${${_in_target_var_name}} PARENT_SCOPE)
    set(target ${${_in_target_var_name}})
    add_library(${target} ${_in_library_type})
    target_compile_options(${target} PRIVATE ${VALO_COMPILE_OPTIONS})

    # Add include files if they exist
    target_include_directories(${target} PRIVATE ${CMAKE_CURRENT_LIST_DIR}/Source)
    if(EXISTS ${CMAKE_CURRENT_LIST_DIR}/Include)
        target_include_directories(${target} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/Include)
    endif()

    if(VALO_ENABLE_TESTS)
        # Enable test coverage
        if(VALO_ENABLE_TEST_COVERAGE AND CMAKE_BUILD_TYPE STREQUAL "Debug")
            target_compile_options(${target} PRIVATE -fprofile-arcs -ftest-coverage)
            target_link_libraries(${target} PRIVATE gcov)
        endif()

        # Add tests if they exist
        if(EXISTS ${CMAKE_CURRENT_LIST_DIR}/Test)
            add_subdirectory(Test)
        endif()
    endif()

    # Copy library assets
    set(target_asset_dir ${CMAKE_CURRENT_LIST_DIR}/${VALO_ASSET_DIRECTORY_NAME})
    if(EXISTS ${target_asset_dir})
        add_custom_target(${target}CopyEngineAssets
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${target_asset_dir}
            ${VALO_ENGINE_ASSET_DIRECTORY}
        )
        add_dependencies(${target}CopyEngineAssets ValoCreateEngineAssetDirectory)
        add_dependencies(${target} ${target}CopyEngineAssets)
    endif()

    # Copy library shaders
    set(target_shader_dir "${CMAKE_CURRENT_LIST_DIR}/${VALO_SHADER_DIRECTORY_NAME}")
    if(EXISTS ${target_shader_dir})
        add_custom_target(${target}CopyEngineShaders
            COMMAND ${CMAKE_COMMAND} -E copy_directory
            ${target_shader_dir}
            ${VALO_ENGINE_SHADER_DIRECTORY}
        )
        add_dependencies(${target}CopyEngineShaders ValoCreateEngineShaderDirectory)
        add_dependencies(${target} ${target}CopyEngineShaders)
    endif()
endmacro()

macro(valo_add_module_library _in_target_name _in_target_var_name)
    valo_add_library(MODULE ${_in_target_name} ${_in_target_var_name})
endmacro()

macro(valo_add_shared_library _in_target_name _in_target_var_name)
    valo_add_library(SHARED ${_in_target_name} ${_in_target_var_name})
endmacro()

macro(valo_add_static_library _in_target_name _in_target_var_name)
    valo_add_library(STATIC ${_in_target_name} ${_in_target_var_name})
endmacro()

macro(valo_add_example _in_target)
    set(target ${_in_target})

    add_executable(${target})
    target_compile_options(${target} PRIVATE ${VALO_COMPILE_OPTIONS})

    target_link_libraries(${target} PRIVATE
        ${VALO_LIBRARIES}
        ${VALO_EXAMPLES_TARGET}
    )

    # Set a symbolic link so exe can find the /Data folder
    set(link_src "${CMAKE_SOURCE_DIR}/Data")
    if(UNIX AND NOT APPLE)
        set(link_dst "${CMAKE_CURRENT_BINARY_DIR}/Data")
    else()
        set(link_dst "${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/Data")
    endif()

    add_custom_target(${target}CreateEngineFolder
        COMMAND ${CMAKE_COMMAND} -E make_directory
        ${CMAKE_CURRENT_BINARY_DIR}/Engine
    )

    add_custom_target(${target}LinkEngineAssets
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        ${VALO_ENGINE_ASSET_DIRECTORY}
        ${CMAKE_CURRENT_BINARY_DIR}/Engine/${VALO_ASSET_DIRECTORY_NAME}
    )
    add_dependencies(${target}LinkEngineAssets ${target}CreateEngineFolder ValoCreateEngineAssetDirectory)
    add_dependencies(${target} ${target}LinkEngineAssets)

    add_custom_target(${target}LinkEngineShaders
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        ${VALO_ENGINE_SHADER_DIRECTORY}
        ${CMAKE_CURRENT_BINARY_DIR}/Engine/${VALO_SHADER_DIRECTORY_NAME}
    )
    add_dependencies(${target}LinkEngineShaders ${target}CreateEngineFolder ValoCreateEngineShaderDirectory)
    add_dependencies(${target} ${target}LinkEngineShaders)

    add_custom_target(${target}LinkglTFSampleAssets
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        ${VALO_GLTF_SAMPLE_ASSETS_DIRECTORY}
        ${CMAKE_CURRENT_BINARY_DIR}/glTFSampleAssets
    )
    add_dependencies(${target}LinkglTFSampleAssets ${target}CreateEngineFolder ValoCreateEngineShaderDirectory)
    add_dependencies(${target} ${target}LinkglTFSampleAssets)
endmacro()
