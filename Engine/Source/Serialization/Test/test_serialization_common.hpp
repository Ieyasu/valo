#pragma once

#include <valo/serialization/stream.hpp>

#include <valo/vector.hpp>

struct EmptyStruct final
{
    template <typename OutputStream>
    inline void Serialize(OutputStream /*unused*/) const
    {
    }

    template <typename InputStream>
    inline void Deserialize(InputStream /*unused*/)
    {
    }
};

struct ChildStruct final
{
    Int32           integer{};
    UInt64          uinteger{};
    Valo::String      string{};
    Valo::Vector<int> vector{};

    template <typename OutputStream>
    inline void Serialize(OutputStream stream) const
    {
        stream.SerializeProperty("integer", integer);
        stream.SerializeProperty("uinteger", uinteger);
        stream.SerializeProperty("string", string);
        stream.SerializeProperty("vector", vector);
    }

    template <typename InputStream>
    inline void Deserialize(InputStream stream)
    {
        stream.DeserializeProperty("integer", integer);
        stream.DeserializeProperty("uinteger", uinteger);
        stream.DeserializeProperty("string", string);
        stream.DeserializeProperty("vector", vector);
    }
};

struct ParentStruct final
{
    float       floating_point{};
    ChildStruct child{};

    template <typename OutputStream>
    inline void Serialize(OutputStream stream) const
    {
        stream.SerializeProperty("float", floating_point);
        stream.SerializeProperty("child", child);
    }

    template <typename InputStream>
    inline void Deserialize(InputStream stream)
    {
        stream.DeserializeProperty("float", floating_point);
        stream.DeserializeProperty("child", child);
    }
};
