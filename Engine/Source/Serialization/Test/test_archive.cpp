#include "test_serialization_common.hpp"

#include <valo/serialization/archive.hpp>

#include <valo/string.hpp>

#include <catch2/catch_test_macros.hpp>

namespace Valo
{
    TEST_CASE("Valo::Archive", "[serialization]")
    {
        auto archive = Serialization::YamlArchive{};

        auto empty = EmptyStruct{};

        auto parent1           = ParentStruct{};
        parent1.floating_point = 5.0f;
        parent1.child.integer  = -3;
        parent1.child.uinteger = 20;
        parent1.child.string   = "test string";
        parent1.child.vector   = {5, -3, 8};

        auto parent2 = ParentStruct{};

        SECTION("serialize() can serialize empty structs")
        {
            archive.Serialize(empty);

            String result_yaml;
            REQUIRE(archive.Emit(result_yaml));
            REQUIRE(StringFunc::TrimCopy(result_yaml).empty());
        }

        SECTION("deserialize() can deserialize empty structs")
        {
            REQUIRE(archive.Parse("{}"));
            archive.Deserialize(empty);
        }

        SECTION("serialize() can serialize complex structs")
        {
            archive.Serialize(parent1);

            String result_yaml;
            REQUIRE(archive.Emit(result_yaml));
        }

        SECTION("deserialize() can deserialize complex structs")
        {
            const auto* yaml = R"(
                float: 8
                child:
                  integer: -12
                  uinteger: 540
                  string: ""
                  vector:
                    - 4
                    - 8
                    - 16
                    - -32
            )";

            REQUIRE(archive.Parse(yaml));

            archive.Deserialize(parent2);

            REQUIRE(parent2.floating_point == 8.0f);
            REQUIRE(parent2.child.integer == -12);
            REQUIRE(parent2.child.uinteger == 540);
            REQUIRE(parent2.child.string.empty());
            REQUIRE(parent2.child.vector.size() == 4);
            REQUIRE(parent2.child.vector[0] == 4);
            REQUIRE(parent2.child.vector[1] == 8);
            REQUIRE(parent2.child.vector[2] == 16);
            REQUIRE(parent2.child.vector[3] == -32);
        }

        SECTION("serialize() and deserialize() can be used to copy an object")
        {
            archive.Serialize(parent1);
            archive.Deserialize(parent2);

            REQUIRE(parent1.floating_point == parent2.floating_point);
            REQUIRE(parent1.child.integer == parent2.child.integer);
            REQUIRE(parent1.child.uinteger == parent2.child.uinteger);
            REQUIRE(parent1.child.string == parent2.child.string);
            REQUIRE(parent1.child.vector.size() == parent2.child.vector.size());

            for (size_t i = 0; i < parent1.child.vector.size(); ++i)
            {
                REQUIRE(parent1.child.vector[i] == parent2.child.vector[i]);
            }
        }
    }
}
