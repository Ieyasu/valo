#include <valo/serialization/yaml/serialized_tree.hpp>

#include <valo/string.hpp>

#include <catch2/catch_test_macros.hpp>

namespace Valo
{
    TEST_CASE("Valo::SerializedTree", "[serialization]")
    {
        auto tree = SerializedTree();

        SECTION("SerializedTree::parse() can parse trivial JSON")
        {
            const auto yaml = String("{ \"value\": 1.0 }");
            REQUIRE(tree.Parse(yaml));
        }

        SECTION("SerializedTree::parse() can parse trivial YAML")
        {
            const auto yaml = String("value: 1.0");
            REQUIRE(tree.Parse(yaml));
        }

        SECTION("SerializedTree::emit() can emit parsed YAML to file")
        {
            const auto in_yaml = String("value: 1");
            tree.Parse(in_yaml);

            String out_yaml;
            REQUIRE(tree.Emit(out_yaml));
            REQUIRE(StringFunc::TrimCopy(out_yaml) == in_yaml);
        }
    }
}
