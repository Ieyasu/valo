module;

#include <Valo/Macros.hpp>

#include <ryml/ryml.hpp>
#include <ryml/ryml_std.hpp>

export module Valo.Serialization:SerializedTree;

import Valo.Std;

export namespace Valo
{
    class SerializedTree final
    {
    public:
        using NodeType = size_t;

        SerializedTree();

        SerializedTree(const SerializedTree&)            = delete;
        SerializedTree(SerializedTree&&)                 = delete;
        SerializedTree& operator=(const SerializedTree&) = delete;
        SerializedTree& operator=(SerializedTree&&)      = delete;
        ~SerializedTree();

        NodeType GetRoot() const;

        bool Emit(String& yaml) const;

        bool Parse(const String& yaml);

        bool Save(const String& filepath) const;

        bool Load(const String& filepath);

        // Node operations

        static bool IsValid(NodeType node);

        bool IsArray(NodeType node) const;

        bool IsObject(NodeType node) const;

        bool IsValue(NodeType node) const;

        size_t GetArraySize(NodeType node) const;

        NodeType GetChild(NodeType node, size_t index) const;

        NodeType AppendChild(NodeType node);

        NodeType FindChild(NodeType node, StringView name) const;

        NodeType AddChild(NodeType node, StringView name);

        void ToArray(NodeType node);

        void ToObject(NodeType node);

        void ToValue(NodeType node);

        template <typename T>
        T GetValue(NodeType node) const;

        template <typename T>
        void SetValue(NodeType node, const T& value);

    private:
        UniquePtr<ryml::Tree> m_tree{nullptr};
        String                m_buffer{};
    };

    template <typename T>
    void SerializedTree::SetValue(NodeType node, const T& value)
    {
        ToValue(node);

        auto node_ref = m_tree->ref(node);
        node_ref << value;
    }

    template <typename T>
    T SerializedTree::GetValue(NodeType node) const
    {
        VALO_ASSERT(IsValue(node));

        auto node_ref = m_tree->ref(node);
        T    value{};
        node_ref >> value;
        return value;
    }
}
