module;

#include <Valo/Macros.hpp>

#include <ryml/ryml.hpp>
#include <ryml/ryml_std.hpp>

module Valo.Serialization;

import Valo.Logger;
import Valo.Std;

namespace Valo
{
    SerializedTree::SerializedTree() :
        m_tree(new ryml::Tree())
    {
    }

    SerializedTree::~SerializedTree() = default;

    SerializedTree::NodeType SerializedTree::GetRoot() const
    {
        return m_tree->root_id();
    }

    bool SerializedTree::Emit(String& yaml) const
    {
        yaml = ryml::emitrs_yaml<String>(m_tree.get());
        return true;
    }

    bool SerializedTree::Parse(const String& yaml)
    {
        m_tree->clear();
        m_buffer = yaml;
        ryml::parse_in_place(ryml::to_substr(m_buffer), m_tree.get());
        return true;
    }

    bool SerializedTree::Save(const String& filepath) const
    {
        auto file = File(filepath, FileMode::Text, FileAccess::Read);
        if (!file.IsOpen())
        {
            Log::Error("Failed to save serialized data to {}", filepath);
            return false;
        }

        auto yaml = ryml::emitrs_yaml<String>(m_tree.get());
        file.WriteText(yaml);
        return true;
    }

    bool SerializedTree::Load(const String& filepath)
    {
        auto file = File(filepath, FileMode::Text, FileAccess::Read);
        if (!file.IsOpen())
        {
            Log::Error("Failed to open and load serialized data from {}", filepath);
            return false;
        }

        m_tree->clear();
        if (!file.ReadText(m_buffer))
        {
            Log::Error("Failed to read and load serialized data from {}", filepath);
        }
        ryml::parse_in_place(ryml::to_substr(m_buffer), m_tree.get());
        return true;
    }

    bool SerializedTree::IsValid(NodeType node)
    {
        return node != ryml::NONE;
    }

    bool SerializedTree::IsArray(NodeType node) const
    {
        return IsValid(node) && m_tree->is_seq(node);
    }

    bool SerializedTree::IsObject(NodeType node) const
    {
        return IsValid(node) && m_tree->is_map(node);
    }

    bool SerializedTree::IsValue(NodeType node) const
    {
        return IsValid(node) && (m_tree->is_val(node) || m_tree->is_keyval(node));
    }

    size_t SerializedTree::GetArraySize(NodeType node) const
    {
        VALO_ASSERT(IsArray(node));

        return m_tree->num_children(node);
    }

    SerializedTree::NodeType SerializedTree::GetChild(NodeType node, size_t index) const
    {
        VALO_ASSERT(IsArray(node));

        return m_tree->child(node, index);
    }

    SerializedTree::NodeType SerializedTree::AppendChild(NodeType node)
    {
        VALO_ASSERT(IsArray(node));

        return m_tree->append_child(node);
    }

    SerializedTree::NodeType SerializedTree::FindChild(NodeType node, StringView name) const
    {
        VALO_ASSERT(IsObject(node));

        return m_tree->find_child(node, ryml::csubstr(name.begin(), name.size()));
    }

    SerializedTree::NodeType SerializedTree::AddChild(NodeType node, StringView name)
    {
        VALO_ASSERT(IsObject(node));

        auto child_id = m_tree->find_child(node, ryml::csubstr(name.begin(), name.size()));
        if (child_id == ryml::NONE)
        {
            auto name_substr = m_tree->copy_to_arena(ryml::csubstr(name.begin(), name.size()));
            child_id         = m_tree->append_child(node);
            m_tree->to_map(child_id, ryml::to_csubstr(name_substr));
        }
        return child_id;
    }

    void SerializedTree::ToArray(NodeType node)
    {
        VALO_ASSERT(IsValid(node));

        if (IsArray(node))
        {
            return;
        }

        if (m_tree->has_key(node))
        {
            m_tree->change_type(node, ryml::KEYSEQ);
        }
        else
        {
            m_tree->change_type(node, ryml::KEY);
        }
    }

    void SerializedTree::ToObject(NodeType node)
    {
        VALO_ASSERT(IsValid(node));

        if (IsObject(node))
        {
            return;
        }

        if (m_tree->has_key(node))
        {
            m_tree->change_type(node, ryml::KEYMAP);
        }
        else
        {
            m_tree->change_type(node, ryml::MAP);
        }
    }

    void SerializedTree::ToValue(NodeType node)
    {
        VALO_ASSERT(IsValid(node));

        if (IsValue(node))
        {
            return;
        }

        m_tree->remove_children(node);

        if (m_tree->has_key(node))
        {
            const auto keyRef = m_tree->key(node);
            m_tree->to_keyval(node, keyRef, "");
        }
        else
        {
            m_tree->to_val(node, "");
        }
    }
}
