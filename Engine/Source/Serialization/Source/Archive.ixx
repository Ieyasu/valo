module;

#include <magic_enum.hpp>

#include <concepts>
#include <type_traits>

export module Valo.Serialization:Archive;

import Valo.Std;

import :SerializedTree;

export namespace Valo::Serialization
{
    template <typename SerializedTree>
    class Archive;

    template <typename SerializedTree>
    class InputStream final
    {
    public:
        InputStream(const Archive<SerializedTree>& archive, SerializedTree::NodeType node);

        template <typename T>
        void Deserialize(T& value) const;

        template <typename T>
        void DeserializeProperty(StringView name, T& value) const;

    private:
        ObserverPtr<const Archive<SerializedTree>> m_archive{};
        SerializedTree::NodeType                   m_node{};
    };

    template <typename SerializedTree>
    class OutputStream final
    {
    public:
        OutputStream(Archive<SerializedTree>& archive, SerializedTree::NodeType node);

        template <typename T>
        void Serialize(const T& value);

        template <typename T>
        void SerializeProperty(StringView name, const T& value);

    private:
        ObserverPtr<Archive<SerializedTree>> m_archive{};
        SerializedTree::NodeType             m_node{};
    };

    template <typename SerializedTree>
    class Archive final
    {
    public:
        bool Parse(const String& data);

        bool Emit(String& data) const;

        bool Load(const String& filepath);

        bool Save(const String& filepath) const;

        template <typename T>
        void Serialize(const T& value);

        template <typename T>
        void Deserialize(T& value) const;

    private:
        template <typename T>
        void Serialize(SerializedTree::NodeType node, const T& value);

        template <typename T>
        void Deserialize(SerializedTree::NodeType node, T& value) const;

        template <typename T>
        void SerializeProperty(SerializedTree::NodeType node, StringView name, const T& value);

        template <typename T>
        void DeserializeProperty(SerializedTree::NodeType node, StringView name, T& value) const;

        template <typename T>
        void SerializeEnum(SerializedTree::NodeType node, const T& value);

        template <typename T>
        void DeserializeEnum(SerializedTree::NodeType node, T& value) const;

        template <std::ranges::input_range T>
        void SerializeArray(SerializedTree::NodeType node, const T& value);

        template <std::ranges::forward_range T>
        void DeserializeArray(SerializedTree::NodeType node, T& value) const;

        template <typename T>
        void SerializeValue(SerializedTree::NodeType node, const T& value);

        template <typename T>
        void SerializeValue(SerializedTree::NodeType node, T& value) const;

        SerializedTree m_tree{};

        friend class InputStream<SerializedTree>;
        friend class OutputStream<SerializedTree>;
    };

    using YamlArchive = Archive<SerializedTree>;

    template <typename SerializedTree, typename T>
    concept IsSerializable = requires(T t, OutputStream<SerializedTree> p) {
        { t.Serialize(p) } -> std::same_as<void>;
    };

    template <typename SerializedTree, typename T>
    concept IsDeserializable = requires(T t, InputStream<SerializedTree> p) {
        { t.Deserialize(p) } -> std::same_as<void>;
    };

    template <typename SerializedTree>
    InputStream<SerializedTree>::InputStream(const Archive<SerializedTree>& archive, SerializedTree::NodeType node) :
        m_archive(&archive),
        m_node(node)
    {
    }

    template <typename SerializedTree>
    template <typename T>
    inline void InputStream<SerializedTree>::Deserialize(T& value) const
    {
        m_archive->Deserialize(m_node, value);
    }

    template <typename SerializedTree>
    template <typename T>
    inline void InputStream<SerializedTree>::DeserializeProperty(StringView name, T& value) const
    {
        m_archive->DeserializeProperty(m_node, name, value);
    }

    template <typename SerializedTree>
    OutputStream<SerializedTree>::OutputStream(Archive<SerializedTree>& archive, SerializedTree::NodeType node) :
        m_archive(&archive),
        m_node(node)
    {
    }

    template <typename SerializedTree>
    template <typename T>
    inline void OutputStream<SerializedTree>::Serialize(const T& value)
    {
        m_archive->Serialize(m_node, value);
    }

    template <typename SerializedTree>
    template <typename T>
    inline void OutputStream<SerializedTree>::SerializeProperty(StringView name, const T& value)
    {
        m_archive->SerializeProperty(m_node, name, value);
    }

    template <typename SerializedTree>
    bool Archive<SerializedTree>::Parse(const String& data)
    {
        return m_tree.Parse(data);
    }

    template <typename SerializedTree>
    bool Archive<SerializedTree>::Emit(String& data) const
    {
        return m_tree.Emit(data);
    }

    template <typename SerializedTree>
    bool Archive<SerializedTree>::Load(const String& filepath)
    {
        return m_tree.Load(filepath);
    }

    template <typename SerializedTree>
    bool Archive<SerializedTree>::Save(const String& filepath) const
    {
        return m_tree.Save(filepath);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::Serialize(const T& value)
    {
        Serialize(m_tree.GetRoot(), value);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::Deserialize(T& value) const
    {
        Deserialize(m_tree.GetRoot(), value);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::Serialize(SerializedTree::NodeType node, const T& value)
    {
        // Type implements serialization callback
        if constexpr (IsSerializable<SerializedTree, T>)
        {
            value.Serialize(OutputStream(*this, node));
        }
        // Type is an enum
        else if constexpr (std::is_enum_v<T>)
        {
            SerializeEnum(node, value);
        }
        // Type is non-string-like container
        else if constexpr (std::ranges::input_range<T> && std::negation_v<typename std::is_convertible<T, StringView>>)
        {
            SerializeArray(node, value);
        }
        // Type is a raw value or a string-like container
        else
        {
            SerializeValue(node, value);
        }
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::Deserialize(SerializedTree::NodeType node, T& value) const
    {
        if constexpr (IsDeserializable<SerializedTree, T>)
        {
            value.Deserialize(InputStream(*this, node));
        }
        // Type is an enum
        else if constexpr (std::is_enum_v<T>)
        {
            DeserializeEnum(node, value);
        }
        // Type is non-string-like container
        else if constexpr (std::ranges::forward_range<T> && std::negation_v<typename std::is_convertible<T, StringView>>)
        {
            DeserializeArray(node, value);
        }
        // Type is a raw value or a string-like container
        else
        {
            SerializeValue(node, value);
        }
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::SerializeProperty(SerializedTree::NodeType node, StringView name, const T& value)
    {
        m_tree.ToObject(node);

        const auto child_node = m_tree.AddChild(node, name);
        Serialize(child_node, value);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::DeserializeProperty(SerializedTree::NodeType node, StringView name, T& value) const
    {
        if (!m_tree.IsObject(node))
        {
            return;
        }

        const auto child_node = m_tree.FindChild(node, name);
        if (!m_tree.IsValid(child_node))
        {
            return;
        }

        Deserialize(child_node, value);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::SerializeEnum(SerializedTree::NodeType node, const T& value)
    {
        serialize_value(node, String(magic_enum::enum_name(value)));
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::DeserializeEnum(SerializedTree::NodeType node, T& value) const
    {
        auto str_value = String{};
        SerializeValue<String>(node, str_value);

        const auto enum_value = magic_enum::enum_cast<T>(str_value);
        if (enum_value.has_value())
        {
            value = enum_value.value();
        }
    }

    template <typename SerializedTree>
    template <std::ranges::input_range T>
    void Archive<SerializedTree>::SerializeArray(SerializedTree::NodeType node, const T& value)
    {
        m_tree.ToArray(node);

        for (size_t i = 0; i < value.size(); ++i)
        {
            const auto  child_node    = m_tree.AppendChild(node);
            const auto& child_element = value[i];
            Serialize(child_node, child_element);
        }
    }

    template <typename SerializedTree>
    template <std::ranges::forward_range T>
    void Archive<SerializedTree>::DeserializeArray(SerializedTree::NodeType node, T& value) const
    {
        if (!m_tree.IsArray(node))
        {
            return;
        }

        value.resize(m_tree.GetArraySize(node));
        for (size_t i = 0; i < value.size(); ++i)
        {
            const auto& child_node    = m_tree.GetChild(node, i);
            auto&       child_element = value[i];
            Deserialize(child_node, child_element);
        }
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::SerializeValue(SerializedTree::NodeType node, const T& value)
    {
        m_tree.ToValue(node);
        m_tree.SetValue(node, value);
    }

    template <typename SerializedTree>
    template <typename T>
    void Archive<SerializedTree>::SerializeValue(SerializedTree::NodeType node, T& value) const
    {
        if (!m_tree.IsValue(node))
        {
            return;
        }

        value = m_tree.template GetValue<T>(node);
    }
}
