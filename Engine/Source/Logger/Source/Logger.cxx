module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    Logger::Logger(LogLevel level) :
        m_level(level)
    {
    }

    LogLevel Logger::GetLevel() const
    {
        return m_level;
    }

    void Logger::SetLevel(LogLevel level)
    {
        m_level = level;
    }

    void Logger::Execute(LogLevel level, const String& message)
    {
        if (level >= m_level)
        {
            Log(level, message);
        }

        ExecuteNext(level, message);
    }
}
