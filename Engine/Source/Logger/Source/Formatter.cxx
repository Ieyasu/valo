module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    void Formatter::Execute(LogLevel level, const String& message)
    {
        const auto& formatted_message = Format(level, message);
        ExecuteNext(level, formatted_message);
    }
}
