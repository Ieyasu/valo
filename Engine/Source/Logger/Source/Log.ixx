module;

#include <Valo/Macros.hpp>

export module Valo.Logger:Log;

import Valo.Std;

import :Logger;
import :LoggerBuilder;
import :LogHandler;
import :LogLevel;

export namespace Valo::Log
{
    extern Mutex                 g_active_log_handler_mutex;
    extern UniquePtr<LogHandler> g_active_log_handler;

    [[nodiscard]] LoggerBuilder Configure();

    template <typename... Args>
    inline void Log(LogLevel level, const StringView message, Args&&... args)
    {
        ScopedLock<Mutex> lock(g_active_log_handler_mutex);

        if (g_active_log_handler)
        {
            const auto& full_message = StringFunc::Format(
                message,
                StringFunc::ToStringIfEnum(Memory::Forward<Args>(args))...);

            g_active_log_handler->Execute(level, full_message);
        }
    }

    template <typename... Args>
    inline void Trace(const StringView message, Args&&... args)
    {
        Log(LogLevel::Trace, message, Memory::Forward<Args>(args)...);
    }

    template <typename... Args>
    inline void Debug(const StringView message, Args&&... args)
    {
        Log(LogLevel::Debug, message, Memory::Forward<Args>(args)...);
    }

    template <typename... Args>
    inline void Info(const StringView message, Args&&... args)
    {
        Log(LogLevel::Info, message, Memory::Forward<Args>(args)...);
    }

    template <typename... Args>
    inline void Warning(const StringView message, Args&&... args)
    {
        Log(LogLevel::Warning, message, Memory::Forward<Args>(args)...);
    }

    template <typename... Args>
    inline void Error(const StringView message, Args&&... args)
    {
        Log(LogLevel::Error, message, Memory::Forward<Args>(args)...);
    }

    template <typename... Args>
    inline void Fatal(const StringView message, Args&&... args)
    {
        Log(LogLevel::Fatal, message, Memory::Forward<Args>(args)...);
        VALO_ASSERT(false);
    }

    template <typename Predicate, typename... Args>
    inline void CheckTrace(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Trace(message, Memory::Forward<Args>(args)...);
        }
    }

    template <typename Predicate, typename... Args>
    inline void CheckDebug(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Debug(message, Forward<Args>(args)...);
        }
    }

    template <typename Predicate, typename... Args>
    inline void CheckInfo(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Warning(message, Forward<Args>(args)...);
        }
    }

    template <typename Predicate, typename... Args>
    inline void CheckWarning(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Warning(message, Forward<Args>(args)...);
        }
    }

    template <typename Predicate, typename... Args>
    inline void CheckError(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Error(message, Forward<Args>(args)...);
        }
    }

    template <typename Predicate, typename... Args>
    inline void CheckFatal(Predicate predicate, const StringView message, Args&&... args)
    {
        if (!predicate)
        {
            Fatal(message, Forward<Args>(args)...);
        }
    }
}
