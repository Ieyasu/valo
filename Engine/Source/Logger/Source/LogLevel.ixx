export module Valo.Logger:LogLevel;

import Valo.Std;

export namespace Valo::Log
{
    enum class LogLevel : EnumSmall
    {
        Trace,
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
    };
}
