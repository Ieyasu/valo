module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    Mutex                 g_active_log_handler_mutex{};
    UniquePtr<LogHandler> g_active_log_handler = nullptr;

    LoggerBuilder Configure()
    {
        return {};
    }
}
