export module Valo.Logger:LogHandler;

import Valo.Std;

import :LogLevel;

export namespace Valo::Log
{
    class LogHandler
    {
    public:
        LogHandler()                             = default;
        LogHandler(const LogHandler&)            = delete;
        LogHandler(LogHandler&&)                 = delete;
        LogHandler& operator=(const LogHandler&) = delete;
        LogHandler& operator=(LogHandler&&)      = delete;
        virtual ~LogHandler()                    = default;

        void SetNext(UniquePtr<LogHandler> log_handler);

        void ExecuteNext(LogLevel level, const String& message);

        virtual void Execute(LogLevel level, const String& message) = 0;

    private:
        UniquePtr<LogHandler> m_next_log_handler{};
    };
}
