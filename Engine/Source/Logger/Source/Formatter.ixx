export module Valo.Logger:Formatter;

import Valo.Std;

import :LogHandler;
import :LogLevel;

export namespace Valo::Log
{
    class Formatter : public LogHandler
    {
    public:
        void Execute(LogLevel level, const String& message) override;

    protected:
        [[nodiscard]] virtual String Format(LogLevel level, const String& message) = 0;
    };
}
