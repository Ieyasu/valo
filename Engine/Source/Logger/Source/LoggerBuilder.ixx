export module Valo.Logger:LoggerBuilder;

import Valo.Std;

import :Formatter;
import :LogHandler;
import :LogLevel;
import :Logger;

export namespace Valo::Log
{
    class LoggerBuilder
    {
    public:
        LoggerBuilder()                                = default;
        LoggerBuilder(const LoggerBuilder&)            = delete;
        LoggerBuilder(LoggerBuilder&&)                 = delete;
        LoggerBuilder& operator=(const LoggerBuilder&) = delete;
        LoggerBuilder& operator=(LoggerBuilder&&)      = delete;
        ~LoggerBuilder()                               = default;

        template <typename T, typename... Args>
        LoggerBuilder& AddLogger(Args&&... args);

        template <typename T, typename... Args>
        LoggerBuilder& AddFormatter(Args&&... args);

        void Finish();

    private:
        template <typename T, typename... Args>
        LoggerBuilder& AddLogHandler(Args&&... args);

        UniquePtr<LogHandler>   m_root_log_handler{};
        ObserverPtr<LogHandler> m_current_log_handler{};
    };

    template <typename T, typename... Args>
    LoggerBuilder& LoggerBuilder::AddFormatter(Args&&... args)
    {
        return AddLogHandler<T>(Memory::Forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    LoggerBuilder& LoggerBuilder::AddLogger(Args&&... args)
    {
        return AddLogHandler<T>(Memory::Forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    LoggerBuilder& LoggerBuilder::AddLogHandler(Args&&... args)
    {
        auto next_logger = MakeUnique<T>(Memory::Forward<Args>(args)...);
        if (!m_root_log_handler)
        {
            m_root_log_handler    = Memory::Move(next_logger);
            m_current_log_handler = m_root_log_handler.get();
        }
        else
        {
            const auto next_logger_ptr = next_logger.get();
            m_current_log_handler->SetNext(Memory::Move(next_logger));
            m_current_log_handler = next_logger_ptr;
        }
        return *this;
    }

}
