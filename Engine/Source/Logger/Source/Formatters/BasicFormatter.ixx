export module Valo.Logger:BasicFormatter;

import Valo.Std;

import :Formatter;
import :LogLevel;

export namespace Valo::Log
{
    class BasicFormatter final : public Formatter
    {
    public:
        [[nodiscard]] String Format(LogLevel level, const String& message) override;
    };
}
