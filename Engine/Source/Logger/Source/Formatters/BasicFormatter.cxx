module;

#include <Valo/Macros.hpp>

#include <fmt/chrono.h>
#include <fmt/color.h>

#include <ctime>

module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    String BasicFormatter::Format(LogLevel level, const String& message)
    {
        auto        style = fmt::text_style();
        const char* tag{};

        switch (level)
        {
        case LogLevel::Trace:
            tag = "[TRACE]";
            break;
        case LogLevel::Info:
            tag = "[INFO] ";
            break;
        case LogLevel::Debug:
            tag = "[DEBUG]";
            break;
        case LogLevel::Warning:
            style = fmt::emphasis::bold | fg(fmt::terminal_color::yellow);
            tag   = "[WARN] ";
            break;
        case LogLevel::Error:
            style = fmt::emphasis::bold | fg(fmt::terminal_color::red);
            tag   = "[ERROR]";
            break;
        case LogLevel::Fatal:
            style = fmt::emphasis::bold | fg(fmt::terminal_color::red);
            tag   = "[FATAL]";
            break;
        default:
            VALO_ASSERT(false && "Unknown log level");
            return "";
        }

        const auto time = std::time(nullptr);
        return fmt::format(style, "[{:%Y-%m-%d %X}] {} {}", fmt::localtime(time), tag, message);
    }
}
