module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    void LogHandler::SetNext(UniquePtr<LogHandler> log_handler)
    {
        m_next_log_handler = Memory::Move(log_handler);
    }

    void LogHandler::ExecuteNext(LogLevel level, const String& message)
    {
        if (m_next_log_handler)
        {
            m_next_log_handler->Execute(level, message);
        }
    }
}
