module;

#include <Valo/Macros.hpp>

#include <iostream>

module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    ConsoleLogger::ConsoleLogger(LogLevel level) :
        Logger(level)
    {
    }

    void ConsoleLogger::Log(LogLevel level, const String& message)
    {
        auto* stream = &std::cout;
        switch (level)
        {
        case LogLevel::Trace:
        case LogLevel::Info:
        case LogLevel::Debug:
        case LogLevel::Warning:
            break;
        case LogLevel::Error:
        case LogLevel::Fatal:
            stream = &std::cerr;
            break;
        default:
            VALO_ASSERT(false && "Unknown log level");
            break;
        }

        *stream << message << std::endl;
    }
}
