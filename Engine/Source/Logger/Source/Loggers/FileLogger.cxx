module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    FileLogger::FileLogger(const String& path, LogLevel level) :
        Logger(level),
        m_file(path, FileMode::Text, FileAccess::Append)
    {
    }

    void FileLogger::Log(LogLevel /*level*/, const String& message)
    {
        m_file.WriteLine(message);
    }
}
