export module Valo.Logger:ConsoleLogger;

import Valo.Std;

import :Logger;
import :LogLevel;

export namespace Valo::Log
{
    class ConsoleLogger final : public Logger
    {
    public:
        explicit ConsoleLogger(LogLevel level = LogLevel::Info);

        void Log(LogLevel level, const String& message) override;
    };
}
