export module Valo.Logger:FileLogger;

import Valo.Std;

import :Logger;
import :LogLevel;

export namespace Valo::Log
{
    class FileLogger final : public Logger
    {
    public:
        FileLogger() = default;

        explicit FileLogger(const String& path, LogLevel level = LogLevel::Info);

        void Log(LogLevel level, const String& message) override;

    private:
        File m_file{};
    };
}
