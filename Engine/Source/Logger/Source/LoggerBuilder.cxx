module;

#include <utility>

module Valo.Logger;

import Valo.Std;

namespace Valo::Log
{
    void LoggerBuilder::Finish()
    {
        ScopedLock<Mutex> lock(g_active_log_handler_mutex);

        g_active_log_handler = Memory::Move(m_root_log_handler);
        m_root_log_handler   = nullptr;
    }
}
