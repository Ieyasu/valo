export module Valo.Logger:Logger;

import Valo.Std;

import :Formatter;
import :LogLevel;
import :LogHandler;

export namespace Valo::Log
{
    class Logger : public LogHandler
    {
    public:
        explicit Logger(LogLevel level = LogLevel::Info);

        [[nodiscard]] virtual LogLevel GetLevel() const;

        virtual void SetLevel(LogLevel level);

        void Execute(LogLevel level, const String& message) override;

    protected:
        virtual void Log(LogLevel level, const String& message) = 0;

    private:
        LogLevel m_level{};
    };
}
