module;

#include <functional>

export module Valo.RenderGraph:ComputeNode;

import Valo.RHI;
import Valo.Std;

import :Node;
import :RenderGraphBuffer;
import :RenderGraphTexture;

export namespace Valo::Graph
{
    struct ComputeContext final
    {
        RHI::CommandBufferHandle cmd_buffer{};
    };

    using ComputeNodeCallback = Function<void(ComputeContext)>;

    class ComputeNode final : public Node
    {
    public:
        explicit ComputeNode(String name = "Compute Node");

        [[nodiscard]] ComputeNodeCallback GetCallback() const noexcept;

        [[nodiscard]] NodeType GetType() const noexcept override;

        [[nodiscard]] const Vector<RenderGraphTextureHandle>& GetTextures() const noexcept;

        void SetCallback(ComputeNodeCallback callback);

        void SetStorageBuffer(RenderGraphBufferHandle buffer);

        void SetUniformBuffer(RenderGraphBufferHandle buffer);

        void SetSampledTexture(RenderGraphTextureHandle texture);

        void SetStorageTexture(RenderGraphTextureHandle texture);

    private:
        [[nodiscard]] RHI::AccessFlags GetAccessFlags(RenderGraphTextureHandle texture) const;

        [[nodiscard]] RHI::ImageUsageFlags GetUsageFlags(RenderGraphTextureHandle texture) const;

        void SetBuffer(RenderGraphBufferHandle buffer, RHI::BufferUsage usage);

        void SetTexture(RenderGraphTextureHandle texture, RHI::ImageUsage usage);

        ComputeNodeCallback m_callback{};

        Vector<RenderGraphBufferHandle>                     m_buffers{};
        Map<RenderGraphBufferHandle, RHI::AccessFlags>      m_buffer_access_flags{};
        Map<RenderGraphBufferHandle, RHI::BufferUsageFlags> m_buffer_usage_flags{};

        Vector<RenderGraphTextureHandle>                    m_textures{};
        Map<RenderGraphTextureHandle, RHI::AccessFlags>     m_texture_access_flags{};
        Map<RenderGraphTextureHandle, RHI::ImageUsageFlags> m_texture_usage_flags{};

        friend class RenderGraphCompiler;
        friend class RenderGraphValidator;
    };

    using ComputeNodeHandle = ObserverPtr<ComputeNode>;
}
