export module Valo.RenderGraph:RenderGraphBuffer;

import Valo.RHI;
import Valo.Std;

export namespace Valo::Graph
{
    struct RenderGraphBufferDescriptor final
    {
        StringView                      name{};
        RHI::BufferDescriptor::SizeType size{};
    };

    class RenderGraphBuffer final
    {
    public:
        explicit RenderGraphBuffer(RenderGraphBufferDescriptor descriptor);

        const RenderGraphBufferDescriptor& GetDescriptor() const;

    private:
        RenderGraphBufferDescriptor m_descriptor;

        friend class RenderGraph;
    };

    using RenderGraphBufferHandle = PoolHandle<RenderGraphBuffer>;
}
