export module Valo.RenderGraph:CompiledComputeNode;

import Valo.RHI;

import :CompiledNode;
import :ComputeNode;
import :Node;

export namespace Valo::Graph
{
    class CompiledComputeNode final : public CompiledNode
    {
    public:
        [[nodiscard]] NodeType GetType() const noexcept override;

        void Execute(RHI::CommandBufferHandle cmd_buffer) const noexcept;

    private:
        ComputeNodeCallback m_callback{};

        friend class RenderGraphCompiler;
    };
}
