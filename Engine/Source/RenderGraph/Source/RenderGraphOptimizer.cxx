module;

#include <Valo/Profile.hpp>

module Valo.RenderGraph;

import Valo.Profiler;

namespace Valo::Graph
{
    void RenderGraphOptimizer::OptimizeGraph(RenderGraph& graph)  // NOLINT
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);

        TrimNodes(graph);
        ReorderNodes(graph);
        MergeNodes(graph);
    }

    void RenderGraphOptimizer::TrimNodes(RenderGraph& /*graph*/)  // NOLINT
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);
        // TODO
    }

    void RenderGraphOptimizer::ReorderNodes(RenderGraph& /*graph*/)  // NOLINT
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);
        // TODO
    }

    void RenderGraphOptimizer::MergeNodes(RenderGraph& /*graph*/)  // NOLINT
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);
        // TODO
    }
}
