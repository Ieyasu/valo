export module Valo.RenderGraph:CompiledRenderGraph;

import Valo.RHI;
import Valo.Std;

import :CompiledNode;

export namespace Valo::Graph
{
    struct CompiledRenderGraphOutput final
    {
        RHI::ImageHandle      image{};
        RHI::ImageUsageFlags  usage_flags{};
        RHI::AccessFlags      access_flags{};
        RHI::ShaderStageFlags stage_flags{};
    };

    struct CompiledRenderGraph final
    {
        CompiledRenderGraphOutput             output{};
        Vector<UniquePtr<const CompiledNode>> nodes{};
    };
}
