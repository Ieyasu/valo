export module Valo.RenderGraph:RenderGraph;

import Valo.Std;

import :ComputeNode;
import :Node;
import :RenderGraphTexture;
import :RenderNode;

export namespace Valo::Graph
{
    class RenderGraph final
    {
    public:
        RenderGraph()                                    = default;
        RenderGraph(RenderGraph&& other)                 = default;
        RenderGraph(const RenderGraph& other)            = delete;
        RenderGraph& operator=(RenderGraph&& other)      = default;
        RenderGraph& operator=(const RenderGraph& other) = delete;
        ~RenderGraph()                                   = default;

        UInt32 GetRenderWidth() const;

        void SetRenderWidth(UInt32 render_width);

        UInt32 GetRenderHeight() const;

        void SetRenderHeight(UInt32 render_height);

        RenderGraphTextureHandle GetOutputTexture() const;

        void SetOutputTexture(RenderGraphTextureHandle texture);

        ComputeNodeHandle AddComputeNode(String name);

        RenderNodeHandle AddRenderNode(String name);

        RenderGraphTextureHandle CreateTexture(RenderGraphTextureDescriptor descriptor);

    private:
        friend class RenderGraphCompiler;
        friend class RenderGraphOptimizer;
        friend class RenderGraphValidator;

        RenderGraphTextureHandle m_output_texture{};
        Vector<UniquePtr<Node>>  m_nodes{};
        Pool<RenderGraphTexture> m_textures{};

        UInt32 m_render_width{};
        UInt32 m_render_height{};
    };
}
