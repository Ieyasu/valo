module Valo.RenderGraph;

import Valo.RHI;

namespace Valo::Graph
{
    NodeType CompiledComputeNode::GetType() const noexcept
    {
        return NodeType::Compute;
    }

    void CompiledComputeNode::Execute(RHI::CommandBufferHandle cmd_buffer) const noexcept
    {
        m_callback({cmd_buffer});
    }
}
