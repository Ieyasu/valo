module;

#include <Valo/Macros.hpp>

module Valo.RenderGraph;

import Valo.RHI;
import Valo.Std;

namespace Valo::Graph
{
    RenderNode::RenderNode(String name)
    {
        SetName(Memory::Move(name));
    }

    RenderNodeCallback RenderNode::GetCallback() const noexcept
    {
        return m_callback;
    }

    NodeType RenderNode::GetType() const noexcept
    {
        return NodeType::Render;
    }

    const Vector<RenderGraphTextureHandle>& RenderNode::GetTextures() const noexcept
    {
        return m_textures;
    }

    const Vector<RenderGraphTextureHandle>& RenderNode::GetColorAttachments() const noexcept
    {
        return m_color_attachments;
    }

    const Vector<RenderGraphTextureHandle>& RenderNode::GetInputAttachments() const noexcept
    {
        return m_input_attachments;
    }

    RenderGraphTextureHandle RenderNode::GetDepthStencilAttachment() const noexcept
    {
        return m_depth_attachment;
    }

    RHI::AccessFlags RenderNode::GetAccessFlags(RenderGraphTextureHandle texture) const
    {
        const auto it = m_texture_access_flags.find(texture);
        if (it == m_texture_access_flags.end())
        {
            return RHI::AccessFlagBits::None;
        }
        return it->second;
    }

    RHI::ImageUsageFlags RenderNode::GetUsageFlags(RenderGraphTextureHandle texture) const
    {
        VALO_ASSERT(texture.IsValid());

        const auto it = m_texture_usage_flags.find(texture);
        if (it == m_texture_usage_flags.end())
        {
            return RHI::ImageUsage::None;
        }
        return it->second;
    }

    Optional<RHI::ClearValue> RenderNode::GetClearValue(RenderGraphTextureHandle texture) const
    {
        VALO_ASSERT(texture.IsValid());

        const auto it = m_attachment_clear_values.find(texture);
        if (it == m_attachment_clear_values.end())
        {
            return nullopt;
        }
        return it->second;
    }

    void RenderNode::SetCallback(RenderNodeCallback callback)
    {
        VALO_ASSERT(callback);

        m_callback = callback;
    }

    void RenderNode::SetStorageBuffer(RenderGraphBufferHandle buffer)
    {
        VALO_ASSERT(buffer.IsValid());

        SetBuffer(buffer, RHI::BufferUsage::Storage);
    }

    void RenderNode::SetUniformBuffer(RenderGraphBufferHandle buffer)
    {
        VALO_ASSERT(buffer.IsValid());

        SetBuffer(buffer, RHI::BufferUsage::Uniform);
    }

    void RenderNode::SetSampledTexture(RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        SetTexture(texture, RHI::ImageUsage::Sampled);
    }

    void RenderNode::SetStorageTexture(RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        SetTexture(texture, RHI::ImageUsage::Storage);
    }

    void RenderNode::SetDepthAttachment(RenderGraphTextureHandle texture, bool write_depth)
    {
        const auto access_flags = write_depth ? RHI::AccessFlagBits::Read | RHI::AccessFlagBits::Write
                                              : RHI::AccessFlagBits::Read;
        const auto usage_flags  = RHI::ImageUsage::DepthStencilAttachment;
        AddAttachment(texture, access_flags, usage_flags, nullopt);
        m_depth_attachment = texture;
    }

    void RenderNode::SetDepthAttachment(RenderGraphTextureHandle texture, float clear_depth, UInt32 clear_stencil)
    {
        VALO_ASSERT(texture.IsValid());

        const auto clear = RHI::ClearValue{
            .color   = {0, 0, 0, 0},
            .depth   = clear_depth,
            .stencil = clear_stencil,
            .type    = RHI::ClearType::DepthStencil,
        };

        const auto access_flags = RHI::AccessFlagBits::Read | RHI::AccessFlagBits::Write;
        const auto usage_flags  = RHI::ImageUsage::DepthStencilAttachment;
        AddAttachment(texture, access_flags, usage_flags, clear);
        m_depth_attachment = texture;
    }

    void RenderNode::SetColorAttachment(UInt32 index, RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        if (m_color_attachments.size() <= index)
        {
            m_color_attachments.resize(static_cast<size_t>(index) + 1);
        }

        const auto access_flags = RHI::AccessFlagBits::Write;
        const auto usage_flags  = RHI::ImageUsage::ColorAttachment;
        AddAttachment(texture, access_flags, usage_flags, nullopt);
        m_color_attachments[index] = texture;
    }

    void RenderNode::SetColorAttachment(UInt32 index, RenderGraphTextureHandle texture, Array<float, 4> clear_color)
    {
        VALO_ASSERT(texture.IsValid());

        if (m_color_attachments.size() <= index)
        {
            m_color_attachments.resize(static_cast<size_t>(index) + 1);
        }

        const auto clear = RHI::ClearValue{
            .color   = clear_color,
            .depth   = 0.0f,
            .stencil = 0,
            .type    = RHI::ClearType::Color,
        };

        const auto access_flags = RHI::AccessFlagBits::Write;
        const auto usage_flags  = RHI::ImageUsage::ColorAttachment;
        AddAttachment(texture, access_flags, usage_flags, clear);
        m_color_attachments[index] = texture;
    }

    void RenderNode::SetInputAttachment(UInt32 index, RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        if (m_input_attachments.size() <= index)
        {
            m_input_attachments.resize(static_cast<size_t>(index) + 1);
        }

        const auto access_flags = RHI::AccessFlagBits::Read;
        const auto usage_flags  = RHI::ImageUsage::InputAttachment;
        AddAttachment(texture, access_flags, usage_flags, nullopt);
        m_input_attachments[index] = texture;
    }

    void RenderNode::AddAttachment(
        RenderGraphTextureHandle  texture,
        RHI::AccessFlags          access_flags,
        RHI::ImageUsage           usage_flags,
        Optional<RHI::ClearValue> clear_value)
    {
        VALO_ASSERT(texture.IsValid());

        m_texture_access_flags[texture] |= access_flags;
        m_texture_usage_flags[texture] |= usage_flags;

        if (clear_value.has_value())
        {
            m_attachment_clear_values[texture] = *clear_value;
        }
    }

    void RenderNode::SetBuffer(RenderGraphBufferHandle buffer, RHI::BufferUsage usage)
    {
        VALO_ASSERT(buffer.IsValid());

        m_buffer_usage_flags[buffer] |= usage;
        m_buffers.push_back(buffer);
    }

    void RenderNode::SetTexture(RenderGraphTextureHandle texture, RHI::ImageUsage usage_flags)
    {
        VALO_ASSERT(texture.IsValid());

        m_texture_usage_flags[texture] |= usage_flags;
        m_textures.push_back(texture);
    }
}
