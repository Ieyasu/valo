module Valo.RenderGraph;

import Valo.Std;

namespace Valo::Graph
{
    const String& Node::GetName() const noexcept
    {
        return m_name;
    }

    void Node::SetName(String name)
    {
        m_name = Memory::Move(name);
    }
}
