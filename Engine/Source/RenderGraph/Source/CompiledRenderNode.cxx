module;

#include <Valo/Macros.hpp>

module Valo.RenderGraph;

import Valo.RHI;

namespace Valo::Graph
{
    NodeType CompiledRenderNode::GetType() const noexcept
    {
        return NodeType::Render;
    }

    RHI::RenderPassHandle CompiledRenderNode::GetRenderPass() const
    {
        return m_render_pass;
    }

    void CompiledRenderNode::Execute(RHI::CommandBufferHandle cmd_buffer) const
    {
        VALO_ASSERT(m_callback);

        BeginRenderPass(cmd_buffer);
        m_callback({cmd_buffer, m_render_pass, m_subpass_index});
        EndRenderPass(cmd_buffer);
    }

    void CompiledRenderNode::BeginRenderPass(RHI::CommandBufferHandle cmd_buffer) const
    {
        if (m_subpass_index == 0)
        {
            cmd_buffer->CmdBeginRenderPass(m_render_pass, m_framebuffer, m_render_area, m_clear_values);
        }
    }

    void CompiledRenderNode::EndRenderPass(RHI::CommandBufferHandle cmd_buffer) const
    {
        if (m_subpass_index < m_max_subpass_index)
        {
            cmd_buffer->CmdNextSubpass();
        }
        {
            cmd_buffer->CmdEndRenderPass();
        }
    }
}
