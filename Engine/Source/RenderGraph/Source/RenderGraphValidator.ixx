export module Valo.RenderGraph:RenderGraphValidator;

import Valo.RHI;
import Valo.Std;

import :ComputeNode;
import :RenderGraph;
import :RenderGraphTexture;
import :RenderNode;

export namespace Valo::Graph
{
    enum class RenderGraphValidationResultCode : EnumSmall
    {
        MissingCallback,
        InvalidBufferUsage,
        InvalidTextureUsage,
        InvalidTextureWidth,
        InvalidTextureHeight,
        InvalidTextureDepth,
        InvalidTextureFormat,
        InvalidTextureMipLevelCount,
        InvalidTextureArrayLayerCount,
        InvalidTextureDimension,
    };

    struct RenderGraphValidationResult final
    {
        RenderGraphValidationResultCode code{};
        String                          message;
    };

    class RenderGraphValidator final
    {
    public:
        RenderGraphValidator();
        void ValidateGraph(const RenderGraph& graph);

        const Vector<RenderGraphValidationResult>& GetErrors() const;
        const Vector<RenderGraphValidationResult>& GetWarnings() const;

    private:
        struct TextureUsageConflict
        {
            RHI::ImageUsage first;
            RHI::ImageUsage second;
        };

        void ValidateTexture(const RenderGraphTexture& texture);
        void ValidateComputeNode(const ComputeNode& node);
        void ValidateRenderNode(const RenderNode& node);
        void ValidateRenderNodeTexture(const RenderNode& node, RenderGraphTextureHandle texture);

        void LogError(const String& message);
        void LogWarning(const String& message);

        Vector<RenderGraphValidationResult> m_errors{};
        Vector<RenderGraphValidationResult> m_warnings{};

        Vector<TextureUsageConflict> m_texture_usage_conflicts{};
    };
}
