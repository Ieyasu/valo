export module Valo.RenderGraph:RenderGraphTexture;

import Valo.RHI;
import Valo.Std;

export namespace Valo::Graph
{
    struct RenderGraphTextureDescriptor final
    {
        StringView           name{};
        RHI::Format          format{RHI::Format::Undefined};
        RHI::ImageDimensions dimensions{RHI::ImageDimensions::Two};
        UInt32               width{0};
        UInt32               height{0};
        UInt32               depth{1};
        UInt32               mip_level_count{1};
        UInt32               array_layer_count{1};
    };

    class RenderGraphTexture final
    {
    public:
        explicit RenderGraphTexture(RenderGraphTextureDescriptor descriptor);

        const RenderGraphTextureDescriptor& GetDescriptor() const;

    private:
        RenderGraphTextureDescriptor m_descriptor;

        friend class RenderGraph;
    };

    using RenderGraphTextureHandle = PoolHandle<RenderGraphTexture>;
}
