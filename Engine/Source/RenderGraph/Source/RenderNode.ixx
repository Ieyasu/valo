module;

#include <functional>

export module Valo.RenderGraph:RenderNode;

import Valo.RHI;
import Valo.Std;

import :Node;
import :RenderGraphBuffer;
import :RenderGraphTexture;

export namespace Valo::Graph
{
    struct RenderContext final
    {
        RHI::CommandBufferHandle cmd_buffer{};
        RHI::RenderPassHandle    render_pass{};
        UInt32                   subpass_index{};
    };

    using RenderNodeCallback = Function<void(RenderContext)>;

    class RenderNode final : public Node
    {
    public:
        RenderNode() = default;

        explicit RenderNode(String name = "Render Node");

        [[nodiscard]] RenderNodeCallback GetCallback() const noexcept;

        [[nodiscard]] NodeType GetType() const noexcept override;

        [[nodiscard]] const Vector<RenderGraphTextureHandle>& GetTextures() const noexcept;

        [[nodiscard]] const Vector<RenderGraphTextureHandle>& GetColorAttachments() const noexcept;

        [[nodiscard]] const Vector<RenderGraphTextureHandle>& GetInputAttachments() const noexcept;

        [[nodiscard]] RenderGraphTextureHandle GetDepthStencilAttachment() const noexcept;

        void SetCallback(RenderNodeCallback callback);

        void SetStorageBuffer(RenderGraphBufferHandle buffer);

        void SetUniformBuffer(RenderGraphBufferHandle buffer);

        void SetSampledTexture(RenderGraphTextureHandle texture);

        void SetStorageTexture(RenderGraphTextureHandle texture);

        void SetDepthAttachment(RenderGraphTextureHandle texture, bool write_depth);

        void SetDepthAttachment(RenderGraphTextureHandle texture, float clear_depth, UInt32 clear_stencil);

        void SetColorAttachment(UInt32 index, RenderGraphTextureHandle texture);

        void SetColorAttachment(UInt32 index, RenderGraphTextureHandle texture, Array<float, 4> clear_color);

        void SetInputAttachment(UInt32 index, RenderGraphTextureHandle texture);

    private:
        RHI::AccessFlags GetAccessFlags(RenderGraphTextureHandle texture) const;

        RHI::ImageUsageFlags GetUsageFlags(RenderGraphTextureHandle texture) const;

        Optional<RHI::ClearValue> GetClearValue(RenderGraphTextureHandle texture) const;

        void AddAttachment(
            RenderGraphTextureHandle  texture,
            RHI::AccessFlags          access_flags,
            RHI::ImageUsage           usage_flags,
            Optional<RHI::ClearValue> clear_value);

        void SetBuffer(RenderGraphBufferHandle buffer, RHI::BufferUsage usage);
        void SetTexture(RenderGraphTextureHandle texture, RHI::ImageUsage usage_flags);

        RenderNodeCallback m_callback{};

        Vector<RenderGraphBufferHandle>                     m_buffers{};
        Map<RenderGraphBufferHandle, RHI::AccessFlags>      m_buffer_access_flags{};
        Map<RenderGraphBufferHandle, RHI::BufferUsageFlags> m_buffer_usage_flags{};

        Vector<RenderGraphTextureHandle> m_textures{};
        Vector<RenderGraphTextureHandle> m_color_attachments{};
        Vector<RenderGraphTextureHandle> m_input_attachments{};
        RenderGraphTextureHandle         m_depth_attachment{};

        Map<RenderGraphTextureHandle, RHI::AccessFlags>     m_texture_access_flags{};
        Map<RenderGraphTextureHandle, RHI::ImageUsageFlags> m_texture_usage_flags{};
        Map<RenderGraphTextureHandle, RHI::ClearValue>      m_attachment_clear_values{};

        friend class RenderGraphCompiler;
        friend class RenderGraphValidator;
    };

    using RenderNodeHandle = ObserverPtr<RenderNode>;
}
