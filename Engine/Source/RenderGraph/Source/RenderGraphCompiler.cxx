module;

#include <Valo/Profile.hpp>

#include <algorithm>
#include <string>
#include <vector>

module Valo.RenderGraph;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

import :CompiledRenderGraph;

namespace Valo::Graph
{
    RenderGraphCompiler::RenderGraphCompiler(RHI::DeviceHandle device) :
        m_device(device)
    {
    }

    bool RenderGraphCompiler::CompileGraph(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);

        m_graph_validator.ValidateGraph(graph);

        for (const auto& warning : m_graph_validator.GetWarnings())
        {
            Log::Warning(warning.message);
        }

        for (const auto& error : m_graph_validator.GetErrors())
        {
            Log::Error(error.message);
        }

        if (!m_graph_validator.GetErrors().empty())
        {
            return false;
        }

        return CompileNodes(graph, out_compiled_graph);
    }

    bool RenderGraphCompiler::CompileNodes(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);

        // Create all textures and attachments used by the render node
        if (!CreateTextures(graph, out_compiled_graph))
        {
            Log::Error("Failed to compile render graph: failed to create textures");
            return false;
        }

        m_previous_usages.clear();
        m_previous_accesses.clear();

        // Create render passes and frame buffers
        for (const auto& node : graph.m_nodes)
        {
            switch (node->GetType())
            {
            case NodeType::Compute:
            {
                const auto& compute_node          = static_cast<ComputeNode&>(*node);
                const auto* compiled_compute_node = CompileComputeNode(graph, compute_node);
                if (!compiled_compute_node)
                {
                    Log::Error("Failed to compile render graph: failed to compile compute node '{}'", node->GetName());
                    return false;
                }
                out_compiled_graph.nodes.emplace_back(compiled_compute_node);
                break;
            }
            case NodeType::Render:
            {
                const auto& render_node          = static_cast<RenderNode&>(*node);
                const auto* compiled_render_node = CompileRenderNode(graph, render_node);
                if (!compiled_render_node)
                {
                    Log::Error("Failed to compile render graph: failed to compile render node '{}'", node->GetName());
                    return false;
                }
                out_compiled_graph.nodes.emplace_back(compiled_render_node);
                break;
            }
            default:
            {
                Log::Error(
                    "Failed to compile render graph: node '{}' has unknown type {}",
                    node->GetName(),
                    node->GetType());
                return false;
            }
            }
        }

        return true;
    }

    CompiledComputeNode* RenderGraphCompiler::CompileComputeNode(const RenderGraph& /*graph*/, const ComputeNode& node)
    {
        auto* compiled_compute_node       = new CompiledComputeNode{};
        compiled_compute_node->m_callback = node.GetCallback();
        return compiled_compute_node;
    }

    CompiledRenderNode* RenderGraphCompiler::CompileRenderNode(const RenderGraph& graph, const RenderNode& node)
    {
        // Clear old data
        m_attachments.clear();
        m_attachment_textures.clear();
        m_clear_values.clear();
        m_color_attachment_refs.clear();
        m_input_attachment_refs.clear();
        m_depth_attachment_ref.reset();
        m_attachment_descs.clear();
        m_subpass_descs.clear();

        // Currently only one subpass is supported
        m_subpass_descs.resize(1);
        auto& subpass_desc = m_subpass_descs.front();

        // Create color attachment references i.e. attachments we render color data to
        for (auto attachment : node.GetColorAttachments())
        {
            auto attachment_index = m_attachments.size();

            const auto it = std::find(m_attachments.begin(), m_attachments.end(), attachment);
            if (it == m_attachments.end())
            {
                m_attachments.emplace_back(attachment);
            }
            else
            {
                attachment_index = std::distance(m_attachments.begin(), it);
            }

            m_color_attachment_refs.push_back({
                .access_flags = node.GetAccessFlags(attachment),
                .attachment   = static_cast<UInt32>(attachment_index),
            });
        }

        // Create input attachment references i.e. read-only attachments that come from subpasses
        for (auto attachment : node.GetInputAttachments())
        {
            auto attachment_index = m_attachments.size();

            const auto it = std::find(m_attachments.begin(), m_attachments.end(), attachment);
            if (it == m_attachments.end())
            {
                m_attachments.emplace_back(attachment);
            }
            else
            {
                attachment_index = std::distance(m_attachments.begin(), it);
            }

            m_input_attachment_refs.push_back({
                .access_flags = node.GetAccessFlags(attachment),
                .attachment   = static_cast<UInt32>(attachment_index),
            });
        }

        // Create depth-stencil attachment reference
        if (node.GetDepthStencilAttachment().IsValid())
        {
            auto attachment_index = m_attachments.size();

            const auto attachment = node.GetDepthStencilAttachment();
            const auto it         = std::find(m_attachments.begin(), m_attachments.end(), attachment);
            if (it == m_attachments.end())
            {
                m_attachments.emplace_back(attachment);
            }
            else
            {
                attachment_index = std::distance(m_attachments.begin(), it);
            }

            m_depth_attachment_ref = RHI::AttachmentReference{
                .access_flags = node.GetAccessFlags(attachment),
                .attachment   = static_cast<UInt32>(attachment_index),
            };
        }

        // Create an attachment descriptors that the attachment references will point to
        m_attachment_descs.resize(m_attachments.size());
        for (size_t i = 0; i < m_attachment_descs.size(); ++i)
        {
            const auto& attachment   = m_attachments[i];
            const auto  access_flags = node.GetAccessFlags(attachment);
            const auto  usage_flags  = node.GetUsageFlags(attachment);

            auto& attachment_desc                = m_attachment_descs[i];
            attachment_desc.format               = attachment->GetDescriptor().format;
            attachment_desc.initial_access_flags = m_previous_accesses[attachment];
            attachment_desc.final_access_flags   = access_flags;
            attachment_desc.initial_usage_flags  = m_previous_usages[attachment];
            attachment_desc.final_usage_flags    = usage_flags;

            // Set load operation based on previous access
            const auto clear_value = node.GetClearValue(attachment);
            if (clear_value.has_value())
            {
                attachment_desc.load_op     = RHI::AttachmentLoadOp::Clear;
                attachment_desc.clear_value = clear_value.value();
            }
            else if (attachment_desc.initial_access_flags == RHI::AccessFlagBits::None)
            {
                attachment_desc.load_op = RHI::AttachmentLoadOp::DontCare;
            }
            else
            {
                attachment_desc.load_op = RHI::AttachmentLoadOp::Load;
            }
            m_clear_values.push_back(attachment_desc.clear_value);

            // Store only if we aren't in last node
            const auto is_output = (attachment == graph.GetOutputTexture());
            if (!is_output && m_last_nodes[attachment] == &node)
            {
                attachment_desc.store_op = RHI::AttachmentStoreOp::DontCare;
            }
            else
            {
                attachment_desc.store_op = RHI::AttachmentStoreOp::Store;
            }

            m_previous_accesses[attachment] = access_flags;
            m_previous_usages[attachment]   = usage_flags;
        }

        // Reference attachments from subpass
        subpass_desc.color_attachments = m_color_attachment_refs;
        subpass_desc.input_attachments = m_input_attachment_refs;

        auto depth_attachment_ref = RHI::AttachmentReference{};
        if (m_depth_attachment_ref.has_value())
        {
            depth_attachment_ref                  = m_depth_attachment_ref.value();
            subpass_desc.depth_stencil_attachment = &depth_attachment_ref;
        }
        else
        {
            subpass_desc.depth_stencil_attachment = nullptr;
        }

        // Create the render pass
        const auto render_pass_desc = RHI::RenderPassDescriptor{
            .attachments = m_attachment_descs,
            .subpasses   = m_subpass_descs,
        };

        auto [rp_result, render_pass] = m_device->CreateRenderPass(render_pass_desc);
        if (rp_result != Valo::RHI::ResultCode::Success)
        {
            Log::Error(
                "Failed to compile render node '{}': failed to create render pass ({})",
                node.GetName(),
                rp_result);
            return nullptr;
        }
        m_device->SetObjectName(render_pass, node.GetName());

        // Get attachment textures
        m_attachment_textures.resize(m_attachments.size());
        for (size_t i = 0; i < m_attachment_textures.size(); ++i)
        {
            m_attachment_textures[i] = m_textures[m_attachments[i].Get()];
        }

        // Create frame buffer for the render pass
        const auto framebuffer_desc = RHI::FramebufferDescriptor{
            .render_pass = render_pass,
            .attachments = m_attachment_textures,
            // We can assume that all attachments are the same size
            .width       = m_attachments.front()->GetDescriptor().width,
            .height      = m_attachments.front()->GetDescriptor().height,
            .layers      = 1,
        };

        auto [fb_result, framebuffer] = m_device->CreateFramebuffer(framebuffer_desc);
        if (fb_result != Valo::RHI::ResultCode::Success)
        {
            Log::Error(
                "Failed to compile render node '{}': failed to create framebuffer ({})",
                node.GetName(),
                fb_result);
            return nullptr;
        }
        m_device->SetObjectName(framebuffer, node.GetName() + " Framebuffer");

        // Create a compiled render node
        auto* compiled_render_node                = new CompiledRenderNode{};
        compiled_render_node->m_callback          = node.GetCallback();
        compiled_render_node->m_render_pass       = render_pass;
        compiled_render_node->m_framebuffer       = framebuffer;
        compiled_render_node->m_render_area       = {0, 0, framebuffer_desc.width, framebuffer_desc.height};
        compiled_render_node->m_clear_values      = m_clear_values;
        compiled_render_node->m_subpass_index     = 0;
        compiled_render_node->m_max_subpass_index = static_cast<UInt32>(m_subpass_descs.size() - 1);
        return compiled_render_node;
    }

    bool RenderGraphCompiler::CreateTextures(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph)
    {
        // Get the usage flags for all textures and attachments
        m_last_nodes.clear();
        m_texture_usage_flags.clear();
        for (const auto& node : graph.m_nodes)
        {
            switch (node->GetType())
            {
            case NodeType::Compute:
                CreateComputeNodeTextures(static_cast<ComputeNode&>(*node));
                break;
            case NodeType::Render:
                CreateRenderNodeTextures(static_cast<RenderNode&>(*node));
                break;
            default:
                Log::Error("Failed to compile node {}: unknown NodeType {}", node->GetName(), node->GetType());
                break;
            }
        }

        // Create the textures
        m_textures.clear();
        for (const auto& graph_texture : graph.m_textures)
        {
            const auto  is_output  = (&graph_texture == graph.GetOutputTexture().Get());
            const auto& descriptor = graph_texture.GetDescriptor();

            auto texture_desc = RHI::ImageDescriptor{
                .usage             = m_texture_usage_flags[&graph_texture],
                .format            = descriptor.format,
                .dimensions        = descriptor.dimensions,
                .width             = descriptor.width,
                .height            = descriptor.height,
                .depth             = descriptor.depth,
                .mip_level_count   = descriptor.mip_level_count,
                .array_layer_count = descriptor.array_layer_count,
            };

            // If usage flags are zero, it means the texture is unused. Skip creation.
            if (texture_desc.usage == 0)
            {
                continue;
            }

            // Output needs to be blitted to the swapchain
            if (is_output)
            {
                texture_desc.usage |= RHI::ImageUsage::TransferSrc;
            }

            auto [texture_result, texture] = m_device->CreateImage(texture_desc);
            if (texture_result != Valo::RHI::ResultCode::Success)
            {
                Log::Error(
                    "Failed to create render graph textures: failed to create texture '{}' ({})",
                    descriptor.name,
                    texture_result);
                return false;
            }
            m_device->SetObjectName(texture, descriptor.name);

            m_textures[&graph_texture] = texture;

            // Set the output texture
            if (is_output)
            {
                out_compiled_graph.output = CompiledRenderGraphOutput{
                    .image        = texture,
                    .usage_flags  = RHI::ImageUsage::ColorAttachment,
                    .access_flags = RHI::AccessFlagBits::Write,
                    .stage_flags  = RHI::ShaderStage::Fragment,
                };
            }
        }

        if (!out_compiled_graph.output.image.IsValid())
        {
            Log::Error("Failed to create render graph textures: no output texture set");
            return false;
        }

        return true;
    }

    void RenderGraphCompiler::CreateComputeNodeTextures(const ComputeNode& node)
    {
        for (const auto& texture : node.GetTextures())
        {
            m_texture_usage_flags[texture.Get()] |= node.GetUsageFlags(texture);
            m_last_nodes[texture] = &node;
        }
    }

    void RenderGraphCompiler::CreateRenderNodeTextures(const RenderNode& node)
    {
        for (const auto& texture : node.GetTextures())
        {
            m_texture_usage_flags[texture.Get()] |= node.GetUsageFlags(texture);
            m_last_nodes[texture] = &node;
        }

        for (const auto& attachment : node.GetColorAttachments())
        {
            m_texture_usage_flags[attachment.Get()] |= node.GetUsageFlags(attachment);
            m_last_nodes[attachment] = &node;
        }

        for (const auto& attachment : node.GetInputAttachments())
        {
            m_texture_usage_flags[attachment.Get()] |= node.GetUsageFlags(attachment);
            m_last_nodes[attachment] = &node;
        }

        const auto& depth_attachment = node.GetDepthStencilAttachment();
        if (depth_attachment.IsValid())
        {
            m_texture_usage_flags[depth_attachment.Get()] |= node.GetUsageFlags(depth_attachment);
            m_last_nodes[depth_attachment] = &node;
        }
    }
}
