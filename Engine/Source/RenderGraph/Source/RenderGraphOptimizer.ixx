export module Valo.RenderGraph:RenderGraphOptimizer;

import :RenderGraph;

export namespace Valo::Graph
{
    class RenderGraphOptimizer final
    {
    public:
        void OptimizeGraph(RenderGraph& graph);

    private:
        void TrimNodes(RenderGraph& graph);
        void ReorderNodes(RenderGraph& graph);
        void MergeNodes(RenderGraph& graph);
    };
}
