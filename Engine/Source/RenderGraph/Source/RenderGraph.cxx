module Valo.RenderGraph;

import Valo.Logger;
import Valo.Std;

namespace Valo::Graph
{
    ComputeNodeHandle RenderGraph::AddComputeNode(String name)
    {
        auto* node = new ComputeNode(Memory::Move(name));
        m_nodes.emplace_back(node);
        return node;
    }

    RenderNodeHandle RenderGraph::AddRenderNode(String name)
    {
        auto* node = new RenderNode(Memory::Move(name));
        m_nodes.emplace_back(node);
        return node;
    }

    RenderGraphTextureHandle RenderGraph::CreateTexture(RenderGraphTextureDescriptor descriptor)
    {
        return m_textures.Emplace(descriptor);
    }

    UInt32 RenderGraph::GetRenderWidth() const
    {
        return m_render_width;
    }

    void RenderGraph::SetRenderWidth(UInt32 render_width)
    {
        m_render_width = render_width;
    }

    UInt32 RenderGraph::GetRenderHeight() const
    {
        return m_render_height;
    }

    void RenderGraph::SetRenderHeight(UInt32 render_height)
    {
        m_render_height = render_height;
    }

    RenderGraphTextureHandle RenderGraph::GetOutputTexture() const
    {
        return m_output_texture;
    }

    void RenderGraph::SetOutputTexture(RenderGraphTextureHandle output_texture)
    {
        m_output_texture = output_texture;
    }
}
