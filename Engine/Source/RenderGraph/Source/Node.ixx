export module Valo.RenderGraph:Node;

import Valo.Std;

export namespace Valo::Graph
{
    enum class NodeType : EnumSmall
    {
        Compute,
        Render,
        Custom,
    };

    class Node
    {
    public:
        Node() = default;

        Node(const Node& other)            = delete;
        Node& operator=(const Node& other) = delete;

        Node(Node&& other)            = default;
        Node& operator=(Node&& other) = default;

        virtual ~Node() = default;

        virtual NodeType GetType() const noexcept = 0;

        [[nodiscard]] const String& GetName() const noexcept;

        void SetName(String name);

    private:
        String m_name{"Node"};
    };
}
