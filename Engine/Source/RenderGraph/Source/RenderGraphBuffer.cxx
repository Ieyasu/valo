module Valo.RenderGraph;

namespace Valo::Graph
{
    RenderGraphBuffer::RenderGraphBuffer(RenderGraphBufferDescriptor descriptor) :
        m_descriptor(descriptor)
    {
    }

    const RenderGraphBufferDescriptor& RenderGraphBuffer::GetDescriptor() const
    {
        return m_descriptor;
    }
}
