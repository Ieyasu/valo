module Valo.RenderGraph;

namespace Valo::Graph
{
    RenderGraphTexture::RenderGraphTexture(RenderGraphTextureDescriptor descriptor) :
        m_descriptor(descriptor)
    {
    }

    const RenderGraphTextureDescriptor& RenderGraphTexture::GetDescriptor() const
    {
        return m_descriptor;
    }
}
