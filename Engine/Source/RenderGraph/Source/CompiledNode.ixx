export module Valo.RenderGraph:CompiledNode;

import :Node;

export namespace Valo::Graph
{
    class CompiledNode
    {
    public:
        CompiledNode()                               = default;
        CompiledNode(const CompiledNode&)            = delete;
        CompiledNode(CompiledNode&&)                 = delete;
        CompiledNode& operator=(const CompiledNode&) = delete;
        CompiledNode& operator=(CompiledNode&&)      = delete;

        virtual ~CompiledNode() = default;

        virtual NodeType GetType() const noexcept = 0;
    };
}
