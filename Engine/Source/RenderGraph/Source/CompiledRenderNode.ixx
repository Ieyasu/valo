export module Valo.RenderGraph:CompiledRenderNode;

import Valo.RHI;
import Valo.Std;

import :CompiledNode;
import :RenderNode;
import :Node;

export namespace Valo::Graph
{
    class CompiledRenderNode final : public CompiledNode
    {
    public:
        [[nodiscard]] NodeType GetType() const noexcept override;

        [[nodiscard]] RHI::RenderPassHandle GetRenderPass() const;

        void Execute(RHI::CommandBufferHandle cmd_buffer) const;

    private:
        void BeginRenderPass(RHI::CommandBufferHandle cmd_buffer) const;

        void EndRenderPass(RHI::CommandBufferHandle cmd_buffer) const;

        RenderNodeCallback           m_callback{};
        RHI::RenderPassHandle        m_render_pass{};
        RHI::FramebufferHandle       m_framebuffer{};
        RHI::Rect2D                  m_render_area{};
        SmallVector<RHI::ClearValue> m_clear_values{};
        UInt32                       m_subpass_index{};
        UInt32                       m_max_subpass_index{};

        friend class RenderGraphCompiler;
    };
}
