export module Valo.RenderGraph:RenderGraphCompiler;

import Valo.RHI;
import Valo.Std;

import :CompiledComputeNode;
import :CompiledRenderGraph;
import :CompiledRenderNode;
import :ComputeNode;
import :Node;
import :RenderGraph;
import :RenderGraphOptimizer;
import :RenderGraphTexture;
import :RenderGraphValidator;
import :RenderNode;

export namespace Valo::Graph
{
    class RenderGraphCompiler final
    {
    public:
        explicit RenderGraphCompiler(RHI::DeviceHandle device);

        bool CompileGraph(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph);

    private:
        bool CreateTextures(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph);

        bool CompileNodes(const RenderGraph& graph, CompiledRenderGraph& out_compiled_graph);

        CompiledComputeNode* CompileComputeNode(const RenderGraph& graph, const ComputeNode& node);

        CompiledRenderNode* CompileRenderNode(const RenderGraph& graph, const RenderNode& node);

        void CreateComputeNodeTextures(const ComputeNode& node);

        void CreateRenderNodeTextures(const RenderNode& node);

        RenderGraphValidator m_graph_validator{};

        RHI::DeviceHandle m_device{};

        // Texture usage/acess info
        Map<RenderGraphTextureHandle, RHI::AccessFlags>     m_previous_accesses{};
        Map<RenderGraphTextureHandle, RHI::ImageUsageFlags> m_previous_usages{};

        // Texture create info
        Map<RenderGraphTextureHandle, const Node*>           m_last_nodes{};
        Map<const RenderGraphTexture*, RHI::ImageUsageFlags> m_texture_usage_flags{};
        Map<const RenderGraphTexture*, RHI::ImageHandle>     m_textures{};

        // Render node create info
        Vector<RenderGraphTextureHandle>   m_attachments{};
        Vector<RHI::ImageHandle>           m_attachment_textures{};
        Vector<RHI::ClearValue>            m_clear_values{};
        Vector<RHI::AttachmentReference>   m_color_attachment_refs{};
        Vector<RHI::AttachmentReference>   m_input_attachment_refs{};
        Optional<RHI::AttachmentReference> m_depth_attachment_ref{};
        Vector<RHI::AttachmentDescriptor>  m_attachment_descs{};
        Vector<RHI::SubpassDescriptor>     m_subpass_descs{};
    };
}
