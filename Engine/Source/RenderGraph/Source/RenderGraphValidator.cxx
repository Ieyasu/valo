module;

#include <Valo/Profile.hpp>

#include <vector>

module Valo.RenderGraph;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

namespace Valo::Graph
{
    RenderGraphValidator::RenderGraphValidator()
    {
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::ColorAttachment, RHI::ImageUsage::Sampled});
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::ColorAttachment, RHI::ImageUsage::Storage});
        m_texture_usage_conflicts.push_back(
            {RHI::ImageUsage::ColorAttachment, RHI::ImageUsage::DepthStencilAttachment});
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::ColorAttachment, RHI::ImageUsage::InputAttachment});
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::Storage, RHI::ImageUsage::Sampled});
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::Storage, RHI::ImageUsage::DepthStencilAttachment});
        m_texture_usage_conflicts.push_back({RHI::ImageUsage::Storage, RHI::ImageUsage::InputAttachment});
    }

    const Vector<RenderGraphValidationResult>& RenderGraphValidator::GetErrors() const
    {
        return m_errors;
    }

    const Vector<RenderGraphValidationResult>& RenderGraphValidator::GetWarnings() const
    {
        return m_warnings;
    }

    void RenderGraphValidator::ValidateGraph(const RenderGraph& graph)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RenderGraph);

        m_errors.clear();
        m_warnings.clear();

        for (const auto& texture : graph.m_textures)
        {
            ValidateTexture(texture);
        }

        for (const auto& node : graph.m_nodes)
        {
            switch (node->GetType())
            {
            case NodeType::Compute:
                ValidateComputeNode(static_cast<ComputeNode&>(*node));
                break;
            case NodeType::Render:
                ValidateRenderNode(static_cast<RenderNode&>(*node));
                break;
            default:
                Log::Fatal("Failed to validate node {}: unkown NodeType {}", node->GetName(), node->GetType());
                break;
            }
        }
    }

    void RenderGraphValidator::ValidateComputeNode(const ComputeNode& node)
    {
        // Make sure the node has a callback set
        if (!node.m_callback)
        {
            const auto* const message = "No callback configured.";
            m_errors.push_back({RenderGraphValidationResultCode::MissingCallback, message});
        }
    }

    void RenderGraphValidator::ValidateRenderNode(const RenderNode& node)
    {
        // Make sure the node has a callback set
        if (!node.m_callback)
        {
            const auto* const message = "No callback configured.";
            m_errors.push_back({RenderGraphValidationResultCode::MissingCallback, message});
        }

        // Make sure errors are only reported once per texture
        auto textures = Set<RenderGraphTextureHandle>();

        if (node.GetDepthStencilAttachment().IsValid())
        {
            textures.insert(node.GetDepthStencilAttachment());
        }

        for (const auto attachment : node.GetColorAttachments())
        {
            textures.insert(attachment);
        }

        for (const auto attachment : node.GetInputAttachments())
        {
            textures.insert(attachment);
        }

        for (const auto texture : node.GetTextures())
        {
            textures.insert(texture);
        }

        for (const auto texture : textures)
        {
            ValidateRenderNodeTexture(node, texture);
        }
    }

    void RenderGraphValidator::ValidateTexture(const RenderGraphTexture& texture)
    {
        const RenderGraphTextureDescriptor& descriptor = texture.GetDescriptor();

        if (descriptor.width == 0)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'width={}'. "
                "Texture width must be greater than 0.",
                descriptor.name,
                descriptor.width);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureWidth, message});
        }

        if (descriptor.height == 0)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'height={}'. "
                "Texture height must be greater than 0.",
                descriptor.name,
                descriptor.height);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureHeight, message});
        }

        if (descriptor.depth == 0)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'depth={}'. "
                "Texture depth must be greater than 0.",
                descriptor.name,
                descriptor.depth);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureDepth, message});
        }

        if (descriptor.format == RHI::Format::Undefined)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'format=Format::Undefined'. "
                "Texture format must be a valid format.",
                descriptor.name);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureFormat, message});
        }

        if (descriptor.mip_level_count == 0)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'mip_level_count={}'. "
                "Mip level count must be greater than 0.",
                descriptor.name,
                descriptor.mip_level_count);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureMipLevelCount, message});
        }

        if (descriptor.array_layer_count == 0)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'array_layer_count={}'. "
                "Array layer count must be greater than 0.",
                descriptor.name,
                descriptor.array_layer_count);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureArrayLayerCount, message});
        }

        if (descriptor.dimensions == RHI::ImageDimensions::One && (descriptor.height > 1 || descriptor.depth > 1))
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has invalid values 'height={}' and 'depth={}'. "
                "1D textures must have height and depth of 1.",
                descriptor.name,
                descriptor.height,
                descriptor.depth);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureDimension, message});
        }

        if (descriptor.dimensions == RHI::ImageDimensions::Two && descriptor.depth > 1)
        {
            const auto message = StringFunc::Format(
                "Texture '{}' has an invalid value 'depth={}'. "
                "2D textures must have depth of 1.",
                descriptor.name,
                descriptor.height,
                descriptor.depth);
            m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureDimension, message});
        }
    }

    void RenderGraphValidator::ValidateRenderNodeTexture(const RenderNode& node, RenderGraphTextureHandle texture)
    {
        const RenderGraphTextureDescriptor& descriptor = texture->GetDescriptor();

        const auto usage_flags = node.GetUsageFlags(texture);
        for (const auto& conflict : m_texture_usage_conflicts)
        {
            if ((conflict.first & usage_flags) && (conflict.second & usage_flags))
            {
                const auto message = StringFunc::Format(
                    "Render node '{}' has a texture '{}' width conflicting usages. "
                    "Texture used as '{}' must not be used as '{}'.",
                    node.GetName(),
                    descriptor.name,
                    conflict.first,
                    conflict.second);
                m_errors.push_back({RenderGraphValidationResultCode::InvalidTextureUsage, message});
            }
        }
    }
}
