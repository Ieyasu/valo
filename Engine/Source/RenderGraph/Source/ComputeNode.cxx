module;

#include <Valo/Macros.hpp>

module Valo.RenderGraph;

import Valo.RHI;
import Valo.Std;

namespace Valo::Graph
{
    ComputeNode::ComputeNode(String name)
    {
        SetName(Memory::Move(name));
    }

    ComputeNodeCallback ComputeNode::GetCallback() const noexcept
    {
        return m_callback;
    }

    NodeType ComputeNode::GetType() const noexcept
    {
        return NodeType::Compute;
    }

    const Vector<RenderGraphTextureHandle>& ComputeNode::GetTextures() const noexcept
    {
        return m_textures;
    }

    void ComputeNode::SetCallback(ComputeNodeCallback callback)
    {
        m_callback = callback;
    }

    void ComputeNode::SetStorageBuffer(RenderGraphBufferHandle buffer)
    {
        VALO_ASSERT(buffer.IsValid());

        SetBuffer(buffer, RHI::BufferUsage::Storage);
    }

    void ComputeNode::SetUniformBuffer(RenderGraphBufferHandle buffer)
    {
        VALO_ASSERT(buffer.IsValid());

        SetBuffer(buffer, RHI::BufferUsage::Uniform);
    }

    void ComputeNode::SetSampledTexture(RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        SetTexture(texture, RHI::ImageUsage::Sampled);
    }

    void ComputeNode::SetStorageTexture(RenderGraphTextureHandle texture)
    {
        VALO_ASSERT(texture.IsValid());

        SetTexture(texture, RHI::ImageUsage::Storage);
    }

    RHI::AccessFlags ComputeNode::GetAccessFlags(RenderGraphTextureHandle texture) const
    {
        VALO_ASSERT(texture.IsValid());

        const auto it = m_texture_access_flags.find(texture);
        if (it == m_texture_access_flags.end())
        {
            return RHI::AccessFlagBits::None;
        }
        return it->second;
    }

    RHI::ImageUsageFlags ComputeNode::GetUsageFlags(RenderGraphTextureHandle texture) const
    {
        VALO_ASSERT(texture.IsValid());

        const auto it = m_texture_usage_flags.find(texture);
        if (it == m_texture_usage_flags.end())
        {
            return RHI::ImageUsage::None;
        }
        return it->second;
    }

    void ComputeNode::SetBuffer(RenderGraphBufferHandle buffer, RHI::BufferUsage usage)
    {
        VALO_ASSERT(buffer.IsValid());

        m_buffer_usage_flags[buffer] |= usage;
        m_buffers.push_back(buffer);
    }

    void ComputeNode::SetTexture(RenderGraphTextureHandle texture, RHI::ImageUsage usage)
    {
        VALO_ASSERT(texture.IsValid());

        m_texture_usage_flags[texture] |= usage;
        m_textures.push_back(texture);
    }
}
