cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(RenderGraph VALO_RENDERGRAPH_TARGET)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/_Module.ixx
        Source/CompiledComputeNode.ixx
        Source/CompiledNode.ixx
        Source/CompiledRenderNode.ixx
        Source/CompiledRenderGraph.ixx
        Source/ComputeNode.ixx
        Source/Node.ixx
        Source/RenderGraph.ixx
        Source/RenderGraphBuffer.ixx
        Source/RenderGraphCompiler.ixx
        Source/RenderGraphOptimizer.ixx
        Source/RenderGraphTexture.ixx
        Source/RenderGraphValidator.ixx
        Source/RenderNode.ixx
    PRIVATE
        Source/CompiledComputeNode.cxx
        Source/CompiledRenderNode.cxx
        Source/ComputeNode.cxx
        Source/Node.cxx
        Source/RenderGraph.cxx
        Source/RenderGraphBuffer.cxx
        Source/RenderGraphCompiler.cxx
        Source/RenderGraphOptimizer.cxx
        Source/RenderGraphTexture.cxx
        Source/RenderGraphValidator.cxx
        Source/RenderNode.cxx
)

target_link_libraries(${target} PUBLIC
    ${VALO_LOGGER_TARGET}
    ${VALO_STD_TARGET}
    ${VALO_RHI_TARGET}
)
