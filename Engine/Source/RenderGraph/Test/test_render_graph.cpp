#include <valo/rendergraph/render_graph_texture.hpp>
#include <valo/rendergraph/render_graph_validator.hpp>

#include <valo/rendergraph.hpp>
#include <valo/rhi.hpp>

#include <catch2/catch_test_macros.hpp>

namespace Valo
{
    using Graph::RenderGraph;
    using Graph::RenderGraphTextureDescriptor;
    using Graph::RenderGraphValidationResultCode;
    using Graph::RenderGraphValidator;
    using RHI::Format;

    TEST_CASE("Render graph validation", "[render_graph]")
    {
        constexpr auto default_texture_width  = static_cast<UInt32>(1024);
        constexpr auto default_texture_height = static_cast<UInt32>(1024);

        auto graph     = RenderGraph{};
        auto validator = RenderGraphValidator{};

        auto render_node1 = graph.AddRenderNode("Render Node 1");
        auto render_node2 = graph.AddRenderNode("Render Node 2");

        auto color_descriptor   = RenderGraphTextureDescriptor{};
        color_descriptor.name   = "Color Buffer";
        color_descriptor.width  = default_texture_width;
        color_descriptor.height = default_texture_height;
        color_descriptor.format = Format::R8G8B8A8_UNorm;

        auto depth_descriptor   = RenderGraphTextureDescriptor{};
        depth_descriptor.name   = "Depth Buffer";
        depth_descriptor.width  = default_texture_width;
        depth_descriptor.height = default_texture_height;
        depth_descriptor.format = Format::D32_Float;

        const auto& color_buffer1 = graph.CreateTexture(color_descriptor);
        const auto& color_buffer2 = graph.CreateTexture(color_descriptor);
        const auto& depth_buffer  = graph.CreateTexture(depth_descriptor);

        render_node1->SetColorAttachment(0, color_buffer1);
        render_node1->SetDepthAttachment(color_buffer1, false);

        render_node2->SetInputAttachment(0, color_buffer1);
        render_node2->SetColorAttachment(0, color_buffer2);
        render_node2->SetDepthAttachment(depth_buffer, true);

        SECTION("Invalid: attachment used as both color and depth")
        {
            validator.ValidateGraph(graph);
            REQUIRE(validator.GetWarnings().empty());
            REQUIRE(validator.GetErrors().size() == 1);
            REQUIRE(validator.GetErrors().front().code == RenderGraphValidationResultCode::InvalidTextureUsage);
        }
    }

}
