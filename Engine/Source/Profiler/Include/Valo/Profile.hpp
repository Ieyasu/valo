#pragma once

#include <source_location>

#ifndef VALO_ENABLE_PROFILING_THREADS
    #define VALO_ENABLE_PROFILING_THREADS 1
#endif

#ifndef VALO_ENABLE_PROFILING_FRAMES
    #define VALO_ENABLE_PROFILING_FRAMES 1
#endif

#ifndef VALO_ENABLE_PROFILING_EVENTS
    #define VALO_ENABLE_PROFILING_EVENTS 1
#endif

#ifndef VALO_ENABLE_PROFILING_DETAILED_EVENTS
    #define VALO_ENABLE_PROFILING_DETAILED_EVENTS 0
#endif

#if VALO_ENABLE_PROFILING_THREADS
    #define VALO_PROFILE_BEGIN_THREAD(Name) Valo::Profile::BeginThread(Name)
    #define VALO_PROFILE_END_THREAD()       Valo::Profile::EndThread()
#else
    #define VALO_PROFILE_BEGIN_THREAD(Name)
    #define VALO_PROFILE_END_THREAD()
#endif

#if VALO_ENABLE_PROFILING_FRAMES
    #define VALO_PROFILE_SCOPED_FRAME(Name) Valo::ScopedFrame _ValoProfileFrame_##Name(#Name)
#else
    #define VALO_PROFILE_SCOPED_FRAME(Name)
#endif

#if VALO_ENABLE_PROFILING_EVENTS
    #define VALO_PROFILE_SCOPED_EVENT(Group, Name) Valo::ScopedEvent _ValoProfileScope_##Group_##Name(#Group "::" #Name)
    #define VALO_PROFILE_SCOPED_FUNCTION(Group) \
        Valo::ScopedEvent _ValoProfileFunction(std::source_location::current().function_name())
#else
    #define VALO_PROFILE_SCOPED_EVENT(Group, Name)
    #define VALO_PROFILE_SCOPED_FUNCTION(Group)
#endif

#if VALO_ENABLE_PROFILING_DETAILED_EVENTS
    #define VALO_PROFILE_SCOPED_EVENT_DETAILED(Group, Name) VALO_PROFILE_SCOPED_EVENT(Group, Name)
    #define VALO_PROFILE_SCOPED_FUNCTION_DETAILED(Group)    VALO_PROFILE_SCOPED_FUNCTION(Group)
#else
    #define VALO_PROFILE_SCOPED_EVENT_DETAILED(Group, Name)
    #define VALO_PROFILE_SCOPED_FUNCTION_DETAILED(Group)
#endif
