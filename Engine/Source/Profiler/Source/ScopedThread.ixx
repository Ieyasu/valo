export module Valo.Profiler:ScopedThread;

export namespace Valo
{
    class ScopedThread final
    {
    public:
        explicit ScopedThread(const char* name);
        ~ScopedThread();

        ScopedThread(const ScopedThread&)            = delete;
        ScopedThread(ScopedThread&&)                 = delete;
        ScopedThread& operator=(const ScopedThread&) = delete;
        ScopedThread& operator=(ScopedThread&&)      = delete;
    };
}
