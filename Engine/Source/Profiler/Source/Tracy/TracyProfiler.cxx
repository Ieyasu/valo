module;

#include <tracy/TracyC.h>

#include <cstring>

module Valo.Profiler;

import Valo.Std;

namespace Valo
{
    void TracyProfiler::BeginThread(const StringView name)
    {
        TracyCSetThreadName(name.data());
    }

    void TracyProfiler::EndThread()
    {
    }

    void TracyProfiler::BeginFrame(const StringView name)
    {
        auto& thread_data      = per_thread_data[GetThreadId()];
        thread_data.frame_name = String(name);
        TracyCFrameMarkStart(thread_data.frame_name.c_str());

        BeginEvent(name);
    }

    void TracyProfiler::EndFrame()
    {
        EndEvent();

        auto& thread_data = per_thread_data[GetThreadId()];
        TracyCFrameMarkEnd(thread_data.frame_name.c_str());
        thread_data.frame_name = {};
    }

    void TracyProfiler::BeginEvent(const StringView name)
    {
        auto& thread_data = per_thread_data[GetThreadId()];
        TracyCZone(context, true);
        TracyCZoneName(context, name.data(), name.size());
        thread_data.context_stack.push(context);
    }

    void TracyProfiler::EndEvent()
    {
        auto& thread_data = per_thread_data[GetThreadId()];
        TracyCZoneEnd(thread_data.context_stack.top());
        thread_data.context_stack.pop();
    }
}
