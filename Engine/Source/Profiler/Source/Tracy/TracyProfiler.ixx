module;

#include <tracy/TracyC.h>

export module Valo.Profiler:TracyProfiler;

import Valo.Std;

import :Profiler;

namespace Valo
{
    class TracyProfiler : public Profiler
    {
    public:
        void BeginThread(const StringView name) override;

        void EndThread() override;

        void BeginFrame(const StringView name) override;

        void EndFrame() override;

        void BeginEvent(const StringView name) override;

        void EndEvent() override;

    private:
        struct PerThreadData final
        {
            Stack<TracyCZoneCtx> context_stack{};
            String               frame_name{};
        };

        Map<ThreadId, PerThreadData> per_thread_data{};
    };
}
