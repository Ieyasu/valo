module Valo.Profiler;

import :Profiler;

namespace Valo
{
    ScopedThread::ScopedThread(const char* name)
    {
        Profile::BeginThread(name);
    }

    ScopedThread::~ScopedThread()
    {
        Profile::EndThread();
    }
}
