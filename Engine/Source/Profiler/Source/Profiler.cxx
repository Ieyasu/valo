module Valo.Profiler;

import Valo.Std;

import :TracyProfiler;

namespace Valo::Profile
{
    namespace
    {
        TracyProfiler g_default_profiler{};
    }

    Mutex     g_active_profiler_mutex{};
    Profiler* g_active_profiler = &g_default_profiler;

    void BeginThread(const StringView name)
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->BeginThread(name);
        }
    }

    void EndThread()
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->EndThread();
        }
    }

    void BeginFrame(const StringView name)
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->BeginFrame(name);
        }
    }

    void EndFrame()
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->EndFrame();
        }
    }

    void BeginEvent(const StringView name)
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->BeginEvent(name);
        }
    }

    void EndEvent()
    {
        ScopedLock<Mutex> lock(g_active_profiler_mutex);

        if (g_active_profiler != nullptr)
        {
            g_active_profiler->EndEvent();
        }
    }
}
