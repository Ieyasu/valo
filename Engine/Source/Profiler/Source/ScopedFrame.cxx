module Valo.Profiler;

import :Profiler;

namespace Valo
{
    ScopedFrame::ScopedFrame(const char* name)
    {
        Profile::BeginFrame(name);
    }

    ScopedFrame::~ScopedFrame()
    {
        Profile::EndFrame();
    }
}
