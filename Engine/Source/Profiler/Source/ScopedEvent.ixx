export module Valo.Profiler:ScopedEvent;

export namespace Valo
{
    class ScopedEvent final
    {
    public:
        explicit ScopedEvent(const char* name);
        ~ScopedEvent();

        ScopedEvent(const ScopedEvent&)            = delete;
        ScopedEvent(ScopedEvent&&)                 = delete;
        ScopedEvent& operator=(const ScopedEvent&) = delete;
        ScopedEvent& operator=(ScopedEvent&&)      = delete;
    };
}
