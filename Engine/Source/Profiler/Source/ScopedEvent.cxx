module;

#include <Valo/Profile.hpp>

module Valo.Profiler;

namespace Valo
{
    ScopedEvent::ScopedEvent(const char* name)
    {
        Profile::BeginEvent(name);
    }

    ScopedEvent::~ScopedEvent()
    {
        Profile::EndEvent();
    }
}
