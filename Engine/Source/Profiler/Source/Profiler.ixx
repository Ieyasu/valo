export module Valo.Profiler:Profiler;

import Valo.Std;

export namespace Valo
{
    class Profiler
    {
    public:
        Profiler()                           = default;
        Profiler(const Profiler&)            = delete;
        Profiler(Profiler&&)                 = delete;
        Profiler& operator=(const Profiler&) = delete;
        Profiler& operator=(Profiler&&)      = delete;
        virtual ~Profiler()                  = default;

        virtual void BeginThread(StringView name) = 0;

        virtual void EndThread() = 0;

        virtual void BeginFrame(StringView name) = 0;

        virtual void EndFrame() = 0;

        virtual void BeginEvent(StringView name) = 0;

        virtual void EndEvent() = 0;
    };
}

export namespace Valo::Profile
{
    extern Mutex     g_active_profiler_mutex;
    extern Profiler* g_active_profiler;

    void BeginThread(StringView name);

    void EndThread();

    void BeginFrame(StringView name);

    void EndFrame();

    void BeginEvent(StringView name);

    void EndEvent();
}
