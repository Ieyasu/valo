export module Valo.Profiler:ScopedFrame;

export namespace Valo
{
    class ScopedFrame final
    {
    public:
        explicit ScopedFrame(const char* name);
        ~ScopedFrame();

        ScopedFrame(const ScopedFrame&)            = delete;
        ScopedFrame(ScopedFrame&&)                 = delete;
        ScopedFrame& operator=(const ScopedFrame&) = delete;
        ScopedFrame& operator=(ScopedFrame&&)      = delete;
    };
}
