cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(Profiler VALO_PROFILER_TARGET)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/_Module.ixx
        Source/Tracy/TracyProfiler.ixx
        Source/Profiler.ixx
        Source/ScopedEvent.ixx
        Source/ScopedFrame.ixx
        Source/ScopedThread.ixx
    PRIVATE
        Source/Tracy/TracyProfiler.cxx
        Source/Profiler.cxx
        Source/ScopedEvent.cxx
        Source/ScopedFrame.cxx
        Source/ScopedThread.cxx
)

target_link_libraries(${target} PUBLIC
    ${VALO_STD_TARGET}
)

# Tracy

find_package(Tracy CONFIG REQUIRED)
target_link_libraries(${target} PRIVATE Tracy::TracyClient)
