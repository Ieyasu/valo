cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(Window VALO_WINDOW_TARGET)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/_Module.ixx
        Source/DisplayManager.ixx
        Source/Window.ixx
        Source/WindowContext.ixx
        Source/WindowVulkanContext.ixx
        Source/GLFW/GLFWVulkanContext.ixx
        Source/GLFW/GLFWWindow.ixx
    PRIVATE
        Source/DisplayManager.cxx
        Source/Window.cxx
        Source/GLFW/GLFWVulkanContext.cxx
        Source/GLFW/GLFWWindow.cxx
)

target_link_libraries(${target} PUBLIC
    ${VALO_STD_TARGET}
    ${VALO_ENGINE_TARGET}
    ${VALO_LOGGER_TARGET}
    ValoVulkan
)

find_package(glfw3 REQUIRED)
target_link_libraries(${target} PRIVATE glfw)
target_compile_definitions(${target} PRIVATE GLFW_INCLUDE_VULKAN)
