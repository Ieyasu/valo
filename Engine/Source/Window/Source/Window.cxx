module;

#include <Valo/Profile.hpp>

module Valo.Window;

import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    ResultValue<CreateWindowResult, WindowHandle> Window::Create(const WindowDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        auto* window      = CreateWindow(descriptor);
        window->m_context = CreateVulkanContext(*window);
        return WindowHandle(window, DestroyWindow);
    }

    Window::~Window()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        DestroyVulkanContext(m_context);
    }

    WindowContextHandle Window::GetContext() const
    {
        return m_context;
    }
}
