export module Valo.Window:DisplayManager;

import Valo.Engine;
import Valo.Std;

import :Window;

export namespace Valo
{
    class DisplayManager final : public EngineModule
    {
    public:
        WindowHandle GetWindow() const;

    private:
        bool Initialize(Engine& engine) override;

        void Update() override;

        ObserverPtr<Engine> m_engine{};
        WindowHandle        m_window{};
    };

    using DisplayManagerHandle = ObserverPtr<DisplayManager>;
}
