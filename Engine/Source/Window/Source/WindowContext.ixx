export module Valo.Window:WindowContext;

export namespace Valo
{
    class WindowContext
    {
    public:
        WindowContext()                                = default;
        WindowContext(const WindowContext&)            = delete;
        WindowContext(WindowContext&&)                 = delete;
        WindowContext& operator=(const WindowContext&) = delete;
        WindowContext& operator=(WindowContext&&)      = delete;
        virtual ~WindowContext()                       = default;
    };

    using WindowContextHandle = WindowContext*;
}
