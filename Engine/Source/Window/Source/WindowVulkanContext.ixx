module;

#define VULKAN_HPP_ASSERT_ON_RESULT(x)
#include <vulkan/vulkan.hpp>

export module Valo.Window:WindowVulkanContext;

import Valo.Std;

import :WindowContext;

export namespace Valo
{
    enum class CreateVulkanSurfaceResult : EnumSmall
    {
        Success = 0,
        Unknown = 1,
    };

    class WindowVulkanContext : public WindowContext
    {
    public:
        virtual Vector<String> GetInstanceExtensions() const = 0;

        virtual ResultValue<CreateVulkanSurfaceResult, vk::SurfaceKHR> CreateSurface(vk::Instance instance) = 0;
    };

    using WindowVulkanContextHandle = WindowVulkanContext*;
}
