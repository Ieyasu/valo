export module Valo.Window:Window;

import Valo.Std;

import :WindowContext;

export namespace Valo
{
    class Window;
    using WindowHandle = SharedPtr<Window>;

    struct WindowDescriptor final
    {
        StringView title{};
        UInt32     width{0};
        UInt32     height{0};
    };

    enum class CreateWindowResult : EnumSmall
    {
        Success = 0,
        Unknown = 1,
    };

    class Window
    {
    public:
        static ResultValue<CreateWindowResult, WindowHandle> Create(const WindowDescriptor& descriptor);

        Window() = default;

        Window(const Window& other) = delete;
        Window(Window&& other)      = delete;

        Window& operator=(const Window& other) = delete;
        Window& operator=(Window&& other)      = delete;

        virtual ~Window();

        virtual void Close() = 0;

        virtual void Hide() = 0;

        virtual void Show() = 0;

        virtual bool IsOpen() const = 0;

        virtual bool IsVisible() const = 0;

        virtual const String& GetTitle() const = 0;

        virtual void SetTitle(String title) = 0;

        virtual UInt32 GetWidth() const = 0;

        virtual void SetWidth(UInt32 width) = 0;

        virtual UInt32 GetHeight() const = 0;

        virtual void SetHeight(UInt32 height) = 0;

        virtual UInt32 GetPixelWidth() const = 0;

        virtual UInt32 GetPixelHeight() const = 0;

        WindowContextHandle GetContext() const;

    private:
        WindowContextHandle m_context{};
    };
}
