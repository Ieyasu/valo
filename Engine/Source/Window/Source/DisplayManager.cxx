module;

#include <Valo/Profile.hpp>

module Valo.Window;

import Valo.Engine;
import Valo.Logger;
import Valo.Profiler;

namespace Valo
{
    bool DisplayManager::Initialize(Engine& engine)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        m_engine = &engine;

        // Create window
        const auto windowDesc = WindowDescriptor{
            .title  = "Valo",
            .width  = 1280,
            .height = 720,
        };
        const auto [windowResult, window] = Window::Create(windowDesc);
        if (windowResult != CreateWindowResult::Success)
        {
            Log::Error("Failed to create DisplayManger: failed to create Window");
            return false;
        }
        m_window = window;

        return true;
    }

    void DisplayManager::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        if (!m_window->IsOpen() && m_engine->IsRunning())
        {
            m_engine->Stop();
        }
    }

    WindowHandle DisplayManager::GetWindow() const
    {
        return m_window;
    }
}
