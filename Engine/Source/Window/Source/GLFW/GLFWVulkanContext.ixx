module;

#define VULKAN_HPP_ASSERT_ON_RESULT(x)
#include <vulkan/vulkan.hpp>

export module Valo.Window:GLFWVulkanContext;

import Valo.Std;

import :GLFWWindow;
import :WindowVulkanContext;

export namespace Valo
{
    class GLFWVulkanContext final : public WindowVulkanContext
    {
    public:
        explicit GLFWVulkanContext(const GLFWWindow& window);

        Vector<String> GetInstanceExtensions() const override;

        ResultValue<CreateVulkanSurfaceResult, vk::SurfaceKHR> CreateSurface(vk::Instance instance) override;

    private:
        const GLFWWindow& m_window;
    };
}
