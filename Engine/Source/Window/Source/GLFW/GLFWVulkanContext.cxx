module;

#include <Valo/Profile.hpp>

#define VULKAN_HPP_ASSERT_ON_RESULT(x)
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>

module Valo.Window;

import Valo.Logger;
import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    GLFWVulkanContext::GLFWVulkanContext(const GLFWWindow& window) :
        m_window(window)
    {
    }

    ResultValue<CreateVulkanSurfaceResult, vk::SurfaceKHR> GLFWVulkanContext::CreateSurface(vk::Instance instance)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        auto* csurface = VkSurfaceKHR{};

        auto* const cinstance = static_cast<const VkInstance&>(instance);
        const auto  cresult   = glfwCreateWindowSurface(cinstance, m_window.Native(), nullptr, &csurface);

        if (vk::Result{cresult} != vk::Result::eSuccess)
        {
            Log::Error("glfwCreateWindowSurface() failed ({})", cresult);
            return CreateVulkanSurfaceResult::Unknown;
        }

        return vk::SurfaceKHR{csurface};
    }

    Vector<String> GLFWVulkanContext::GetInstanceExtensions() const
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        auto        glfw_extension_count = UInt32{0};
        auto* const glfw_extensions      = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

        auto extensions = Vector<String>(glfw_extension_count);
        for (size_t i = 0; i < glfw_extension_count; ++i)
        {
            extensions[i] = glfw_extensions[i];
        }
        return extensions;
    }
}
