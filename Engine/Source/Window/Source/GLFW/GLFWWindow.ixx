module;

#include <GLFW/glfw3.h>

export module Valo.Window:GLFWWindow;

import Valo.Std;

import :Window;
import :WindowContext;

export namespace Valo
{
    Window* CreateWindow(const WindowDescriptor& descriptor);

    void DestroyWindow(Window* window);

    WindowContext* CreateVulkanContext(const Window& window);

    void DestroyVulkanContext(WindowContext* context);

    class GLFWWindow final : public Window
    {
    public:
        GLFWWindow(String title, const WindowDescriptor& descriptor);

        GLFWWindow(const GLFWWindow& other)            = delete;
        GLFWWindow& operator=(const GLFWWindow& other) = delete;

        GLFWWindow(GLFWWindow&& other)            = delete;
        GLFWWindow& operator=(GLFWWindow&& other) = delete;

        ~GLFWWindow() override;

        GLFWwindow* Native() const;

        void Close() override;

        void Hide() override;

        void Show() override;

        bool IsOpen() const override;

        bool IsVisible() const override;

        const String& GetTitle() const override;

        void SetTitle(String title) override;

        UInt32 GetWidth() const override;

        void SetWidth(UInt32 width) override;

        UInt32 GetHeight() const override;

        void SetHeight(UInt32 height) override;

        UInt32 GetPixelWidth() const override;

        UInt32 GetPixelHeight() const override;

    private:
        String      m_title;
        GLFWwindow* m_window{nullptr};
    };
}
