module;

#include <Valo/Profile.hpp>

#include <GLFW/glfw3.h>

module Valo.Window;

import Valo.Profiler;
import Valo.Std;

import :Window;

namespace Valo
{
    // These ensure glfwTerminate() and glfwInit() are called correctly
    // when using multiple windows.
    Mutex    g_mutex{};
    UInt32 g_window_count{0};

    Window* CreateWindow(const WindowDescriptor& descriptor)
    {
        ScopedLock<Mutex> lock(g_mutex);

        if (g_window_count++ == 0)
        {
            glfwInit();
        }

        return new GLFWWindow(descriptor.title.data(), descriptor);
    }

    void DestroyWindow(Window* window)
    {
        ScopedLock<Mutex> lock(g_mutex);

        if (window != nullptr)
        {
            delete window;

            if (--g_window_count == 0)
            {
                glfwTerminate();
            }
        }
    }

    WindowContext* CreateVulkanContext(const Window& window)
    {
        return new GLFWVulkanContext(static_cast<const GLFWWindow&>(window));
    }

    void DestroyVulkanContext(WindowContext* context)
    {
        delete context;
    }

    GLFWWindow::GLFWWindow(String title, const WindowDescriptor& descriptor) :
        m_title(Memory::Move(title))
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        const auto width  = static_cast<int>(descriptor.width);
        const auto height = static_cast<int>(descriptor.height);

        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        m_window = glfwCreateWindow(width, height, descriptor.title.data(), nullptr, nullptr);
    }

    GLFWWindow::~GLFWWindow()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        glfwDestroyWindow(m_window);
    }

    GLFWwindow* GLFWWindow::Native() const
    {
        return m_window;
    }

    void GLFWWindow::Close()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        glfwDestroyWindow(m_window);
    }

    void GLFWWindow::Hide()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        glfwHideWindow(m_window);
    }

    void GLFWWindow::Show()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Window);

        glfwShowWindow(m_window);
    }

    bool GLFWWindow::IsOpen() const
    {
        return glfwWindowShouldClose(m_window) == 0;
    }

    bool GLFWWindow::IsVisible() const
    {
        return glfwGetWindowAttrib(m_window, GLFW_VISIBLE) == GLFW_TRUE;
    }

    const String& GLFWWindow::GetTitle() const
    {
        return m_title;
    }

    void GLFWWindow::SetTitle(String title)
    {
        m_title = Memory::Move(title);
        glfwSetWindowTitle(m_window, m_title.c_str());
    }

    UInt32 GLFWWindow::GetWidth() const
    {
        Int32 width{0};
        Int32 height{0};
        glfwGetWindowSize(m_window, &width, &height);
        return static_cast<UInt32>(width);
    }

    void GLFWWindow::SetWidth(UInt32 width)
    {
        glfwSetWindowSize(m_window, static_cast<int>(width), static_cast<int>(GetHeight()));
    }

    UInt32 GLFWWindow::GetHeight() const
    {
        Int32 width{0};
        Int32 height{0};
        glfwGetWindowSize(m_window, &width, &height);
        return static_cast<UInt32>(height);
    }

    void GLFWWindow::SetHeight(UInt32 height)
    {
        glfwSetWindowSize(m_window, static_cast<int>(GetWidth()), static_cast<int>(height));
    }

    UInt32 GLFWWindow::GetPixelHeight() const
    {
        Int32 width{0};
        Int32 height{0};
        glfwGetFramebufferSize(m_window, &width, &height);
        return static_cast<UInt32>(height);
    }

    UInt32 GLFWWindow::GetPixelWidth() const
    {
        Int32 width{0};
        Int32 height{0};
        glfwGetFramebufferSize(m_window, &width, &height);
        return static_cast<UInt32>(width);
    }
}
