#include <valo/assetdb/asset_database.hpp>
#include <valo/core/engine.hpp>
#include <valo/renderer/importers/gltf_importer.hpp>

#include <catch2/catch_test_macros.hpp>

#include <filesystem>

namespace Valo
{
    TEST_CASE("Valo::GLTFImporter", "[renderer]")
    {
        const auto test_root_dir     = Path(VALO_TEST_GLTF_ASSET_GENERATOR_PATH);
        const auto test_positive_dir = test_root_dir / "Output" / "Positive";
        const auto test_negative_dir = test_root_dir / "Output" / "Negative";

        auto engine = Engine();

        // Asset database module is required for importing assets
        engine.Require<GltfImporter>();
        auto database = engine.Require<AssetDatabase>();
        auto context  = AssetImportContext{};

        SECTION("GLTFImporter imports valid glTF-Asset-Generator assets")
        {
            constexpr auto total_gltf_file_count = 171;

            // Sanity check to ensure all files are loaded
            auto imported_gltf_file_count = 0;

            // Iterate over every directory in the test directory
            for (const auto& entry : std::filesystem::recursive_directory_iterator{test_positive_dir})
            {
                const auto& path = entry.path();
                if (!entry.is_regular_file() || !path.has_extension())
                {
                    continue;
                }

                const auto extension = path.extension();
                if (extension == ".gltf" || extension == ".glb")
                {
                    // Import
                    context.Initialize(path.string());
                    database->Import(context);
                    imported_gltf_file_count++;

                    REQUIRE(context.GetErrors().empty());
                    // REQUIRE(context.warnings().empty());
                }
            }

            REQUIRE(imported_gltf_file_count == total_gltf_file_count);
        }

        SECTION("GLTFImporter doesn't import invalid glTF-Asset-Generator assets")
        {
            constexpr auto total_gltf_file_count = 14;

            // Sanity check to ensure all files are loaded
            auto imported_gltf_file_count = 0;

            // Iterate over every directory in the test directory
            for (const auto& entry : std::filesystem::recursive_directory_iterator{test_negative_dir})
            {
                const auto& path = entry.path();
                if (!entry.is_regular_file() || !path.has_extension())
                {
                    continue;
                }

                const auto extension = path.extension();
                if (extension == ".gltf" || extension == ".glb")
                {
                    // Import
                    context.Initialize(path.string());
                    // database.import(context);
                    imported_gltf_file_count++;

                    // REQUIRE(!context.errors().empty());
                }
            }

            REQUIRE(imported_gltf_file_count == total_gltf_file_count);
        }
    }
}
