cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(Renderer VALO_RENDERER_TARGET)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/_Module.ixx
        Source/Pipeline/Pass/DeferredGBufferPass.ixx
        Source/Pipeline/Pass/TonemapPass.ixx
        Source/Pipeline/ComputeContext.ixx
        Source/Pipeline/HighendPipeline.ixx
        Source/Pipeline/RenderContext.ixx
        Source/Pipeline/RenderPipeline.ixx
        Source/Pipeline/RenderPipelineBuffer.ixx
        Source/Pipeline/RenderPipelineBuilder.ixx
        Source/Pipeline/RenderPipelineCompiler.ixx
        Source/Pipeline/RenderPipelineScheduler.ixx
        Source/Pipeline/RenderPipelineState.ixx
        Source/Pipeline/RenderPipelineTexture.ixx
        Source/Resource/BufferStore.ixx
        Source/Resource/ComputeShader.ixx
        Source/Resource/ComputeShaderStore.ixx
        Source/Resource/DeviceBuffer.ixx
        Source/Resource/DeviceBufferView.ixx
        Source/Resource/Material.ixx
        Source/Resource/MaterialPass.ixx
        Source/Resource/MaterialStore.ixx
        Source/Resource/Mesh.ixx
        Source/Resource/MeshStore.ixx
        Source/Resource/RenderElement.ixx
        Source/Resource/RenderSet.ixx
        Source/Resource/RenderSetStore.ixx
        Source/Resource/RenderTag.ixx
        Source/Resource/Sampler.ixx
        Source/Resource/ShaderEffect.ixx
        Source/Resource/ShaderProperties.ixx
        Source/Resource/ShaderSemantics.ixx
        Source/Resource/ShaderStore.ixx
        Source/Resource/Store.ixx
        Source/Resource/Texture.ixx
        Source/Resource/TextureStore.ixx
        Source/Utility/MeshUtility.ixx
        Source/Utility/MikkTSpace.ixx
        Source/Camera.ixx
        Source/PipelineCache.ixx
        Source/Renderer.ixx
        Source/RenderSettings.ixx
        Source/RenderUniforms.ixx
        Source/ResourceManager.ixx
        Source/Types.ixx
    PRIVATE
        Source/Pipeline/Pass/DeferredGBufferPass.cxx
        Source/Pipeline/Pass/TonemapPass.cxx
        Source/Pipeline/ComputeContext.cxx
        Source/Pipeline/HighendPipeline.cxx
        Source/Pipeline/RenderContext.cxx
        Source/Pipeline/RenderPipelineBuilder.cxx
        Source/Pipeline/RenderPipelineCompiler.cxx
        Source/Pipeline/RenderPipelineScheduler.cxx
        Source/Pipeline/RenderPipelineState.cxx
        Source/Resource/BufferStore.cxx
        Source/Resource/ComputeShader.cxx
        Source/Resource/ComputeShaderStore.cxx
        Source/Resource/DeviceBuffer.cxx
        Source/Resource/DeviceBufferView.cxx
        Source/Resource/Material.cxx
        Source/Resource/MaterialPass.cxx
        Source/Resource/MaterialStore.cxx
        Source/Resource/Mesh.cxx
        Source/Resource/MeshStore.cxx
        Source/Resource/RenderSet.cxx
        Source/Resource/RenderSetStore.cxx
        Source/Resource/RenderTag.cxx
        Source/Resource/Sampler.cxx
        Source/Resource/ShaderProperties.cxx
        Source/Resource/ShaderSemantics.cxx
        Source/Resource/ShaderStore.cxx
        Source/Resource/Store.cxx
        Source/Resource/Texture.cxx
        Source/Resource/TextureStore.cxx
        Source/Utility/MeshUtility.cxx
        Source/Utility/MikkTSpace.cxx
        Source/Camera.cxx
        Source/PipelineCache.cxx
        Source/Renderer.cxx
        Source/RenderUniforms.cxx
        Source/ResourceManager.cxx
)

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    target_compile_options(${target} PRIVATE -Wno-float-equal)
endif()

target_link_libraries(${target} PUBLIC
    ${VALO_ASSETDATABASE_TARGET}
    ${VALO_STD_TARGET}
    ${VALO_ENGINE_TARGET}
    ${VALO_RHI_TARGET}
    ${VALO_LOGGER_TARGET}
    ${VALO_MATH_TARGET}
    ${VALO_RENDERGRAPH_TARGET}
    ${VALO_SERIALIZATION_TARGET}
    ${VALO_SHADERCOMPILER_TARGET}
)

# Importer libraries

find_package(mikktspace CONFIG REQUIRED)
target_link_libraries(${target} PRIVATE mikktspace::mikktspace)
