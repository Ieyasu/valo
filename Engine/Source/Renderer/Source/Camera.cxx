module Valo.Renderer;

import Valo.Math;

namespace Valo
{
    Camera::Camera()
    {
        UpdateProjection();
    }

    Mat4 Camera::GetViewProjectionMatrix() const
    {
        return m_view_projection_matrix;
    }

    Mat4 Camera::GetProjectionMatrix() const
    {
        return m_projection_matrix;
    }

    void Camera::SetProjectionMatrix(Mat4 projection_matrix)
    {
        m_projection_matrix = projection_matrix;
        UpdateParameters();
    }

    Mat4 Camera::GetViewMatrix() const
    {
        return m_view_matrix;
    }

    void Camera::SetViewMatrix(Mat4 view_matrix)
    {
        m_view_matrix = view_matrix;
    }

    float Camera::GetAspectRatio() const
    {
        return m_aspect_ratio;
    }

    void Camera::SetAspectRatio(float aspect_ratio)
    {
        m_aspect_ratio = aspect_ratio;
        UpdateProjection();
    }

    float Camera::GetVerticalFov() const
    {
        return m_vertical_fov;
    }

    void Camera::SetVerticalFov(float vertical_fov)
    {
        m_vertical_fov = vertical_fov;
        UpdateProjection();
    }

    float Camera::GetNearPlane() const
    {
        return m_near_plane;
    }

    void Camera::SetNearPlane(float near_plane)
    {
        m_near_plane = near_plane;
        UpdateProjection();
    }

    void Camera::UpdateProjection()
    {
        m_projection_matrix = Math::CameraProjectionMatrix(m_vertical_fov, m_aspect_ratio, m_near_plane);
    }

    void Camera::UpdateParameters()
    {
        m_vertical_fov = Math::RadToDeg(2.0f * Math::Atan(1.0f / m_projection_matrix[1][1]));
        m_aspect_ratio = Math::Abs(m_projection_matrix[1][1] / m_projection_matrix[0][0]);
    }
}
