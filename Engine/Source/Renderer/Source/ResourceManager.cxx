module;

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;
import Valo.Window;

namespace Valo
{
    UniquePtr<ResourceManager> ResourceManager::Create(const DisplayManagerHandle& display_manager)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<ResourceManager>(new ResourceManager());

        // Create a RHI instance
        const auto instance_desc = RHI::InstanceDescriptor{
            .window_context = display_manager->GetWindow()->GetContext(),
        };

        auto [instance_result, instance] = RHI::Instance::Create(instance_desc);
        if (instance_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create ResourceManager: failed to create RHI::Instance");
            return nullptr;
        }
        self->m_instance = Memory::Move(instance);

        // Create a RHI device
        const auto device_desc = RHI::DeviceDescriptor{
            .window_context = display_manager->GetWindow()->GetContext(),
        };

        const auto [device_result, device] = self->m_instance->CreateDevice(device_desc);
        if (device_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create ResourceManager: failed to create RHI::Device ({})", device_result);
            return nullptr;
        }
        self->m_device = device;

        // Add window to the device for presentation
        const auto [add_window_result, _] = device->AddWindow(display_manager->GetWindow());
        if (add_window_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create ResourceManager: failed to add Window to RHI::Device ({})", add_window_result);
            return nullptr;
        }

        // Create empty descriptor set layout
        const auto layout_desc             = RHI::DescriptorSetLayoutDescriptor{};
        const auto [layout_result, layout] = device->CreateDescriptorSetLayout(layout_desc);
        if (layout_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create ResourceManager: failed to create empty descriptor set layout");
            return nullptr;
        }
        self->SetObjectName(layout, "Empty");
        self->m_empty_descriptor_set_layout = layout;

        // Create per frame data
        self->m_per_frame_data.resize(3);
        for (auto& per_frame_data : self->m_per_frame_data)
        {
            per_frame_data.cmd_pool_manager = RHI::CommandPoolManager::Create(device);
        }

        // Create staging buffer for uploading resources to the device
        self->m_staging_buffer_size          = static_cast<RHI::DeviceSizeType>(128u * 1024u * 1024u);
        const auto staging_buffer_descriptor = RHI::BufferDescriptor{
            .size   = self->m_staging_buffer_size,
            .usage  = RHI::BufferUsage::TransferSrc,
            .access = RHI::BufferHostAccess::RequiredSequential,
        };

        self->m_staging_buffer = self->CreateBuffer(staging_buffer_descriptor);
        if (!self->m_staging_buffer.IsValid())
        {
            Log::Error("Failed to create ResourceManager: failed to create staging buffer");
            return nullptr;
        }
        self->SetObjectName(self->m_staging_buffer, "Staging Buffer");

        // Make uploading immediately available
        self->BeginUpload();

        return self;
    }

    ResourceManager::~ResourceManager()
    {
        if (m_device == nullptr)
        {
            return;
        }

        m_device->WaitIdle();

        for (const auto& frame_data : m_per_frame_data)
        {
            DestroyPerFrameResources(frame_data);
        }
        m_per_frame_data.clear();

        // TODO: Destroy remaining resources
        DestroyDescriptorSetLayout(m_empty_descriptor_set_layout, true);
        DestroyBuffer(m_staging_buffer, true);

        m_instance->DestroyDevice(m_device);
    }

    RHI::DeviceHandle ResourceManager::GetDevice() const
    {
        return m_device;
    }

    UInt32 ResourceManager::GetCurrentFrame() const
    {
        return m_current_frame;
    }

    UInt32 ResourceManager::GetFramesInFlight() const
    {
        return static_cast<UInt32>(m_per_frame_data.size());
    }

    void ResourceManager::BeginFrame()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        EndUpload();
        m_frame_in_progress = true;
    }

    void ResourceManager::EndFrame()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        // Increment to next frame
        m_current_frame = (m_current_frame + 1) % GetFramesInFlight();

        // Reset all command buffers for the next frame.
        auto& frame_data = m_per_frame_data[m_current_frame];
        frame_data.cmd_pool_manager->ResetCommandPools();

        // Destroy pending buffers and images that are no longer accessed by any frame.
        DestroyPendingResources(frame_data);

        m_frame_in_progress = false;
        BeginUpload();
    }

    void ResourceManager::BeginUpload()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);
        VALO_ASSERT(!m_frame_in_progress);

        auto& frame_data = m_per_frame_data[m_current_frame];

        // Aquire transfer command buffer
        const auto& queue                      = m_device->GetGraphicsQueue();
        const auto [aquire_result, cmd_buffer] = frame_data.cmd_pool_manager->AquireCommandBuffer(queue);
        if (aquire_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to begin resource upload: failed to aquire transfer command buffer ({})", aquire_result);
            return;
        }
        frame_data.transfer_cmd_buffer = cmd_buffer;
        cmd_buffer->SetName("Transfer Command Buffer");

        // Begin transfer command buffer
        const auto begin_result = frame_data.transfer_cmd_buffer->Begin();
        if (begin_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to begin resource upload: failed to aquire transfer command buffer ({})", aquire_result);
            return;
        }
    }

    void ResourceManager::EndUpload()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);
        VALO_ASSERT(!m_frame_in_progress);

        const auto& frame_data = m_per_frame_data[m_current_frame];

        const auto end_result = frame_data.transfer_cmd_buffer->End();
        if (end_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to end resource upload: failed to finalize transfer command buffer ({})", end_result);
            return;
        }

        const auto submit_result = frame_data.transfer_cmd_buffer->Submit();
        if (submit_result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to end resource upload: failed to submit transfer command buffer ({})", submit_result);
            return;
        }
    }

    void ResourceManager::SetObjectName(RHI::BaseResourceHandle handle, StringView name)
    {
        m_device->SetObjectName(handle, name);
    }

    RHI::CommandBufferHandle ResourceManager::AquireCommandBuffer()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);
        VALO_ASSERT(m_frame_in_progress);

        const auto& per_frame_data          = m_per_frame_data[m_current_frame];
        const auto& queue                   = m_device->GetGraphicsQueue();
        const auto [result, command_buffer] = per_frame_data.cmd_pool_manager->AquireCommandBuffer(queue);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to aquire command buffer: {}", result);
            return {};
        }
        return command_buffer;
    }

    RHI::BufferHandle ResourceManager::CreateBuffer(const RHI::BufferDescriptor& descriptor)
    {
        const auto [result, buffer] = m_device->CreateBuffer(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create buffer: {}", result);
            return {};
        }

        return buffer;
    }

    void ResourceManager::DestroyBuffer(RHI::BufferHandle buffer, bool immediate)
    {
        if (!buffer.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyBuffer(buffer);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_buffer_destroys.push_back(buffer);
        }
    }

    RHI::ImageHandle ResourceManager::CreateImage(const RHI::ImageDescriptor& descriptor)
    {
        const auto [result, image] = m_device->CreateImage(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create image: {}", result);
            return {};
        }

        return image;
    }

    void ResourceManager::DestroyImage(RHI::ImageHandle image, bool immediate)
    {
        if (!image.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyImage(image);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_image_destroys.push_back(image);
        }
    }

    RHI::SamplerHandle ResourceManager::CreateSampler(const RHI::SamplerDescriptor& descriptor)
    {
        const auto [result, sampler] = m_device->CreateSampler(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create sampler: {}", result);
            return {};
        }
        return sampler;
    }

    void ResourceManager::DestroySampler(RHI::SamplerHandle sampler, bool immediate)
    {
        if (!sampler.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroySampler(sampler);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_sampler_destroys.push_back(sampler);
        }
    }

    RHI::DescriptorSetHandle ResourceManager::CreateDescriptorSet(const RHI::DescriptorSetDescriptor& descriptor)
    {
        const auto [result, set] = m_device->CreateDescriptorSet(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create descriptor set: {}", result);
            return {};
        }
        return set;
    }

    void ResourceManager::DestroyDescriptorSet(RHI::DescriptorSetHandle set, bool immediate)
    {
        if (!set.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyDescriptorSet(set);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_descriptor_set_destroys.push_back(set);
        }
    }

    RHI::ComputePipelineHandle ResourceManager::CreateComputePipeline(const RHI::ComputePipelineDescriptor& descriptor)
    {
        const auto [result, pipeline] = m_device->CreateComputePipeline(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create compute pipeline: {}", result);
            return {};
        }
        return pipeline;
    }

    void ResourceManager::DestroyComputePipeline(RHI::ComputePipelineHandle pipeline, bool immediate)
    {
        if (!pipeline.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyComputePipeline(pipeline);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_compute_pipeline_destroys.push_back(pipeline);
        }
    }

    RHI::GraphicsPipelineHandle ResourceManager::CreateGraphicsPipeline(
        const RHI::GraphicsPipelineDescriptor& descriptor)
    {
        const auto [result, pipeline] = m_device->CreateGraphicsPipeline(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create graphics pipeline: {}", result);
            return {};
        }
        return pipeline;
    }

    void ResourceManager::DestroyGraphicsPipeline(RHI::GraphicsPipelineHandle pipeline, bool immediate)
    {
        if (!pipeline.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyGraphicsPipeline(pipeline);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_graphics_pipeline_destroys.push_back(pipeline);
        }
    }

    RHI::PipelineLayoutHandle ResourceManager::CreatePipelineLayout(const RHI::PipelineLayoutDescriptor& descriptor)
    {
        const auto [result, pipeline] = m_device->CreatePipelineLayout(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create pipeline layout: {}", result);
            return {};
        }
        return pipeline;
    }

    void ResourceManager::DestroyPipelineLayout(RHI::PipelineLayoutHandle layout, bool immediate)
    {
        if (!layout.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyPipelineLayout(layout);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_pipeline_layout_destroys.push_back(layout);
        }
    }

    void ResourceManager::UpdateDescriptorSets(const Span<const RHI::DescriptorSetUpdate>& updates)
    {
        m_device->UpdateDescriptorSets(updates);
    }

    RHI::DescriptorSetLayoutHandle ResourceManager::CreateDescriptorSetLayout(
        const RHI::DescriptorSetLayoutDescriptor& descriptor)
    {
        const auto [result, layout] = m_device->CreateDescriptorSetLayout(descriptor);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to create descriptor set layout: {}", result);
            return {};
        }
        return layout;
    }

    void ResourceManager::DestroyDescriptorSetLayout(RHI::DescriptorSetLayoutHandle layout, bool immediate)
    {
        if (!layout.IsValid())
        {
            return;
        }

        if (immediate)
        {
            m_device->DestroyDescriptorSetLayout(layout);
        }
        else
        {
            auto& frame_data = m_per_frame_data[m_current_frame];
            frame_data.pending_descriptor_set_layout_destroys.push_back(layout);
        }
    }

    void ResourceManager::UploadMappableBuffer(
        const void*       data,
        RHI::BufferHandle buffer,
        size_t            buffer_offset,
        size_t            size)
    {
        VALO_ASSERT(!m_frame_in_progress);
        VALO_ASSERT(data != nullptr);
        VALO_ASSERT(size > 0);
        VALO_ASSERT(buffer.IsValid());

        const auto [result, mapped_memory] = m_device->MapBuffer(buffer);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to upload mappable buffer: failed to map buffer ({})", result);
            return;
        }

        Memory::Memcpy(static_cast<char*>(mapped_memory) + buffer_offset, data, size);
        m_device->UnmapBuffer(buffer);
    }

    void ResourceManager::UploadBuffer(const void* data, RHI::BufferHandle buffer, size_t buffer_offset, size_t size)
    {
        VALO_ASSERT(!m_frame_in_progress);
        VALO_ASSERT(data != nullptr);
        VALO_ASSERT(size > 0);
        VALO_ASSERT(buffer.IsValid());
        VALO_ASSERT(m_staging_buffer_offset + size <= m_staging_buffer_size);

        // Copy data to staging buffer
        const auto [result, mapped_memory] = m_device->MapBuffer(m_staging_buffer);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to upload buffer: failed to map staging buffer ({})", result);
            return;
        }

        Memory::Memcpy(static_cast<char*>(mapped_memory) + m_staging_buffer_offset, data, size);
        m_device->UnmapBuffer(m_staging_buffer);

        // Copy from staging buffer to device buffer
        auto& frame_data = m_per_frame_data[m_current_frame];
        frame_data.transfer_cmd_buffer
            ->CmdCopyBuffer(m_staging_buffer, m_staging_buffer_offset, buffer, buffer_offset, size);
        m_staging_buffer_offset += size;
    }

    void ResourceManager::UploadImage(
        const void*         data,
        RHI::DeviceSizeType size,
        RHI::ImageHandle    image,
        UInt32              image_width,
        UInt32              image_height,
        UInt32              image_depth,
        UInt32              image_offset_x,
        UInt32              image_offset_y,
        UInt32              image_offset_z,
        UInt32              image_mip_level,
        UInt32              image_array_layer)
    {
        VALO_ASSERT(!m_frame_in_progress);
        VALO_ASSERT(data != nullptr);
        VALO_ASSERT(size > 0);
        VALO_ASSERT(image.IsValid());
        VALO_ASSERT(m_staging_buffer_offset + size <= m_staging_buffer_size);

        // Copy data to staging buffer
        const auto [result, mapped_memory] = m_device->MapBuffer(m_staging_buffer);
        if (result != RHI::ResultCode::Success)
        {
            Log::Error("Failed to upload image: failed to map staging buffer ({})", result);
            return;
        }

        Memory::Memcpy(static_cast<char*>(mapped_memory) + m_staging_buffer_offset, data, size);
        m_device->UnmapBuffer(m_staging_buffer);

        auto& frame_data = m_per_frame_data[m_current_frame];

        // Transition resource for copying
        auto image_barrier = RHI::ImageMemoryBarrier{
            .src_access_flags = RHI::AccessFlagBits::None,
            .src_usage_flags  = RHI::ImageUsage::None,

            .dst_access_flags = RHI::AccessFlagBits::Write,
            .dst_usage_flags  = RHI::ImageUsage::TransferDst,

            .image            = image,
            .base_mip_level   = image_mip_level,
            .level_count      = 1,
            .base_array_layer = image_array_layer,
            .layer_count      = 1,
        };
        frame_data.transfer_cmd_buffer->CmdPipelineBarrier({}, {&image_barrier, 1});

        // Copy from staging buffer to image
        frame_data.transfer_cmd_buffer->CmdCopyBufferToImage(
            m_staging_buffer,
            m_staging_buffer_offset,
            image,
            image_width,
            image_height,
            image_depth,
            image_offset_x,
            image_offset_y,
            image_offset_z,
            image_mip_level,
            image_array_layer);

        // Transition resource for sampling
        image_barrier.src_access_flags = image_barrier.dst_access_flags;
        image_barrier.src_usage_flags  = image_barrier.dst_usage_flags;
        image_barrier.dst_access_flags = RHI::AccessFlagBits::Read;
        image_barrier.dst_usage_flags  = RHI::ImageUsage::Sampled;
        image_barrier.dst_stage_flags  = RHI::ShaderStage::All;
        frame_data.transfer_cmd_buffer->CmdPipelineBarrier({}, {&image_barrier, 1});

        m_staging_buffer_offset += size;
    }

    void ResourceManager::DestroyPerFrameResources(const FrameData& frame_data)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        DestroyPendingResources(frame_data);
    }

    void ResourceManager::DestroyPendingResources(const FrameData& frame_data)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        for (const auto& buffer : frame_data.pending_buffer_destroys)
        {
            m_device->DestroyBuffer(buffer);
        }

        for (const auto& image : frame_data.pending_image_destroys)
        {
            m_device->DestroyImage(image);
        }

        for (const auto& sampler : frame_data.pending_sampler_destroys)
        {
            m_device->DestroySampler(sampler);
        }

        for (const auto& set : frame_data.pending_descriptor_set_destroys)
        {
            m_device->DestroyDescriptorSet(set);
        }

        for (const auto& layout : frame_data.pending_descriptor_set_layout_destroys)
        {
            m_device->DestroyDescriptorSetLayout(layout);
        }

        for (const auto& pipeline : frame_data.pending_compute_pipeline_destroys)
        {
            m_device->DestroyComputePipeline(pipeline);
        }

        for (const auto& pipeline : frame_data.pending_graphics_pipeline_destroys)
        {
            m_device->DestroyGraphicsPipeline(pipeline);
        }

        for (const auto& layout : frame_data.pending_pipeline_layout_destroys)
        {
            m_device->DestroyPipelineLayout(layout);
        }
    }

    RHI::DescriptorSetLayoutHandle ResourceManager::GetEmptyDescriptorSetLayout() const
    {
        return m_empty_descriptor_set_layout;
    }
}
