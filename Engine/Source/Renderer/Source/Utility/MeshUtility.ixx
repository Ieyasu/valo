module;

#include <Valo/Macros.hpp>

export module Valo.Renderer:MeshUtility;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

export namespace Valo
{
    VALO_FLAGS(MeshNormalWeightAlgorithm, MeshNormalWeightAlgorithmFlags){
        None         = 0,
        Area         = Bit::Set<Flags>(0),
        Angle        = Bit::Set<Flags>(1),
        AreaAndAngle = Bit::Set<Flags>(0, 1),
    };

    class MeshUtility final
    {
    public:
        static void CalculateNormals(
            RHI::PrimitiveTopology         topology,
            Span<const UInt32>             indices,
            Span<const Vec3>               positions,
            MeshNormalWeightAlgorithmFlags normal_weight_algorithm_flags,
            Vector<Vec3>&                  out_normals);

        static void CalculateTangents(
            RHI::PrimitiveTopology topology,
            Span<const UInt32>     indices,
            Span<const Vec3>       positions,
            Span<const Vec3>       normals,
            Span<const Vec2>       uvs,
            Vector<Vec4>&          out_tangents);

        static void GenerateIndices(
            RHI::PrimitiveTopology topology,
            Span<const Vec3>       positions,
            Vector<UInt32>&        out_indices);
    };
}
