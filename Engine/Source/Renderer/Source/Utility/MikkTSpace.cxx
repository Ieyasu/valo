module;

#include <Valo/Macros.hpp>

#include <mikktspace.h>

module Valo.Renderer;

import Valo.Logger;
import Valo.RHI;
import Valo.Std;
import Valo.Math;

namespace Valo
{
    static MikkTSpace g_mikktspace = {};

    MikkTSpace::MikkTSpace()
    {
        m_iface.m_getNumFaces = [](const SMikkTSpaceContext* ctx)
        {
            auto* data = static_cast<MikkTSpace::Data*>(ctx->m_pUserData);
            return static_cast<int>(data->indices.size() / 3);
        };

        m_iface.m_getNumVerticesOfFace = [](const SMikkTSpaceContext* /*ctx*/, const int /*tri_idx*/)
        {
            return 3;
        };

        m_iface.m_getPosition =
            [](const SMikkTSpaceContext* ctx, float out[], const int itri, const int ivert)  // NOLINT
        {
            auto* data   = static_cast<MikkTSpace::Data*>(ctx->m_pUserData);
            auto  index  = data->indices[3 * itri + ivert];
            auto  vertex = data->positions[index];
            out[0]       = vertex.x;
            out[1]       = vertex.y;
            out[2]       = vertex.z;
        };

        m_iface.m_getNormal = [](const SMikkTSpaceContext* ctx, float out[], const int itri, const int ivert)  // NOLINT
        {
            auto* data   = static_cast<MikkTSpace::Data*>(ctx->m_pUserData);
            auto  index  = data->indices[3 * itri + ivert];
            auto  normal = data->normals[index];
            out[0]       = normal.x;
            out[1]       = normal.y;
            out[2]       = normal.z;
        };

        m_iface.m_getTexCoord =
            [](const SMikkTSpaceContext* ctx, float out[], const int itri, const int ivert)  // NOLINT
        {
            auto* mesh_data = static_cast<MikkTSpace::Data*>(ctx->m_pUserData);
            auto  index     = mesh_data->indices[3 * itri + ivert];
            auto  uv        = mesh_data->uvs[index];
            out[0]          = uv.x;
            out[1]          = uv.y;
        };

        m_iface.m_setTSpaceBasic = [](const SMikkTSpaceContext* ctx,
                                      const float               tangent[],  // NOLINT
                                      const float               sign,
                                      const int                 tri_idx,
                                      const int                 vert_idx)
        {
            auto* data                = static_cast<MikkTSpace::Data*>(ctx->m_pUserData);
            auto  index               = data->indices[3 * tri_idx + vert_idx];
            auto  t                   = Vec3(tangent[0], tangent[1], tangent[2]);
            data->out_tangents[index] = Vec4(t, sign);
        };
    }

    void MikkTSpace::CalculateTangents(
        RHI::PrimitiveTopology topology,
        Span<const UInt32>     indices,
        Span<const Vec3>       positions,
        Span<const Vec3>       normals,
        Span<const Vec2>       uvs,
        Vector<Vec4>&          out_tangents)
    {
        VALO_ASSERT(positions.size() == normals.size());
        VALO_ASSERT(positions.size() == uvs.size());

        out_tangents.resize(positions.size());

        switch (topology)
        {
        case RHI::PrimitiveTopology::TriangleList:
        {
            auto data = Data{indices, positions, normals, uvs, out_tangents};

            SMikkTSpaceContext context{};
            context.m_pUserData  = &data;
            context.m_pInterface = &g_mikktspace.m_iface;
            genTangSpaceDefault(&context);
            break;
        }
        case RHI::PrimitiveTopology::PointList:
        // case RHI::PrimitiveTopology::LineLoop:
        // case RHI::PrimitiveTopology::LineStrip:
        case RHI::PrimitiveTopology::LineList:
            // case RHI::PrimitiveTopology::TriangleFan:
            // case RHI::PrimitiveTopology::TriangleStrip:
            Log::Fatal("Failed to generate tangents: unsupported topology");
            break;
        default:
            Log::Fatal("Failed to generate tangents: unknown topology");
            break;
        }
    }
}
