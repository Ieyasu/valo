module;

#include <mikktspace.h>

export module Valo.Renderer:MikkTSpace;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

export namespace Valo
{
    class MikkTSpace final
    {
    public:
        MikkTSpace();

        static void CalculateTangents(
            RHI::PrimitiveTopology topology,
            Span<const UInt32>     indices,
            Span<const Vec3>       positions,
            Span<const Vec3>       normals,
            Span<const Vec2>       uvs,
            Vector<Vec4>&          out_tangents);

    private:
        struct Data
        {
            Span<const UInt32> indices;
            Span<const Vec3>   positions;
            Span<const Vec3>   normals;
            Span<const Vec2>   uvs;
            Vector<Vec4>&      out_tangents;
        };

        SMikkTSpaceInterface m_iface{};
    };

}
