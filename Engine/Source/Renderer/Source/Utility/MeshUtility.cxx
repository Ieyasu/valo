module;

#include <glm/vector_relational.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :MikkTSpace;

namespace Valo
{
    void MeshUtility::CalculateNormals(
        RHI::PrimitiveTopology         topology,
        Span<const UInt32>             indices,
        Span<const Vec3>               positions,
        MeshNormalWeightAlgorithmFlags normal_weight_algorithm_flags,
        Vector<Vec3>&                  out_normals)
    {
        out_normals.resize(positions.size(), Vec3(1, 0, 0));

        switch (topology)
        {
        case RHI::PrimitiveTopology::TriangleList:

            // Primitives with no triangles do not contribute
            if (indices.size() < 3)
            {
                return;
            }

            for (size_t i = 0; i < indices.size() - 2; i += 3)
            {
                const auto idx1 = indices[i];
                const auto idx2 = indices[i + 1];
                const auto idx3 = indices[i + 2];
                const auto v0   = positions[idx1];
                const auto v1   = positions[idx2];
                const auto v2   = positions[idx3];

                auto normal = Math::Cross(v1 - v0, v2 - v0);
                if (normal_weight_algorithm_flags & MeshNormalWeightAlgorithm::Area)
                {
                    normal = Math::Normalize(normal);
                }

                if (normal_weight_algorithm_flags & MeshNormalWeightAlgorithm::Angle)
                {
                    out_normals[idx1] += normal;
                    out_normals[idx2] += normal;
                    out_normals[idx3] += normal;
                }
                else
                {
                    out_normals[idx1] += normal * Math::Angle(Math::Normalize(v1 - v0), Math::Normalize(v2 - v0));
                    out_normals[idx2] += normal * Math::Angle(Math::Normalize(v2 - v1), Math::Normalize(v0 - v1));
                    out_normals[idx3] += normal * Math::Angle(Math::Normalize(v0 - v2), Math::Normalize(v1 - v2));
                }
            }
            break;
        // case RHI::PrimitiveTopology::TriangleFan:
        // case RHI::PrimitiveTopology::TriangleStrip:
        case RHI::PrimitiveTopology::PointList:
        // case RHI::PrimitiveTopology::LineLoop:
        // case RHI::PrimitiveTopology::LineStrip:
        case RHI::PrimitiveTopology::LineList:
            Log::Fatal("Failed to generate normals: unsupported topology");
            return;
        default:
            Log::Fatal("Failed to generate normals: unknown topology");
            return;
        }

        for (auto& normal : out_normals)
        {
            normal = Math::Normalize(normal);
        }
    }

    void MeshUtility::CalculateTangents(
        RHI::PrimitiveTopology topology,
        Span<const UInt32>     indices,
        Span<const Vec3>       positions,
        Span<const Vec3>       normals,
        Span<const Vec2>       uvs,
        Vector<Vec4>&          out_tangents)
    {
        MikkTSpace::CalculateTangents(topology, indices, positions, normals, uvs, out_tangents);
    }

    void MeshUtility::GenerateIndices(
        RHI::PrimitiveTopology topology,
        Span<const Vec3>       positions,
        Vector<UInt32>&        out_indices)
    {
        switch (topology)
        {
        case RHI::PrimitiveTopology::PointList:
            out_indices.reserve(positions.size());
            for (UInt32 i = 0; i < positions.size(); ++i)
            {
                out_indices.push_back(i);
            }
            break;
        // case RHI::PrimitiveTopology::LineLoop:
        //     out_indices.reserve(2 * positions.size());
        //     for (UInt32 i = 0; i < positions.size(); ++i)
        //     {
        //         out_indices.push_back(i);
        //         out_indices.push_back((i + 1) % positions.size());
        //     }
        //     break;
        // case RHI::PrimitiveTopology::LineStrip:
        //     out_indices.reserve(2 * positions.size());
        //     for (UInt32 i = 0; i < positions.size() - 1; ++i)
        //     {
        //         out_indices.push_back(i);
        //         out_indices.push_back(i + 1);
        //     }
        //     break;
        case RHI::PrimitiveTopology::LineList:
            out_indices.reserve(2 * positions.size());
            for (UInt32 i = 0; i < positions.size() - 1; i += 2)
            {
                out_indices.push_back(i);
                out_indices.push_back(i + 1);
            }
            break;
        // case RHI::PrimitiveTopology::TriangleFan:
        //     out_indices.reserve(3 * positions.size());
        //     for (UInt32 i = 1; i < positions.size() - 1; ++i)
        //     {
        //         out_indices.push_back(i);
        //         out_indices.push_back(i + 1);
        //         out_indices.push_back(0);
        //     }
        //     break;
        // case RHI::PrimitiveTopology::TriangleStrip:
        //     out_indices.reserve(3 * positions.size());
        //     for (UInt32 i = 0; i < positions.size() - 2; ++i)
        //     {
        //         out_indices.push_back(i);
        //         out_indices.push_back(i + 1 + i % 2);
        //         out_indices.push_back(i + 2 - i % 2);
        //     }
        //     break;
        case RHI::PrimitiveTopology::TriangleList:
            out_indices.reserve(3 * positions.size());
            for (UInt32 i = 0; i < positions.size() - 2; i += 3)
            {
                out_indices.push_back(i);
                out_indices.push_back(i + 1);
                out_indices.push_back(i + 2);
            }
            break;
        default:
            Log::Fatal("Failed to generate indices: unknown topology");
            return;
        }
    }
}
