module;

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Engine;
import Valo.Logger;
import Valo.Math;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    RenderUniforms::RenderUniforms(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store) :
        m_resource_manager(resource_manager),
        m_buffer_store(buffer_store)
    {
    }

    RenderUniforms::~RenderUniforms()
    {
        m_resource_manager->DestroyPipelineLayout(m_per_view_pipeline_layout);
        m_resource_manager->DestroyDescriptorSet(m_per_view_descriptor_set);
        m_resource_manager->DestroyDescriptorSetLayout(m_per_view_descriptor_set_layout);
    }

    UniquePtr<RenderUniforms> RenderUniforms::Create(
        ResourceManagerHandle resource_manager,
        BufferStoreHandle     buffer_store)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<RenderUniforms>(new RenderUniforms(resource_manager, buffer_store));

        self->m_time_params_buffer_view = buffer_store->CreateDynamicBufferView(1, sizeof(TimeParams));
        if (!self->m_time_params_buffer_view.IsValid())
        {
            Log::Error("Failed to create RenderUniforms: failed to create global shader parameters.");
            return {};
        }

        self->m_previous_time_params_buffer_view = buffer_store->CreateDynamicBufferView(1, sizeof(TimeParams));
        if (!self->m_previous_time_params_buffer_view.IsValid())
        {
            Log::Error("Failed to create RenderUniforms: failed to create global shader parameters.");
            return {};
        }

        self->m_view_params_buffer_view = buffer_store->CreateDynamicBufferView(1, sizeof(ViewParams));
        if (!self->m_view_params_buffer_view.IsValid())
        {
            Log::Error("Failed to create RenderUniforms: failed to create global shader parameters.");
            return {};
        }

        self->m_previous_view_params_buffer_view = buffer_store->CreateDynamicBufferView(1, sizeof(ViewParams));
        if (!self->m_previous_view_params_buffer_view.IsValid())
        {
            Log::Error("Failed to create RenderUniforms: failed to create global shader parameters.");
            return {};
        }

        // Create descriptor set layout
        {
            auto layout_bindings = Array<RHI::DescriptorSetLayoutBinding, 4>();

            layout_bindings[0].descriptor_count = 1;
            layout_bindings[0].stage            = RHI::ShaderStage::Vertex | RHI::ShaderStage::Fragment;
            layout_bindings[0].type             = RHI::DescriptorType::UniformBufferDynamic;
            layout_bindings[0].binding          = 0;

            layout_bindings[1]         = layout_bindings[0];
            layout_bindings[1].binding = 1;

            layout_bindings[2]         = layout_bindings[0];
            layout_bindings[2].binding = 2;

            layout_bindings[3]         = layout_bindings[0];
            layout_bindings[3].binding = 3;

            auto layout_desc = RHI::DescriptorSetLayoutDescriptor{
                .bindings = layout_bindings,
            };

            const auto layout = resource_manager->CreateDescriptorSetLayout(layout_desc);
            if (!layout.IsValid())
            {
                Log::Error("Failed to create RenderUniforms: failed to create descriptor set layout.");
                return {};
            }
            self->m_per_view_descriptor_set_layout = layout;
            resource_manager->SetObjectName(layout, "Global Uniforms");
        }

        // Create pipeline layout
        {
            auto layout_desc = RHI::PipelineLayoutDescriptor{
                .layouts              = {&self->m_per_view_descriptor_set_layout, 1},
                .push_constant_ranges = {},
            };

            const auto layout = resource_manager->CreatePipelineLayout(layout_desc);
            if (!layout.IsValid())
            {
                Log::Error("Failed to create RenderUniforms: failed to create pipeline layout.");
                return {};
            }
            self->m_per_view_pipeline_layout = layout;
            resource_manager->SetObjectName(layout, "Global Uniforms");
        }

        // Create descriptor set
        {
            auto set_desc = RHI::DescriptorSetDescriptor{
                .layout = self->m_per_view_descriptor_set_layout,
            };

            const auto set = resource_manager->CreateDescriptorSet(set_desc);
            if (!set.IsValid())
            {
                Log::Error("Failed to create RenderUniforms: failed to create descriptor set.");
                return {};
            }
            self->m_per_view_descriptor_set = set;
            resource_manager->SetObjectName(set, "Global Uniforms");
        }

        // Update descriptor set
        auto updates = Vector<RHI::DescriptorSetUpdate>(4);

        updates[0].descriptor_type  = RHI::DescriptorType::UniformBufferDynamic;
        updates[0].set              = self->m_per_view_descriptor_set;
        updates[0].descriptor_count = 1;
        updates[0].binding          = 0;
        updates[0].buffer           = self->m_time_params_buffer_view->GetBuffer()->GetNative();
        updates[0].buffer_offset    = 0;
        updates[0].buffer_range     = self->m_time_params_buffer_view->GetRange();

        updates[1]               = updates[0];
        updates[1].binding       = 1;
        updates[1].buffer        = self->m_previous_time_params_buffer_view->GetBuffer()->GetNative();
        updates[1].buffer_offset = 0;
        updates[1].buffer_range  = self->m_previous_time_params_buffer_view->GetRange();

        updates[2]               = updates[0];
        updates[2].binding       = 2;
        updates[2].buffer        = self->m_view_params_buffer_view->GetBuffer()->GetNative();
        updates[2].buffer_offset = 0;
        updates[2].buffer_range  = self->m_view_params_buffer_view->GetRange();

        updates[3]               = updates[0];
        updates[3].binding       = 3;
        updates[3].buffer        = self->m_previous_view_params_buffer_view->GetBuffer()->GetNative();
        updates[3].buffer_offset = 0;
        updates[3].buffer_range  = self->m_previous_view_params_buffer_view->GetRange();

        resource_manager->UpdateDescriptorSets(updates);

        return self;
    }

    RHI::PipelineLayoutHandle RenderUniforms::GetPerViewPipelineLayout() const
    {
        return m_per_view_pipeline_layout;
    }

    RHI::DescriptorSetLayoutHandle RenderUniforms::GetPerViewDescriptorSetLayout() const
    {
        return m_per_view_descriptor_set_layout;
    }

    RHI::DescriptorSetHandle RenderUniforms::GetPerViewDescriptorSet() const
    {
        return m_per_view_descriptor_set;
    }

    Array<UInt32, 4> RenderUniforms::GetPerViewDynamicOffsets() const
    {
        const auto per_view_dynamic_offsets = Array<UInt32, 4>{
            m_buffer_store->CalculateDynamicOffset(m_time_params_buffer_view),
            m_buffer_store->CalculateDynamicOffset(m_previous_time_params_buffer_view),
            m_buffer_store->CalculateDynamicOffset(m_view_params_buffer_view),
            m_buffer_store->CalculateDynamicOffset(m_previous_view_params_buffer_view),
        };
        return per_view_dynamic_offsets;
    }

    void RenderUniforms::Update(const Camera& camera, const Time& time)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        // Upload time parameters
        auto previous_time_params = m_time_params;
        m_time_params.delta       = time.GetDeltaTime().count();
        m_time_params.frame       = static_cast<UInt32>(time.GetFrame());
        m_time_params.total       = time.GetTimeSinceStart().count();

        m_buffer_store->UploadBuffer(&m_time_params, m_time_params_buffer_view, sizeof(TimeParams));
        m_buffer_store->UploadBuffer(&previous_time_params, m_previous_time_params_buffer_view, sizeof(TimeParams));

        // Upload view parameters
        auto previous_view_params                    = m_view_params;
        m_view_params.view_matrix                    = camera.GetViewMatrix();
        m_view_params.view_projection_matrix         = camera.GetProjectionMatrix() * camera.GetViewMatrix();
        m_view_params.view_projection_matrix_inverse = Math::Inverse(m_view_params.view_projection_matrix);
        m_view_params.jitter                         = Vec2(0);

        m_buffer_store->UploadBuffer(&m_view_params, m_view_params_buffer_view, sizeof(ViewParams));
        m_buffer_store->UploadBuffer(&previous_view_params, m_previous_view_params_buffer_view, sizeof(ViewParams));
    }
}
