export module Valo.Renderer:Renderer;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;
import Valo.Window;

import :BufferStore;
import :ComputeShader;
import :ComputeShaderStore;
import :Material;
import :MaterialStore;
import :Mesh;
import :MeshStore;
import :PipelineCache;
import :RenderPipeline;
import :RenderPipelineCompiler;
import :RenderPipelineScheduler;
import :RenderSet;
import :RenderSetStore;
import :RenderUniforms;
import :ResourceManager;
import :ShaderStore;
import :TextureStore;

export namespace Valo
{
    class RenderPipeline;

    class Renderer final : public EngineModule
    {
    public:
        Renderer()                                 = default;
        Renderer(const Renderer& other)            = delete;
        Renderer(Renderer&& other)                 = delete;
        Renderer& operator=(const Renderer& other) = delete;
        Renderer& operator=(Renderer&& other)      = delete;
        ~Renderer() override                       = default;

        ComputeShaderHandle CreateComputeShader(StringView name);

        void DestroyComputeShader(ComputeShaderHandle compute_shader);

        MaterialHandle CreateMaterial(StringView name);

        void DestroyMaterial(MaterialHandle material);

        MeshHandle CreateMesh();

        void DestroyMesh(MeshHandle mesh);

        RenderSetHandle GetOrCreateRenderSet(const RenderSetDescriptor& descriptor);

        RenderSetHandle CreateRenderSet(const RenderSetDescriptor& descriptor);

        void DestroyRenderSet(RenderSetHandle render_set);

        PipelineCacheHandle GetPipelineCache()
        {
            return m_pipeline_cache.get();
        }

    protected:
        bool Initialize(Engine& engine) override;

        void Update() override;

        void Deinitialize() override;

    private:
        struct PerFrameData final
        {
            RHI::CommandBufferHandle render_cmd_buffer{};
        };

        // Dependencies
        DisplayManagerHandle              m_display_manager{};
        TimeHandle                        m_time{};
        ObserverPtr<ShaderCompilerModule> m_shader_compiler{};

        // Resources
        UniquePtr<ResourceManager> m_resource_manager{};
        UniquePtr<PipelineCache>   m_pipeline_cache{};
        UniquePtr<RenderUniforms>  m_render_uniforms{};

        UniquePtr<BufferStore>        m_buffer_store{};
        UniquePtr<ComputeShaderStore> m_compute_shader_store{};

    public:  // HACK TODO: Make private once importers can create new stuff
        UniquePtr<MeshStore>      m_mesh_store{};
        UniquePtr<MaterialStore>  m_material_store{};
        UniquePtr<RenderSetStore> m_render_set_store{};
        UniquePtr<ShaderStore>    m_shader_store{};
        UniquePtr<TextureStore>   m_texture_store{};

    private:
        // Render pipeline
        UniquePtr<RenderPipeline>          m_render_pipeline{};
        UniquePtr<RenderPipelineCompiler>  m_render_pipeline_compiler{};
        UniquePtr<RenderPipelineScheduler> m_render_pipeline_scheduler{};

        friend class RenderPipeline;
        friend class GltfImporter;
        friend class ImageImporter;
        friend class ObjImporter;
    };

    using RendererHandle = ObserverPtr<Renderer>;
}
