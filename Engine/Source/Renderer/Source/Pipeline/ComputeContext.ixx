export module Valo.Renderer:ComputeContext;

import Valo.Math;
import Valo.RenderGraph;
import Valo.Std;

import :ComputeShader;

export namespace Valo
{
    class ComputeContext final
    {
    public:
        void Dispatch(ComputeShaderHandle compute_shader, UVec3 group_counts) const;

    private:
        explicit ComputeContext(const Graph::ComputeContext& context);

        ObserverPtr<const Graph::ComputeContext> m_context{};

        friend class RenderPipelineBuilder;
    };

    using ComputeNodeCallback = Function<void(ComputeContext)>;
}
