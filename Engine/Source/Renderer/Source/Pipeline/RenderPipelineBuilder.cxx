module;

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Profiler;
import Valo.RenderGraph;
import Valo.Std;

namespace Valo
{
    RenderPipelineBuilder::RenderPipelineBuilder(Graph::RenderGraph& graph) :
        m_graph(&graph)
    {
    }

    UInt32 RenderPipelineBuilder::GetRenderWidth() const
    {
        return m_graph->GetRenderWidth();
    }

    UInt32 RenderPipelineBuilder::GetRenderHeight() const
    {
        return m_graph->GetRenderHeight();
    }

    RenderPipelineTextureHandle RenderPipelineBuilder::GetOutputTexture() const
    {
        return m_graph->GetOutputTexture();
    }

    void RenderPipelineBuilder::SetOutputTexture(RenderPipelineTextureHandle texture)
    {
        m_graph->SetOutputTexture(texture);
    }

    ComputePassHandle RenderPipelineBuilder::AddComputePass(String name)
    {
        return ComputePassHandle{m_graph->AddComputeNode(name)};
    }

    RenderPassHandle RenderPipelineBuilder::AddRenderPass(String name)
    {
        return RenderPassHandle{m_graph->AddRenderNode(name)};
    }

    RenderPipelineTextureHandle RenderPipelineBuilder::AddTexture(RenderPipelineTextureDescriptor descriptor)
    {
        return m_graph->CreateTexture(descriptor);
    }

    void RenderPipelineBuilder::SetCallback(ComputePassHandle node, ComputeNodeCallback callback)
    {
        const auto callback_wrapper = [callback](const Graph::ComputeContext& context)
        {
            callback(ComputeContext{context});
        };

        node.m_value->SetCallback(callback_wrapper);
    }

    void RenderPipelineBuilder::SetStorageBuffer(ComputePassHandle node, RenderPipelineBufferHandle buffer)
    {
        node.m_value->SetStorageBuffer(buffer);
    }

    void RenderPipelineBuilder::SetUniformBuffer(ComputePassHandle node, RenderPipelineBufferHandle buffer)
    {
        node.m_value->SetUniformBuffer(buffer);
    }

    void RenderPipelineBuilder::SetSampledTexture(ComputePassHandle node, RenderPipelineTextureHandle texture)
    {
        node.m_value->SetSampledTexture(texture);
    }

    void RenderPipelineBuilder::SetStorageTexture(ComputePassHandle node, RenderPipelineTextureHandle texture)
    {
        node.m_value->SetStorageTexture(texture);
    }

    void RenderPipelineBuilder::SetCallback(RenderPassHandle node, RenderNodeCallback callback)
    {
        const auto callback_wrapper = [callback](const Graph::RenderContext& context)
        {
            callback(RenderContext{context});
        };

        node.m_value->SetCallback(callback_wrapper);
    }

    void RenderPipelineBuilder::SetStorageBuffer(RenderPassHandle node, RenderPipelineBufferHandle buffer)
    {
        node.m_value->SetStorageBuffer(buffer);
    }

    void RenderPipelineBuilder::SetUniformBuffer(RenderPassHandle node, RenderPipelineBufferHandle buffer)
    {
        node.m_value->SetUniformBuffer(buffer);
    }

    void RenderPipelineBuilder::SetSampledTexture(RenderPassHandle node, RenderPipelineTextureHandle texture)
    {
        node.m_value->SetSampledTexture(texture);
    }

    void RenderPipelineBuilder::SetStorageTexture(RenderPassHandle node, RenderPipelineTextureHandle texture)
    {
        node.m_value->SetStorageTexture(texture);
    }

    void RenderPipelineBuilder::SetDepthAttachment(
        RenderPassHandle            node,
        RenderPipelineTextureHandle texture,
        bool                        write_depth)
    {
        node.m_value->SetDepthAttachment(texture, write_depth);
    }

    void RenderPipelineBuilder::SetDepthAttachment(
        RenderPassHandle            node,
        RenderPipelineTextureHandle texture,
        float                       clear_depth,
        UInt32                      clear_stencil)
    {
        node.m_value->SetDepthAttachment(texture, clear_depth, clear_stencil);
    }

    void RenderPipelineBuilder::SetColorAttachment(
        RenderPassHandle            node,
        UInt32                      index,
        RenderPipelineTextureHandle texture)
    {
        node.m_value->SetColorAttachment(index, texture);
    }

    void RenderPipelineBuilder::SetColorAttachment(
        RenderPassHandle            node,
        UInt32                      index,
        RenderPipelineTextureHandle texture,
        Array<float, 4>             clear_color)
    {
        node.m_value->SetColorAttachment(index, texture, clear_color);
    }

    void RenderPipelineBuilder::SetInputAttachment(
        RenderPassHandle            node,
        UInt32                      index,
        RenderPipelineTextureHandle texture)
    {
        node.m_value->SetInputAttachment(index, texture);
    }
}
