module;

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    UniquePtr<RenderPipelineCompiler> RenderPipelineCompiler::Create(ResourceManagerHandle resource_manager)
    {
        return UniquePtr<RenderPipelineCompiler>(new RenderPipelineCompiler(resource_manager));
    }

    RenderPipelineCompiler::RenderPipelineCompiler(ResourceManagerHandle resource_manager) :
        m_graph_compiler(resource_manager->GetDevice())
    {
    }

    bool RenderPipelineCompiler::Compile(RenderPipelineDescriptor descriptor, RenderPipeline& out_render_pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        out_render_pipeline.m_descriptor = descriptor;

        // Configure the render graph
        m_graph.SetRenderHeight(descriptor.resolution.height);
        m_graph.SetRenderWidth(descriptor.resolution.width);

        // Building is implemented by the RenderPipeline
        auto builder = RenderPipelineBuilder(m_graph);
        out_render_pipeline.Build(builder);

        // Compile the built graph
        return m_graph_compiler.CompileGraph(m_graph, out_render_pipeline.m_compiled_graph);
    }
}
