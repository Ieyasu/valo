module;

#include "Defines.hpp"

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Math;
import Valo.Profiler;
import Valo.RenderGraph;
import Valo.Std;

namespace Valo
{
    ComputeContext::ComputeContext(const Graph::ComputeContext& context) :
        m_context(&context)
    {
    }

    void ComputeContext::Dispatch(ComputeShaderHandle compute_shader, UVec3 group_counts) const
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        const auto descriptor_set  = compute_shader->GetDescriptorSet();
        const auto pipeline_layout = compute_shader->GetShaderPass()->pipeline_layout;
        const auto pipeline        = compute_shader->GetPipeline();

        m_context->cmd_buffer->CmdBindComputePipeline(pipeline);
        m_context->cmd_buffer
            ->CmdBindDescriptorSets(pipeline_layout, {&descriptor_set, 1}, {}, VALO_MATERIAL_DESCRIPTOR_SET_INDEX);
        m_context->cmd_buffer->CmdDispatch(group_counts.x, group_counts.y, group_counts.z);
    }
}
