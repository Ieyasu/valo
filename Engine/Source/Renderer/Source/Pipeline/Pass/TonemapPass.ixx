export module Valo.Renderer:TonemapPass;

import Valo.RenderGraph;
import Valo.Std;
import Valo.Math;

import :ComputeShader;
import :RenderPipelineBuilder;
import :RenderPipelineState;
import :RenderPipelineTexture;
import :Renderer;
import :RenderSet;

export namespace Valo
{
    class TonemapPass final
    {
    public:
        void Initialize(Renderer& renderer);

        void Build(RenderPipelineBuilder& builder);

        void Update(RenderPipelineState& state);

        void SetHdrColor(RenderPipelineTextureHandle texture);

        RenderPipelineTextureHandle GetLdrColor() const;

    private:
        // Resources
        ComputeShaderHandle m_compute_shader{};
        UVec3               m_group_counts{};

        // Pipeline texture
        ComputePassHandle           m_compute_pass{};
        RenderPipelineTextureHandle m_hdr_color_ref{};
        RenderPipelineTextureHandle m_ldr_color_ref{};
    };
}
