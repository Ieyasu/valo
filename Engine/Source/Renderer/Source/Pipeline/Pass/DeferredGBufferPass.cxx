module;

#include <Valo/Macros.hpp>

#include <vector>

module Valo.Renderer;

import Valo.RHI;

namespace Valo
{
    void DeferredGBufferPass::AddRenderSet(RenderSetHandle render_set)
    {
        m_render_sets.push_back(render_set);
    }

    void DeferredGBufferPass::SetSceneColor(RenderPipelineTextureHandle texture)
    {
        m_scene_color = texture;
    }

    void DeferredGBufferPass::SetSceneDepth(RenderPipelineTextureHandle texture)
    {
        m_scene_depth = texture;
    }

    void DeferredGBufferPass::SetSceneVelocity(RenderPipelineTextureHandle texture)
    {
        m_scene_velocity = texture;
    }

    RenderPipelineTextureHandle DeferredGBufferPass::GetGBufferA() const
    {
        return m_gbufferA;
    }

    RenderPipelineTextureHandle DeferredGBufferPass::GetGBufferB() const
    {
        return m_gbufferB;
    }

    RenderPipelineTextureHandle DeferredGBufferPass::GetGBufferC() const
    {
        return m_gbufferC;
    }

    RenderPipelineTextureHandle DeferredGBufferPass::GetGBufferD() const
    {
        return m_gbufferD;
    }

    void DeferredGBufferPass::Build(RenderPipelineBuilder& builder)
    {
        m_gbufferA = builder.AddTexture({
            .name   = "GBufferA - Albedo (RGB) Material Flags (A)",
            .format = RHI::Format::R8G8B8A8_UNorm,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        m_gbufferB = builder.AddTexture({
            .name   = "GBufferB - Normal (RGB) Material ID (A)",
            .format = RHI::Format::R8G8B8A8_UNorm,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        m_gbufferC = builder.AddTexture({
            .name   = "GBufferC - Emissive (RGB)",
            .format = RHI::Format::B10G11R11_Float,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        m_gbufferD = builder.AddTexture({
            .name   = "GBufferD - Occlusion (R) Roughness (G) Metallic (B) Material Misc (A)",
            .format = RHI::Format::R8G8B8A8_UNorm,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        const auto render_pass = builder.AddRenderPass("Deferred GBuffer Render Pass");
        builder.SetColorAttachment(render_pass, 0, m_scene_color, {0, 0, 0, 0});
        builder.SetColorAttachment(render_pass, 1, m_gbufferA, {0, 0, 0, 0});
        builder.SetColorAttachment(render_pass, 2, m_gbufferB, {0, 0, 0, 0});
        builder.SetColorAttachment(render_pass, 3, m_gbufferC, {0, 0, 0, 0});
        builder.SetColorAttachment(render_pass, 4, m_gbufferD, {0, 0, 0, 0});
        builder.SetColorAttachment(render_pass, 5, m_scene_velocity, {0, 0, 0, 0});
        builder.SetDepthAttachment(render_pass, m_scene_depth, true);

        // clang-format off
        builder.SetCallback(render_pass, [&](const RenderContext& context)
        {
            for (auto& render_set : m_render_sets)
            {
                VALO_ASSERT(render_set.IsValid());
                context.Render(*render_set);
            }
        });
        // clang-format on
    }
}
