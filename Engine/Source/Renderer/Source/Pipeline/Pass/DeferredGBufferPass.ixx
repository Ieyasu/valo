export module Valo.Renderer:DeferredGBufferPass;

import Valo.RenderGraph;
import Valo.Std;

import :RenderPipelineBuilder;
import :RenderPipelineTexture;
import :RenderSet;

export namespace Valo
{
    class DeferredGBufferPass final
    {
    public:
        void Build(RenderPipelineBuilder& builder);

        void AddRenderSet(RenderSetHandle render_set);

        void SetSceneColor(RenderPipelineTextureHandle texture);
        void SetSceneDepth(RenderPipelineTextureHandle texture);
        void SetSceneVelocity(RenderPipelineTextureHandle texture);

        RenderPipelineTextureHandle GetGBufferA() const;
        RenderPipelineTextureHandle GetGBufferB() const;
        RenderPipelineTextureHandle GetGBufferC() const;
        RenderPipelineTextureHandle GetGBufferD() const;

    private:
        Vector<RenderSetHandle> m_render_sets{};

        // Inputs
        RenderPipelineTextureHandle m_scene_color{};
        RenderPipelineTextureHandle m_scene_depth{};
        RenderPipelineTextureHandle m_scene_velocity{};

        // Outputs
        RenderPipelineTextureHandle m_gbufferA{};
        RenderPipelineTextureHandle m_gbufferB{};
        RenderPipelineTextureHandle m_gbufferC{};
        RenderPipelineTextureHandle m_gbufferD{};
    };
}
