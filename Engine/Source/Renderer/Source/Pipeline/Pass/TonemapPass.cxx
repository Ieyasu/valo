module Valo.Renderer;

import Valo.RHI;

namespace Valo
{
    void TonemapPass::SetHdrColor(RenderPipelineTextureHandle texture)
    {
        m_hdr_color_ref = texture;
    }

    RenderPipelineTextureHandle TonemapPass::GetLdrColor() const
    {
        return m_ldr_color_ref;
    }

    void TonemapPass::Initialize(Renderer& renderer)
    {
        m_compute_shader = renderer.CreateComputeShader("Valo/Tonemap");
    }

    void TonemapPass::Build(RenderPipelineBuilder& builder)
    {
        m_ldr_color_ref = builder.AddTexture({
            .name   = "LDR Color",
            .format = RHI::Format::R8G8B8A8_UNorm,
            .width  = m_hdr_color_ref->GetDescriptor().width,
            .height = m_hdr_color_ref->GetDescriptor().height,
        });

        m_compute_pass = builder.AddComputePass("Tonemap Pass");
        builder.SetStorageTexture(m_compute_pass, m_hdr_color_ref);
        builder.SetStorageTexture(m_compute_pass, m_ldr_color_ref);

        // clang-format off
        builder.SetCallback(m_compute_pass, [&](const ComputeContext& context)
        {
            context.Dispatch(m_compute_shader, m_group_counts);
        });
        // clang-format on
    }

    void TonemapPass::Update(RenderPipelineState& state)
    {
        (void)state;
        // const auto hdr_color = state.GetTexture(m_compute_pass, 0);
        // const auto ldr_color = state.GetTexture(m_compute_pass, 1);

        // const auto thread_counts = UVec3{8, 8, 1};
        // const auto texture_size  = UVec3{hdr_color->GetWidth(), hdr_color->GetHeight(), 1};
        // m_group_counts           = (texture_size + thread_counts - UVec3{1}) / thread_counts;

        // m_compute_shader->SetTexture("_hdr_color", hdr_color);
        // m_compute_shader->SetTexture("_ldr_color", ldr_color);
    }
}
