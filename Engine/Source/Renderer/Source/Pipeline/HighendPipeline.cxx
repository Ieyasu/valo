module;

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Profiler;
import Valo.RHI;

namespace Valo
{
    void HighendPipeline::Initialize(Renderer& renderer)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        // Create render sets
        m_shadow_render_set = renderer.GetOrCreateRenderSet(RenderSetDescriptor{
            .tags = {RenderTag::Shadow},
        });

        m_opaque_render_set = renderer.GetOrCreateRenderSet(RenderSetDescriptor{
            .tags = {RenderTag::Opaque},
        });

        m_transparent_render_set = renderer.GetOrCreateRenderSet(RenderSetDescriptor{
            .tags = {RenderTag::Transparent},
        });

        // Initialize passes
        m_gbuffer_pass.AddRenderSet(m_opaque_render_set);

        // m_tonemap_pass.Initialize(renderer);
    }

    void HighendPipeline::Build(RenderPipelineBuilder& builder)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        const auto& scene_color = builder.AddTexture({
            .name   = "Scene Color (HDR)",
            .format = RHI::Format::R16G16B16A16_Float,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        const auto& scene_depth = builder.AddTexture({
            .name   = "Scene Depth",
            .format = RHI::Format::D32_Float,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        const auto& scene_velocity = builder.AddTexture({
            .name   = "Velocity (XY)",
            .format = RHI::Format::R16G16_Float,
            .width  = builder.GetRenderWidth(),
            .height = builder.GetRenderHeight(),
        });

        // GBuffer pass
        m_gbuffer_pass.SetSceneColor(scene_color);
        m_gbuffer_pass.SetSceneDepth(scene_depth);
        m_gbuffer_pass.SetSceneVelocity(scene_velocity);
        m_gbuffer_pass.Build(builder);

        // Tonemap pass
        // m_tonemap_pass.SetHdrColor(m_gbuffer_pass.GetGBufferA());
        // m_tonemap_pass.Build(builder);

        builder.SetOutputTexture(m_gbuffer_pass.GetGBufferA());
    }

    void HighendPipeline::Update(RenderPipelineState& /*state*/)
    {
        // m_tonemap_pass.Update(state);
    }
}
