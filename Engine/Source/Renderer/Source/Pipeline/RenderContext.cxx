module;

#include <Valo/Profile.hpp>

#include "Defines.hpp"

module Valo.Renderer;

import Valo.Profiler;
import Valo.RenderGraph;

namespace Valo
{
    RenderContext::RenderContext(const Graph::RenderContext& context) :
        m_context(&context)
    {
    }

    void RenderContext::Render(RenderSet& render_set) const
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        render_set.Draw(*m_context);
    }
}
