module Valo.Renderer;

import Valo.RenderGraph;

namespace Valo
{
    RenderPipelineState::RenderPipelineState(Graph::CompiledRenderGraph& compiled_graph) :
        m_compiled_graph(&compiled_graph)
    {
    }
}
