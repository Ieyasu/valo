export module Valo.Renderer:RenderPipelineBuilder;

import Valo.RenderGraph;
import Valo.Std;

import :ComputeContext;
import :RenderContext;
import :RenderPipelineBuffer;
import :RenderPipelineTexture;

export namespace Valo
{
    struct ComputePassHandle
    {
    public:
        ComputePassHandle() = default;

    private:
        explicit ComputePassHandle(Graph::ComputeNodeHandle value) :
            m_value(value)
        {
        }

        Graph::ComputeNodeHandle m_value{};

        friend class RenderPipelineBuilder;
    };

    struct RenderPassHandle
    {
    public:
        RenderPassHandle() = default;

    private:
        explicit RenderPassHandle(Graph::RenderNodeHandle value) :
            m_value(value)
        {
        }

        Graph::RenderNodeHandle m_value{};

        friend class RenderPipelineBuilder;
    };

    class RenderPipelineBuilder final
    {
    public:
        explicit RenderPipelineBuilder(Graph::RenderGraph& graph);
        RenderPipelineBuilder(RenderPipelineBuilder&& other)                 = delete;
        RenderPipelineBuilder(const RenderPipelineBuilder& other)            = delete;
        RenderPipelineBuilder& operator=(RenderPipelineBuilder&& other)      = delete;
        RenderPipelineBuilder& operator=(const RenderPipelineBuilder& other) = delete;
        ~RenderPipelineBuilder()                                             = default;

        UInt32 GetRenderWidth() const;

        UInt32 GetRenderHeight() const;

        RenderPipelineTextureHandle GetOutputTexture() const;

        void SetOutputTexture(RenderPipelineTextureHandle texture);

        ComputePassHandle AddComputePass(String name);

        RenderPassHandle AddRenderPass(String name);

        RenderPipelineTextureHandle AddTexture(RenderPipelineTextureDescriptor descriptor);

        void SetCallback(ComputePassHandle node, ComputeNodeCallback callback);

        void SetStorageBuffer(ComputePassHandle node, RenderPipelineBufferHandle buffer);

        void SetUniformBuffer(ComputePassHandle node, RenderPipelineBufferHandle buffer);

        void SetSampledTexture(ComputePassHandle node, RenderPipelineTextureHandle texture);

        void SetStorageTexture(ComputePassHandle node, RenderPipelineTextureHandle texture);

        void SetCallback(RenderPassHandle node, RenderNodeCallback callback);

        void SetStorageBuffer(RenderPassHandle node, RenderPipelineBufferHandle buffer);

        void SetUniformBuffer(RenderPassHandle node, RenderPipelineBufferHandle buffer);

        void SetSampledTexture(RenderPassHandle node, RenderPipelineTextureHandle texture);

        void SetStorageTexture(RenderPassHandle node, RenderPipelineTextureHandle texture);

        void SetDepthAttachment(RenderPassHandle node, RenderPipelineTextureHandle texture, bool write_depth);

        void SetDepthAttachment(
            RenderPassHandle            node,
            RenderPipelineTextureHandle texture,
            float                       clear_depth,
            UInt32                      clear_stencil);

        void SetColorAttachment(RenderPassHandle node, UInt32 index, RenderPipelineTextureHandle texture);

        void SetColorAttachment(
            RenderPassHandle            node,
            UInt32                      index,
            RenderPipelineTextureHandle texture,
            Array<float, 4>             clear_color);

        void SetInputAttachment(RenderPassHandle node, UInt32 index, RenderPipelineTextureHandle texture);

    private:
        ObserverPtr<Graph::RenderGraph> m_graph{};
    };
}
