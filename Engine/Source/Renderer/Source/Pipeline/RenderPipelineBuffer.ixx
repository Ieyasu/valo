export module Valo.Renderer:RenderPipelineBuffer;

import Valo.RenderGraph;

export namespace Valo
{
    using RenderPipelineBufferHandle     = Graph::RenderGraphBufferHandle;
    using RenderPipelineBufferDescriptor = Graph::RenderGraphBufferDescriptor;
}
