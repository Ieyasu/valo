export module Valo.Renderer:RenderPipelineTexture;

import Valo.RenderGraph;

export namespace Valo
{
    using RenderPipelineTextureHandle     = Graph::RenderGraphTextureHandle;
    using RenderPipelineTextureDescriptor = Graph::RenderGraphTextureDescriptor;
}
