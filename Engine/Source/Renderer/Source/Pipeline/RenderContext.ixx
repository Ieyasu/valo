module;

#include <functional>

export module Valo.Renderer:RenderContext;

import Valo.RenderGraph;
import Valo.Std;

import :RenderSet;

export namespace Valo
{
    class RenderContext final
    {
    public:
        void Render(RenderSet& render_set) const;

    private:
        explicit RenderContext(const Graph::RenderContext& context);

        ObserverPtr<const Graph::RenderContext> m_context{};

        friend class RenderPipelineBuilder;
    };

    using RenderNodeCallback = Function<void(RenderContext)>;
}
