export module Valo.Renderer:RenderPipelineScheduler;

import Valo.RHI;
import Valo.Std;

import :RenderPipeline;
import :RenderUniforms;
import :ResourceManager;

export namespace Valo
{
    class RenderPipelineScheduler final
    {
    public:
        static UniquePtr<RenderPipelineScheduler> Create(
            ResourceManagerHandle resource_manager,
            RenderUniformsHandle  render_uniforms);

        void Run(RenderPipeline& render_pipeline);

    private:
        explicit RenderPipelineScheduler(ResourceManagerHandle resource_manager, RenderUniformsHandle render_uniforms);

        void Update(RenderPipeline& render_pipeline);

        void Execute(const RenderPipeline& render_pipeline);

        static void LogExecuteError(StringView message, RHI::CommandBufferHandle command_buffer, RHI::ResultCode result);

        ResourceManagerHandle m_resource_manager{};
        RenderUniformsHandle  m_render_uniforms{};
    };
}
