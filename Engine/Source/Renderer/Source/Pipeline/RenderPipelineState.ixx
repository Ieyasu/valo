export module Valo.Renderer:RenderPipelineState;

import Valo.RenderGraph;
import Valo.Std;

import :DeviceBuffer;
import :RenderPipelineBuffer;
import :RenderPipelineTexture;
import :Texture;

export namespace Valo
{
    class RenderPipelineState final
    {
    public:
        explicit RenderPipelineState(Graph::CompiledRenderGraph& compiled_graph);

        DeviceBufferHandle GetBuffer(RenderPipelineBufferHandle buffer_ref);

        TextureHandle GetTexture(RenderPipelineTextureHandle texture_ref);

    private:
        ObserverPtr<Graph::CompiledRenderGraph> m_compiled_graph{};
    };
}
