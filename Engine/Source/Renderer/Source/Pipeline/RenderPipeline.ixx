module;

#include <Valo/Profile.hpp>

export module Valo.Renderer:RenderPipeline;

import Valo.Profiler;
import Valo.RenderGraph;
import Valo.Std;

import :RenderPipelineBuilder;
import :RenderPipelineState;
import :Types;

export namespace Valo
{
    class Renderer;
    using RendererHandle = ObserverPtr<Renderer>;

    struct RenderPipelineDescriptor final
    {
        Resolution resolution{};
    };

    class RenderPipeline
    {
    public:
        template <typename T>
        static UniquePtr<RenderPipeline> Create(RendererHandle renderer);

        RenderPipeline()                                 = default;
        RenderPipeline(const RenderPipeline&)            = delete;
        RenderPipeline(RenderPipeline&&)                 = delete;
        RenderPipeline& operator=(const RenderPipeline&) = delete;
        RenderPipeline& operator=(RenderPipeline&&)      = delete;
        virtual ~RenderPipeline()                        = default;

    protected:
        virtual void Initialize(Renderer& renderer) = 0;

        virtual void Build(RenderPipelineBuilder& builder) = 0;

        virtual void Update(RenderPipelineState& state) = 0;

    private:
        RenderPipelineDescriptor   m_descriptor{};
        Graph::CompiledRenderGraph m_compiled_graph{};

        // Dependencies
        RendererHandle m_renderer{};

        friend class RenderPipelineCompiler;
        friend class RenderPipelineScheduler;
    };

    template <typename T>
    UniquePtr<RenderPipeline> RenderPipeline::Create(RendererHandle renderer)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto render_pipeline = Valo::MakeUnique<T>();
        static_cast<RenderPipeline*>(render_pipeline.get())->Initialize(*renderer);
        return render_pipeline;
    }
}
