export module Valo.Renderer:RenderPipelineCompiler;

import Valo.RenderGraph;
import Valo.Std;

import :RenderPipeline;
import :ResourceManager;

export namespace Valo
{
    class RenderPipelineCompiler final
    {
    public:
        static UniquePtr<RenderPipelineCompiler> Create(ResourceManagerHandle resource_manager);

        RenderPipelineCompiler(RenderPipelineCompiler&& other)                 = delete;
        RenderPipelineCompiler(const RenderPipelineCompiler& other)            = delete;
        RenderPipelineCompiler& operator=(RenderPipelineCompiler&& other)      = delete;
        RenderPipelineCompiler& operator=(const RenderPipelineCompiler& other) = delete;
        ~RenderPipelineCompiler()                                              = default;

        bool Compile(RenderPipelineDescriptor descriptor, RenderPipeline& out_render_pipeline);

    private:
        explicit RenderPipelineCompiler(ResourceManagerHandle resource_manager);

        Graph::RenderGraph         m_graph{};
        Graph::RenderGraphCompiler m_graph_compiler;
    };
}
