export module Valo.Renderer:HighendPipeline;

import Valo.RenderGraph;

import :DeferredGBufferPass;
import :RenderPipeline;
import :RenderPipelineBuilder;
import :RenderPipelineState;
import :RenderSet;
import :TonemapPass;

export namespace Valo
{
    class HighendPipeline final : public RenderPipeline
    {
    private:
        void Initialize(Renderer& renderer) override;

        void Build(RenderPipelineBuilder& builder) override;

        void Update(RenderPipelineState& state) override;

        RenderSetHandle m_shadow_render_set{};
        RenderSetHandle m_opaque_render_set{};
        RenderSetHandle m_transparent_render_set{};

        DeferredGBufferPass m_gbuffer_pass{};
        TonemapPass         m_tonemap_pass{};
    };
}
