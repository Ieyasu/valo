module;

#include "Defines.hpp"

#include <Valo/Profile.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.RenderGraph;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    UniquePtr<RenderPipelineScheduler> RenderPipelineScheduler::Create(
        ResourceManagerHandle resource_manager,
        RenderUniformsHandle  render_uniforms)
    {
        return UniquePtr<RenderPipelineScheduler>(new RenderPipelineScheduler(resource_manager, render_uniforms));
    }

    RenderPipelineScheduler::RenderPipelineScheduler(
        ResourceManagerHandle resource_manager,
        RenderUniformsHandle  render_uniforms) :
        m_resource_manager(resource_manager),
        m_render_uniforms(render_uniforms)
    {
    }

    void RenderPipelineScheduler::Run(RenderPipeline& render_pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        Update(render_pipeline);
        Execute(render_pipeline);
    }

    void RenderPipelineScheduler::Update(RenderPipeline& render_pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto state = RenderPipelineState(render_pipeline.m_compiled_graph);
        render_pipeline.Update(state);
    }

    void RenderPipelineScheduler::Execute(const RenderPipeline& render_pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        // Aquire the command buffer
        auto primary_cmd_buffer = m_resource_manager->AquireCommandBuffer();
        if (!primary_cmd_buffer.IsValid())
        {
            Log::Error("Failed to execute RenderPipeline: failed to aquire primary command buffer");
            return;
        }
        primary_cmd_buffer->SetName("Primary Command Buffer");

        // Begin command buffer
        const auto begin_result = primary_cmd_buffer->Begin();
        if (begin_result != RHI::ResultCode::Success)
        {
            LogExecuteError("failed to begin command buffer", primary_cmd_buffer, begin_result);
            return;
        }

        // Update view
        const auto&   compiled_graph = render_pipeline.m_compiled_graph;
        const auto    render_width   = render_pipeline.m_descriptor.resolution.width;
        const auto    render_height  = render_pipeline.m_descriptor.resolution.height;
        RHI::Viewport viewport       = {};
        viewport.x                   = 0;
        viewport.y                   = 0;
        viewport.width               = static_cast<float>(render_width);
        viewport.height              = static_cast<float>(render_height);
        viewport.min_depth           = 0.0f;
        viewport.max_depth           = 1.0f;
        primary_cmd_buffer->CmdSetViewport(viewport);

        RHI::Scissor scissor = {};
        scissor.x            = 0;
        scissor.y            = 0;
        scissor.width        = render_width;
        scissor.height       = render_height;
        primary_cmd_buffer->CmdSetScissor(scissor);

        const auto per_view_descriptor_set = m_render_uniforms->GetPerViewDescriptorSet();
        primary_cmd_buffer->CmdBindDescriptorSets(
            m_render_uniforms->GetPerViewPipelineLayout(),
            {&per_view_descriptor_set, 1},
            m_render_uniforms->GetPerViewDynamicOffsets(),
            VALO_VIEW_DESCRIPTOR_SET_INDEX);

        // Encode commands to the command buffer
        for (const auto& node : compiled_graph.nodes)
        {
            switch (node->GetType())
            {
            case Graph::NodeType::Compute:
                static_cast<const Graph::CompiledComputeNode&>(*node).Execute(primary_cmd_buffer);
                break;
            case Graph::NodeType::Render:
                static_cast<const Graph::CompiledRenderNode&>(*node).Execute(primary_cmd_buffer);
                break;
            default:
                const auto message = StringFunc::Format("unknown node type {}", node->GetType());
                LogExecuteError(message, primary_cmd_buffer, {});
                return;
            }
        }

        // Present on screen
        const auto image_barrier1 = RHI::ImageMemoryBarrier{
            .src_stage_flags  = compiled_graph.output.stage_flags,
            .src_access_flags = compiled_graph.output.access_flags,
            .src_usage_flags  = compiled_graph.output.usage_flags,

            .dst_stage_flags  = RHI::ShaderStage::None,
            .dst_access_flags = RHI::AccessFlagBits::Read,
            .dst_usage_flags  = RHI::ImageUsage::TransferSrc,

            .image            = compiled_graph.output.image,
            .base_mip_level   = 0,
            .level_count      = 1,
            .base_array_layer = 0,
            .layer_count      = 1,
        };

        const auto image_barrier2 = RHI::ImageMemoryBarrier{
            .src_stage_flags  = RHI::ShaderStage::None,
            .src_access_flags = RHI::AccessFlagBits::Read,
            .src_usage_flags  = RHI::ImageUsage::TransferSrc,

            .dst_stage_flags  = compiled_graph.output.stage_flags,
            .dst_access_flags = compiled_graph.output.access_flags,
            .dst_usage_flags  = compiled_graph.output.usage_flags,

            .image            = compiled_graph.output.image,
            .base_mip_level   = 0,
            .level_count      = 1,
            .base_array_layer = 0,
            .layer_count      = 1,
        };
        primary_cmd_buffer->CmdPipelineBarrier({}, {&image_barrier1, 1});
        primary_cmd_buffer->CmdPresent(compiled_graph.output.image, render_width, render_height);
        primary_cmd_buffer->CmdPipelineBarrier({}, {&image_barrier2, 1});

        // End the submit the command buffer
        const auto end_result = primary_cmd_buffer->End();
        if (end_result != RHI::ResultCode::Success)
        {
            LogExecuteError("failed to finalize command buffer", primary_cmd_buffer, end_result);
            return;
        }

        const auto submit_result = primary_cmd_buffer->Submit();
        if (submit_result != RHI::ResultCode::Success)
        {
            LogExecuteError("failed to submit command buffer", primary_cmd_buffer, submit_result);
            return;
        }
    }

    void RenderPipelineScheduler::LogExecuteError(
        StringView               message,
        RHI::CommandBufferHandle command_buffer,
        RHI::ResultCode          result)
    {
        Log::Error(
            "Failed to execute render pipeline (command_buffer = {}, result_code = {}): {}",
            command_buffer->GetName(),
            result,
            message);
    }
}
