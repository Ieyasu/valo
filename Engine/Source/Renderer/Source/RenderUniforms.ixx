export module Valo.Renderer:RenderUniforms;

import Valo.Engine;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :BufferStore;
import :Camera;
import :DeviceBuffer;
import :DeviceBufferView;
import :ResourceManager;

export namespace Valo
{
    class RenderUniforms final
    {
    public:
        static UniquePtr<RenderUniforms> Create(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store);

        RenderUniforms(const RenderUniforms&)            = delete;
        RenderUniforms(RenderUniforms&&)                 = delete;
        RenderUniforms& operator=(const RenderUniforms&) = delete;
        RenderUniforms& operator=(RenderUniforms&&)      = delete;

        ~RenderUniforms();

        void Update(const Camera& camera, const Time& time);

        RHI::PipelineLayoutHandle      GetPerViewPipelineLayout() const;
        RHI::DescriptorSetLayoutHandle GetPerViewDescriptorSetLayout() const;
        RHI::DescriptorSetHandle       GetPerViewDescriptorSet() const;
        Array<UInt32, 4>               GetPerViewDynamicOffsets() const;

    private:
        RenderUniforms(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store);

        struct TimeParams final
        {
            float  total;
            float  delta;
            UInt32 frame;
            float  _padding;
        };

        struct ViewParams final
        {
            Mat4 view_matrix;
            Mat4 view_projection_matrix;
            Mat4 view_projection_matrix_inverse;
            Vec2 linearize_depth_params;
            Vec2 jitter;
        };

        // Dependencies
        ResourceManagerHandle m_resource_manager{};
        BufferStoreHandle     m_buffer_store{};

        // Descriptor sets
        RHI::PipelineLayoutHandle      m_per_view_pipeline_layout{};
        RHI::DescriptorSetLayoutHandle m_per_view_descriptor_set_layout{};
        RHI::DescriptorSetHandle       m_per_view_descriptor_set{};

        // Parameters
        TimeParams             m_time_params{};
        DeviceBufferViewHandle m_time_params_buffer_view{};
        DeviceBufferViewHandle m_previous_time_params_buffer_view{};

        ViewParams             m_view_params{};
        DeviceBufferViewHandle m_view_params_buffer_view{};
        DeviceBufferViewHandle m_previous_view_params_buffer_view{};
    };

    using RenderUniformsHandle = ObserverPtr<RenderUniforms>;
}
