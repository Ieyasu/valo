export module Valo.Renderer:Texture;

import Valo.Engine;
import Valo.RHI;
import Valo.Std;

import :Sampler;

export namespace Valo
{
    using TextureFormat     = RHI::Format;
    using TextureDimensions = RHI::ImageDimensions;
    using TextureUsageFlags = RHI::ImageUsageFlags;
    using TextureUsage      = RHI::ImageUsage;

    struct TextureDescriptor final
    {
        RHI::ImageHandle  image{};
        SamplerHandle     sampler{};
        TextureUsageFlags usage{TextureUsage::None};
        TextureFormat     format{TextureFormat::Undefined};
        TextureDimensions dimensions{TextureDimensions::Two};
        UInt32          width{0};
        UInt32          height{0};
        UInt32          depth{0};
        UInt32          mip_level_count{0};
        UInt32          array_layer_count{0};
    };

    class Texture final : public Object
    {
    public:
        Texture(const Texture& texture, SamplerHandle sampler);

        Texture(
            TextureStoreHandle       store,
            RHI::ImageHandle         image,
            SamplerHandle            sampler,
            const TextureDescriptor& descriptor);

        void SetName(String name) override;

        RHI::ImageHandle GetImage() const;

        SamplerHandle GetSampler() const;

        TextureFormat GetFormat() const;

        UInt32 GetWidth() const;

        UInt32 GetHeight() const;

        UInt32 GetDepth() const;

        UInt32 GetMipLevelCount() const;

        UInt32 GetArrayLayerCount() const;

        bool IsDirty() const;

        bool IsDeviceLocal() const;

        void MakeDeviceLocal();

    private:
        TextureStoreHandle m_store{};
        RHI::ImageHandle   m_image{};
        SamplerHandle      m_sampler{};

        TextureFormat m_format{};
        UInt32      m_width{};
        UInt32      m_height{};
        UInt32      m_depth{};
        UInt32      m_mip_level_count{};
        UInt32      m_array_layer_count{};

        // Data upload
        bool m_is_dirty{};
        bool m_is_device_local{};
        bool m_owns_image{};

        friend class TextureStore;
    };

    using TextureHandle = ObjectPoolHandle<Texture>;
}
