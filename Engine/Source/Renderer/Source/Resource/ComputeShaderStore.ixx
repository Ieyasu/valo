module;

#include "Defines.hpp"

export module Valo.Renderer:ComputeShaderStore;

import Valo.Engine;
import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :ComputeShader;
import :PipelineCache;
import :ResourceManager;
import :ShaderSemantics;
import :ShaderStore;
import :Store;
import :TextureStore;

export namespace Valo
{
    class ComputeShaderStore final : public Store
    {
    public:
        static UniquePtr<ComputeShaderStore> Create(
            ResourceManagerHandle resource_manager,
            TextureStoreHandle    texture_store,
            ShaderStoreHandle     shader_store,
            PipelineCacheHandle   pipeline_cache);

        void Update();

        ComputeShaderHandle CreateComputeShader(StringView filepath);

        void DestroyComputeShader(ComputeShaderHandle compute_shader);

    private:
        ComputeShaderStore(
            ResourceManagerHandle resource_manager,
            TextureStoreHandle    texture_store,
            ShaderStoreHandle     shader_store,
            PipelineCacheHandle   pipeline_cache);

        // Dependencies
        TextureStoreHandle  m_texture_store{};
        ShaderStoreHandle   m_shader_store{};
        PipelineCacheHandle m_pipeline_cache{};

        // Resources
        ObjectPool<ComputeShader>        m_compute_shaders{};
        Vector<RHI::DescriptorSetUpdate> m_update_buffer{};
    };

    using ComputeShaderStoreHandle = ObserverPtr<ComputeShaderStore>;
}
