module;

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    BufferStore::BufferStore(ResourceManagerHandle resource_manager) :
        Store(resource_manager)
    {
    }

    UniquePtr<BufferStore> BufferStore::Create(ResourceManagerHandle resource_manager)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<BufferStore>(new BufferStore(resource_manager));

        const auto frames_in_flight = resource_manager->GetFramesInFlight();
        const auto
            dynamic_buffer_alignment = resource_manager->GetDevice()->GetLimits().min_uniform_buffer_offset_alignment;

        // Create a single large buffer for dynamic UBO's
        const auto dynamic_buffer_size       = static_cast<DeviceSizeType>(frames_in_flight * 1024UL * 1024UL);
        const auto dynamic_buffer_descriptor = DeviceBufferDescriptor{
            .count  = Memory::Align(dynamic_buffer_size, dynamic_buffer_alignment),
            .stride = sizeof(Byte),
            .usage  = DeviceBufferUsage::Uniform,
            .access = DeviceBufferHostAccess::FastSequential,
        };

        self->m_dynamic_buffer = self->CreateBuffer(dynamic_buffer_descriptor);
        if (!self->m_dynamic_buffer.IsValid())
        {
            Log::Error("Failed to create BufferStore: failed to create staging buffer");
            return nullptr;
        }
        self->m_dynamic_buffer->SetName("Dynamic Buffer");

        // Create a single large buffer for index buffers
        const auto index_buffer_size       = 32 * 1024UL * 1024UL;
        const auto index_buffer_descriptor = DeviceBufferDescriptor{
            .count  = index_buffer_size,
            .stride = sizeof(Byte),
            .usage  = DeviceBufferUsage::Index | DeviceBufferUsage::TransferDst,
            .access = DeviceBufferHostAccess::None,
        };

        self->m_index_buffer = self->CreateBuffer(index_buffer_descriptor);
        if (!self->m_index_buffer.IsValid())
        {
            Log::Error("Failed to create BufferStore: failed to create index buffer");
            return nullptr;
        }
        self->m_index_buffer->SetName("Index Buffer");

        // Create a single large buffer for index buffers
        const auto vertex_buffer_size       = 32 * 1024UL * 1024UL;
        const auto vertex_buffer_descriptor = DeviceBufferDescriptor{
            .count  = vertex_buffer_size,
            .stride = sizeof(Byte),
            .usage  = DeviceBufferUsage::Vertex | DeviceBufferUsage::TransferDst,
            .access = DeviceBufferHostAccess::None,
        };

        self->m_vertex_buffer = self->CreateBuffer(vertex_buffer_descriptor);
        if (!self->m_vertex_buffer.IsValid())
        {
            Log::Error("Failed to create BufferStore: failed to create vertex buffer");
            return nullptr;
        }
        self->m_vertex_buffer->SetName("Vertex Buffer");

        return self;
    }

    BufferStore::~BufferStore()
    {
        DestroyBuffer(m_dynamic_buffer);
        DestroyBuffer(m_index_buffer);
        DestroyBuffer(m_vertex_buffer);
    }

    DeviceBufferHandle BufferStore::CreateBuffer(const DeviceBufferDescriptor& descriptor)
    {
        const auto buffer_descriptor = RHI::BufferDescriptor{
            .size   = descriptor.count * descriptor.stride,
            .usage  = descriptor.usage,
            .access = descriptor.access,
        };

        const auto is_device_local = descriptor.access == DeviceBufferHostAccess::None
            || descriptor.access == DeviceBufferHostAccess::Auto;
        const auto buffer = GetResourceManager()->CreateBuffer(buffer_descriptor);
        const auto device_buffer = m_buffers.Emplace(this, buffer, descriptor.count, descriptor.stride, is_device_local);
        return device_buffer;
    }

    void BufferStore::DestroyBuffer(DeviceBufferHandle buffer)
    {
        GetResourceManager()->DestroyBuffer(buffer->GetNative());
        m_buffers.Erase(buffer);
    }

    DeviceBufferViewHandle BufferStore::CreateDynamicBufferView(DeviceSizeType count, DeviceSizeType stride)
    {
        const auto alignment  = GetResourceManager()->GetDevice()->GetLimits().min_uniform_buffer_offset_alignment;
        const auto range      = count * Memory::Align(stride, alignment);
        const auto new_offset = m_dynamic_buffer_offset + range;
        VALO_ASSERT(new_offset <= m_dynamic_buffer->GetSizeInBytes() / GetResourceManager()->GetFramesInFlight());

        const auto buffer_view  = m_buffer_views.Emplace(m_dynamic_buffer, m_dynamic_buffer_offset, range, true);
        m_dynamic_buffer_offset = new_offset;
        return buffer_view;
    }

    DeviceBufferViewHandle BufferStore::CreateIndexBufferView(DeviceSizeType count, DeviceSizeType stride)
    {
        const auto range      = count * stride;
        const auto new_offset = m_index_buffer_offset + range;
        VALO_ASSERT(new_offset <= m_index_buffer->GetSizeInBytes());

        const auto buffer_view = m_buffer_views.Emplace(m_index_buffer, m_index_buffer_offset, range, true);
        m_index_buffer_offset  = new_offset;
        return buffer_view;
    }

    DeviceBufferViewHandle BufferStore::CreateVertexBufferView(DeviceSizeType count, DeviceSizeType stride)
    {
        const auto range      = count * stride;
        const auto new_offset = m_vertex_buffer_offset + range;
        VALO_ASSERT(new_offset <= m_vertex_buffer->GetSizeInBytes());

        const auto buffer_view = m_buffer_views.Emplace(m_vertex_buffer, m_vertex_buffer_offset, range, true);
        m_vertex_buffer_offset = new_offset;
        return buffer_view;
    }

    void BufferStore::DestroyBufferView(DeviceBufferViewHandle buffer_view)
    {
        m_buffer_views.Erase(buffer_view);
    }

    void BufferStore::UploadBuffer(const void* data, DeviceBufferViewHandle buffer_view, DeviceSizeType size)
    {
        const auto offset = buffer_view->IsDynamic() ? CalculateDynamicOffset(buffer_view) : buffer_view->GetOffset();

        if (buffer_view->GetBuffer()->IsDeviceLocal())
        {
            GetResourceManager()->UploadBuffer(data, buffer_view->GetBuffer()->GetNative(), offset, size);
        }
        else
        {
            GetResourceManager()->UploadMappableBuffer(data, buffer_view->GetBuffer()->GetNative(), offset, size);
        }
    }

    UInt32 BufferStore::CalculateDynamicOffset(DeviceBufferViewHandle buffer_view) const
    {
        VALO_ASSERT(buffer_view->IsDynamic());

        const auto frame_scaled_size = GetResourceManager()->GetCurrentFrame()
            * buffer_view->GetBuffer()->GetSizeInBytes();
        const auto dynamic_offset = frame_scaled_size / GetResourceManager()->GetFramesInFlight();

        VALO_ASSERT(frame_scaled_size % GetResourceManager()->GetFramesInFlight() == 0);

        return static_cast<UInt32>(dynamic_offset + buffer_view->GetOffset());
    }
}
