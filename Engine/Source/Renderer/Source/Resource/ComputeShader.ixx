export module Valo.Renderer:ComputeShader;

import Valo.Engine;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :DeviceBuffer;
import :DeviceBufferView;
import :ShaderEffect;
import :ShaderProperties;
import :Texture;

export namespace Valo
{
    class ComputeShader final : public Object
    {
    public:
        explicit ComputeShader();

        RHI::ComputePipelineHandle GetPipeline() const;

        RHI::DescriptorSetHandle GetDescriptorSet() const;

        Optional<UInt32> GetPropertyId(StringView property_name) const;

        ShaderPassHandle GetShaderPass() const;

        ShaderStageHandle GetShaderStage() const;

        void SetAllBuffers(DeviceBufferHandle value);
        void SetBuffer(UInt32 property_id, DeviceBufferView value);
        void SetBuffer(StringView property_name, DeviceBufferView value);
        void SetBuffer(UInt32 property_id, UInt32 index, DeviceBufferView value);
        void SetBuffer(StringView property_name, UInt32 index, DeviceBufferView value);

        void SetAllTextures(TextureHandle value);
        void SetTexture(UInt32 property_id, TextureHandle value);
        void SetTexture(StringView property_name, TextureHandle value);
        void SetTexture(UInt32 property_id, UInt32 index, TextureHandle value);
        void SetTexture(StringView property_name, UInt32 index, TextureHandle value);

        void SetFloat(UInt32 property_id, float value);
        void SetFloat(StringView property_name, float value);

        void SetVec2(UInt32 property_id, Vec2 value);
        void SetVec2(StringView property_name, Vec2 value);

        void SetVec3(UInt32 property_id, Vec3 value);
        void SetVec3(StringView property_name, Vec3 value);

        void SetVec4(UInt32 property_id, Vec4 value);
        void SetVec4(StringView property_name, Vec4 value);

        template <typename OutputStream>
        void Serialize(OutputStream stream) const;

        template <typename InputStream>
        void Deserialize(InputStream stream);

    private:
        // Serialized data
        String m_name{"Compute Shader"};
        String m_compute_shader_path{};

        // Populated data
        RHI::ComputePipelineHandle m_pipeline{};
        ShaderEffectHandle         m_effect{};
        ShaderProperties           m_properties{};

        friend class ComputeShaderStore;
        friend class PipelineCache;
    };

    using ComputeShaderHandle = ObjectPoolHandle<ComputeShader>;

    template <typename OutputStream>
    void ComputeShader::Serialize(OutputStream stream) const
    {
        stream.SerializeProperty("name", m_name);
        stream.SerializeProperty("shader_path", m_compute_shader_path);
    }

    template <typename InputStream>
    void ComputeShader::Deserialize(InputStream stream)
    {
        stream.DeserializeProperty("name", m_name);
        stream.DeserializeProperty("shader_path", m_compute_shader_path);
    }
}
