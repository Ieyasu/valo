export module Valo.Renderer:DeviceBufferView;

import Valo.Std;

import :DeviceBuffer;

export namespace Valo
{
    class DeviceBufferView final
    {
    public:
        using SizeType = DeviceSizeType;

        DeviceBufferView() = default;

        DeviceBufferView(DeviceBufferHandle buffer);  // NOLINT

        DeviceBufferView(DeviceBufferHandle buffer, size_t offset, size_t range, bool is_dynamic);

        DeviceBufferHandle GetBuffer() const;

        DeviceSizeType GetOffset() const;

        DeviceSizeType GetRange() const;

        bool IsDynamic() const;

    private:
        DeviceBufferHandle m_buffer{};
        DeviceSizeType     m_offset{};
        DeviceSizeType     m_range{};
        bool               m_is_dynamic{};
    };

    using DeviceBufferViewHandle = PoolHandle<DeviceBufferView>;
}
