export module Valo.Renderer:RenderSet;

import Valo.RenderGraph;
import Valo.RHI;
import Valo.Std;

import :Material;
import :PipelineCache;
import :RenderElement;
import :RenderTag;

export namespace Valo
{
    struct RenderSetDescriptor final
    {
        Set<RenderTag> tags{};
    };

    class RenderSet final
    {
    public:
        RenderSet() = default;

        RenderSet(PipelineCacheHandle pipeline_cache, RenderSetDescriptor descriptor);

        const Set<RenderTag>& GetRenderTags() const;

        const Vector<RenderElement>& GetRenderElements() const;

        void Clear();

        void Insert(RenderElement element);

        void Draw(const Graph::RenderContext& context);

    private:
        struct DrawCall final
        {
            // Material data
            RHI::GraphicsPipelineHandle pipeline{};
            RHI::PipelineLayoutHandle   pipeline_layout{};
            RHI::DescriptorSetHandle    per_material_descriptor_set{};
            RHI::DescriptorSetHandle    per_object_descriptor_set{};

            // Index data
            RHI::BufferHandle index_buffer{};
            UInt32            index_buffer_offset{};
            UInt32            index_count{};
            RHI::IndexFormat  index_format{};

            // Vertex data
            SmallVector<RHI::BufferHandle>   vertex_buffers{};
            SmallVector<RHI::DeviceSizeType> vertex_buffer_offsets{};

            // Instance data
            UInt32 instance_count{0};
            UInt32 first_instance{0};
        };

        void SetRenderPass(RHI::RenderPassHandle render_pass, UInt32 subpass_index);

        void RecacheDrawCalls();

        void CacheDrawCall(const RenderElement& element);

        PipelineCacheHandle   m_pipeline_cache{};
        RHI::RenderPassHandle m_render_pass{};
        UInt32                m_subpass_index{};

        RenderSetDescriptor   m_descriptor{};
        Vector<RenderElement> m_elements{};
        Vector<DrawCall>      m_draw_calls{};

        friend class RenderPipeline;
        friend class RenderSetStore;
    };

    using RenderSetHandle = PoolHandle<RenderSet>;
}
