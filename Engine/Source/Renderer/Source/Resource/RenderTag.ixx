export module Valo.Renderer:RenderTag;

import Valo.Std;

export namespace Valo
{
    class RenderTag final
    {
    public:
        static const RenderTag None;
        static const RenderTag Opaque;
        static const RenderTag Shadow;
        static const RenderTag Transparent;

        explicit RenderTag(String name);

        bool operator==(const RenderTag& other) const;
        bool operator!=(const RenderTag& other) const;
        bool operator<=(const RenderTag& other) const;
        bool operator>=(const RenderTag& other) const;
        bool operator<(const RenderTag& other) const;
        bool operator>(const RenderTag& other) const;

        template <typename OutputStream>
        void Serialize(OutputStream stream) const;

        template <typename InputStream>
        void Deserialize(InputStream stream);

    private:
        String m_name{};
    };

    template <typename OutputStream>
    inline void RenderTag::Serialize(OutputStream stream) const
    {
        stream.Serialize(m_name);
    }

    template <typename InputStream>
    inline void RenderTag::Deserialize(InputStream stream)
    {
        stream.Deserialize(m_name);
    }
}
