export module Valo.Renderer:ShaderEffect;

import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :ShaderProperties;

export namespace Valo
{
    /**
     * @brief Source code and reflection for single shader stage.
     *
     * Contains the compiled shader sources as a binary blob and extracted reflection data for a single shader stage,
     * such as compute, fragment, vertex, or other available shader stages.
     */
    struct ShaderStage final
    {
        /**
         * @brief The binary sources for the shader stage.
         *
         * The format of the binaries depend on the rendering backend they are meant for. For Vulkan, the binary
         * contains SPIRV. For DirectX 12, the binary contains DXIL. For Metal, the binary contains AIR.
         */
        BinaryBlob binary{};

        /**
         * @brief Reflection data for the shader.
         */
        ShaderReflection reflection{};
    };

    /**
     * @brief Handle pointing to a ShaderStage object.
     */
    using ShaderStageHandle = ObserverPtr<const ShaderStage>;

    /**
     * @brief A set of shader stages that form a shader pass.
     *
     * Contains one or more shader stages grouped together. For example, can contain a compute shader stage for compute
     * work or vertex and fragment stages for rasterization work.
     */
    struct ShaderPass final
    {
        /**
         * @brief Stages in this shader pass.
         */
        Vector<ShaderStageHandle> stages{};

        /**
         * @brief Pipeline layout for the shader stages.
         */
        RHI::PipelineLayoutHandle pipeline_layout{};
    };

    /**
     * @brief Handle pointing to a ShaderPass object.
     */
    using ShaderPassHandle = ObserverPtr<const ShaderPass>;

    /**
     * @brief A list of shader passes that form a single shader effect.
     *
     * Contains one or more shader passes grouped together. For example, can contain one pass to render object depth
     * for a Z-prepass and a second pass to render the objects with forward rendering.
     */
    struct ShaderEffect final
    {
        /**
         * @brief Name of the shader effect.
         */
        String name{};

        /**
         * @brief The passes that form the shader effect.
         */
        Vector<ShaderPass> passes{};

        /**
         * @brief Table containing information on how properties map to descriptor sets and binding indices.
         */
        ShaderPropertyTable property_table{};

        /**
         * @brief Pipeline layout for the shader stages.
         */
        Array<RHI::DescriptorSetLayoutHandle, 4> descriptor_set_layouts{};
    };

    /**
     * @brief Handle pointing to a ShaderEffect object.
     */
    using ShaderEffectHandle = ObserverPtr<const ShaderEffect>;
}
