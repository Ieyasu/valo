module;

#include <Valo/Profile.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    MeshStore::MeshStore(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store) :
        Store(resource_manager),
        m_buffer_store(buffer_store)
    {
    }

    UniquePtr<MeshStore> MeshStore::Create(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<MeshStore>(new MeshStore(resource_manager, buffer_store));
        return self;
    }

    void MeshStore::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        for (auto& mesh : m_meshes)
        {
            if (mesh.m_is_dirty)
            {
                UploadMesh(mesh);
            }
        }
    }

    MeshHandle MeshStore::CreateMesh()
    {
        auto mesh     = m_meshes.Emplace();
        mesh->m_store = this;
        return mesh;
    }

    void MeshStore::DestroyMesh(MeshHandle mesh)
    {
        for (auto& primitive : mesh->m_primitives)
        {
            if (primitive.device_buffer.IsValid())
            {
                m_buffer_store->DestroyBufferView(primitive.device_buffer);
            }
            primitive.device_buffer = {};
        }

        for (auto& vertex_buffer : mesh->m_vertex_buffers)
        {
            if (vertex_buffer.device_buffer.IsValid())
            {
                m_buffer_store->DestroyBufferView(vertex_buffer.device_buffer);
            }
            vertex_buffer.device_buffer = {};
        }

        m_meshes.Erase(mesh);
    }

    void MeshStore::UploadMesh(Mesh& mesh)
    {
        mesh.m_is_dirty = false;

        for (auto& primitive : mesh.m_primitives)
        {
            const auto& host_buffer = primitive.host_buffer;

            auto& device_buffer_view = primitive.device_buffer;
            if (device_buffer_view.IsValid() && device_buffer_view->GetRange() < host_buffer.size())
            {
                m_buffer_store->DestroyBufferView(device_buffer_view);
                device_buffer_view = {};
            }

            if (!device_buffer_view.IsValid())
            {
                const auto new_buffer = m_buffer_store->CreateIndexBufferView(host_buffer.size(), sizeof(Byte));
                if (!new_buffer.IsValid())
                {
                    Log::Error("Failed to upload mesh: failed to create vertex buffer");
                    return;
                }
                device_buffer_view = new_buffer;
            }

            m_buffer_store->UploadBuffer(host_buffer.data(), device_buffer_view, host_buffer.size());
        }

        for (auto& vertex_buffer : mesh.m_vertex_buffers)
        {
            const auto& host_buffer        = vertex_buffer.host_buffer;
            auto&       device_buffer_view = vertex_buffer.device_buffer;
            if (device_buffer_view.IsValid() && device_buffer_view->GetRange() < host_buffer.size())
            {
                m_buffer_store->DestroyBufferView(device_buffer_view);
                device_buffer_view = {};
            }

            if (!device_buffer_view.IsValid())
            {
                const auto new_buffer = m_buffer_store->CreateVertexBufferView(host_buffer.size(), sizeof(Byte));
                if (!new_buffer.IsValid())
                {
                    Log::Error("Failed to upload mesh: failed to create vertex buffer");
                    return;
                }
                device_buffer_view = new_buffer;
            }

            m_buffer_store->UploadBuffer(host_buffer.data(), device_buffer_view, host_buffer.size());
        }
    }
}
