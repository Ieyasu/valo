module;

#include <string>

module Valo.Renderer;

import Valo.Std;

namespace Valo
{
    const RenderTag RenderTag::None        = RenderTag{"None"};
    const RenderTag RenderTag::Opaque      = RenderTag{"Opaque"};
    const RenderTag RenderTag::Shadow      = RenderTag{"Shadow"};
    const RenderTag RenderTag::Transparent = RenderTag{"Transparent"};

    RenderTag::RenderTag(String name) :
        m_name(Memory::Move(name))
    {
    }

    bool RenderTag::operator==(const RenderTag& other) const
    {
        return m_name == other.m_name;
    }

    bool RenderTag::operator!=(const RenderTag& other) const
    {
        return m_name != other.m_name;
    }

    bool RenderTag::operator<=(const RenderTag& other) const
    {
        return m_name <= other.m_name;
    }

    bool RenderTag::operator>=(const RenderTag& other) const
    {
        return m_name >= other.m_name;
    }

    bool RenderTag::operator<(const RenderTag& other) const
    {
        return m_name < other.m_name;
    }

    bool RenderTag::operator>(const RenderTag& other) const
    {
        return m_name > other.m_name;
    }
}
