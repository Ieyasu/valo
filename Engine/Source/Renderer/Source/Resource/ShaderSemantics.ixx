export module Valo.Renderer:ShaderSemantics;

import Valo.RHI;
import Valo.Std;

import :Texture;

export namespace Valo
{
    enum class VertexAttribute : EnumSmall
    {
        Position = 0,
        Normal   = 1,
        Tangent  = 2,
        UV       = 3,
        Color    = 4,
    };

    enum class MaterialTexture : EnumSmall
    {
        Albedo          = 0,
        Normal          = 1,
        Emissive        = 2,
        ORM             = 3,
        Transmission    = 4,
        Sheen           = 5,
        Clearcoat       = 6,
        ClearcoatNormal = 7,
        Iridescence     = 8,
    };

    struct VertexInputSemantic final
    {
    public:
        VertexInputSemantic(String name, VertexAttribute attribute, RHI::Format format);

        const String& GetName() const;

        UInt32 GetBinding() const;

        UInt32 GetStride() const;

        VertexAttribute GetAttribute() const;

        RHI::Format GetFormat() const;

        static Optional<VertexInputSemantic> Get(StringView name);

        static const VertexInputSemantic& Get(VertexAttribute attribute);

    private:
        String          m_name{};
        UInt32        m_stride{0};
        VertexAttribute m_attribute{0};
        RHI::Format     m_format{RHI::Format::Undefined};
    };

    struct MaterialTextureSemantic final
    {
    public:
        MaterialTextureSemantic(String name, MaterialTexture type);

        const String& GetName() const;

        MaterialTexture GetType() const;

        static Optional<MaterialTextureSemantic> Get(StringView name);

        static const MaterialTextureSemantic& Get(MaterialTexture texture);

    private:
        String          m_name{};
        MaterialTexture m_type{};
    };
}
