export module Valo.Renderer:TextureStore;

import Valo.Engine;
import Valo.Std;

import :ResourceManager;
import :Sampler;
import :Store;
import :Texture;

export namespace Valo
{
    class TextureStore final : public Store
    {
    public:
        TextureStore(const TextureStore&)            = delete;
        TextureStore(TextureStore&&)                 = delete;
        TextureStore& operator=(const TextureStore&) = delete;
        TextureStore& operator=(TextureStore&&)      = delete;
        ~TextureStore() override;

        static UniquePtr<TextureStore> Create(ResourceManagerHandle resource_manager);

        TextureHandle CreateTexture(const TextureDescriptor& descriptor);

        void DestroyTexture(TextureHandle texture);

        SamplerHandle CreateSampler(const SamplerDescriptor& descriptor);

        void DestroySampler(SamplerHandle sampler);

        void UploadTexture(
            Span<const Byte> data,
            TextureHandle    texture,
            UInt32         mip_level   = 0,
            UInt32         array_layer = 0);

        void UploadTexture(
            Span<const Byte> data,
            TextureHandle    texture,
            UInt32         width,
            UInt32         height,
            UInt32         depth,
            UInt32         offset_x,
            UInt32         offset_y,
            UInt32         offset_z,
            UInt32         mip_level,
            UInt32         array_layer);

        // Default resources

        TextureHandle GetDefaultTextureBlack() const;

        TextureHandle GetDefaultTextureGray() const;

        TextureHandle GetDefaultTextureWhite() const;

        TextureHandle GetDefaultTextureAlbedo() const;

        TextureHandle GetDefaultTextureEmissive() const;

        TextureHandle GetDefaultTextureNormal() const;

        TextureHandle GetDefaulTextureOrm() const;

        SamplerHandle GetDefaultSamplerNearestClamp() const;

        SamplerHandle GetDefaultSamplerNearestRepeat() const;

        SamplerHandle GetDefaultSamplerLinearClamp() const;

        SamplerHandle GetDefaultSamplerLinearRepeat() const;

    private:
        explicit TextureStore(ResourceManagerHandle resource_manager);

        void CreateDefaultResources();

        void DestroyDefaultResources();

        // Resources
        ObjectPool<Texture> m_textures{};
        ObjectPool<Sampler> m_samplers{};

        // Default resources
        TextureHandle m_black_texture{};
        TextureHandle m_gray_texture{};
        TextureHandle m_white_texture{};
        TextureHandle m_normal_texture{};
        TextureHandle m_orm_texture{};
        SamplerHandle m_nearest_clamp_sampler{};
        SamplerHandle m_nearest_repeat_sampler{};
        SamplerHandle m_linear_clamp_sampler{};
        SamplerHandle m_linear_repeat_sampler{};
    };

    using TextureStoreHandle = ObserverPtr<TextureStore>;
}
