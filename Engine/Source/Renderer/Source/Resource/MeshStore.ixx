export module Valo.Renderer:MeshStore;

import Valo.Engine;
import Valo.RHI;
import Valo.Std;

import :BufferStore;
import :DeviceBufferView;
import :Mesh;
import :ResourceManager;
import :Store;

export namespace Valo
{
    class MeshStore final : public Store
    {
    public:
        static UniquePtr<MeshStore> Create(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store);

        void Update();

        MeshHandle CreateMesh();

        void DestroyMesh(MeshHandle mesh);

    private:
        explicit MeshStore(ResourceManagerHandle resource_manager, BufferStoreHandle buffer_store);

        void UploadMesh(Mesh& mesh);

        void UploadBuffer(
            const String&       name,
            const Vector<Byte>& host_buffer,
            DeviceBufferView&   device_buffer_view,
            RHI::BufferUsage    usage_flags);

        // CPU mesh data
        ObjectPool<Mesh> m_meshes{};

        // Dependencies
        BufferStoreHandle m_buffer_store{};
    };

    using MeshStoreHandle = ObserverPtr<MeshStore>;
}
