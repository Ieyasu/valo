module;

#include <Valo/Macros.hpp>

#include <limits>
#include <vector>

module Valo.Renderer;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    Mesh::Mesh() :
        Object("Mesh")
    {
        m_vertex_buffers.resize(4);

        InitializeVertexBuffer(VertexAttribute::Position);
        InitializeVertexBuffer(VertexAttribute::Normal);
        InitializeVertexBuffer(VertexAttribute::Tangent);
        InitializeVertexBuffer(VertexAttribute::UV);
    }

    const Vector<MeshPrimitive>& Mesh::GetPrimitives() const
    {
        VALO_ASSERT(IsValid());

        return m_primitives;
    }

    const Vector<MeshVertexBuffer>& Mesh::GetVertexBuffers() const
    {
        VALO_ASSERT(IsValid());

        return m_vertex_buffers;
    }

    void Mesh::Clear()
    {
        // TODO
        m_is_dirty = true;
    }

    void Mesh::SetPositions(const Vector<Vec3>& positions)
    {
        WriteFloat32Buffer(positions, VertexAttribute::Position);
        m_is_dirty = true;
    }

    void Mesh::SetNormals(const Vector<Vec3>& normals)
    {
        WriteSNorm16Buffer(normals, VertexAttribute::Normal);
        m_is_dirty = true;
    }

    void Mesh::SetTangents(const Vector<Vec4>& tangents)
    {
        WriteSNorm16Buffer(tangents, VertexAttribute::Tangent);
        m_is_dirty = true;
    }

    void Mesh::SetUVs(const Vector<Vec2>& uvs)
    {
        WriteFloat32Buffer(uvs, VertexAttribute::UV);
        m_is_dirty = true;
    }

    void Mesh::SetColors(const Vector<Vec4>& colors)
    {
        WriteFloat32Buffer(colors, VertexAttribute::Color);
        m_is_dirty = true;
    }

    void Mesh::SetIndices(UInt32 primitive_index, const Vector<UInt32>& indices)
    {
        VALO_ASSERT(IsValid());

        if (indices.empty())
        {
            return;
        }

        m_is_dirty = true;

        if (m_primitives.size() <= primitive_index)
        {
            m_primitives.resize(primitive_index + 1);
        }

        // Find the largest index
        auto max_index = indices.front();
        for (const auto& index : indices)
        {
            max_index = Math::Max(max_index, index);
        }

        // Use 16 bit indices if largst index is small enough, otherwise use 32 bit indices
        auto& out_primitive = m_primitives[primitive_index];
        if (max_index > std::numeric_limits<UInt16>::max() - 1)
        {
            out_primitive.format = RHI::IndexFormat::UInt32;
            out_primitive.host_buffer.resize(indices.size() * sizeof(UInt32));
            Memory::Memcpy(out_primitive.host_buffer.data(), indices.data(), indices.size() * sizeof(UInt32));
        }
        else
        {
            out_primitive.format = RHI::IndexFormat::UInt16;
            out_primitive.host_buffer.resize(indices.size() * sizeof(UInt16));
            for (size_t i = 0; i < indices.size(); ++i)
            {
                auto index = static_cast<UInt16>(indices[i]);
                Memory::Memcpy(out_primitive.host_buffer.data() + i * sizeof(UInt16), &index, sizeof(UInt16));
            }
        }
    }

    void Mesh::Resize(size_t size)
    {
        VALO_ASSERT(IsValid());

        auto& positions = GetVertexBuffer(VertexAttribute::Position);
        positions.host_buffer.resize(size * positions.stride);

        auto& normals = GetVertexBuffer(VertexAttribute::Normal);
        normals.host_buffer.resize(size * normals.stride);

        auto& tangents = GetVertexBuffer(VertexAttribute::Tangent);
        tangents.host_buffer.resize(size * tangents.stride);

        auto& uvs = GetVertexBuffer(VertexAttribute::UV);
        uvs.host_buffer.resize(size * uvs.stride);

        m_vertex_count = size;
    }

    MeshVertexBuffer& Mesh::GetVertexBuffer(VertexAttribute attribute)
    {
        VALO_ASSERT(static_cast<size_t>(attribute) < m_vertex_buffers.size());

        return m_vertex_buffers[static_cast<size_t>(attribute)];
    }

    void Mesh::InitializeVertexBuffer(VertexAttribute attribute)
    {
        const auto& semantics = VertexInputSemantic::Get(attribute);

        auto& vertex_buffer     = GetVertexBuffer(attribute);
        vertex_buffer.attribute = semantics.GetAttribute();
        vertex_buffer.stride    = semantics.GetStride();
        vertex_buffer.format    = semantics.GetFormat();
    }

    bool Mesh::IsValid() const
    {
        constexpr auto required_attributes = Array<VertexAttribute, 5>{
            VertexAttribute::Position,
            VertexAttribute::Normal,
            VertexAttribute::Tangent,
            VertexAttribute::UV,
        };

        // Make sure all required attributes are available
        for (const auto& attribute : required_attributes)
        {
            if (m_vertex_buffers.size() < static_cast<size_t>(attribute))
            {
                VALO_ASSERT(false);
                return false;
            }
        }

        // Make sure all vertex buffers have the same number of elements
        for (const auto& attribute : required_attributes)
        {
            const auto& vertex_buffer = m_vertex_buffers[static_cast<size_t>(attribute)];
            if (vertex_buffer.stride == 0)
            {
                VALO_ASSERT(false);
                return false;
            }

            const auto vertex_count = vertex_buffer.host_buffer.size() / vertex_buffer.stride;
            if (vertex_count != m_vertex_count)
            {
                VALO_ASSERT(false);
                return false;
            }
        }

        return true;
    }
}
