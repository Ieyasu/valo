module Valo.Renderer;

import Valo.RHI;
import Valo.Std;

namespace Valo
{
    DeviceBuffer::DeviceBuffer() :
        Object("Device Buffer")
    {
    }

    DeviceBuffer::DeviceBuffer(
        BufferStoreHandle store,
        RHI::BufferHandle buffer,
        DeviceSizeType    count,
        DeviceSizeType    stride,
        bool              is_device_local) :
        Object("Device Buffer"),
        m_store(store),
        m_buffer(Memory::Move(buffer)),
        m_count(count),
        m_stride(stride),
        m_is_device_local(is_device_local)
    {
    }

    void DeviceBuffer::SetName(String name)
    {
        m_store->GetResourceManager()->SetObjectName(GetNative(), name);
        Object::SetName(Memory::Move(name));
    }

    RHI::BufferHandle DeviceBuffer::GetNative() const
    {
        return m_buffer;
    }

    DeviceSizeType DeviceBuffer::GetSizeInBytes() const
    {
        return m_count * m_stride;
    }

    DeviceSizeType DeviceBuffer::GetCount() const
    {
        return m_count;
    }

    DeviceSizeType DeviceBuffer::GetStride() const
    {
        return m_stride;
    }

    bool DeviceBuffer::IsDeviceLocal() const
    {
        return m_is_device_local;
    }

    void DeviceBuffer::MakeDeviceLocal()
    {
        m_is_device_local = true;
    }
}
