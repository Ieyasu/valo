export module Valo.Renderer:RenderSetStore;

import Valo.Engine;
import Valo.Std;

import :MaterialStore;
import :MeshStore;
import :PipelineCache;
import :RenderSet;
import :ResourceManager;
import :Store;

export namespace Valo
{
    class RenderSetStore final : public Store
    {
    public:
        static UniquePtr<RenderSetStore> Create(
            ResourceManagerHandle resource_manager,
            PipelineCacheHandle   pipeline_cache);

        const Pool<RenderSet>& GetRenderSets() const;

        RenderSetHandle GetOrCreateRenderSet(const RenderSetDescriptor& descriptor);

        RenderSetHandle CreateRenderSet(const RenderSetDescriptor& descriptor);

        void DestroyRenderSet(RenderSetHandle render_set);

    private:
        RenderSetStore(ResourceManagerHandle resource_manager, PipelineCacheHandle pipeline_cache);

        // Dependencies
        PipelineCacheHandle m_pipeline_cache{};

        // Resources
        Pool<RenderSet> m_render_sets{};
    };

    using RenderSetStoreHandle = ObserverPtr<RenderSetStore>;
}
