export module Valo.Renderer:RenderElement;

import Valo.Math;
import Valo.Std;

import :Material;
import :Mesh;

export namespace Valo
{
    struct RenderElement final
    {
        Mat4                        transform{};
        ObserverPtr<const Material> material;
        ObserverPtr<const Mesh>     mesh;
    };
}
