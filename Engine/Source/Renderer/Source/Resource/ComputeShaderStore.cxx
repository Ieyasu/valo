module;

#include <string>

module Valo.Renderer;

import Valo.Serialization;
import Valo.Std;

namespace Valo
{
    ComputeShaderStore::ComputeShaderStore(
        ResourceManagerHandle resource_manager,
        TextureStoreHandle    texture_store,
        ShaderStoreHandle     shader_store,
        PipelineCacheHandle   pipeline_cache) :
        Store(resource_manager),
        m_texture_store(texture_store),
        m_shader_store(shader_store),
        m_pipeline_cache(pipeline_cache)
    {
    }

    UniquePtr<ComputeShaderStore> ComputeShaderStore::Create(
        ResourceManagerHandle resource_manager,
        TextureStoreHandle    texture_store,
        ShaderStoreHandle     shader_store,
        PipelineCacheHandle   pipeline_cache)
    {
        return UniquePtr<ComputeShaderStore>(
            new ComputeShaderStore(resource_manager, texture_store, shader_store, pipeline_cache));
    }

    void ComputeShaderStore::Update()
    {
        for (auto& compute_shader : m_compute_shaders)
        {
            auto& shader_properties = compute_shader.m_properties;
            if (shader_properties.IsDirty())
            {
                shader_properties.UpdateDescriptorSet(GetResourceManager(), m_update_buffer);
                GetResourceManager()->SetObjectName(shader_properties.GetDescriptorSet(), compute_shader.GetName());
            }
        }
    }

    ComputeShaderHandle ComputeShaderStore::CreateComputeShader(StringView filepath)
    {
        auto compute_shader = m_compute_shaders.Emplace();

        // Deserialize compute_shader
        auto archive = Serialization::YamlArchive{};
        archive.Load(String("Engine/Assets/ComputeShaders/") + String(filepath) + String(".yml"));
        archive.Deserialize(*compute_shader);

        const auto shader_stage = m_shader_store->GetShaderStage(compute_shader->m_compute_shader_path);
        auto       shader_pass  = ShaderPass{.stages = {shader_stage}};

        // Load shader data
        compute_shader->m_effect     = m_shader_store->CreateShaderEffect(String(filepath), {&shader_pass, 1});
        compute_shader->m_properties = ShaderProperties(compute_shader->m_effect->property_table);
        compute_shader->m_pipeline   = m_pipeline_cache->GetOrCreatePipeline(compute_shader);

        // Ensure all properties have bound values
        compute_shader->SetAllTextures(m_texture_store->GetDefaultTextureBlack());
        compute_shader->SetAllBuffers({});

        return compute_shader;
    }

    void ComputeShaderStore::DestroyComputeShader(ComputeShaderHandle compute_shader)
    {
        m_compute_shaders.Erase(compute_shader);
    }
}
