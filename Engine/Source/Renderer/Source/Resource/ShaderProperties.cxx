module;

#include <Valo/Macros.hpp>

#include <map>
#include <string>
#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    ShaderProperties::ShaderProperties(const ShaderPropertyTable& table) :
        m_table(&table)
    {
        for (const auto& name_property_id_pair : table.property_ids)
        {
            const auto& property_id = name_property_id_pair.second;
            if (IsTexture(property_id))
            {
                m_textures[property_id] = {};
            }
            else if (IsBuffer(property_id))
            {
                m_buffers[property_id] = {};
            }
        }
    }

    bool ShaderProperties::IsDirty() const
    {
        return m_is_dirty;
    }

    RHI::DescriptorSetHandle ShaderProperties::GetDescriptorSet() const
    {
        return m_descriptor_set;
    }

    const ShaderPropertyTable& ShaderProperties::GetTable() const
    {
        return *m_table;
    }

    Optional<UInt32> ShaderProperties::GetPropertyId(StringView name) const
    {
        const auto& it = m_table->property_ids.find(name);
        if (it == m_table->property_ids.end())
        {
            return nullopt;
        }
        return it->second;
    }

    bool ShaderProperties::IsBuffer(ShaderPropertyId property_id) const
    {
        const auto descriptor_type = m_table->property_bindings.at(property_id).type;

        return descriptor_type == RHI::DescriptorType::UniformBuffer
            || descriptor_type == RHI::DescriptorType::UniformBufferDynamic
            || descriptor_type == RHI::DescriptorType::StorageBuffer
            || descriptor_type == RHI::DescriptorType::StorageBufferDynamic;
    }

    void ShaderProperties::SetAllBuffers(DeviceBufferHandle value)
    {
        for (const auto& name_property_id_pair : m_table->property_ids)
        {
            const auto property_id = name_property_id_pair.second;
            if (IsBuffer(property_id))
            {
                SetBuffer(property_id, 0, value);
            }
        }
    }

    void ShaderProperties::SetBuffer(ShaderPropertyId property_id, UInt32 array_index, DeviceBufferView value)
    {
        VALO_ASSERT(array_index == 0);

        (void)array_index;
        m_is_dirty                = true;
        m_buffers.at(property_id) = value;
    }

    void ShaderProperties::SetBuffer(StringView property_name, UInt32 index, DeviceBufferView value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetBuffer(*id, index, value);
        }
    }

    bool ShaderProperties::IsTexture(ShaderPropertyId property_id) const
    {
        const auto descriptor_type = m_table->property_bindings.at(property_id).type;

        return descriptor_type == RHI::DescriptorType::CombinedImageSampler
            || descriptor_type == RHI::DescriptorType::SampledImage
            || descriptor_type == RHI::DescriptorType::StorageImage;
    }

    void ShaderProperties::SetAllTextures(TextureHandle value)
    {
        for (const auto& name_property_id_pair : m_table->property_ids)
        {
            const auto& property_id = name_property_id_pair.second;
            if (IsTexture(property_id))
            {
                SetTexture(property_id, 0, value);
            }
        }
    }

    void ShaderProperties::SetTexture(ShaderPropertyId property_id, UInt32 array_index, TextureHandle value)
    {
        VALO_ASSERT(array_index == 0);

        (void)array_index;
        m_is_dirty                 = true;
        m_textures.at(property_id) = value;
    }

    void ShaderProperties::SetTexture(StringView property_name, UInt32 array_index, TextureHandle value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetTexture(*id, array_index, value);
        }
    }

    void ShaderProperties::SetFloat(ShaderPropertyId property_id, float value)
    {
        m_is_dirty = true;
        (void)property_id;
        (void)value;
    }

    void ShaderProperties::SetFloat(StringView property_name, float value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetFloat(*id, value);
        }
    }

    void ShaderProperties::SetVec2(ShaderPropertyId property_id, Vec2 value)
    {
        m_is_dirty = true;
        (void)property_id;
        (void)value;
    }

    void ShaderProperties::SetVec2(StringView property_name, Vec2 value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetVec2(*id, value);
        }
    }

    void ShaderProperties::SetVec3(ShaderPropertyId property_id, Vec3 value)
    {
        m_is_dirty = true;
        (void)property_id;
        (void)value;
    }

    void ShaderProperties::SetVec3(StringView property_name, Vec3 value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetVec3(*id, value);
        }
    }

    void ShaderProperties::SetVec4(ShaderPropertyId property_id, Vec4 value)
    {
        m_is_dirty = true;
        (void)property_id;
        (void)value;
    }

    void ShaderProperties::SetVec4(StringView property_name, Vec4 value)
    {
        const auto id = GetPropertyId(property_name);
        if (id.has_value())
        {
            SetVec4(*id, value);
        }
    }

    void ShaderProperties::UpdateDescriptorSet(
        ResourceManagerHandle             resource_manager,
        Vector<RHI::DescriptorSetUpdate>& update_buffer)
    {
        // Only update if things have changed
        if (!m_is_dirty)
        {
            return;
        }
        m_is_dirty = false;

        // If there is no layout, no need to update
        if (!m_table->layout.IsValid())
        {
            return;
        }

        // Destroy the old descriptor set
        if (m_descriptor_set.IsValid())
        {
            resource_manager->DestroyDescriptorSet(GetDescriptorSet());
        }

        // Create a new descriptor set
        const auto descriptor_set_desc = RHI::DescriptorSetDescriptor{
            .layout = m_table->layout,
        };
        m_descriptor_set = resource_manager->CreateDescriptorSet(descriptor_set_desc);
        if (!m_descriptor_set.IsValid())
        {
            return;
        }

        // Update descriptor set
        GetDescriptorSetUpdates(update_buffer);
        resource_manager->UpdateDescriptorSets(update_buffer);
    }

    void ShaderProperties::GetDescriptorSetUpdates(Vector<RHI::DescriptorSetUpdate>& out_updates) const
    {
        out_updates.clear();

        // Create out_updates for all descriptors
        for (const auto& property_binding : m_table->property_bindings)
        {
            auto& update            = out_updates.emplace_back();
            update.descriptor_count = 1;
            update.descriptor_type  = property_binding.type;
            update.binding          = property_binding.binding;
            update.set              = m_descriptor_set;
            update.array_index      = 0;
            update.descriptor_count = 1;

            switch (update.descriptor_type)
            {
            case RHI::DescriptorType::CombinedImageSampler:
            case RHI::DescriptorType::SampledImage:
            case RHI::DescriptorType::StorageImage:
            {
                const auto& texture = m_textures.at(property_binding.property_id);
                update.image        = texture->GetImage();
                update.sampler      = texture->GetSampler()->GetNative();
                break;
            }
            case RHI::DescriptorType::StorageBuffer:
            case RHI::DescriptorType::StorageBufferDynamic:
            case RHI::DescriptorType::UniformBuffer:
            case RHI::DescriptorType::UniformBufferDynamic:
            {
                const auto& buffer   = m_buffers.at(property_binding.property_id);
                update.buffer        = buffer.GetBuffer()->GetNative();
                update.buffer_offset = buffer.GetOffset();
                update.buffer_range  = buffer.GetRange();
                break;
            }
            default:
            {
                Log::Fatal("Unexpected descriptor type {}", update.descriptor_type);
                break;
            }
            }
        }
    }
}
