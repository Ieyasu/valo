module Valo.Renderer;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    Material::Material() :
        Object("Material")
    {
    }

    const Vector<MaterialPass>& Material::GetPasses() const
    {
        return m_passes;
    }

    RHI::DescriptorSetHandle Material::GetDescriptorSet() const
    {
        return m_properties.GetDescriptorSet();
    }

    Optional<UInt32> Material::GetPropertyId(StringView name) const
    {
        return m_properties.GetPropertyId(name);
    }

    void Material::SetAllBuffers(DeviceBufferHandle value)
    {
        m_properties.SetAllBuffers(value);
    }

    void Material::SetBuffer(UInt32 property_id, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_id, 0, value);
    }

    void Material::SetBuffer(UInt32 property_id, UInt32 array_index, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_id, array_index, value);
    }

    void Material::SetBuffer(StringView property_name, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_name, 0, value);
    }

    void Material::SetBuffer(StringView property_name, UInt32 index, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_name, index, value);
    }

    void Material::SetAllTextures(TextureHandle value)
    {
        m_properties.SetAllTextures(value);
    }

    void Material::SetTexture(MaterialTexture type, TextureHandle value)
    {
        const auto& property_name = MaterialTextureSemantic::Get(type).GetName();
        m_properties.SetTexture(property_name, 0, value);
    }

    void Material::SetTexture(UInt32 property_id, TextureHandle value)
    {
        m_properties.SetTexture(property_id, 0, value);
    }

    void Material::SetTexture(UInt32 property_id, UInt32 index, TextureHandle value)
    {
        m_properties.SetTexture(property_id, index, value);
    }

    void Material::SetTexture(StringView property_name, TextureHandle value)
    {
        m_properties.SetTexture(property_name, 0, value);
    }

    void Material::SetTexture(StringView property_name, UInt32 index, TextureHandle value)
    {
        m_properties.SetTexture(property_name, index, value);
    }

    void Material::SetFloat(UInt32 property_id, float value)
    {
        m_properties.SetFloat(property_id, value);
    }

    void Material::SetFloat(StringView property_name, float value)
    {
        m_properties.SetFloat(property_name, value);
    }

    void Material::SetVec2(UInt32 property_id, Vec2 value)
    {
        m_properties.SetVec2(property_id, value);
    }

    void Material::SetVec2(StringView property_name, Vec2 value)
    {
        m_properties.SetVec2(property_name, value);
    }

    void Material::SetVec3(UInt32 property_id, Vec3 value)
    {
        m_properties.SetVec3(property_id, value);
    }

    void Material::SetVec3(StringView property_name, Vec3 value)
    {
        m_properties.SetVec3(property_name, value);
    }

    void Material::SetVec4(UInt32 property_id, Vec4 value)
    {
        m_properties.SetVec4(property_id, value);
    }

    void Material::SetVec4(StringView property_name, Vec4 value)
    {
        m_properties.SetVec4(property_name, value);
    }
}
