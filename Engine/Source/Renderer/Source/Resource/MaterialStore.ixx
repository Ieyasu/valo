module;

#include "Defines.hpp"

export module Valo.Renderer:MaterialStore;

import Valo.Engine;
import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :Material;
import :ResourceManager;
import :ShaderSemantics;
import :ShaderStore;
import :Store;
import :Texture;
import :TextureStore;

export namespace Valo
{
    class MaterialStore final : public Store
    {
    public:
        static UniquePtr<MaterialStore> Create(
            ResourceManagerHandle resource_manager,
            TextureStoreHandle    texture_store,
            ShaderStoreHandle     shader_store);

        void Update();

        MaterialHandle CreateMaterial(StringView filepath);

        void DestroyMaterial(MaterialHandle material);

        TextureHandle GetDefaultTexture(MaterialTexture type);

    private:
        MaterialStore(
            ResourceManagerHandle resource_manager,
            TextureStoreHandle    texture_store,
            ShaderStoreHandle     shader_store);

        // Dependencies
        TextureStoreHandle m_texture_store{};
        ShaderStoreHandle  m_shader_store{};

        // Resources
        ObjectPool<Material>             m_materials{};
        Vector<RHI::DescriptorSetUpdate> m_update_buffer{};
    };

    using MaterialStoreHandle = ObserverPtr<MaterialStore>;
}
