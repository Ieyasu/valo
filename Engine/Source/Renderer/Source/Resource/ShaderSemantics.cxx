module;

#include <Valo/Macros.hpp>

#include <ranges>

module Valo.Renderer;

import Valo.RHI;
import Valo.Std;

namespace Valo
{
    static const Vector<VertexInputSemantic> g_vertex_input_semantics = {
        {"_in_position", VertexAttribute::Position,    RHI::Format::R32G32B32_Float},
        {  "_in_normal",   VertexAttribute::Normal, RHI::Format::R16G16B16A16_SNorm},
        { "_in_tangent",  VertexAttribute::Tangent, RHI::Format::R16G16B16A16_SNorm},
        {      "_in_uv",       VertexAttribute::UV,       RHI::Format::R32G32_Float},
        {   "_in_color",    VertexAttribute::Color, RHI::Format::R32G32B32A32_Float},
    };

    static const Vector<MaterialTextureSemantic> g_material_texture_semantics = {
        {          "_albedo_texture",          MaterialTexture::Albedo},
        {          "_normal_texture",          MaterialTexture::Normal},
        {        "_emissive_texture",        MaterialTexture::Emissive},
        {             "_orm_texture",             MaterialTexture::ORM},
        {     "_tranmission_texture",    MaterialTexture::Transmission},
        {           "_sheen_texture",           MaterialTexture::Sheen},
        {       "_clearcoat_texture",       MaterialTexture::Clearcoat},
        {"_clearcoat_normal_texture", MaterialTexture::ClearcoatNormal},
        {     "_iridescence_texture",     MaterialTexture::Iridescence},
    };

    VertexInputSemantic::VertexInputSemantic(String name, VertexAttribute attribute, RHI::Format format) :
        m_name(Memory::Move(name)),
        m_stride(RHI::FormatInfo(format).GetStride()),
        m_attribute(attribute),
        m_format(format)
    {
    }

    UInt32 VertexInputSemantic::GetBinding() const
    {
        return static_cast<UInt32>(m_attribute);
    }

    UInt32 VertexInputSemantic::GetStride() const
    {
        return static_cast<UInt32>(m_stride);
    }

    VertexAttribute VertexInputSemantic::GetAttribute() const
    {
        return m_attribute;
    }

    RHI::Format VertexInputSemantic::GetFormat() const
    {
        return m_format;
    }

    Optional<VertexInputSemantic> VertexInputSemantic::Get(StringView name)
    {
        const auto predicate = [&name](const VertexInputSemantic& semantic)
        {
            return semantic.m_name == name;
        };

        const auto it = std::ranges::find_if(g_vertex_input_semantics, predicate);
        if (it == g_vertex_input_semantics.end())
        {
            return nullopt;
        }
        return *it;
    }

    const VertexInputSemantic& VertexInputSemantic::Get(VertexAttribute attribute)
    {
        const auto predicate = [&attribute](const VertexInputSemantic& semantic)
        {
            return semantic.GetAttribute() == attribute;
        };

        const auto it = std::ranges::find_if(g_vertex_input_semantics, predicate);
        VALO_ASSERT(it != g_vertex_input_semantics.end());
        return *it;
    }

    MaterialTextureSemantic::MaterialTextureSemantic(String name, MaterialTexture type) :
        m_name(Memory::Move(name)),
        m_type(type)
    {
    }

    const String& MaterialTextureSemantic::GetName() const
    {
        return m_name;
    }

    MaterialTexture MaterialTextureSemantic::GetType() const
    {
        return m_type;
    }

    Optional<MaterialTextureSemantic> MaterialTextureSemantic::Get(StringView name)
    {
        const auto predicate = [&name](const MaterialTextureSemantic& semantic)
        {
            return semantic.GetName() == name;
        };

        const auto it = std::ranges::find_if(g_material_texture_semantics, predicate);
        if (it == g_material_texture_semantics.end())
        {
            return nullopt;
        }
        return *it;
    }

    const MaterialTextureSemantic& MaterialTextureSemantic::Get(MaterialTexture texture_type)
    {
        const auto predicate = [&texture_type](const MaterialTextureSemantic& semantic)
        {
            return semantic.GetType() == texture_type;
        };

        const auto it = std::ranges::find_if(g_material_texture_semantics, predicate);
        VALO_ASSERT(it != g_material_texture_semantics.end());
        return *it;
    }
}
