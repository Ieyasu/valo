export module Valo.Renderer:Sampler;

import Valo.Engine;
import Valo.RHI;
import Valo.Std;

export namespace Valo
{
    class TextureStore;
    using TextureStoreHandle = ObserverPtr<TextureStore>;

    using SamplerAddressMode = RHI::SamplerAddressMode;
    using SamplerFilter      = RHI::SamplerFilter;
    using SamplerMipmapMode  = RHI::SamplerMipmapMode;
    using SamplerDescriptor  = RHI::SamplerDescriptor;

    class Sampler final : public Object
    {
    public:
        explicit Sampler(TextureStoreHandle store, RHI::SamplerHandle sampler);

        RHI::SamplerHandle GetNative() const;

        void SetName(String name) override;

    private:
        TextureStoreHandle m_store{};
        RHI::SamplerHandle m_sampler{};
    };

    using SamplerHandle = ObjectPoolHandle<Sampler>;
}
