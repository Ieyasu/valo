export module Valo.Renderer:DeviceBuffer;

import Valo.Engine;
import Valo.RHI;
import Valo.Std;

export namespace Valo
{
    class BufferStore;
    using BufferStoreHandle = ObserverPtr<BufferStore>;

    using DeviceSizeType         = RHI::DeviceSizeType;
    using DeviceBufferHostAccess = RHI::BufferHostAccess;
    using DeviceBufferUsageFlags = RHI::BufferUsageFlags;
    using DeviceBufferUsage      = RHI::BufferUsage;

    struct DeviceBufferDescriptor final
    {
        DeviceSizeType         count{0};
        DeviceSizeType         stride{0};
        DeviceBufferUsageFlags usage{0};
        DeviceBufferHostAccess access{DeviceBufferHostAccess::Auto};
    };

    class DeviceBuffer final : public Object
    {
    public:
        DeviceBuffer();

        DeviceBuffer(
            BufferStoreHandle store,
            RHI::BufferHandle buffer,
            DeviceSizeType    count,
            DeviceSizeType    stride,
            bool              is_device_local);

        void SetName(String name) override;

        RHI::BufferHandle GetNative() const;

        DeviceSizeType GetSizeInBytes() const;

        DeviceSizeType GetCount() const;

        DeviceSizeType GetStride() const;

        bool IsDeviceLocal() const;

        void MakeDeviceLocal();

    private:
        BufferStoreHandle m_store{};
        RHI::BufferHandle m_buffer{};
        DeviceSizeType    m_count{};
        DeviceSizeType    m_stride{};
        bool              m_is_device_local{};

        friend class BufferStore;
    };

    using DeviceBufferHandle = PoolHandle<DeviceBuffer>;
}
