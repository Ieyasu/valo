export module Valo.Renderer:ShaderStore;

import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :Material;
import :ResourceManager;
import :ShaderEffect;
import :Store;

export namespace Valo
{
    class ShaderStore final : public Store
    {
    public:
        static UniquePtr<ShaderStore> Create(
            ResourceManagerHandle resource_manager,
            ShaderCompilerHandle  shader_compiler);

        ShaderStore(const ShaderStore&)            = delete;
        ShaderStore(ShaderStore&&)                 = delete;
        ShaderStore& operator=(const ShaderStore&) = delete;
        ShaderStore& operator=(ShaderStore&&)      = delete;

        ~ShaderStore() override = default;

        ShaderEffectHandle CreateShaderEffect(String name, Span<ShaderPass> passes);

        ShaderEffectHandle GetShaderEffect(StringView name);

        ShaderStageHandle GetShaderStage(StringView filepath);

    private:
        ShaderStore(ResourceManagerHandle resource_manager, ShaderCompilerHandle shader_compiler);

        void InitializeShaderEffect(ShaderEffect& effect);

        static RHI::DescriptorType ToDescriptorType(DescriptorTypeReflection type);
        static RHI::ShaderStage    ToShaderStageFlags(ShaderStageReflection stage);

        // Resources
        UnorderedMap<String, ShaderEffect> m_shader_effects{};
        UnorderedMap<String, ShaderStage>  m_shader_stages{};

        // Utility
        ShaderCompilerHandle m_shader_compiler{};
    };

    using ShaderStoreHandle = ObserverPtr<ShaderStore>;
}
