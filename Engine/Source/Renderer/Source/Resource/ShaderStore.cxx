module;

#include "Defines.hpp"

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

#include <string>
#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Math;
import Valo.Profiler;
import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

namespace Valo
{
    ShaderStore::ShaderStore(ResourceManagerHandle resource_manager, ShaderCompilerHandle shader_compiler) :
        Store(resource_manager),
        m_shader_compiler(shader_compiler)
    {
    }

    UniquePtr<ShaderStore> ShaderStore::Create(
        ResourceManagerHandle resource_manager,
        ShaderCompilerHandle  shader_compiler)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        return UniquePtr<ShaderStore>(new ShaderStore(resource_manager, shader_compiler));
    }

    ShaderEffectHandle ShaderStore::CreateShaderEffect(String name, Span<ShaderPass> passes)
    {
        VALO_ASSERT(!m_shader_effects.contains(name));

        auto& shader_effect = m_shader_effects.emplace(name, ShaderEffect{}).first->second;

        shader_effect = ShaderEffect{
            .name   = Memory::Move(name),
            .passes = Vector<ShaderPass>(passes.begin(), passes.end()),
        };

        InitializeShaderEffect(shader_effect);

        return &shader_effect;
    }

    ShaderEffectHandle ShaderStore::GetShaderEffect(StringView name)
    {
        const auto it = m_shader_effects.find(String(name));
        if (it == m_shader_effects.end())
        {
            Log::Error("Failed to get shader effect: no shader effect with name '{}'", name);
            return {};
        }
        return &it->second;
    }

    ShaderStageHandle ShaderStore::GetShaderStage(StringView filepath)
    {
        const auto filepath_str = String(filepath);

        const auto it = m_shader_stages.find(filepath_str);
        if (it != m_shader_stages.end())
        {
            return &it->second;
        }

        const auto shader_filepath = "Engine/Shaders/" + filepath_str;
        const auto shader_options  = ShaderCompilerOptions{
             .Filepath = shader_filepath,
             .Language = ShaderLanguage::GLSL,
             .IR       = ShaderIR::SPIRV,
        };

        const auto binary = m_shader_compiler->Compile(shader_options);
        if (!binary.has_value())
        {
            Log::Error("Failed to get shader stage: compilation failed");
            return nullptr;
        }

        const auto reflection = m_shader_compiler->Reflect(shader_filepath, binary.value());
        if (!reflection.has_value())
        {
            Log::Error("Failed to get shader stage: reflection failed");
            return nullptr;
        }

        m_shader_stages[filepath_str] = ShaderStage{
            .binary     = binary.value(),
            .reflection = reflection.value(),
        };
        return &m_shader_stages[filepath_str];
    }

    void ShaderStore::InitializeShaderEffect(ShaderEffect& effect)
    {
        // Create bindings for each descriptor set layout.
        // Use map to make sure descriptor sets are in ascending order to match the reflection data.
        auto bindings_per_set = Map<size_t, Map<String, RHI::DescriptorSetLayoutBinding>>();
        for (const auto& pass : effect.passes)
        {
            for (const auto& stage : pass.stages)
            {
                for (const auto& binding_reflection : stage->reflection.GetDescriptorSetBindings())
                {
                    auto& bindings = bindings_per_set[binding_reflection.set];

                    // Find an existing binding if available
                    RHI::DescriptorSetLayoutBinding* binding = nullptr;
                    const auto descriptor_count = Math::Max(binding_reflection.array_size, static_cast<UInt32>(1));
                    for (auto& pair : bindings)
                    {
                        if (pair.second.binding != binding_reflection.binding)
                        {
                            continue;
                        }

                        const auto& binding_name = pair.first;
                        binding                  = &pair.second;

                        // Make sure the binding matches
                        if (binding_name != binding_reflection.name)
                        {
                            Log::Error(
                                "Failed to create material template: names for binding {} do not match between "
                                "shader stages ({} != {})",
                                binding_reflection.binding,
                                binding_name,
                                binding_reflection.name);
                            return;
                        }

                        if (binding->type != ToDescriptorType(binding_reflection.type))
                        {
                            Log::Error(
                                "Failed to create material template: types for binding {} do not match between "
                                "shader stages ({} != {})",
                                binding_reflection.binding,
                                binding->type,
                                ToDescriptorType(binding_reflection.type));
                            return;
                        }

                        if (binding->descriptor_count != descriptor_count)
                        {
                            Log::Error(
                                "Failed to create material template descriptor counts for binding {} do not match "
                                "between "
                                "shader stages ({} != {})",
                                binding_reflection.binding,
                                binding->descriptor_count,
                                descriptor_count);
                            return;
                        }
                    }

                    // Create a new binding if old was not found
                    if (binding == nullptr)
                    {
                        binding = &bindings[binding_reflection.name];
                    }

                    // Set the binding information
                    binding->binding          = binding_reflection.binding;
                    binding->descriptor_count = descriptor_count;
                    binding->stage |= ToShaderStageFlags(stage->reflection.GetStage());
                    binding->type = ToDescriptorType(binding_reflection.type);
                }
            }
        }

        // Create properties to material template
        auto material_property_bindings = bindings_per_set[VALO_MATERIAL_DESCRIPTOR_SET_INDEX];
        for (const auto& name_binding_pair : material_property_bindings)
        {
            const auto& name        = name_binding_pair.first;
            const auto& binding     = name_binding_pair.second;
            const auto  property_id = static_cast<UInt32>(effect.property_table.property_bindings.size());

            const auto property_binding = ShaderPropertyBinding{
                .type        = binding.type,
                .binding     = binding.binding,
                .property_id = property_id,
            };

            effect.property_table.property_ids[name] = property_id;
            effect.property_table.property_bindings.push_back(property_binding);
        }

        // Create the descriptor set layouts based on the bindings
        auto bindings = SmallVector<RHI::DescriptorSetLayoutBinding>();
        for (const auto& set_bindings_pair : bindings_per_set)
        {
            // Add all bindings to array
            bindings.clear();
            for (const auto& name_binding_pair : set_bindings_pair.second)
            {
                bindings.push_back(name_binding_pair.second);
            }

            // Create descriptor set layout
            const auto set         = set_bindings_pair.first;
            const auto layout_desc = RHI::DescriptorSetLayoutDescriptor{
                .bindings = bindings,
            };

            auto descriptor_set_layout = GetResourceManager()->CreateDescriptorSetLayout(layout_desc);
            if (!descriptor_set_layout.IsValid())
            {
                Log::Error("Failed to create descriptor set layouts");
                return;
            }
            GetResourceManager()->SetObjectName(descriptor_set_layout, effect.name);

            // Save the descriptor set layout to the material template
            effect.descriptor_set_layouts[set] = descriptor_set_layout;

            if (set == VALO_MATERIAL_DESCRIPTOR_SET_INDEX)
            {
                effect.property_table.layout = descriptor_set_layout;
            }
        }

        // Find index of last non-empty descriptor set
        const auto& layouts    = effect.descriptor_set_layouts;
        auto        last_index = static_cast<Int32>(layouts.size() - 1);
        for (; last_index >= 0; --last_index)
        {
            if (layouts[last_index] != GetResourceManager()->GetEmptyDescriptorSetLayout())
            {
                break;
            }
        }

        // Fill missing descriptor set layouts with empty layouts
        auto active_layouts = SmallVector<RHI::DescriptorSetLayoutHandle>(last_index);
        for (Int32 i = 0; i < last_index; ++i)
        {
            active_layouts[i] = layouts[i].IsValid() ? layouts[i] : GetResourceManager()->GetEmptyDescriptorSetLayout();
        }

        // Create the pipeline layout
        auto pipeline_layout_desc = RHI::PipelineLayoutDescriptor{
            .layouts              = active_layouts,
            .push_constant_ranges = {},
        };

        const auto pipeline_layout = GetResourceManager()->CreatePipelineLayout(pipeline_layout_desc);
        if (!pipeline_layout.IsValid())
        {
            return;
        }
        GetResourceManager()->SetObjectName(pipeline_layout, effect.name);

        for (auto& pass : effect.passes)
        {
            pass.pipeline_layout = pipeline_layout;
        }
    }

    RHI::DescriptorType ShaderStore::ToDescriptorType(DescriptorTypeReflection type)
    {
        switch (type)
        {
        case DescriptorTypeReflection::Sampler:
            return RHI::DescriptorType::Sampler;
        case DescriptorTypeReflection::CombinedImageSampler:
            return RHI::DescriptorType::CombinedImageSampler;
        case DescriptorTypeReflection::SampledImage:
            return RHI::DescriptorType::SampledImage;
        case DescriptorTypeReflection::StorageImage:
            return RHI::DescriptorType::StorageImage;
        case DescriptorTypeReflection::UniformBuffer:
            return RHI::DescriptorType::UniformBufferDynamic;
        case DescriptorTypeReflection::StorageBuffer:
            return RHI::DescriptorType::StorageBuffer;
        default:
            Log::Fatal("Fail");
            return {};
        }
    }

    RHI::ShaderStage ShaderStore::ToShaderStageFlags(ShaderStageReflection stage)
    {
        switch (stage)
        {
        case ShaderStageReflection::Compute:
            return RHI::ShaderStage::Compute;
        case ShaderStageReflection::Fragment:
            return RHI::ShaderStage::Fragment;
        case ShaderStageReflection::Geometry:
            return RHI::ShaderStage::Geometry;
        case ShaderStageReflection::None:
            return RHI::ShaderStage::None;
        case ShaderStageReflection::TessellationControl:
            return RHI::ShaderStage::TessellationControl;
        case ShaderStageReflection::TessellationEvaluation:
            return RHI::ShaderStage::TessellationEvaluation;
        case ShaderStageReflection::Vertex:
            return RHI::ShaderStage::Vertex;
        default:
            Log::Fatal("Fail");
            return {};
        }
    }
}
