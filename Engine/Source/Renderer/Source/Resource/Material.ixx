export module Valo.Renderer:Material;

import Valo.Engine;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :DeviceBuffer;
import :DeviceBufferView;
import :MaterialPass;
import :ShaderEffect;
import :ShaderProperties;
import :ShaderSemantics;
import :Texture;

export namespace Valo
{
    class Material final : public Object
    {
    public:
        Material();

        const Vector<MaterialPass>& GetPasses() const;

        RHI::DescriptorSetHandle GetDescriptorSet() const;

        Optional<UInt32> GetPropertyId(StringView property_name) const;

        void SetAllBuffers(DeviceBufferHandle value);
        void SetBuffer(UInt32 property_id, DeviceBufferView value);
        void SetBuffer(StringView property_name, DeviceBufferView value);
        void SetBuffer(UInt32 property_id, UInt32 index, DeviceBufferView value);
        void SetBuffer(StringView property_name, UInt32 index, DeviceBufferView value);

        void SetAllTextures(TextureHandle value);
        void SetTexture(MaterialTexture type, TextureHandle value);
        void SetTexture(UInt32 property_id, TextureHandle value);
        void SetTexture(StringView property_name, TextureHandle value);
        void SetTexture(UInt32 property_id, UInt32 index, TextureHandle value);
        void SetTexture(StringView property_name, UInt32 index, TextureHandle value);

        void SetFloat(UInt32 property_id, float value);
        void SetFloat(StringView property_name, float value);

        void SetVec2(UInt32 property_id, Vec2 value);
        void SetVec2(StringView property_name, Vec2 value);

        void SetVec3(UInt32 property_id, Vec3 value);
        void SetVec3(StringView property_name, Vec3 value);

        void SetVec4(UInt32 property_id, Vec4 value);
        void SetVec4(StringView property_name, Vec4 value);

        template <typename OutputStream>
        void Serialize(OutputStream stream) const;

        template <typename InputStream>
        void Deserialize(InputStream stream);

    private:
        // Serialized data
        Vector<MaterialPass> m_passes{};

        // Populated data
        ShaderEffectHandle m_effect{};
        ShaderProperties   m_properties{};

        friend class MaterialStore;
    };

    using MaterialHandle = ObjectPoolHandle<Material>;

    template <typename OutputStream>
    void Material::Serialize(OutputStream stream) const
    {
        Object::Serialize(stream);
        stream.SerializeProperty("passess", m_passes);
    }

    template <typename InputStream>
    void Material::Deserialize(InputStream stream)
    {
        Object::Deserialize(stream);
        stream.DeserializeProperty("passes", m_passes);
    }
}
