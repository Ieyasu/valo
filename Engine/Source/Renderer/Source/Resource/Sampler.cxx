module Valo.Renderer;

import Valo.RHI;
import Valo.Std;

import :TextureStore;

namespace Valo
{
    Sampler::Sampler(TextureStoreHandle store, RHI::SamplerHandle sampler) :
        Object("Sampler"),
        m_store(store),
        m_sampler(Memory::Move(sampler))
    {
    }

    RHI::SamplerHandle Sampler::GetNative() const
    {
        return m_sampler;
    }

    void Sampler::SetName(String name)
    {
        m_store->GetResourceManager()->SetObjectName(m_sampler, name);
        Object::SetName(Memory::Move(name));
    }
}
