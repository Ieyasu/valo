module Valo.Renderer;

namespace Valo
{
    Store::Store(ResourceManagerHandle resource_manager) :
        m_resource_manager(resource_manager)
    {
    }

    ResourceManagerHandle Store::GetResourceManager() const
    {
        return m_resource_manager;
    }
}
