module Valo.Renderer;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    ComputeShader::ComputeShader() :
        Object("Compute Shader")
    {
    }

    RHI::ComputePipelineHandle ComputeShader::GetPipeline() const
    {
        return m_pipeline;
    }

    RHI::DescriptorSetHandle ComputeShader::GetDescriptorSet() const
    {
        return m_properties.GetDescriptorSet();
    }

    Optional<UInt32> ComputeShader::GetPropertyId(StringView name) const
    {
        return m_properties.GetPropertyId(name);
    }

    ShaderPassHandle ComputeShader::GetShaderPass() const
    {
        return &m_effect->passes.front();
    }

    ShaderStageHandle ComputeShader::GetShaderStage() const
    {
        return GetShaderPass()->stages.front();
    }

    void ComputeShader::SetAllBuffers(DeviceBufferHandle value)
    {
        m_properties.SetAllBuffers(value);
    }

    void ComputeShader::SetBuffer(UInt32 property_id, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_id, 0, value);
    }

    void ComputeShader::SetBuffer(UInt32 property_id, UInt32 array_index, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_id, array_index, value);
    }

    void ComputeShader::SetBuffer(StringView property_name, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_name, 0, value);
    }

    void ComputeShader::SetBuffer(StringView property_name, UInt32 index, DeviceBufferView value)
    {
        m_properties.SetBuffer(property_name, index, value);
    }

    void ComputeShader::SetAllTextures(TextureHandle value)
    {
        m_properties.SetAllTextures(value);
    }

    void ComputeShader::SetTexture(UInt32 property_id, TextureHandle value)
    {
        m_properties.SetTexture(property_id, 0, value);
    }

    void ComputeShader::SetTexture(UInt32 property_id, UInt32 index, TextureHandle value)
    {
        m_properties.SetTexture(property_id, index, value);
    }

    void ComputeShader::SetTexture(StringView property_name, TextureHandle value)
    {
        m_properties.SetTexture(property_name, 0, value);
    }

    void ComputeShader::SetTexture(StringView property_name, UInt32 index, TextureHandle value)
    {
        m_properties.SetTexture(property_name, index, value);
    }

    void ComputeShader::SetFloat(UInt32 property_id, float value)
    {
        m_properties.SetFloat(property_id, value);
    }

    void ComputeShader::SetFloat(StringView property_name, float value)
    {
        m_properties.SetFloat(property_name, value);
    }

    void ComputeShader::SetVec2(UInt32 property_id, Vec2 value)
    {
        m_properties.SetVec2(property_id, value);
    }

    void ComputeShader::SetVec2(StringView property_name, Vec2 value)
    {
        m_properties.SetVec2(property_name, value);
    }

    void ComputeShader::SetVec3(UInt32 property_id, Vec3 value)
    {
        m_properties.SetVec3(property_id, value);
    }

    void ComputeShader::SetVec3(StringView property_name, Vec3 value)
    {
        m_properties.SetVec3(property_name, value);
    }

    void ComputeShader::SetVec4(UInt32 property_id, Vec4 value)
    {
        m_properties.SetVec4(property_id, value);
    }

    void ComputeShader::SetVec4(StringView property_name, Vec4 value)
    {
        m_properties.SetVec4(property_name, value);
    }
}
