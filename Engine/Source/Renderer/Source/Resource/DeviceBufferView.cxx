module;

#include <Valo/Macros.hpp>

module Valo.Renderer;

namespace Valo
{
    DeviceBufferView::DeviceBufferView(DeviceBufferHandle buffer) :
        DeviceBufferView(buffer, 0, buffer->GetSizeInBytes(), false)
    {
    }

    DeviceBufferView::DeviceBufferView(
        DeviceBufferHandle buffer,
        DeviceSizeType     offset,
        DeviceSizeType     range,
        bool               is_dynamic) :
        m_buffer(buffer),
        m_offset(offset),
        m_range(range),
        m_is_dynamic(is_dynamic)
    {
        VALO_ASSERT(offset + range <= buffer->GetSizeInBytes());
    }

    DeviceBufferHandle DeviceBufferView::GetBuffer() const
    {
        return m_buffer;
    }

    DeviceSizeType DeviceBufferView::GetOffset() const
    {
        return m_offset;
    }

    DeviceSizeType DeviceBufferView::GetRange() const
    {
        return m_range;
    }

    bool DeviceBufferView::IsDynamic() const
    {
        return m_is_dynamic;
    }
}
