module;

#include <Valo/Profile.hpp>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    TextureStore::TextureStore(ResourceManagerHandle resource_manager) :
        Store(resource_manager)
    {
    }

    TextureStore::~TextureStore()
    {
        DestroyDefaultResources();
    }

    UniquePtr<TextureStore> TextureStore::Create(ResourceManagerHandle resource_manager)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<TextureStore>(new TextureStore(resource_manager));
        self->CreateDefaultResources();
        return self;
    }

    TextureHandle TextureStore::CreateTexture(const TextureDescriptor& descriptor)
    {
        auto image   = descriptor.image;
        auto sampler = descriptor.sampler;

        if (!image.IsValid())
        {
            const auto image_descriptor = RHI::ImageDescriptor{
                .usage             = descriptor.usage,
                .format            = descriptor.format,
                .dimensions        = descriptor.dimensions,
                .width             = descriptor.width,
                .height            = descriptor.height,
                .depth             = descriptor.depth,
                .mip_level_count   = descriptor.mip_level_count,
                .array_layer_count = descriptor.array_layer_count};

            image = GetResourceManager()->CreateImage(image_descriptor);
        }

        if (!image.IsValid())
        {
            Log::Error("Failed to create texture: failed to create image");
            return {};
        }

        if (!sampler.IsValid())
        {
            sampler = GetDefaultSamplerLinearRepeat();
        }

        return m_textures.Emplace(this, image, sampler, descriptor);
    }

    void TextureStore::DestroyTexture(TextureHandle texture)
    {
        GetResourceManager()->DestroyImage(texture->GetImage());
        m_textures.Erase(texture);
    }

    SamplerHandle TextureStore::CreateSampler(const SamplerDescriptor& descriptor)
    {
        const auto sampler = GetResourceManager()->CreateSampler(descriptor);
        if (!sampler.IsValid())
        {
            Log::Error("Failed to create sampler");
            return {};
        }

        return m_samplers.Emplace(this, sampler);
    }

    void TextureStore::DestroySampler(SamplerHandle sampler)
    {
        GetResourceManager()->DestroySampler(sampler->GetNative());
        m_samplers.Erase(sampler);
    }

    void TextureStore::UploadTexture(Span<const Byte> data, TextureHandle texture, UInt32 mip_level, UInt32 array_layer)
    {
        UploadTexture(
            data,
            texture,
            texture->GetWidth(),
            texture->GetHeight(),
            texture->GetDepth(),
            0,
            0,
            0,
            mip_level,
            array_layer);
    }

    void TextureStore::UploadTexture(
        Span<const Byte> data,
        TextureHandle    texture,
        UInt32           width,
        UInt32           height,
        UInt32           depth,
        UInt32           offset_x,
        UInt32           offset_y,
        UInt32           offset_z,
        UInt32           mip_level,
        UInt32           array_layer)
    {
        GetResourceManager()->UploadImage(
            data.data(),
            data.size(),
            texture->GetImage(),
            width,
            height,
            depth,
            offset_x,
            offset_y,
            offset_z,
            mip_level,
            array_layer);
    }

    TextureHandle TextureStore::GetDefaultTextureBlack() const
    {
        return m_black_texture;
    }

    TextureHandle TextureStore::GetDefaultTextureGray() const
    {
        return m_gray_texture;
    }

    TextureHandle TextureStore::GetDefaultTextureWhite() const
    {
        return m_white_texture;
    }

    TextureHandle TextureStore::GetDefaultTextureAlbedo() const
    {
        return m_white_texture;
    }

    TextureHandle TextureStore::GetDefaultTextureEmissive() const
    {
        return m_black_texture;
    }

    TextureHandle TextureStore::GetDefaultTextureNormal() const
    {
        return m_normal_texture;
    }

    TextureHandle TextureStore::GetDefaulTextureOrm() const
    {
        return m_orm_texture;
    }

    SamplerHandle TextureStore::GetDefaultSamplerNearestClamp() const
    {
        return m_nearest_clamp_sampler;
    }

    SamplerHandle TextureStore::GetDefaultSamplerNearestRepeat() const
    {
        return m_nearest_repeat_sampler;
    }

    SamplerHandle TextureStore::GetDefaultSamplerLinearClamp() const
    {
        return m_linear_clamp_sampler;
    }

    SamplerHandle TextureStore::GetDefaultSamplerLinearRepeat() const
    {
        return m_linear_repeat_sampler;
    }

    void TextureStore::CreateDefaultResources()
    {
        // Create samplers
        auto sampler_nearest_desc = SamplerDescriptor{
            .min_filter  = SamplerFilter::Nearest,
            .mag_filter  = SamplerFilter::Nearest,
            .mipmap_mode = SamplerMipmapMode::Linear,
        };

        sampler_nearest_desc.address_mode_u = SamplerAddressMode::ClampToBorder;
        sampler_nearest_desc.address_mode_v = SamplerAddressMode::ClampToBorder;
        sampler_nearest_desc.address_mode_w = SamplerAddressMode::ClampToBorder,
        m_nearest_clamp_sampler             = CreateSampler(sampler_nearest_desc);
        m_nearest_clamp_sampler->SetName("Nearest Clamp Sampler");

        sampler_nearest_desc.address_mode_u = SamplerAddressMode::Repeat;
        sampler_nearest_desc.address_mode_v = SamplerAddressMode::Repeat;
        sampler_nearest_desc.address_mode_w = SamplerAddressMode::Repeat;
        m_nearest_repeat_sampler            = CreateSampler(sampler_nearest_desc);
        m_nearest_repeat_sampler->SetName("Nearest Repeat Sampler");

        auto sampler_linear_desc = SamplerDescriptor{
            .min_filter  = SamplerFilter::Linear,
            .mag_filter  = SamplerFilter::Linear,
            .mipmap_mode = SamplerMipmapMode::Linear,
        };

        sampler_linear_desc.address_mode_u = SamplerAddressMode::ClampToBorder;
        sampler_linear_desc.address_mode_v = SamplerAddressMode::ClampToBorder;
        sampler_linear_desc.address_mode_w = SamplerAddressMode::ClampToBorder,
        m_linear_clamp_sampler             = CreateSampler(sampler_linear_desc);
        m_linear_clamp_sampler->SetName("Linear Clamp Sampler");

        sampler_linear_desc.address_mode_u = SamplerAddressMode::Repeat;
        sampler_linear_desc.address_mode_v = SamplerAddressMode::Repeat;
        sampler_linear_desc.address_mode_w = SamplerAddressMode::Repeat;
        m_linear_repeat_sampler            = CreateSampler(sampler_linear_desc);
        m_linear_repeat_sampler->SetName("Linear Repeat Sampler");

        // Create textures
        auto texture_descriptor              = TextureDescriptor{};
        texture_descriptor.usage             = TextureUsage::TransferDst | TextureUsage::Sampled;
        texture_descriptor.array_layer_count = 1;
        texture_descriptor.mip_level_count   = 1;
        texture_descriptor.width             = 1;
        texture_descriptor.height            = 1;
        texture_descriptor.depth             = 1;

        texture_descriptor.format = TextureFormat::R8G8B8A8_UNorm;
        m_black_texture           = CreateTexture(texture_descriptor);
        m_black_texture->SetName("Default Black Texture");

        texture_descriptor.format = TextureFormat::R8G8B8A8_UNorm;
        m_gray_texture            = CreateTexture(texture_descriptor);
        m_gray_texture->SetName("Default Gray Texture");

        texture_descriptor.format = TextureFormat::R8G8B8A8_UNorm;
        m_white_texture           = CreateTexture(texture_descriptor);
        m_white_texture->SetName("Default White Texture");

        texture_descriptor.format = TextureFormat::R16G16B16A16_UNorm;
        m_normal_texture          = CreateTexture(texture_descriptor);
        m_normal_texture->SetName("Default Normal Texture");

        texture_descriptor.format = TextureFormat::R8G8B8A8_UNorm;
        m_orm_texture             = CreateTexture(texture_descriptor);
        m_orm_texture->SetName("Default ORM Texture");

        // Upload texture data
        auto bytes = Array<Byte, 4>{0, 0, 0, 0};
        UploadTexture(bytes, m_black_texture);

        bytes = {128, 128, 128, 255};
        UploadTexture(bytes, m_gray_texture);

        bytes = {255, 255, 255, 255};
        UploadTexture(bytes, m_white_texture);

        bytes = {255, 255, 0, 255};
        UploadTexture(bytes, m_orm_texture);

        auto bits16  = Array<UInt16, 4>{32767, 32767, 65535, 65535};
        auto bytes16 = Array<Byte, 8>{};
        Memory::Memcpy(bytes16.data(), bits16.data(), bytes16.size());
        UploadTexture(bytes16, m_normal_texture);
    }

    void TextureStore::DestroyDefaultResources()
    {
        DestroyTexture(m_black_texture);
        DestroyTexture(m_gray_texture);
        DestroyTexture(m_white_texture);
        DestroyTexture(m_normal_texture);
        DestroyTexture(m_orm_texture);
        DestroySampler(m_nearest_clamp_sampler);
        DestroySampler(m_nearest_repeat_sampler);
        DestroySampler(m_linear_clamp_sampler);
        DestroySampler(m_linear_repeat_sampler);
    }
}
