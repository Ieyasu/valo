export module Valo.Renderer:BufferStore;

import Valo.Std;

import :DeviceBuffer;
import :DeviceBufferView;
import :ResourceManager;
import :Store;

export namespace Valo
{
    class BufferStore final : public Store
    {
    public:
        static UniquePtr<BufferStore> Create(ResourceManagerHandle resource_manager);

        BufferStore(const BufferStore&)            = delete;
        BufferStore(BufferStore&&)                 = delete;
        BufferStore& operator=(const BufferStore&) = delete;
        BufferStore& operator=(BufferStore&&)      = delete;
        ~BufferStore() override;

        DeviceBufferHandle CreateBuffer(const DeviceBufferDescriptor& descriptor);

        void DestroyBuffer(DeviceBufferHandle buffer);

        DeviceBufferViewHandle CreateDynamicBufferView(DeviceSizeType count, DeviceSizeType stride);

        DeviceBufferViewHandle CreateIndexBufferView(DeviceSizeType count, DeviceSizeType stride);

        DeviceBufferViewHandle CreateVertexBufferView(DeviceSizeType count, DeviceSizeType stride);

        void DestroyBufferView(DeviceBufferViewHandle buffer_view);

        void UploadBuffer(const void* data, DeviceBufferViewHandle buffer_view, DeviceSizeType size);

        UInt32 CalculateDynamicOffset(DeviceBufferViewHandle buffer_view) const;

    private:
        explicit BufferStore(ResourceManagerHandle resource_manager);

        // Resources
        Pool<DeviceBuffer>     m_buffers{};
        Pool<DeviceBufferView> m_buffer_views{};

        // Dynamic uniforms, index buffers, and vertex buffers are all stored
        // in big arrays to allow bindless use cases in the future.
        DeviceBufferHandle m_dynamic_buffer{};
        DeviceSizeType     m_dynamic_buffer_offset{};

        DeviceBufferHandle m_index_buffer{};
        DeviceSizeType     m_index_buffer_offset{};

        DeviceBufferHandle m_vertex_buffer{};
        DeviceSizeType     m_vertex_buffer_offset{};
    };

    using BufferStoreHandle = ObserverPtr<BufferStore>;
}
