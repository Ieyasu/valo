export module Valo.Renderer:Store;

import :ResourceManager;

namespace Valo
{
    class Store
    {
    public:
        explicit Store(ResourceManagerHandle resource_manager);

        Store(const Store&)            = delete;
        Store(Store&&)                 = delete;
        Store& operator=(const Store&) = delete;
        Store& operator=(Store&&)      = delete;
        virtual ~Store()               = default;

        ResourceManagerHandle GetResourceManager() const;

    private:
        // Dependencies
        ResourceManagerHandle m_resource_manager{};
    };
}
