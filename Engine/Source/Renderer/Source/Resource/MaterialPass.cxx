module;

#include <vector>

module Valo.Renderer;

import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

namespace Valo
{
    const String& MaterialPass::GetName() const
    {
        return m_name;
    }

    const RenderTag& MaterialPass::GetTag() const
    {
        return m_tag;
    }

    RHI::CullMode MaterialPass::GetCullMode() const
    {
        return m_cull_mode;
    }

    RHI::PolygonMode MaterialPass::GetPolygonMode() const
    {
        return m_polygon_mode;
    }

    bool MaterialPass::IsDepthTestEnabled() const
    {
        return m_depth_test_enabled;
    }

    bool MaterialPass::IsDepthWriteEnabled() const
    {
        return m_depth_write_enabled;
    }

    RHI::CompareOp MaterialPass::GetDepthCompare() const
    {
        return m_depth_compare;
    }

    const String& MaterialPass::GetVertexShaderPath() const
    {
        return m_vertex_shader_path;
    }

    const String& MaterialPass::GetFragmentShaderPath() const
    {
        return m_fragment_shader_path;
    }

    ShaderPassHandle MaterialPass::GetShaderPass() const
    {
        return m_shader_pass;
    }

    ShaderStageHandle MaterialPass::GetVertexShader() const
    {
        for (const auto& stage : m_shader_pass->stages)
        {
            if (stage->reflection.GetStage() == ShaderStageReflection::Vertex)
            {
                return stage;
            }
        }
        return nullptr;
    }

    ShaderStageHandle MaterialPass::GetFragmentShader() const
    {
        for (const auto& stage : m_shader_pass->stages)
        {
            if (stage->reflection.GetStage() == ShaderStageReflection::Fragment)
            {
                return stage;
            }
        }
        return nullptr;
    }
}
