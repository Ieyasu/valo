export module Valo.Renderer:MaterialPass;

import Valo.RHI;
import Valo.Std;

import :RenderTag;
import :ShaderEffect;

export namespace Valo
{
    class MaterialPass final
    {
    public:
        const String& GetName() const;

        const RenderTag& GetTag() const;

        RHI::CullMode GetCullMode() const;

        RHI::PolygonMode GetPolygonMode() const;

        bool IsDepthTestEnabled() const;

        bool IsDepthWriteEnabled() const;

        RHI::CompareOp GetDepthCompare() const;

        const String& GetVertexShaderPath() const;

        const String& GetFragmentShaderPath() const;

        ShaderPassHandle GetShaderPass() const;

        ShaderStageHandle GetVertexShader() const;

        ShaderStageHandle GetFragmentShader() const;

        template <typename OutputStream>
        void Serialize(OutputStream stream) const;

        template <typename InputStream>
        void Deserialize(InputStream stream);

    private:
        // Serialized data
        String           m_name{"Material Pass"};
        RenderTag        m_tag{RenderTag::None};
        RHI::CullMode    m_cull_mode{RHI::CullMode::Back};
        RHI::PolygonMode m_polygon_mode{RHI::PolygonMode::Fill};
        RHI::CompareOp   m_depth_compare{RHI::CompareOp::GreaterOrEqual};
        bool             m_depth_test_enabled{true};
        bool             m_depth_write_enabled{true};
        String           m_vertex_shader_path{};
        String           m_fragment_shader_path{};

        // Shader pass
        ShaderPassHandle m_shader_pass{};

        friend class MaterialStore;
    };

    template <typename OutputStream>
    void MaterialPass::Serialize(OutputStream stream) const
    {
        stream.SerializeProperty("name", m_name);
        stream.SerializeProperty("tag", m_tag);
        stream.SerializeProperty("cull_mode", m_cull_mode);
        stream.SerializeProperty("polygon_mode", m_polygon_mode);
        stream.SerializeProperty("depth_compare", m_depth_compare);
        stream.SerializeProperty("depth_test", m_depth_test_enabled);
        stream.SerializeProperty("depth_write", m_depth_write_enabled);
        stream.SerializeProperty("vertex_path", m_vertex_shader_path);
        stream.SerializeProperty("fragment_path", m_fragment_shader_path);
    }

    template <typename InputStream>
    void MaterialPass::Deserialize(InputStream stream)
    {
        stream.DeserializeProperty("name", m_name);
        stream.DeserializeProperty("tag", m_tag);
        stream.DeserializeProperty("polygon_mode", m_polygon_mode);
        stream.DeserializeProperty("depth_compare", m_depth_compare);
        stream.DeserializeProperty("depth_test", m_depth_test_enabled);
        stream.DeserializeProperty("depth_write", m_depth_write_enabled);
        stream.DeserializeProperty("vertex_path", m_vertex_shader_path);
        stream.DeserializeProperty("fragment_path", m_fragment_shader_path);
    }
}
