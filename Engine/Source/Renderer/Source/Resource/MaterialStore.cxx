module;

#include <Valo/Profile.hpp>

#include <string>
#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.Serialization;
import Valo.Std;

namespace Valo
{
    MaterialStore::MaterialStore(
        ResourceManagerHandle resource_manager,
        TextureStoreHandle    texture_store,
        ShaderStoreHandle     shader_store) :
        Store(resource_manager),
        m_texture_store(texture_store),
        m_shader_store(shader_store)
    {
    }

    UniquePtr<MaterialStore> MaterialStore::Create(
        ResourceManagerHandle resource_manager,
        TextureStoreHandle    texture_store,
        ShaderStoreHandle     shader_store)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<MaterialStore>(new MaterialStore(resource_manager, texture_store, shader_store));
        return self;
    }

    void MaterialStore::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        for (auto& material : m_materials)
        {
            auto& properties = material.m_properties;
            if (properties.IsDirty())
            {
                properties.UpdateDescriptorSet(GetResourceManager(), m_update_buffer);
                GetResourceManager()->SetObjectName(properties.GetDescriptorSet(), material.GetName());
            }
        }
    }

    MaterialHandle MaterialStore::CreateMaterial(StringView filepath)
    {
        // Deserialize material
        auto archive = Serialization::YamlArchive{};
        archive.Load(String("Engine/Assets/Materials/") + String(filepath) + String(".yml"));

        Vector<ShaderPass> shader_passes{};

        auto material = m_materials.Emplace();
        archive.Deserialize(*material);

        const auto& test = *material;
        (void)test;

        // TODO: Inherited materials
        for (const auto& pass : material->GetPasses())
        {
            auto& shader_pass = shader_passes.emplace_back();
            if (!pass.GetFragmentShaderPath().empty())
            {
                const auto shader_stage = m_shader_store->GetShaderStage(pass.GetFragmentShaderPath());
                shader_pass.stages.push_back(shader_stage);
            }

            if (!pass.GetVertexShaderPath().empty())
            {
                const auto shader_stage = m_shader_store->GetShaderStage(pass.GetVertexShaderPath());
                shader_pass.stages.push_back(shader_stage);
            }
        }

        material->m_effect     = m_shader_store->CreateShaderEffect(String(filepath), shader_passes);
        material->m_properties = ShaderProperties(material->m_effect->property_table);

        for (size_t i = 0; i < material->m_passes.size(); ++i)
        {
            material->m_passes[i].m_shader_pass = &material->m_effect->passes[i];
        }

        // First, ensure all properties have valid values
        material->SetAllTextures(m_texture_store->GetDefaultTextureBlack());
        material->SetAllBuffers({});

        // Set default material textures
        material->SetTexture(MaterialTexture::Albedo, GetDefaultTexture(MaterialTexture::Albedo));
        material->SetTexture(MaterialTexture::Normal, GetDefaultTexture(MaterialTexture::Normal));
        material->SetTexture(MaterialTexture::Emissive, GetDefaultTexture(MaterialTexture::Emissive));
        material->SetTexture(MaterialTexture::ORM, GetDefaultTexture(MaterialTexture::ORM));
        material->SetTexture(MaterialTexture::Transmission, GetDefaultTexture(MaterialTexture::Transmission));
        material->SetTexture(MaterialTexture::Sheen, GetDefaultTexture(MaterialTexture::Albedo));
        material->SetTexture(MaterialTexture::Clearcoat, GetDefaultTexture(MaterialTexture::Clearcoat));
        material->SetTexture(MaterialTexture::Iridescence, GetDefaultTexture(MaterialTexture::Iridescence));
        material->SetTexture(MaterialTexture::ClearcoatNormal, GetDefaultTexture(MaterialTexture::ClearcoatNormal));

        return material;
    }

    void MaterialStore::DestroyMaterial(MaterialHandle material)
    {
        m_materials.Erase(material);
    }

    TextureHandle MaterialStore::GetDefaultTexture(MaterialTexture type)
    {
        switch (type)
        {
        case MaterialTexture::Albedo:
        case MaterialTexture::Transmission:
        case MaterialTexture::Sheen:
        case MaterialTexture::Clearcoat:
        case MaterialTexture::Iridescence:
            return m_texture_store->GetDefaultTextureWhite();
        case MaterialTexture::Normal:
        case MaterialTexture::ClearcoatNormal:
            return m_texture_store->GetDefaultTextureNormal();
        case MaterialTexture::Emissive:
            return m_texture_store->GetDefaultTextureBlack();
        case MaterialTexture::ORM:
            return m_texture_store->GetDefaulTextureOrm();
        default:
            Log::Error("Unknown MaterialTexture type {}", type);
            return m_texture_store->GetDefaultTextureBlack();
        }
    }
}
