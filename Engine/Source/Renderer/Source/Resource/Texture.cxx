module Valo.Renderer;

import Valo.RHI;
import Valo.Std;

import :TextureStore;

namespace Valo
{
    Texture::Texture(
        TextureStoreHandle       store,
        RHI::ImageHandle         image,
        SamplerHandle            sampler,
        const TextureDescriptor& descriptor) :
        Object("Texture"),
        m_store(store),
        m_image(Memory::Move(image)),
        m_sampler(sampler),
        m_format(descriptor.format),
        m_width(descriptor.width),
        m_height(descriptor.height),
        m_depth(descriptor.depth),
        m_mip_level_count(descriptor.mip_level_count),
        m_array_layer_count(descriptor.array_layer_count),
        m_owns_image(true)
    {
    }

    Texture::Texture(const Texture& texture, SamplerHandle sampler) :
        Object(texture.GetName()),
        m_store(texture.m_store),
        m_image(texture.GetImage()),
        m_sampler(sampler),
        m_format(texture.GetFormat()),
        m_width(texture.GetWidth()),
        m_height(texture.GetHeight()),
        m_depth(texture.GetDepth()),
        m_mip_level_count(texture.GetMipLevelCount()),
        m_array_layer_count(texture.GetArrayLayerCount())
    {
    }

    void Texture::SetName(String name)
    {
        if (m_owns_image)
        {
            m_store->GetResourceManager()->SetObjectName(GetImage(), name);
        }
        Object::SetName(Memory::Move(name));
    }

    RHI::ImageHandle Texture::GetImage() const
    {
        return m_image;
    }

    SamplerHandle Texture::GetSampler() const
    {
        return m_sampler;
    }

    TextureFormat Texture::GetFormat() const
    {
        return m_format;
    }

    UInt32 Texture::GetWidth() const
    {
        return m_width;
    }

    UInt32 Texture::GetHeight() const
    {
        return m_height;
    }

    UInt32 Texture::GetDepth() const
    {
        return m_depth;
    }

    UInt32 Texture::GetMipLevelCount() const
    {
        return m_mip_level_count;
    }

    UInt32 Texture::GetArrayLayerCount() const
    {
        return m_array_layer_count;
    }

    bool Texture::IsDirty() const
    {
        return m_is_dirty;
    }

    bool Texture::IsDeviceLocal() const
    {
        return m_is_device_local;
    }

    void Texture::MakeDeviceLocal()
    {
        m_is_device_local = true;
    }
}
