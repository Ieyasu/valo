module;

#include <Valo/Macros.hpp>

#include <algorithm>
#include <limits>
#include <ranges>

export module Valo.Renderer:Mesh;

import Valo.Engine;
import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :DeviceBufferView;
import :Material;
import :ShaderSemantics;

export namespace Valo
{
    VALO_FLAGS(MeshFlagBits, MeshFlags){
        None      = 0,
        Immutable = Bit::Set<Flags>(0),
    };

    struct MeshPrimitive
    {
        RHI::IndexFormat       format{RHI::IndexFormat::UInt32};
        Vector<Byte>           host_buffer{};
        DeviceBufferViewHandle device_buffer{};
    };

    struct MeshVertexBuffer
    {
        VertexAttribute        attribute{};
        RHI::Format            format{RHI::Format::Undefined};
        UInt32                 stride{0};
        Vector<Byte>           host_buffer{};
        DeviceBufferViewHandle device_buffer{};
    };

    class MeshStore;

    class Mesh final : public Object
    {
    public:
        Mesh();

        const Vector<MeshPrimitive>& GetPrimitives() const;

        const Vector<MeshVertexBuffer>& GetVertexBuffers() const;

        void Clear();

        void SetIndices(UInt32 primitive_index, const Vector<UInt32>& indices);

        void SetPositions(const Vector<Vec3>& positions);

        void SetNormals(const Vector<Vec3>& normals);

        void SetTangents(const Vector<Vec4>& tangents);

        void SetUVs(const Vector<Vec2>& uvs);

        void SetColors(const Vector<Vec4>& colors);

    private:
        void Resize(size_t size);

        template <typename T>
        void WriteBuffer(const Vector<T>& values, VertexAttribute attribute);

        template <typename T>
        void WriteUNorm16Buffer(const Vector<T>& values, VertexAttribute attribute);

        template <typename T>
        void WriteSNorm16Buffer(const Vector<T>& values, VertexAttribute attribute);

        template <typename T>
        void WriteFloat32Buffer(const Vector<T>& values, VertexAttribute attribute);

        template <typename T>
        static void GetBufferMinMax(const Vector<T>& values, float& out_min, float& out_max);

        void InitializeVertexBuffer(VertexAttribute attribute);

        MeshVertexBuffer& GetVertexBuffer(VertexAttribute attribute);

        bool IsValid() const;

        ObserverPtr<MeshStore> m_store{nullptr};

        Vector<MeshPrimitive>    m_primitives{};
        Vector<MeshVertexBuffer> m_vertex_buffers{};
        size_t                   m_vertex_count{};
        bool                     m_is_dirty{};

        friend class MeshStore;
    };

    using MeshHandle = ObjectPoolHandle<Mesh>;

    template <typename T>
    void Mesh::WriteBuffer(const Vector<T>& values, VertexAttribute attribute)
    {
        VALO_ASSERT(IsValid());

        if (values.empty())
        {
            return;
        }

        float min{};
        float max{};
        GetBufferMinMax(values, min, max);

        if (min >= 0.0f && max <= 1.0f)
        {
            WriteUNorm16Buffer(values, attribute);
        }
        else if (min >= -1.0f && max <= 1.0f)
        {
            WriteSNorm16Buffer(values, attribute);
        }
        else
        {
            WriteFloat32Buffer(values, attribute);
        }
    }

    template <typename T>
    void Mesh::WriteUNorm16Buffer(const Vector<T>& values, VertexAttribute attribute)
    {
        static_assert(T::length() >= 2 && T::length() <= 4);

        VALO_ASSERT(IsValid());

        auto& out_buffer = GetVertexBuffer(attribute);
        switch (T::length())
        {
        case 2:
            out_buffer.format = RHI::Format::R16G16_UNorm;
            break;
        case 3:
        case 4:
            out_buffer.format = RHI::Format::R16G16B16A16_UNorm;
            break;
        default:
            return;
        }
        const auto format_info     = RHI::FormatInfo(out_buffer.format);
        const auto component_count = format_info.GetComponentCount();
        out_buffer.stride          = format_info.GetStride();

        Resize(Math::Max(values.size(), m_vertex_count));

        constexpr auto max = std::numeric_limits<UInt16>::max();
        for (size_t i = 0; i < values.size(); ++i)
        {
            for (size_t j = 0; j < T::length(); ++j)
            {
                const auto value      = values[i][static_cast<int>(j)];
                const auto normalized = static_cast<UInt16>(std::clamp(value, 0.0f, 1.0f) * max);
                const auto offset     = (i * component_count + j) * sizeof(UInt16);
                Memory::Memcpy(out_buffer.host_buffer.data() + offset, &normalized, sizeof(Int16));
            }
        }
    }

    template <typename T>
    void Mesh::WriteSNorm16Buffer(const Vector<T>& values, VertexAttribute attribute)
    {
        static_assert(T::length() >= 2 && T::length() <= 4);

        VALO_ASSERT(IsValid());

        auto& out_buffer = GetVertexBuffer(attribute);
        switch (T::length())
        {
        case 2:
            out_buffer.format = RHI::Format::R16G16_SNorm;
            break;
        case 3:
        case 4:
            out_buffer.format = RHI::Format::R16G16B16A16_SNorm;
            break;
        default:
            return;
        }
        const auto format_info     = RHI::FormatInfo(out_buffer.format);
        const auto component_count = format_info.GetComponentCount();
        out_buffer.stride          = format_info.GetStride();

        Resize(Math::Max(values.size(), m_vertex_count));

        constexpr auto max = std::numeric_limits<Int16>::max();
        for (size_t i = 0; i < values.size(); ++i)
        {
            for (size_t j = 0; j < T::length(); ++j)
            {
                const auto value      = values[i][static_cast<int>(j)];
                const auto normalized = static_cast<Int16>(std::clamp(value, -1.0f, 1.0f) * max);
                const auto offset     = (i * component_count + j) * sizeof(Int16);
                Memory::Memcpy(out_buffer.host_buffer.data() + offset, &normalized, sizeof(Int16));
            }
        }
    }

    template <typename T>
    void Mesh::WriteFloat32Buffer(const Vector<T>& values, VertexAttribute attribute)
    {
        static_assert(T::length() >= 2 && T::length() <= 4);

        VALO_ASSERT(IsValid());

        auto& out_buffer = GetVertexBuffer(attribute);
        switch (T::length())
        {
        case 2:
            out_buffer.format = RHI::Format::R32G32_Float;
            break;
        case 3:
            out_buffer.format = RHI::Format::R32G32B32_Float;
            break;
        case 4:
            out_buffer.format = RHI::Format::R32G32B32A32_Float;
            break;
        default:
            return;
        }
        out_buffer.stride = RHI::FormatInfo(out_buffer.format).GetStride();

        Resize(Math::Max(values.size(), m_vertex_count));

        Memory::Memcpy(out_buffer.host_buffer.data(), values.data(), out_buffer.host_buffer.size());
    }

    template <typename T>
    void Mesh::GetBufferMinMax(const Vector<T>& values, float& out_min, float& out_max)
    {
        auto min_element = values.front();
        auto max_element = values.front();

        for (const auto& element : values)
        {
            min_element = Math::Min(min_element, element);
            max_element = Math::Max(max_element, element);
        }

        out_min = min_element[0];
        out_max = max_element[0];
        for (size_t i = 1; i < T::length(); ++i)
        {
            out_min = Math::Min(out_min, min_element[i]);
            out_max = Math::Max(out_max, max_element[i]);
        }
    }
}
