export module Valo.Renderer:ShaderProperties;

import Valo.Math;
import Valo.RHI;
import Valo.Std;

import :DeviceBuffer;
import :DeviceBufferView;
import :ResourceManager;
import :Texture;

export namespace Valo
{
    using ShaderPropertyId = UInt32;

    struct ShaderPropertyBinding final
    {
        RHI::DescriptorType type{};
        UInt32            binding{};
        UInt32            property_id{};
    };

    struct ShaderPropertyTable final
    {
        RHI::DescriptorSetLayoutHandle        layout{};
        Vector<ShaderPropertyBinding>         property_bindings{};
        Map<String, ShaderPropertyId, Less<>> property_ids{};
    };

    class ShaderProperties final
    {
    public:
        ShaderProperties() = default;

        explicit ShaderProperties(const ShaderPropertyTable& table);

        bool IsDirty() const;

        RHI::DescriptorSetHandle GetDescriptorSet() const;

        const ShaderPropertyTable& GetTable() const;

        Optional<UInt32> GetPropertyId(StringView name) const;

        bool IsBuffer(ShaderPropertyId property_id) const;
        void SetAllBuffers(DeviceBufferHandle value);
        void SetBuffer(ShaderPropertyId property_id, UInt32 array_index, DeviceBufferView value);
        void SetBuffer(StringView property_name, UInt32 array_index, DeviceBufferView value);

        bool IsTexture(ShaderPropertyId property_id) const;
        void SetAllTextures(TextureHandle value);
        void SetTexture(ShaderPropertyId property_id, UInt32 array_index, TextureHandle value);
        void SetTexture(StringView property_name, UInt32 array_index, TextureHandle value);

        void SetFloat(ShaderPropertyId property_id, float value);
        void SetFloat(StringView property_name, float value);

        void SetVec2(ShaderPropertyId property_id, Vec2 value);
        void SetVec2(StringView property_name, Vec2 value);

        void SetVec3(ShaderPropertyId property_id, Vec3 value);
        void SetVec3(StringView property_name, Vec3 value);

        void SetVec4(ShaderPropertyId property_id, Vec4 value);
        void SetVec4(StringView property_name, Vec4 value);

        void UpdateDescriptorSet(
            ResourceManagerHandle             resource_manager,
            Vector<RHI::DescriptorSetUpdate>& update_buffer);

    private:
        void GetDescriptorSetUpdates(Vector<RHI::DescriptorSetUpdate>& out_updates) const;

        ObserverPtr<const ShaderPropertyTable>  m_table{};
        Map<ShaderPropertyId, DeviceBufferView> m_buffers{};
        Map<ShaderPropertyId, TextureHandle>    m_textures{};
        bool                                    m_is_dirty{};
        RHI::DescriptorSetHandle                m_descriptor_set{};
    };
}
