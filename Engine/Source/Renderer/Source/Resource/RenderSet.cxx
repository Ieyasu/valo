module;

#include "Defines.hpp"

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Profiler;
import Valo.RenderGraph;
import Valo.RHI;
import Valo.Std;

namespace Valo
{
    RenderSet::RenderSet(PipelineCacheHandle pipeline_cache, RenderSetDescriptor descriptor) :
        m_pipeline_cache(pipeline_cache),
        m_descriptor(Memory::Move(descriptor))
    {
    }

    const Set<RenderTag>& RenderSet::GetRenderTags() const
    {
        return m_descriptor.tags;
    }

    const Vector<RenderElement>& RenderSet::GetRenderElements() const
    {
        return m_elements;
    }

    void RenderSet::Clear()
    {
        m_elements.clear();
        m_draw_calls.clear();
    }

    void RenderSet::SetRenderPass(RHI::RenderPassHandle render_pass, UInt32 subpass_index)
    {
        if (m_render_pass == render_pass && m_subpass_index == subpass_index)
        {
            return;
        }

        m_render_pass   = render_pass;
        m_subpass_index = subpass_index;
        RecacheDrawCalls();
    }

    void RenderSet::Insert(RenderElement element)
    {
        m_elements.push_back(element);

        CacheDrawCall(element);
    }

    void RenderSet::Draw(const Graph::RenderContext& context)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        SetRenderPass(context.render_pass, context.subpass_index);

        auto active_pipeline = RHI::GraphicsPipelineHandle{};
        for (const auto& draw_call : m_draw_calls)
        {
            VALO_ASSERT(draw_call.pipeline.IsValid());

            // Bind material pipeline and descriptor sets
            if (draw_call.pipeline != active_pipeline)
            {
                // Get and bind pipeline
                context.cmd_buffer->CmdBindGraphicsPipeline(draw_call.pipeline);
                active_pipeline = draw_call.pipeline;

                if (draw_call.per_material_descriptor_set.IsValid())
                {
                    context.cmd_buffer->CmdBindDescriptorSets(
                        draw_call.pipeline_layout,
                        {&draw_call.per_material_descriptor_set, 1},
                        {},
                        VALO_MATERIAL_DESCRIPTOR_SET_INDEX);
                }

                if (draw_call.per_object_descriptor_set.IsValid())
                {
                    context.cmd_buffer->CmdBindDescriptorSets(
                        draw_call.pipeline_layout,
                        {&draw_call.per_object_descriptor_set, 1},
                        {},
                        VALO_DESCRIPTOR_SET_PER_OBJECT);
                }
            }

            // Bind mesh
            context.cmd_buffer->CmdBindVertexBuffers(draw_call.vertex_buffers, draw_call.vertex_buffer_offsets, 0);

            context.cmd_buffer->CmdBindIndexBuffer(
                draw_call.index_buffer,
                draw_call.index_buffer_offset,
                draw_call.index_format);

            // Draw
            context.cmd_buffer
                ->CmdDrawIndexed(draw_call.index_count, draw_call.instance_count, 0, draw_call.first_instance, 0);
        }
    }

    void RenderSet::RecacheDrawCalls()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        m_draw_calls.clear();

        for (const auto& element : m_elements)
        {
            CacheDrawCall(element);
        }
    }

    void RenderSet::CacheDrawCall(const RenderElement& element)
    {
        if (!m_render_pass.IsValid())
        {
            return;
        }

        auto vertex_buffers        = SmallVector<RHI::BufferHandle>{};
        auto vertex_buffer_offsets = SmallVector<RHI::DeviceSizeType>{};
        for (const auto& vertex_buffer : element.mesh->GetVertexBuffers())
        {
            vertex_buffers.push_back(vertex_buffer.device_buffer->GetBuffer()->GetNative());
            vertex_buffer_offsets.push_back(vertex_buffer.device_buffer->GetOffset());
        }

        // Add draw call for all material passes that match the render tags of the set
        for (const auto& material_pass : element.material->GetPasses())
        {
            if (!GetRenderTags().contains(material_pass.GetTag()))
            {
                continue;
            }

            for (const auto& primitive : element.mesh->GetPrimitives())
            {
                const auto bytes_per_index = primitive.format == RHI::IndexFormat::UInt16 ? 2 : 4;

                const auto pipeline = m_pipeline_cache->GetOrCreatePipeline(
                    material_pass,
                    m_render_pass,
                    m_subpass_index);

                const auto draw_call = DrawCall{
                    // Material data
                    .pipeline                    = pipeline,
                    .pipeline_layout             = material_pass.GetShaderPass()->pipeline_layout,
                    .per_material_descriptor_set = element.material->GetDescriptorSet(),
                    .per_object_descriptor_set   = {},

                    // Mesh data
                    .index_buffer        = primitive.device_buffer->GetBuffer()->GetNative(),
                    .index_buffer_offset = static_cast<UInt32>(primitive.device_buffer->GetOffset()),
                    .index_count         = static_cast<UInt32>(primitive.device_buffer->GetRange()) / bytes_per_index,
                    .index_format        = primitive.format,

                    .vertex_buffers        = vertex_buffers,
                    .vertex_buffer_offsets = vertex_buffer_offsets,

                    // Instance data
                    .instance_count = 1,
                    .first_instance = 0,
                };

                m_draw_calls.push_back(draw_call);
            }
        }
    }
}
