module;

#include <Valo/Profile.hpp>

#include <set>

module Valo.Renderer;

import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    RenderSetStore::RenderSetStore(ResourceManagerHandle resource_manager, PipelineCacheHandle pipeline_cache) :
        Store(resource_manager),
        m_pipeline_cache(pipeline_cache)
    {
    }

    UniquePtr<RenderSetStore> RenderSetStore::Create(
        ResourceManagerHandle resource_manager,
        PipelineCacheHandle   pipeline_cache)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        return UniquePtr<RenderSetStore>(new RenderSetStore(resource_manager, pipeline_cache));
    }

    const Pool<RenderSet>& RenderSetStore::GetRenderSets() const
    {
        return m_render_sets;
    }

    RenderSetHandle RenderSetStore::GetOrCreateRenderSet(const RenderSetDescriptor& descriptor)
    {
        const auto tags_match = [&descriptor](const RenderSet& set)
        {
            return descriptor.tags == set.GetRenderTags();
        };

        const auto render_set = m_render_sets.Find(tags_match);
        if (render_set.IsValid())
        {
            return render_set;
        }

        return CreateRenderSet(descriptor);
    }

    RenderSetHandle RenderSetStore::CreateRenderSet(const RenderSetDescriptor& descriptor)
    {
        return m_render_sets.Emplace(m_pipeline_cache, descriptor);
    }

    void RenderSetStore::DestroyRenderSet(RenderSetHandle render_set)
    {
        m_render_sets.Erase(render_set);
    }
}
