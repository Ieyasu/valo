export module Valo.Renderer:RenderSettings;

import Valo.Std;

export namespace Valo
{
    enum class GraphicsAPI : EnumSmall
    {
        Vulkan = 0,
    };

    struct RendererSettings
    {
        GraphicsAPI graphics_api;
    };
}
