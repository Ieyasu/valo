module;

#include <Valo/Profile.hpp>

#include <algorithm>

module Valo.Renderer;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Logger;
import Valo.Math;
import Valo.Profiler;
import Valo.ShaderCompiler;
import Valo.Std;
import Valo.Window;

import :HighendPipeline;

namespace Valo
{
    bool Renderer::Initialize(Engine& engine)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto shader_compiler_module = engine.Require<ShaderCompilerModule>();
        m_display_manager           = engine.Require<DisplayManager>();
        m_time                      = engine.Require<Time>();

        // Create resource manager
        m_resource_manager = ResourceManager::Create(m_display_manager);
        if (!m_resource_manager)
        {
            Log::Error("Failed to initialize Renderer: failed to create ResourceManager.");
            return false;
        }

        // Create pipeline cache
        m_pipeline_cache = PipelineCache::Create(m_resource_manager.get());
        if (!m_pipeline_cache)
        {
            Log::Error("Failed to initialize Renderer: failed to create PipelineCache.");
            return false;
        }

        // Create resource stores
        m_buffer_store = BufferStore::Create(m_resource_manager.get());
        if (!m_buffer_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create BufferStore.");
            return false;
        }

        m_texture_store = TextureStore::Create(m_resource_manager.get());
        if (!m_texture_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create TextureStore.");
            return false;
        }

        m_mesh_store = MeshStore::Create(m_resource_manager.get(), m_buffer_store.get());
        if (!m_mesh_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create MeshStore.");
            return false;
        }

        m_shader_store = ShaderStore::Create(m_resource_manager.get(), shader_compiler_module->GetShaderCompiler());
        if (!m_shader_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create ShaderStore.");
            return false;
        }

        m_compute_shader_store = ComputeShaderStore::Create(
            m_resource_manager.get(),
            m_texture_store.get(),
            m_shader_store.get(),
            m_pipeline_cache.get());
        if (!m_compute_shader_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create ComputeShaderStore.");
            return false;
        }

        m_material_store = MaterialStore::Create(m_resource_manager.get(), m_texture_store.get(), m_shader_store.get());
        if (!m_material_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create MaterialStore.");
            return false;
        }

        m_render_set_store = RenderSetStore::Create(m_resource_manager.get(), m_pipeline_cache.get());
        if (!m_render_set_store)
        {
            Log::Error("Failed to initialize Renderer: failed to create RenderSetStore.");
            return false;
        }

        m_render_uniforms = RenderUniforms::Create(m_resource_manager.get(), m_buffer_store.get());
        if (!m_render_uniforms)
        {
            Log::Error("Failed to initialize Renderer: failed to create RenderUniforms.");
            return false;
        }

        m_render_pipeline = RenderPipeline::Create<HighendPipeline>(this);
        if (!m_render_pipeline)
        {
            Log::Error("Failed to initialize Renderer: failed to create RenderPipeline.");
            return false;
        }

        m_render_pipeline_compiler = RenderPipelineCompiler::Create(m_resource_manager.get());
        if (!m_render_pipeline_compiler)
        {
            Log::Error("Failed to initialize Renderer: failed to create RenderPipelineCompiler.");
            return false;
        }

        m_render_pipeline_scheduler = RenderPipelineScheduler::Create(m_resource_manager.get(), m_render_uniforms.get());
        if (!m_render_pipeline_scheduler)
        {
            Log::Error("Failed to initialize Renderer: failed to create RenderPipelineScheduler.");
            return false;
        }

        // Build render pipeline
        auto render_pipeline_desc       = RenderPipelineDescriptor{};
        render_pipeline_desc.resolution = {
            .width  = m_display_manager->GetWindow()->GetWidth(),
            .height = m_display_manager->GetWindow()->GetHeight(),
        };

        if (!m_render_pipeline_compiler->Compile(render_pipeline_desc, *m_render_pipeline))
        {
            Log::Error("Failed to initialize renderer: failed to compile render pipeline.");
            return false;
        }

        return true;
    }

    void Renderer::Deinitialize()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        // Free resources first
        m_material_store   = nullptr;
        m_texture_store    = nullptr;
        m_shader_store     = nullptr;
        m_mesh_store       = nullptr;
        m_render_set_store = nullptr;
        m_render_pipeline  = nullptr;

        // Free resource manager last
        m_resource_manager = nullptr;
    }

    void Renderer::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        const auto distance = 5.0f;
        // const auto distance = 400.0f;
        auto       camera   = Camera();
        const auto rotation = 0.25f * Math::Sin(m_time->GetTimeSinceStart().count());
        const auto t        = Math::Translate(Mat4{1.0f}, Vec3{0, 0, distance});
        const auto r        = Math::Rotate(Mat4{1.0f}, rotation, Vec3{0, 1, 0});
        camera.SetViewMatrix(Math::Inverse(t * r));
        camera.SetAspectRatio(
            static_cast<float>(m_display_manager->GetWindow()->GetWidth())
            / static_cast<float>(m_display_manager->GetWindow()->GetHeight()));

        // Upload dirty resources to GPU
        m_mesh_store->Update();
        m_compute_shader_store->Update();
        m_material_store->Update();
        m_render_uniforms->Update(camera, *m_time);

        // Execute commands
        m_resource_manager->BeginFrame();
        m_render_pipeline_scheduler->Run(*m_render_pipeline);
        m_resource_manager->EndFrame();
    }

    ComputeShaderHandle Renderer::CreateComputeShader(StringView name)
    {
        return m_compute_shader_store->CreateComputeShader(name);
    }

    void Renderer::DestroyComputeShader(ComputeShaderHandle compute_shader)
    {
        m_compute_shader_store->DestroyComputeShader(compute_shader);
    }

    MaterialHandle Renderer::CreateMaterial(StringView name)
    {
        return m_material_store->CreateMaterial(name);
    }

    void Renderer::DestroyMaterial(MaterialHandle material)
    {
        m_material_store->DestroyMaterial(material);
    }

    MeshHandle Renderer::CreateMesh()
    {
        return m_mesh_store->CreateMesh();
    }

    void Renderer::DestroyMesh(MeshHandle mesh)
    {
        m_mesh_store->DestroyMesh(mesh);
    }

    RenderSetHandle Renderer::GetOrCreateRenderSet(const RenderSetDescriptor& descriptor)
    {
        return m_render_set_store->GetOrCreateRenderSet(descriptor);
    }

    RenderSetHandle Renderer::CreateRenderSet(const RenderSetDescriptor& descriptor)
    {
        return m_render_set_store->CreateRenderSet(descriptor);
    }

    void Renderer::DestroyRenderSet(RenderSetHandle render_set)
    {
        m_render_set_store->DestroyRenderSet(render_set);
    }
}
