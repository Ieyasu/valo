module;

#include <functional>

export module Valo.Renderer:PipelineCache;

import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :ComputeShader;
import :Material;
import :MaterialPass;
import :ResourceManager;

export namespace Valo
{
    class PipelineCache final
    {
    public:
        static UniquePtr<PipelineCache> Create(ResourceManagerHandle resource_manager);

        PipelineCache(const PipelineCache&)            = delete;
        PipelineCache(PipelineCache&&)                 = delete;
        PipelineCache& operator=(const PipelineCache&) = delete;
        PipelineCache& operator=(PipelineCache&&)      = delete;
        ~PipelineCache();

        RHI::ComputePipelineHandle GetOrCreatePipeline(ComputeShaderHandle compute_shader);

        RHI::GraphicsPipelineHandle GetOrCreatePipeline(
            const MaterialPass&   material_pass,
            RHI::RenderPassHandle render_pass,
            UInt32                subpass_index);

    private:
        struct CacheKey final
        {
            const MaterialPass*   material_pass{nullptr};
            RHI::RenderPassHandle render_pass{0};
            UInt32                subpass_index{0};

            bool operator==(const CacheKey& other) const
            {
                return material_pass == other.material_pass && render_pass == other.render_pass
                    && subpass_index == other.subpass_index;
            }
        };

        struct CacheKeyHash final
        {
            size_t operator()(const CacheKey& key) const
            {
                size_t res = 17;
                res        = res * 31 + std::hash<const MaterialPass*>()(key.material_pass);
                res        = res * 31 + std::hash<RHI::RenderPassHandle::NativeType>()(key.render_pass.GetNative());
                res        = res * 31 + std::hash<UInt32>()(key.subpass_index);
                return res;
            }
        };

        explicit PipelineCache(ResourceManagerHandle resource_manager);

        RHI::ComputePipelineHandle CreatePipeline(ComputeShaderHandle compute_shader);

        RHI::GraphicsPipelineHandle CreatePipeline(
            const MaterialPass&   material_pass,
            RHI::RenderPassHandle render_pass,
            UInt32                subpass_index);

        static Vector<RHI::VertexAttributeDescriptor>      CreateVertexAttributes(const MaterialPass& pass);
        static Vector<RHI::ColorBlendAttachmentDescriptor> CreateColorBlendAttachments(const MaterialPass& pass);
        static Vector<RHI::ShaderStageDescriptor>          CreateShaderStages(const MaterialPass& pass);
        static RHI::ShaderStage                            ToShaderStageFlags(ShaderStageReflection stage);

        UnorderedMap<CacheKey, RHI::GraphicsPipelineHandle, CacheKeyHash> m_graphics_cache{};
        UnorderedMap<ComputeShaderHandle, RHI::ComputePipelineHandle>     m_compute_cache{};

        // Dependencies
        ResourceManagerHandle m_resource_manager{};
    };

    using PipelineCacheHandle = ObserverPtr<PipelineCache>;
}
