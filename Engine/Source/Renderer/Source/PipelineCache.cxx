module;

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

#include <vector>

module Valo.Renderer;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.ShaderCompiler;
import Valo.Std;

import :ShaderSemantics;

namespace Valo
{
    PipelineCache::PipelineCache(ResourceManagerHandle resource_manager) :
        m_resource_manager(resource_manager)
    {
    }

    PipelineCache::~PipelineCache()
    {
        for (const auto& key_pipeline_pair : m_compute_cache)
        {
            const auto& pipeline = key_pipeline_pair.second;
            m_resource_manager->DestroyComputePipeline(pipeline);
        }

        for (const auto& key_pipeline_pair : m_graphics_cache)
        {
            const auto& pipeline = key_pipeline_pair.second;
            m_resource_manager->DestroyGraphicsPipeline(pipeline);
        }
    }

    UniquePtr<PipelineCache> PipelineCache::Create(ResourceManagerHandle resource_manager)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Renderer);

        auto self = UniquePtr<PipelineCache>(new PipelineCache(resource_manager));
        return self;
    }

    RHI::ComputePipelineHandle PipelineCache::GetOrCreatePipeline(ComputeShaderHandle compute_shader)
    {
        auto it = m_compute_cache.find(compute_shader);
        if (it != m_compute_cache.end())
        {
            return it->second;
        }

        return CreatePipeline(compute_shader);
    }

    RHI::GraphicsPipelineHandle PipelineCache::GetOrCreatePipeline(
        const MaterialPass&   material_pass,
        RHI::RenderPassHandle render_pass,
        UInt32                subpass_index)
    {
        const auto cache_key = CacheKey{
            .material_pass = &material_pass,
            .render_pass   = render_pass,
            .subpass_index = subpass_index,
        };

        auto it = m_graphics_cache.find(cache_key);
        if (it != m_graphics_cache.end())
        {
            return it->second;
        }

        return CreatePipeline(material_pass, render_pass, subpass_index);
    }

    RHI::ComputePipelineHandle PipelineCache::CreatePipeline(ComputeShaderHandle compute_shader)
    {
        VALO_ASSERT(!m_compute_cache.contains(compute_shader));

        auto pipeline_desc                = RHI::ComputePipelineDescriptor{};
        pipeline_desc.layout              = compute_shader->GetShaderPass()->pipeline_layout;
        pipeline_desc.shader_stage.stage  = RHI::ShaderStage::Compute;
        pipeline_desc.shader_stage.name   = compute_shader->GetShaderStage()->reflection.GetEntryPoint();
        pipeline_desc.shader_stage.binary = compute_shader->GetShaderStage()->binary;

        auto pipeline = m_resource_manager->CreateComputePipeline(pipeline_desc);
        if (!pipeline.IsValid())
        {
            Log::Error("Failed to create cached compute pipeline");
            return {};
        }
        m_resource_manager->SetObjectName(pipeline, compute_shader->GetName());

        m_compute_cache[compute_shader] = pipeline;
        return pipeline;
    }

    RHI::GraphicsPipelineHandle PipelineCache::CreatePipeline(
        const MaterialPass&   material_pass,
        RHI::RenderPassHandle render_pass,
        UInt32                subpass_index)
    {
        const auto cache_key = CacheKey{
            .material_pass = &material_pass,
            .render_pass   = render_pass,
            .subpass_index = subpass_index,
        };

        VALO_ASSERT(!m_graphics_cache.contains(cache_key));

        // Get the vertex and fragment shaders
        const auto vertex_attributes       = CreateVertexAttributes(material_pass);
        const auto shader_stages           = CreateShaderStages(material_pass);
        const auto color_blend_attachments = CreateColorBlendAttachments(material_pass);

        auto pipeline_desc                    = RHI::GraphicsPipelineDescriptor{};
        pipeline_desc.input_assembly_topology = RHI::PrimitiveTopology::TriangleList;
        pipeline_desc.layout                  = material_pass.GetShaderPass()->pipeline_layout;
        pipeline_desc.shader_stages           = shader_stages;

        pipeline_desc.rasterizer_cull_mode          = material_pass.GetCullMode();
        pipeline_desc.rasterizer_polygon_mode       = material_pass.GetPolygonMode();
        pipeline_desc.rasterizer_winding            = RHI::Winding::CounterClockwise;
        pipeline_desc.rasterizer_enable_discard     = false;  // TODO: Shadow pass needs these
        pipeline_desc.rasterizer_enable_depth_clamp = false;  // TODO: Shadow pass needs these

        pipeline_desc.depth_enable_test    = material_pass.IsDepthTestEnabled();
        pipeline_desc.depth_enable_write   = material_pass.IsDepthWriteEnabled();
        pipeline_desc.depth_compare_method = material_pass.GetDepthCompare();

        pipeline_desc.vertex_attributes = vertex_attributes;

        pipeline_desc.color_blend_enable_logic_op = false;
        pipeline_desc.color_blend_attachments     = color_blend_attachments;

        // Viewports and scissors are handled dynamically
        auto viewports            = Vector<RHI::Viewport>(1);
        auto scissors             = Vector<RHI::Scissor>(1);
        pipeline_desc.viewports   = viewports;
        pipeline_desc.scissors    = scissors;
        const auto dynamic_states = Vector<RHI::DynamicState>{RHI::DynamicState::Scissors, RHI::DynamicState::Viewport};
        pipeline_desc.dynamic_states = dynamic_states;

        pipeline_desc.render_pass         = render_pass;
        pipeline_desc.render_pass_subpass = subpass_index;

        auto pipeline = m_resource_manager->CreateGraphicsPipeline(pipeline_desc);
        if (!pipeline.IsValid())
        {
            Log::Error("Failed to create cached graphics pipeline");
            return {};
        }
        m_resource_manager->SetObjectName(pipeline, material_pass.GetName());

        m_graphics_cache[cache_key] = pipeline;
        return pipeline;
    }

    Vector<RHI::VertexAttributeDescriptor> PipelineCache::CreateVertexAttributes(const MaterialPass& pass)
    {
        const auto  shader_stage = pass.GetVertexShader();
        const auto& stage_inputs = shader_stage->reflection.GetStageInputs();

        auto attributes = Vector<RHI::VertexAttributeDescriptor>(stage_inputs.size());
        for (size_t i = 0; i < attributes.size(); ++i)
        {
            const auto semantics = VertexInputSemantic::Get(stage_inputs[i].name);
            VALO_ASSERT(semantics.has_value());

            auto& attribute    = attributes[i];
            attribute.location = stage_inputs[i].location;
            attribute.binding  = semantics.value().GetBinding();
            attribute.format   = semantics.value().GetFormat();
            attribute.stride   = semantics.value().GetStride();
        }
        return attributes;
    }

    Vector<RHI::ColorBlendAttachmentDescriptor> PipelineCache::CreateColorBlendAttachments(const MaterialPass& pass)
    {
        const auto  shader_stage  = pass.GetFragmentShader();
        const auto& stage_outputs = shader_stage->reflection.GetStageOutputs();

        auto attachments = Vector<RHI::ColorBlendAttachmentDescriptor>(stage_outputs.size());
        for (auto& attachment : attachments)
        {
            attachment.enable_blend = false;  // TODO: Make alpha blending configurable
        }
        return attachments;
    }

    Vector<RHI::ShaderStageDescriptor> PipelineCache::CreateShaderStages(const MaterialPass& pass)
    {
        const auto& shader_pass   = pass.GetShaderPass();
        auto        shader_stages = Vector<RHI::ShaderStageDescriptor>(shader_pass->stages.size());
        for (size_t i = 0; i < shader_stages.size(); ++i)
        {
            const auto& shader_stage      = shader_pass->stages[i];
            auto&       shader_stage_desc = shader_stages[i];
            shader_stage_desc.name        = shader_stage->reflection.GetEntryPoint();
            shader_stage_desc.stage       = ToShaderStageFlags(shader_stage->reflection.GetStage());
            shader_stage_desc.binary      = shader_stage->binary;
        }
        return shader_stages;
    }

    RHI::ShaderStage PipelineCache::ToShaderStageFlags(ShaderStageReflection stage)
    {
        switch (stage)
        {
        case ShaderStageReflection::Compute:
            return RHI::ShaderStage::Compute;
        case ShaderStageReflection::Fragment:
            return RHI::ShaderStage::Fragment;
        case ShaderStageReflection::Geometry:
            return RHI::ShaderStage::Geometry;
        case ShaderStageReflection::None:
            return RHI::ShaderStage::None;
        case ShaderStageReflection::TessellationControl:
            return RHI::ShaderStage::TessellationControl;
        case ShaderStageReflection::TessellationEvaluation:
            return RHI::ShaderStage::TessellationEvaluation;
        case ShaderStageReflection::Vertex:
            return RHI::ShaderStage::Vertex;
        default:
            Log::Fatal("Fail");
            return RHI::ShaderStage::None;
        }
    }
}
