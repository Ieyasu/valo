export module Valo.Renderer:Camera;

import Valo.Math;

export namespace Valo
{
    class Camera final
    {
    public:
        Camera();

        Mat4 GetViewProjectionMatrix() const;

        Mat4 GetViewMatrix() const;

        void SetViewMatrix(Mat4 view_matrix);

        Mat4 GetProjectionMatrix() const;

        void SetProjectionMatrix(Mat4 projection_matrix);

        float GetAspectRatio() const;

        void SetAspectRatio(float aspect_ratio);

        float GetVerticalFov() const;

        void SetVerticalFov(float vertical_fov);

        float GetNearPlane() const;

        void SetNearPlane(float near_plane);

    private:
        void UpdateProjection();
        void UpdateParameters();

        Mat4 m_view_matrix{1.0f};
        Mat4 m_projection_matrix{};
        Mat4 m_view_projection_matrix{};

        float m_vertical_fov{Math::DegToRad(70)};
        float m_near_plane{0.1f};
        float m_aspect_ratio{0};
    };
}
