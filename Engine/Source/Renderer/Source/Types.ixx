export module Valo.Renderer:Types;

import Valo.Std;

export namespace Valo
{
    struct Resolution final
    {
        UInt32 width{0};
        UInt32 height{0};
    };
}
