export module Valo.Renderer:ResourceManager;

import Valo.RHI;
import Valo.Std;
import Valo.Window;

export namespace Valo
{
    class ResourceManager final
    {
    public:
        static UniquePtr<ResourceManager> Create(const DisplayManagerHandle& display_manager);

        ResourceManager(const ResourceManager&)            = delete;
        ResourceManager(ResourceManager&&)                 = delete;
        ResourceManager& operator=(const ResourceManager&) = delete;
        ResourceManager& operator=(ResourceManager&&)      = delete;

        ~ResourceManager();

        void BeginFrame();

        void EndFrame();

        RHI::DeviceHandle GetDevice() const;

        UInt32 GetCurrentFrame() const;

        UInt32 GetFramesInFlight() const;

        void SetObjectName(RHI::BaseResourceHandle handle, StringView name);

        RHI::CommandBufferHandle AquireCommandBuffer();

        RHI::BufferHandle CreateBuffer(const RHI::BufferDescriptor& descriptor);

        void DestroyBuffer(RHI::BufferHandle buffer, bool immediate = false);

        RHI::ImageHandle CreateImage(const RHI::ImageDescriptor& descriptor);

        void DestroyImage(RHI::ImageHandle image, bool immediate = false);

        RHI::SamplerHandle CreateSampler(const RHI::SamplerDescriptor& descriptor);

        void DestroySampler(RHI::SamplerHandle sampler, bool immediate = false);

        RHI::DescriptorSetHandle CreateDescriptorSet(const RHI::DescriptorSetDescriptor& descriptor);

        void DestroyDescriptorSet(RHI::DescriptorSetHandle set, bool immediate = false);

        void UpdateDescriptorSets(const Span<const RHI::DescriptorSetUpdate>& updates);

        RHI::DescriptorSetLayoutHandle CreateDescriptorSetLayout(const RHI::DescriptorSetLayoutDescriptor& descriptor);

        void DestroyDescriptorSetLayout(RHI::DescriptorSetLayoutHandle layout, bool immediate = false);

        RHI::ComputePipelineHandle CreateComputePipeline(const RHI::ComputePipelineDescriptor& descriptor);

        void DestroyComputePipeline(RHI::ComputePipelineHandle pipeline, bool immediate = false);

        RHI::GraphicsPipelineHandle CreateGraphicsPipeline(const RHI::GraphicsPipelineDescriptor& descriptor);

        void DestroyGraphicsPipeline(RHI::GraphicsPipelineHandle pipeline, bool immediate = false);

        RHI::PipelineLayoutHandle CreatePipelineLayout(const RHI::PipelineLayoutDescriptor& descriptor);

        void DestroyPipelineLayout(RHI::PipelineLayoutHandle layout, bool immediate = false);

        void UploadMappableBuffer(const void* data, RHI::BufferHandle buffer, size_t buffer_offset, size_t size);

        void UploadBuffer(const void* data, RHI::BufferHandle buffer, size_t buffer_offset, size_t size);

        void UploadImage(
            const void*         data,
            RHI::DeviceSizeType size,
            RHI::ImageHandle    image,
            UInt32              image_width,
            UInt32              image_height,
            UInt32              image_depth,
            UInt32              image_offset_x,
            UInt32              image_offset_y,
            UInt32              image_offset_z,
            UInt32              image_mip_level,
            UInt32              image_array_layer);

        RHI::DescriptorSetLayoutHandle GetEmptyDescriptorSetLayout() const;

    private:
        struct FrameData
        {
            UniquePtr<RHI::CommandPoolManager> cmd_pool_manager{};

            // Uploads
            RHI::CommandBufferHandle transfer_cmd_buffer{};

            // Destroys
            Vector<RHI::BufferHandle>              pending_buffer_destroys{};
            Vector<RHI::ImageHandle>               pending_image_destroys{};
            Vector<RHI::SamplerHandle>             pending_sampler_destroys{};
            Vector<RHI::DescriptorSetHandle>       pending_descriptor_set_destroys{};
            Vector<RHI::DescriptorSetLayoutHandle> pending_descriptor_set_layout_destroys{};
            Vector<RHI::ComputePipelineHandle>     pending_compute_pipeline_destroys{};
            Vector<RHI::GraphicsPipelineHandle>    pending_graphics_pipeline_destroys{};
            Vector<RHI::PipelineLayoutHandle>      pending_pipeline_layout_destroys{};
        };

        ResourceManager() = default;

        void BeginUpload();

        void EndUpload();

        void DestroyPerFrameResources(const FrameData& frame_data);

        void DestroyPendingResources(const FrameData& frame_data);

        UniquePtr<RHI::Instance, Function<void(RHI::Instance*)>> m_instance{};
        RHI::DeviceHandle                                        m_device{};
        RHI::DescriptorSetLayoutHandle                           m_empty_descriptor_set_layout{};

        // Internal resources
        RHI::BufferHandle   m_staging_buffer{};
        RHI::DeviceSizeType m_staging_buffer_offset{};
        RHI::DeviceSizeType m_staging_buffer_size{};

        // Frame data
        bool              m_frame_in_progress{};
        UInt32            m_current_frame{0};
        Vector<FrameData> m_per_frame_data{};
    };

    using ResourceManagerHandle = ObserverPtr<ResourceManager>;
}
