#ifndef VALO_SAMPLER_GLSL
#define VALO_SAMPLER_GLSL

#include "../core/math.glsl"

#if defined(HALTON_SAMPLER)
    #include "../sampling/halton_sampler.glsl"
    #define Rng HaltonSampler
#elif defined(SOBOL_SAMPLER)
    #include "../sampling/sobol_sampler.glsl"
    #define Rng SobolSampler
#else
    #include "../sampling/random_sampler.glsl"
    #define Rng RandomSampler
#endif

vec3 _direction_to_world_space(vec3 dir, vec3 world_normal) {
	vec3 u = normalize(cross(world_normal, abs(world_normal.y) > 0.5 ? vec3(1, 0, 0) : vec3(0, 1, 0)));
	vec3 v = cross(u, world_normal);
    return normalize(dir.x * u + dir.y * v + dir.z * world_normal);
}

float mis_power_heuristic(uint f_count, float f_pdf, uint g_count, float g_pdf) {
    float f = f_count * f_pdf;
    float g = g_count * g_pdf;
    return (f * f) / (f * f + g * g);
}

float mis_power_heuristic(float f_pdf, float g_pdf) {
    return mis_power_heuristic(1, f_pdf, 1, g_pdf);
}

// Returns a random uniform sample on the unit circle
vec2 sample_unit_circle(float r1, float r2) {
	float r = sqrt(r1);
    float theta = (2.0 * PI) * r2;
    return vec2(r * cos(theta), r * sin(theta));
}

// Returns a random uniform hemisphere vector
vec3 sample_hemisphere_uniform(vec3 normal, float r1, float r2) {
    float r = sqrt(max(0, 1 - r1 * r1));
    float theta = TWO_PI * r2;

    // Project z up to the unit hemisphere
	vec3 dir;
    dir.x = r * cos(theta);
	dir.y = r * sin(theta);
	dir.z = r1;
    return _direction_to_world_space(dir, normal);
}

// Returns a random cosine weighted hemisphere vector
vec3 sample_hemisphere_cosine(vec3 normal, float r1, float r2) {
    // Generate a uniform point on unit circle
	float r = sqrt(r1);
    float theta = TWO_PI * r2;

    // Project z up to the unit hemisphere
	vec3 dir;
    dir.x = r * cos(theta);
	dir.y = r * sin(theta);
	dir.z = sqrt(1.0 - r1);
    return _direction_to_world_space(dir, normal);
}

// Returns a random gtr1 weighted hemisphere vector
vec3 sample_hemisphere_gtr1(vec3 normal, float roughness, float r1, float r2) {
    float phi = TWO_PI * r1;
    float a = max(0.001, roughness);
    float a2 = a * a;
    float cos_theta = sqrt((1.0 - pow(a2, 1.0 - r2)) / (1.0 - a2));
    float sin_theta = clamp(sqrt(1.0 - (cos_theta * cos_theta)), 0.0, 1.0);
    float sin_phi = sin(phi);
    float cos_phi = cos(phi);

    vec3 dir = vec3(sin_theta * cos_phi, sin_theta * sin_phi, cos_theta);
    return _direction_to_world_space(dir, normal);
}

// Returns a random gtr2 weighted hemisphere vector
vec3 sample_hemisphere_gtr2(vec3 normal, float roughness, float r1, float r2) {
    float phi = TWO_PI * r1;
    float a = max(0.001, roughness);
    float cos_theta = sqrt((1.0 - r2) / (1.0 + (a * a - 1.0) * r2));
    float sin_theta = clamp(sqrt(1.0 - (cos_theta * cos_theta)), 0.0, 1.0);
    float sin_phi = sin(phi);
    float cos_phi = cos(phi);

    vec3 dir = vec3(sin_theta * cos_phi, sin_theta * sin_phi, cos_theta);
    return _direction_to_world_space(dir, normal);
}

#endif // VALO_SAMPLER_GLSL
