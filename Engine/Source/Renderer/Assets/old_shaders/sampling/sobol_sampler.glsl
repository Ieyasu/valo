#ifndef VALO_SOBOL_GLSL
#define VALO_SOBOL_GLSL

#include "../core/math.glsl"
#include "../core/hash.glsl"
#include "random_sampler.glsl"

// Suplemental materials
// http://jcgt.org/published/0009/04/01/paper.pdf
// https://psychopath.io/post/2020_04_14_building_a_sobol_sampler
// https://wiki.blender.org/wiki/Source/Render/Cycles/Sobol

// #define SOBOL_SCRAMBLE_NONE
// #define SOBOL_SCRAMBLE_XOR
// #define SOBOL_SCRAMBLE_LAINE_KARRAS
#define SOBOL_SCRAMBLE_PSYCHOPATH

struct SobolSampler {
    // Fallback sampler after we run out of dimensions
    RandomSampler uniform_random;

    uint seed;
    uint dimension;
    uint sequence_index;
};

layout(set = 0, binding = 31) uniform SobolDirectionBuffer {
    uvec4 _sobol_directions[4096];
};

#if defined(SOBOL_SCRAMBLE_PSYCHOPATH)
uint sobol_scramble(uint x, uint seed) {
    x += x << 2;
    x ^= x * 0xfe9b5742;
    x += seed;
    x *= (seed >> 16) | 1;
    return x;
}
#elif defined(SOBOL_SCRAMBLE_LAINE_KARRAS)
uint sobol_scramble(uint x, uint seed) {
    x += seed;
    x ^= x * 0x6c50b47cu;
    x ^= x * 0xb82f1e52u;
    x ^= x * 0xc7afe638u;
    x ^= x * 0x8d22f6e6u;
    return x;
}
#endif

uint sobol_base(uint index, uint dimension) {
    uint sobol = 0;
    for (uint j = 8 * dimension; index != 0; ++j) {
        uvec4 dir = _sobol_directions[j];
        sobol ^= (index & 1) * dir.x;
        index >>= 1;
        sobol ^= (index & 1) * dir.y;
        index >>= 1;
        sobol ^= (index & 1) * dir.z;
        index >>= 1;
        sobol ^= (index & 1) * dir.w;
        index >>= 1;
    }
    return sobol;
}

#if defined(SOBOL_SCRAMBLE_NONE)

uint sobol_sample(uint index, uint dimension, uint seed) {
    return sobol_base(index, dimension);
}

#elif defined(SOBOL_SCRAMBLE_XOR)

uint sobol_sample(uint index, uint dimension, uint seed) {
    return sobol_base(index, dimension) ^ seed;
}

#elif defined(SOBOL_SCRAMBLE_LAINE_KARRAS) || defined(SOBOL_SCRAMBLE_PSYCHOPATH)

// Owen scramble
uint sobol_scramble_and_reverse(uint index, uint seed) {
    return bitfieldReverse(sobol_scramble(bitfieldReverse(index), seed));
}

uint sobol_sample(uint index, uint dimension, uint seed) {
    uint sobol = sobol_base(index, dimension);
    return sobol_scramble_and_reverse(sobol, seed);
}

#endif

void initialize_rng(inout SobolSampler rng, ivec2 pixel, uvec2 image_size, uint frame_count) {
    // A unique seed for every pixel up to a resolution of 65536 x 65536
    rng.seed = hash(pixel.x ^ (pixel.y << 16) ^ 0x736caf6f);

    // The dimension is incremented after each sample in the pixel
    rng.dimension = 0;

#if defined(SOBOL_SCRAMBLE_NONE)
    rng.sequence_index = frame_count + 1;
#elif defined(SOBOL_SCRAMBLE_XOR)
    rng.sequence_index = (frame_count + 1) ^ rng.seed;
#elif defined(SOBOL_SCRAMBLE_LAINE_KARRAS) || defined(SOBOL_SCRAMBLE_PSYCHOPATH)
    rng.sequence_index = sobol_scramble_and_reverse(frame_count + 1, rng.seed);
#endif
}

float random_uniform(inout SobolSampler rng) {
    return sobol_sample(rng.sequence_index, rng.dimension++, rng.seed) * 2.3283064365387e-10f;
}

#endif // VALO_SOBOL_GLSL
