#ifndef VALO_HALTON_GLSL
#define VALO_HALTON_GLSL

#include "random_sampler.glsl"

// Implementation adapted from http://gruenschloss.org/

struct HaltonSampler {
    // Smallest integer with 2^m_p2 >= width.
    uint p2;

    // Smallest integer with 3^m_p3 >= height.
    uint p3;

    // 3^m_p3 * ((2^m_p2)^(-1) mod 3^m_p3).
    uint x;

    // 2^m_p2 * ((3^m_p3)^(-1) mod 2^m_p2).
    uint y;

    // Product of prime powers, i.e. m_res2 * m_res3.
    uint increment;

    // Each time a new sample is taken, the dimension is incremented by one.
    uint dimension;

    // The current sequence. This is different for each individual pixel across frames.
    uint sequence_index;

    // The backup sampler
    RandomSampler uniform_random;
};

layout(set = 0, binding = 30) uniform HaltonSamplerUniform {
    HaltonSampler _halton_sampler;
};

uint halton_bases[32] = uint[32](2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131);

float halton_sample(uint dimension, uint sample_index) {
    uint base = halton_bases[dimension % 32];

    // Compute radical inversee
    float a = 0;
    float inv_base = 1.0 / float(base);
    for (float mult = inv_base; sample_index != 0; sample_index /= base, mult *= inv_base) {
        a += float(sample_index % base) * mult;
    }
    return a;
}

uint halton2_inverse(uint index, uint digits) {
    index = (index << 16) | (index >> 16);
    index = ((index & 0x00ff00ff) << 8) | ((index & 0xff00ff00) >> 8);
    index = ((index & 0x0f0f0f0f) << 4) | ((index & 0xf0f0f0f0) >> 4);
    index = ((index & 0x33333333) << 2) | ((index & 0xcccccccc) >> 2);
    index = ((index & 0x55555555) << 1) | ((index & 0xaaaaaaaa) >> 1);
    return index >> (32 - digits);
}

uint halton3_inverse(uint index, uint digits) {
    uint result = 0;
    for (uint d = 0; d < digits; ++d) {
        result = result * 3 + index % 3;
        index /= 3;
    }
    return result;
}

uint halton_index(HaltonSampler rng, uvec2 pixel, uint i) {
    uint hx = halton2_inverse(pixel.x, rng.p2);
    uint hy = halton3_inverse(pixel.y, rng.p3);

    // Apply Chinese remainder theorem.
    uint offset = (hx * rng.x + hy * rng.y) % rng.increment;
    return offset + i * rng.increment;
}

void initialize_rng(inout HaltonSampler rng, ivec2 pixel, uvec2 image_size, uint frame_count) {
    uint i = frame_count + 1;
    rng = _halton_sampler;
    rng.dimension = 2;
    rng.sequence_index = halton_index(rng, uvec2(pixel), i);

    // Initialize backup uniform rng
    initialize_rng(rng.uniform_random, pixel, image_size, frame_count);
}

float random_uniform(inout HaltonSampler rng) {
    // If we run out of dimensions, revert back to uniform random
    if (rng.dimension >= 31) {
        return random_uniform(rng.uniform_random);
    }

    return halton_sample(rng.dimension++, rng.sequence_index);
}

#endif // VALO_HALTON_GLSL
