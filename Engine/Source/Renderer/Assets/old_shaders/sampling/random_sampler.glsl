#ifndef VALO_RANDOM_SAMPLER_GLSL
#define VALO_RANDOM_SAMPLER_GLSL

#include "../core/hash.glsl"

struct RandomSampler {
    uint state;
};

void initialize_rng(inout RandomSampler rng, ivec2 pixel, uvec2 image_size, uint frame_count) {
    const uint PRIME1 = 236887699;
    const uint PRIME2 = 593441861;
    const uint PRIME3 = 791030197;

    // Setup per pixel PRNG
    uint thread_index = 1 + pixel.x + pixel.y * uint(image_size.x);
    rng.state = (thread_index * PRIME1 + (frame_count + 1) * PRIME2) * PRIME3;
}

float random_uniform(inout RandomSampler rng) {
    rng.state = hash(rng.state);
    return rng.state * 2.3283064365387e-10f;
}

#endif // VALO_RANDOM_SAMPLER_GLSL
