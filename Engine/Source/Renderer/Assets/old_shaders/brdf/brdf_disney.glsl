#ifndef VALO_BRDF_DISNEY_GLSL
#define VALO_BRDF_DISNEY_GLSL

#include "../sampling/sampler.glsl"
#include "brdf_sample.glsl"

#define REFLECTION_THRESHOLD 0.999

float schlick_fresnel(float u) {
    float m = clamp(1.0 - u, 0.0, 1.0);
    float m2 = m * m;
    return m2 * m2 * m; // pow(m,5)
}

float dielectric_fresnel(float cos_theta_i, float eta) {
    float sinThetaTSq = eta * eta * (1.0f - cos_theta_i * cos_theta_i);

    // Total internal reflection
    if (sinThetaTSq > 1.0) {
        return 1.0;
    }

    float cos_theta_t = sqrt(max(1.0 - sinThetaTSq, 0.0));
    float rs = (eta * cos_theta_t - cos_theta_i) / (eta * cos_theta_t + cos_theta_i);
    float rp = (eta * cos_theta_i - cos_theta_t) / (eta * cos_theta_i + cos_theta_t);

    return 0.5f * (rs * rs + rp * rp);
}

float gtr1(float ndh, float a) {
    if (a >= 1.0) {
        return (1.0 / PI);
    }

    float a2 = a * a;
    float t = 1.0 + (a2 - 1.0) * ndh * ndh;
    return (a2 - 1.0) / (PI * log(a2) * t);
}

float gtr2(float ndh, float a) {
    float a2 = a * a;
    float t = 1.0 + (a2 - 1.0) * ndh * ndh;
    return a2 / (PI * t * t);
}

float smithg_ggx(float ndv, float alpha_g) {
    float a = alpha_g * alpha_g;
    float b = ndv * ndv;
    return 1.0 / (ndv + sqrt(a + b - a * b));
}

vec3 disney_tint(Material mat) {
    float luminance = dot(vec3(0.3, 0.6, 0.1), mat.albedo);
    vec3 tint = luminance > 0.0 ? mat.albedo / luminance : vec3(1.0f);
    return tint;
}

vec3 disney_reflection(Material mat, float ndl, float ndh, float ndv, float vdh, inout float pdf) {
    pdf = 0.0;
    if (ndl <= 0.0) {
		return vec3(0.0);
    }

    float F = dielectric_fresnel(vdh, mat.ior);
    float D = gtr2(ndh, mat.roughness);

    pdf = D * ndh * F / (4.0 * abs(vdh));

    float G = smithg_ggx(abs(ndl), mat.roughness) * smithg_ggx(abs(ndv), mat.roughness);
    return mat.albedo * F * D * G;
}

vec3 disney_refraction(Material mat, float ndl, float ndh, float ndv, float vdh, float ldh, inout float pdf) {
    pdf = 0.0;
    if (ndl >= 0.0) {
        return vec3(0.0);
    }

    float F = dielectric_fresnel(abs(vdh), mat.ior);
    float D = gtr2(ndh, mat.roughness);

    float denomSqrt = ldh + vdh * mat.ior;
    pdf = D * ndh * (1.0 - F) * abs(ldh) / (denomSqrt * denomSqrt);

    float G = smithg_ggx(abs(ndl), mat.roughness) * smithg_ggx(abs(ndv), mat.roughness);
    return mat.albedo * (1.0 - F) * D * G * abs(vdh) * abs(ldh) * 4.0 * mat.ior * mat.ior / (denomSqrt * denomSqrt);
}

vec3 disney_specular(Material mat, vec3 tint, float fh, float ndl, float ndh, float ndv, float vdh, float ldh, inout float pdf) {
    pdf = 0.0;
    if (ndl <= 0.0) {
        return vec3(0.0);
    }

    vec3 c = mix(mat.specular * 0.08 * mix(vec3(1.0), tint, mat.specular_tint), mat.albedo, mat.metallic);

    float D = gtr2(ndh, mat.roughness);
    pdf = D * ndh / (4.0 * vdh);

    vec3 F = mix(c, vec3(1.0), fh);
    float G = smithg_ggx(abs(ndl), mat.roughness) * smithg_ggx(abs(ndv), mat.roughness);
    return F * D * G;
}

vec3 disney_clearcoat(Material mat, float fh, float ndl, float ndh, float ndv, float vdh, inout float pdf) {
    pdf = 0.0;
    if (ndl <= 0.0) {
        return vec3(0.0);
    }

    float D = gtr1(ndh, mix(0.1, 0.001, mat.clearcoat_gloss));
    pdf = D * ndh / (4.0 * vdh);

    float F = mix(0.04, 1.0, fh);
    float G = smithg_ggx(ndl, 0.25) * smithg_ggx(ndv, 0.25);
    return vec3(0.25 * mat.clearcoat * F * D * G);
}

vec3 disney_diffuse(Material mat, vec3 tint, float fh, float ndl, float ndv, float ldh, inout float pdf) {
    pdf = 0.0;
    if (ndl <= 0.0) {
        return vec3(0.0);
    }

    pdf = ndl * (1.0 / PI);

    // Diffuse
    float FL = schlick_fresnel(ndl);
    float FV = schlick_fresnel(ndv);
    float Fd90 = 0.5 + 2.0 * ldh * ldh * mat.roughness;
    float Fd = mix(1.0, Fd90, FL) * mix(1.0, Fd90, FV);

    // Fake Subsurface TODO: Replace with volumetric scattering
    float Fss90 = ldh * ldh * mat.roughness;
    float Fss = mix(1.0, Fss90, FL) * mix(1.0, Fss90, FV);
    float ss = 1.25 * (Fss * (1.0 / (ndl + ndv) - 0.5) + 0.5);

    vec3 sheen = mix(vec3(1.0), tint, mat.sheen_tint);
    vec3 Fsheen = fh * mat.sheen * sheen;
    return ((1.0 / PI) * mix(Fd, ss, mat.subsurface) * mat.albedo + Fsheen) * (1.0 - mat.metallic);
}

vec3 eval_brdf(Material mat, vec3 L, vec3 V, vec3 N, inout float pdf) {
    vec3 H;
    bool refl = dot(N, L) > 0.0;

    if (refl) {
        H = normalize(L + V);
    } else {
        H = normalize(L + V * mat.ior);
    }

    if (dot(V, H) < 0.0) {
        H = -H;
    }

    float ndl = dot(N, L);
    float ndv = dot(N, V);
    float ndh = dot(N, H);
    float vdh = dot(V, H);
    float ldh = dot(L, H);

    float diffuseRatio = 0.5 * (1.0 - mat.metallic);
    float primarySpecRatio = 1.0 / (1.0 + mat.clearcoat);
    float transWeight = (1.0 - mat.metallic) * mat.transmission;

    vec3 brdf = vec3(0.0);
    vec3 bsdf = vec3(0.0);
    float brdf_pdf = 0.0;
    float bsdf_pdf = 0.0;

    if (transWeight > 0.0) {
        if (refl) {
            bsdf = disney_reflection(mat, ndl, ndh, ndv, vdh, bsdf_pdf);
        } else {
            bsdf = disney_refraction(mat, ndl, ndh, ndv, vdh, ldh, bsdf_pdf);
        }
    }

    float m_pdf;
    if (transWeight < 1.0) {

        vec3 tint = disney_tint(mat);
        float fh = schlick_fresnel(ldh);

        brdf += disney_diffuse(mat, tint, fh, ndl, ndv, ldh, m_pdf);
        brdf_pdf += m_pdf * diffuseRatio;

        brdf += disney_specular(mat, tint, fh, ndl, ndh, ndv, vdh, ldh, m_pdf);
        brdf_pdf += m_pdf * primarySpecRatio * (1.0 - diffuseRatio);

        brdf += disney_clearcoat(mat, fh, ndl, ndh, ndv, vdh, m_pdf);
        brdf_pdf += m_pdf * (1.0 - primarySpecRatio) * (1.0 - diffuseRatio);
    }

    pdf = mix(brdf_pdf, bsdf_pdf, transWeight);
    return mix(brdf, bsdf, transWeight);
}

BrdfSample sample_brdf(inout Rng rng, Material mat, vec3 V, vec3 N) {
    float t = random_uniform(rng);
    float r1 = random_uniform(rng);
    float r2 = random_uniform(rng);

    float transWeight = (1.0 - mat.metallic) * mat.transmission;

    BrdfSample brdf;
    if (t < transWeight) {
        vec3 H = sample_hemisphere_gtr2(N, r1, r2, mat.roughness);

        if (dot(V, H) < 0.0) {
            H = -H;
        }

        vec3 R = reflect(-V, H);
        float F = dielectric_fresnel(abs(dot(R, H)), mat.ior);

        // Scale the remainder of the random variable to range [0, 1]
        t = t / transWeight;


        if (t < F) {
            vec3 L = normalize(R);

            float ndl = dot(N, L);
            float ndh = dot(N, H);
            float ndv = dot(N, V);
            float vdh = dot(V, H);

            brdf.light_dir = L;
            brdf.color = disney_reflection(mat, ndl, ndh, ndv, vdh, brdf.pdf);
            brdf.type = SurfaceTypeReflection;
        } else {
            vec3 L = normalize(refract(-V, H, mat.ior));

            float ndl = dot(N, L);
            float ndh = dot(N, H);
            float ndv = dot(N, V);
            float vdh = dot(V, H);
            float ldh = dot(L, H);

            brdf.light_dir = L;
            brdf.color = disney_refraction(mat, ndl, ndh, ndv, vdh, ldh, brdf.pdf);
            brdf.type = SurfaceTypeRefraction;
        }

        brdf.color *= transWeight;
        brdf.pdf *= transWeight;
        return brdf;
    }

    // Scale the remainder of the random variable to range [0, 1]
    t = (1 - t) / (1 - transWeight);

    float diffuseRatio = 0.5 * (1.0 - mat.metallic);
    if (t < diffuseRatio) {
        vec3 L = sample_hemisphere_cosine(N, r1, r2);
        vec3 H = normalize(L + V);

        float ndl = dot(N, L);
        float ndv = dot(N, V);
        float ldh = dot(L, H);
        float fh = schlick_fresnel(ldh);
        vec3 tint = disney_tint(mat);

        brdf.light_dir = L;
        brdf.color = disney_diffuse(mat, tint, fh, ndl, ndv, ldh, brdf.pdf);
        brdf.pdf *= diffuseRatio;
        brdf.color *= (1.0 - transWeight);
        brdf.pdf *= (1.0 - transWeight);
        brdf.type = SurfaceTypeDiffuse;
        return brdf;
    }

    // Scale the remainder of the random variable to range [0, 1]
    t = (1 - t) / (1 - diffuseRatio);

    float primarySpecRatio = 1.0 / (1.0 + mat.clearcoat);
    if (t < primarySpecRatio) {
        vec3 H = sample_hemisphere_gtr2(N, mat.roughness, r1, r2);

        if (dot(V, H) < 0.0)  {
            H = -H;
        }

        vec3 L = normalize(reflect(-V, H));

        float ndl = dot(N, L);
        float ndh = dot(N, H);
        float ndv = dot(N, V);
        float vdh = dot(V, H);
        float ldh = dot(L, H);
        float fh = schlick_fresnel(ldh);
        vec3 tint = disney_tint(mat);

        brdf.light_dir = L;
        brdf.color = disney_specular(mat, tint, fh, ndl, ndh, ndv, vdh, ldh, brdf.pdf);
        brdf.pdf *= primarySpecRatio * (1.0 - diffuseRatio);
        brdf.color *= (1.0 - transWeight);
        brdf.pdf *= (1.0 - transWeight);
        brdf.type = dot(N, H) >= REFLECTION_THRESHOLD ? SurfaceTypeReflection : SurfaceTypeDiffuse;
        return brdf;
    }

    float roughness = mix(0.1, 0.001, mat.clearcoat_gloss);
    vec3 H = sample_hemisphere_gtr1(N, roughness, r1, r2);

    if (dot(V, H) < 0.0) {
        H = -H;
    }

    vec3 L = normalize(reflect(-V, H));

    float ndl = dot(N, L);
    float ndv = dot(N, V);
    float ndh = dot(N, H);
    float vdh = dot(V, H);
    float ldh = dot(L, H);
    float fh = schlick_fresnel(ldh);

    brdf.light_dir = L;
    brdf.color = disney_clearcoat(mat, fh, ndl, ndh, ndv, vdh, brdf.pdf);
    brdf.pdf *= (1.0 - primarySpecRatio) * (1.0 - diffuseRatio);
    brdf.color *= (1.0 - transWeight);
    brdf.pdf *= (1.0 - transWeight);
    brdf.type = dot(N, H) >= REFLECTION_THRESHOLD ? SurfaceTypeReflection : SurfaceTypeDiffuse;

    return brdf;
}

#endif // VALO_BRDF_DISNEY_GLSL
