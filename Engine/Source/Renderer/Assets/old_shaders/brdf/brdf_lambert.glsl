#ifndef VALO_BRDF_LAMBERT_GLSL
#define VALO_BRDF_LAMBERT_GLSL

#include "../core/material.glsl"
#include "../sampling/sampler.glsl"
#include "brdf_sample.glsl"

// BRDF color from predefined light direction
vec3 eval_brdf(Material mat, vec3 l, vec3 v, vec3 n, inout float pdf) {
#ifdef IMPORTANCE_SAMPLE_BRDF
    pdf = dot(l, n) * INV_PI;
#else
    pdf = 0.5 * INV_PI;
#endif
    return mat.albedo.rgb * INV_PI;
}

// BRDF sampling with light direction generation
BrdfSample sample_brdf(inout Rng rng, Material mat, vec3 v, vec3 n) {
    float r1 = random_uniform(rng);
    float r2 = random_uniform(rng);

    BrdfSample brdf;
#ifdef IMPORTANCE_SAMPLE_BRDF
    brdf.light_dir = sample_hemisphere_cosine(n, r1, r2);
    brdf.color = eval_brdf(mat, brdf.light_dir, v, n, brdf.pdf);
    brdf.type = SurfaceTypeDiffuse;
#else
    brdf.light_dir = sample_hemisphere_uniform(n, r1, r2);
    brdf.color = eval_brdf(mat, brdf.light_dir, v, n, brdf.pdf);
    brdf.type = SurfaceTypeDiffuse;
#endif
    return brdf;
}

#endif // VALO_BRDF_LAMBERT_GLSL
