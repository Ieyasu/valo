#ifndef VALO_BRDF_SAMPLE_GLSL
#define VALO_BRDF_SAMPLE_GLSL

#include "../core/math.glsl"

const uint SurfaceTypeDiffuse = 0;
const uint SurfaceTypeReflection = 1;
const uint SurfaceTypeRefraction = 2;

struct BrdfSample {
    vec3 color;
    float pdf;
    vec3 light_dir;
    uint type;
};

#endif // VALO_BRDF_SAMPLE_GLSL
