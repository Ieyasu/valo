#ifndef VALO_BRDF_GLSL
#define VALO_BRDF_GLSL

#include "../core/constants.glsl"
#include "../core/material.glsl"

#if defined(BRDF_DISNEY)
    #include "brdf_disney.glsl"
#else
    #include "brdf_lambert.glsl"
#endif

#endif // VALO_BRDF_GLSL
