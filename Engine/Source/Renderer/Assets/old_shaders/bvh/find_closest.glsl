#ifndef VALO_BVH_FIND_CLOSEST_GLSL
#define VALO_BVH_FIND_CLOSEST_GLSL

#include "bvh_node.glsl"
#include "../core/hit.glsl"
#include "../core/ray.glsl"
#include "../core/intersect.glsl"
#include "../core/object_data.glsl"

void traverse_blas_leaf(Ray ray, uint entity_idx, BvhNode node, inout Hit hit) {
    for (uint i = node.index1; i < node.index2; ++i)
    {
        uvec3 indices = read_triangle(i);
        vec3 v0 = read_vertex(indices.x);
        vec3 v1 = read_vertex(indices.y);
        vec3 v2 = read_vertex(indices.z);

        if (intersect_ray_triangle(ray, v0, v1, v2, hit.coord, hit.t)) {
            hit.type = HitTypeObject;
            hit.primitive_idx = i;
            hit.entity_idx = entity_idx;
        }
    }
}

void traverse_blas(Ray ray, uint entity_idx, inout Hit hit) {
    // Extract the entity's BVH node range from the empty elements of the world to local matrix
    mat4 world_to_local = _entity_world_to_local[entity_idx];
    uint blas_start_idx = uint(world_to_local[0][3]);
    uint blas_end_idx = uint(world_to_local[1][3]);
    world_to_local[0][3] = 0;
    world_to_local[1][3] = 0;

    // Transform ray to objects local space as the BLAS is aligned according to the local space
    ray.origin = (world_to_local * vec4(ray.origin, 1)).xyz;
    ray.direction = mat3(world_to_local) * ray.direction;

    // Normalize ray direction so that we don't run into floating point issues
    // when doing intersection tests. We also need to scale t to local scale.
    float ray_len = length(ray.direction);
    float inv_ray_len = 1.0 / ray_len;
    ray.direction *= inv_ray_len;
    hit.t *= ray_len;

    for (uint i = blas_start_idx; i < blas_end_idx;) {
        BvhNode node = _blas_nodes[i];

        float aabb_t = hit.t;
        if (!intersect_ray_aabb(ray, node.aabb_min, node.aabb_max, aabb_t)) {
            i = node.miss_node;
        } else {
            i = node.hit_node;

            if (is_leaf(node)) {
                #ifdef DEBUG_DRAW_BLAS_BVH
                    hit.type = HitTypeBvh;
                    hit.primitive_idx = i;
                    hit.t = aabb_t;
                #else
                    traverse_blas_leaf(ray, entity_idx, node, hit);
                #endif
            }
        }
    }

    // Restore t value to world scale
    hit.t *= inv_ray_len;
}

void traverse_tlas_leaf(Ray ray, BvhNode leaf, inout Hit hit) {
    if (leaf.data == TLASLeafTypeMesh) {
        traverse_blas(ray, leaf.index1, hit);
    } else if (leaf.data == TLASLeafTypeLight) {
        Light light = _lights[leaf.index1];
        if (intersect_ray_light(ray, light, hit.t)) {
            hit.type = HitTypeLight;
            hit.primitive_idx = leaf.index1;
        }
    }
}

void traverse_tlas(Ray ray, inout Hit hit) {
    hit.type = HitTypeEnvironment;
    hit.uv = vec2(0);
    hit.normal = vec3(0, 0, 1);

    for (uint i = 0; i < _tlas_nodes.length();) {
        BvhNode node = _tlas_nodes[i];

        float aabb_t = hit.t;
        if (!intersect_ray_aabb(ray, node.aabb_min, node.aabb_max, aabb_t)) {
            i = node.miss_node;
        } else {
            i = node.hit_node;

            if (is_leaf(node)) {
                #ifdef DEBUG_DRAW_TLAS_BVH
                    hit.type = HitTypeBvh;
                    hit.primitive_idx = i;
                    hit.t = aabb_t;
                #else
                    traverse_tlas_leaf(ray, node, hit);
                #endif
            }
        }
    }

    hit.point = ray.origin + ray.direction * hit.t;

    if (hit.type == HitTypeObject) {
        // Find surface normal and point
        mat4 local_to_world = _entity_local_to_world[hit.entity_idx];
        hit.material_idx = uint(local_to_world[0][3]);

        uvec3 triangle = read_triangle(hit.primitive_idx);
        vec3 normal = read_interpolated_normal(hit.primitive_idx, hit.coord);

        #ifdef TEXTURES
            hit.uv = read_interpolated_uv(hit.primitive_idx, hit.coord);
            hit.uv.y = 1 - hit.uv.y;
        #endif

        #ifdef NORMAL_TEXTURES
            // Read the tangent space normal and transfer to object space
            Material mat = _materials[hit.material_idx];
            if (mat.normal_texture_idx != INVALID_INDEX) {
                vec4 tangent = read_interpolated_tangent(hit.primitive_idx, hit.coord);
                vec3 bitangent = tangent.w * cross(normal, tangent.xyz);
                vec3 nt = read_normal_texture(mat.normal_texture_idx, hit.uv);
                nt.xy = (2 * nt.xy - 1) * mat.normal_scale;
                nt.z = sqrt(1 - min(dot(nt.xy, nt.xy), 1));
                normal = normalize(nt.x * tangent.xyz + nt.y * bitangent + nt.z * normal);
            }
        #endif

        mat3 normal_matrix = transpose(inverse(mat3(local_to_world)));
        hit.normal = normalize(normal_matrix * normal);
    } else if (hit.type == HitTypeLight) {
        hit.normal = eval_light_normal(_lights[hit.primitive_idx], hit.point);
    }
}

#endif // VALO_BVH_FIND_CLOSEST_GLSL
