#ifndef VALO_BVH_FIND_ANY_GLSL
#define VALO_BVH_FIND_ANY_GLSL

#include "bvh_node.glsl"
#include "../core/hit.glsl"
#include "../core/ray.glsl"
#include "../core/intersect.glsl"
#include "../core/object_data.glsl"

bool traverse_blas_leaf(Ray ray, BvhNode node, float t) {
    for (uint i = node.index1; i < node.index2; ++i)
    {
        uvec3 indices = read_triangle(i);
        vec3 v0 = read_vertex(indices.x);
        vec3 v1 = read_vertex(indices.y);
        vec3 v2 = read_vertex(indices.z);
        float tri_t = t;

        if (intersect_ray_triangle(ray, v0, v1, v2, tri_t)) {
            return true;
        }
    }

    return false;
}

bool traverse_blas(Ray ray, uint entity_idx, float t) {
    // Extract the entity's BVH node range from the empty elements of the world to local matrix
    mat4 world_to_local = _entity_world_to_local[entity_idx];
    uint blas_start_idx = uint(world_to_local[0][3]);
    uint blas_end_idx = uint(world_to_local[1][3]);
    world_to_local[0][3] = 0;
    world_to_local[1][3] = 0;

    // Transform ray to objects local space as the BLAS is aligned according to the local space
    ray.origin = (world_to_local * vec4(ray.origin, 1)).xyz;
    ray.direction = mat3(world_to_local) * ray.direction;

    // Normalize ray direction so that we don't run into floating point issues
    // when doing intersection tests. We also need to scale t to local scale.
    float ray_len = length(ray.direction);
    float inv_ray_len = 1.0 / ray_len;
    ray.direction *= inv_ray_len;
    t *= ray_len;

    for (uint i = blas_start_idx; i < blas_end_idx;) {
        BvhNode node = _blas_nodes[i];

        float aabb_t = t;
        if (!intersect_ray_aabb(ray, node.aabb_min, node.aabb_max, aabb_t)) {
            i = node.miss_node;
        } else {
            i = node.hit_node;

            if (is_leaf(node) && traverse_blas_leaf(ray, node, t)) {
                return true;
            }
        }
    }

    return false;
}

bool traverse_tlas_leaf(Ray ray, BvhNode leaf, float t) {
    if (leaf.data == TLASLeafTypeMesh) {
        return traverse_blas(ray, leaf.index1, t);
    } else if (leaf.data == TLASLeafTypeLight) {
        return intersect_ray_light(ray, _lights[leaf.index1], t);
    }
}

bool traverse_tlas(Ray ray, float t) {
    for (uint i = 0; i < _tlas_nodes.length();) {
        BvhNode node = _tlas_nodes[i];

        float aabb_t = t;
        if (!intersect_ray_aabb(ray, node.aabb_min, node.aabb_max, aabb_t)) {
            i = node.miss_node;
        } else {
            i = node.hit_node;

            if (is_leaf(node) && traverse_tlas_leaf(ray, node, t)) {
                return true;
            }
        }
    }

    return false;
}

#endif // VALO_BVH_FIND_ANY_GLSL
