#ifndef VALO_BVH_NODE_GLSL
#define VALO_BVH_NODE_GLSL

const uint TLASLeafTypeMesh = 0;
const uint TLASLeafTypeLight = 1;

struct BvhNode {
    vec3 aabb_min;
    uint index1;
    vec3 aabb_max;
    uint index2;
    uint hit_node;
    uint miss_node;
    uint data;
    bool is_leaf;
};

bool is_leaf(BvhNode node) {
    return node.is_leaf;
}

#endif // VALO_BVH_NODE_GLSL
