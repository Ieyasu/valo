#ifndef VALO_BVH_GLSL
#define VALO_BVH_GLSL

#include "bvh_node.glsl"

// Top level acceleration structure contains the BVH for all the objects in the scene
layout(set = 0, binding = 40, std430) readonly buffer TLASBuffer { BvhNode _tlas_nodes[]; };

// Each mesh has it's own bottom level acceleration structure in local space coordinates
layout(set = 0, binding = 41, std430) readonly buffer BLASBuffer { BvhNode _blas_nodes[]; };

#include "find_any.glsl"
#include "find_closest.glsl"

#endif // VALO_BVH_GLSL
