#ifndef VALO_UTIL_GLSL
#define VALO_UTIL_GLSL

bool out_of_bounds(ivec2 coord, uvec2 image_size) {
    return coord.x < 0 || coord.y < 0 || coord.x >= image_size.x || coord.y >= image_size.y;
}

bool out_of_bounds(uvec2 coord, uvec2 image_size) {
    return coord.x >= image_size.x || coord.y >= image_size.y;
}

bool out_of_bounds(ivec2 coord, vec2 image_size) {
    return coord.x < 0 || coord.y < 0 || coord.x >= image_size.x || coord.y >= image_size.y;
}

bool out_of_bounds(uvec2 coord, vec2 image_size) {
    return coord.x >= image_size.x || coord.y >= image_size.y;
}

#endif //VALO_UTIL_GLSL
