#ifndef VALO_MESH_DATA_GLSL
#define VALO_MESH_DATA_GLSL

#include "light.glsl"
#include "material.glsl"

// Object transforms

layout(set = 0, binding = 10, std430) readonly buffer ObjectWorldToLocalBuffer { mat4 _entity_world_to_local[]; };
layout(set = 0, binding = 11, std430) readonly buffer ObjectLocalToWorldBuffer { mat4 _entity_local_to_world[]; };

// Mesh data

layout(set = 0, binding = 12) readonly buffer MeshIndexBuffer { uint _mesh_indices[]; };
layout(set = 0, binding = 13) readonly buffer MeshVertexBuffer { float _mesh_vertices[]; };
layout(set = 0, binding = 14) readonly buffer MeshNormalBuffer { float _mesh_normals[]; };

#ifdef NORMAL_TEXTURES
    layout(set = 0, binding = 15) readonly buffer MeshTangentBuffer { float _mesh_tangents[]; };
#endif

#ifdef TEXTURES
    layout(set = 0, binding = 16) readonly buffer MeshUVBuffer { float _mesh_uvs[]; };
#endif

// Object data

layout(set = 0, binding = 17, std430) readonly buffer LightBuffer { Light _lights[]; };
layout(set = 0, binding = 18, std430) readonly buffer MaterialBuffer { Material _materials[]; };

// Texture data

#ifdef ALBEDO_TEXTURES
    layout(set = 0, binding = 20) uniform sampler2DArray _albedo_textures;
#endif

#ifdef NORMAL_TEXTURES
    layout(set = 0, binding = 21) uniform sampler2DArray _normal_textures;
#endif

#ifdef MATERIAL_TEXTURES
    layout(set = 0, binding = 22) uniform sampler2DArray _material_textures;
#endif

// Returns the triangle with the given index
uvec3 read_triangle(uint index) {
    uvec3 triangle;
    triangle.x = _mesh_indices[3 * index];
    triangle.y = _mesh_indices[3 * index + 1];
    triangle.z = _mesh_indices[3 * index + 2];
    return triangle;
}

// Returns the vertex in the given mesh index
vec3 read_vertex(uint index) {
    vec3 vertex;
    vertex.x = _mesh_vertices[3 * index];
    vertex.y = _mesh_vertices[3 * index + 1];
    vertex.z = _mesh_vertices[3 * index + 2];
    return vertex;
}

// Returns the normal in the given mesh index
vec3 read_normal(uint index) {
    vec3 normal;
    normal.x = _mesh_normals[3 * index];
    normal.y = _mesh_normals[3 * index + 1];
    normal.z = _mesh_normals[3 * index + 2];
    return normal;
}

// Returns the interpolated normal of the triangle with the given index.
// The normal is interpolated using the given barycentric coordinates.
vec3 read_interpolated_normal(uint tri_index, vec3 bary) {
    uvec3 triangle = read_triangle(tri_index);
    vec3 n0 = read_normal(triangle.x);
    vec3 n1 = read_normal(triangle.y);
    vec3 n2 = read_normal(triangle.z);
    return normalize(bary.z * n0 + bary.x * n1 + bary.y * n2);
}

#ifdef TEXTURES
// Returns the UV in the given mesh index
vec2 read_uv(uint index) {
    vec2 uv;
    uv.x = _mesh_uvs[2 * index];
    uv.y = _mesh_uvs[2 * index + 1];
    return uv;
}

// Returns the interpolated UV of the triangle with the given index.
// The UV is interpolated using the given barycentric coordinates.
vec2 read_interpolated_uv(uint tri_index, vec3 bary) {
    uvec3 triangle = read_triangle(tri_index);
    vec2 uv0 = read_uv(triangle.x);
    vec2 uv1 = read_uv(triangle.y);
    vec2 uv2 = read_uv(triangle.z);
    return bary.z * uv0 + bary.x * uv1 + bary.y * uv2;
}
#endif

#ifdef NORMAL_TEXTURES
// Returns the normal in the given mesh index
vec4 read_tangent(uint index) {
    vec4 tangent;
    tangent.x = _mesh_tangents[4 * index];
    tangent.y = _mesh_tangents[4 * index + 1];
    tangent.z = _mesh_tangents[4 * index + 2];
    tangent.w = _mesh_tangents[4 * index + 3];
    return tangent;
}

// Returns the interpolated tangent of the triangle with the given index.
// The tangent is interpolated using the given barycentric coordinates.
vec4 read_interpolated_tangent(uint tri_index, vec3 bary) {
    uvec3 triangle = read_triangle(tri_index);
    vec4 t0 = read_tangent(triangle.x);
    vec4 t1 = read_tangent(triangle.y);
    vec4 t2 = read_tangent(triangle.z);
    return normalize(bary.z * t0 + bary.x * t1 + bary.y * t2);
}
#endif

#ifdef ALBEDO_TEXTURES
// Returns the albedo from the albedo texture with the given index.
vec3 read_albedo_texture(uint tex_index, vec2 uv) {
    return texture(_albedo_textures, vec3(uv, tex_index)).rgb;
}
#endif

#ifdef NORMAL_TEXTURES
// Returns the normal from the normal texture with the given index.
vec3 read_normal_texture(uint tex_index, vec2 uv) {
    return texture(_normal_textures, vec3(uv, tex_index)).xyz;
}
#endif

#ifdef MATERIAL_TEXTURES
// Returns the material properties from the material texture with the given index.
vec4 read_material_texture(uint tex_index, vec2 uv) {
    return texture(_material_textures, vec3(uv, tex_index));
}
#endif

// Returns the material with the given index, modified based on
// whether it represents the inside or outside of a solid object.
Material read_material(uint material_idx, vec2 uv, bool inside) {
    Material mat = _materials[material_idx];

#ifdef ALBEDO_TEXTURES
    if (mat.albedo_texture_idx != INVALID_INDEX) {
        vec3 albedo_tex = read_albedo_texture(mat.albedo_texture_idx, uv);
        mat.albedo *= albedo_tex;
    }
#endif

#if defined(MATERIAL_TEXTURES)
    if (mat.material_texture_idx != INVALID_INDEX) {
        vec4 material_tex = read_material_texture(mat.material_texture_idx, uv);
        mat.metallic = material_tex.r;
        mat.occlusion = material_tex.g;
        mat.roughness = 1 - material_tex.a;
    }
#endif

    // Roughness at zero does bad things in disney brdf
    mat.roughness = max(mat.roughness, 0.001);

    // Flip ior if we are inside a material
    if (!inside) {
        mat.ior = 1.0 / mat.ior;
    }

    return mat;
}

#endif // VALO_MESH_DATA_GLSL
