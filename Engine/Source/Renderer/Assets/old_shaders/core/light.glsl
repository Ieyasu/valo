#ifndef VALO_LIGHT_GLSL
#define VALO_LIGHT_GLSL

#include "../sampling/sampler.glsl"

const uint LightTypeRect = 0;
const uint LightTypeDisk = 1;
const uint LightTypeSphere = 2;

struct Light {
    vec3 position;
    uint type;
    vec3 emission;
    float area;
    vec3 u;
    float radius;
    vec3 v;
    float _padding;
};

struct LightSample {
    vec3 point;
    float pdf;
    vec3 normal;
    float distance;
    vec3 direction;
};

vec3 eval_rect_light_normal(Light light, vec3 point) {
    return normalize(cross(light.u, light.v));
}

vec3 eval_disk_light_normal(Light light, vec3 point) {
    return light.u;
}

vec3 eval_sphere_light_normal(Light light, vec3 point) {
    return normalize(point - light.position);
}

vec3 eval_light_normal(Light light, vec3 point) {
    if (light.type == LightTypeRect) {
        return eval_rect_light_normal(light, point);
    } else if (light.type == LightTypeDisk) {
        return eval_disk_light_normal(light, point);
    } else if (light.type == LightTypeSphere) {
        return eval_sphere_light_normal(light, point);
    }
    return vec3(0, 0, 1);
}

float eval_rect_light_pdf(Light light, vec3 light_dir, float t) {
    vec3 normal = normalize(cross(light.u, light.v));
    #ifdef TWO_SIDED_AREA_LIGHTS
        float cosine = abs(dot(normal, -light_dir));
    #else
        float cosine = dot(normal, -light_dir);
    #endif

    if (cosine <= 0) {
        return 0;
    }

    return t * t / (light.area * cosine);
}

float eval_disk_light_pdf(Light light, vec3 light_dir, float t) {
    vec3 normal = light.u;
    #ifdef TWO_SIDED_AREA_LIGHTS
        float cosine = abs(dot(normal, -light_dir));
    #else
        float cosine = dot(normal, -light_dir);
    #endif

    if (cosine <= 0) {
        return 0;
    }

    return t * t / (light.area * cosine);
}

float eval_sphere_light_pdf(Light light, vec3 light_dir, float t) {
    return t * t / (0.25 * light.area);
}

float eval_light_pdf(Light light, vec3 light_dir, float t) {
    if (light.type == LightTypeRect) {
        return eval_rect_light_pdf(light, light_dir, t);
    } else if (light.type == LightTypeDisk) {
        return eval_disk_light_pdf(light, light_dir, t);
    } else if (light.type == LightTypeSphere) {
        return eval_sphere_light_pdf(light, light_dir, t);
    }
    return 0;
}

LightSample sample_rect_light(Light light, vec3 surf_point, float r1, float r2) {
    // Calculate point on the light
    LightSample result;
    result.point = light.position + r1 * light.u + r2 * light.v;
    result.normal = normalize(cross(light.u, light.v));

    // Calculate the direction of the light from surface to the light
    vec3 light_dir = result.point - surf_point;
    result.distance = length(light_dir);
    if (result.distance <= 0) {
        result.pdf = 0;
        return result;
    }
    result.direction = light_dir / result.distance;

    // Calculate area weighted pdf
    float cosine = dot(result.normal, -result.direction);
    if (cosine <= 0) {
#ifdef TWO_SIDED_AREA_LIGHTS
        // Flip normal so that it faces the view direction
        result.normal = -result.normal;
        cosine = -cosine;
#else
        result.pdf = 0;
        return result;
#endif
    }
    result.pdf = result.distance * result.distance / (light.area * cosine);

    return result;
}

LightSample sample_disk_light(Light light, vec3 surf_point, float r1, float r2) {
    vec2 rand = light.radius * sample_unit_circle(r1, r2);
    vec3 normal = light.u;
    vec3 tangent = light.v;
    vec3 bitangent = normalize(cross(light.u, light.v));

    // Calculate point on the light
    LightSample result;
    result.point = light.position + rand.x * tangent + rand.y * bitangent;
    result.normal = normal;

    // Calculate the direction of the light from surface to the light
    vec3 light_dir = result.point - surf_point;
    result.distance = length(light_dir);
    if (result.distance <= 0) {
        result.pdf = 0;
        return result;
    }
    result.direction = light_dir / result.distance;

    // Calculate area weighted pdf
    float cosine = dot(result.normal, -result.direction);
    if (cosine <= 0) {
#ifdef TWO_SIDED_AREA_LIGHTS
        // Flip normal so that it faces the view direction
        result.normal = -result.normal;
        cosine = -cosine;
#else
        result.pdf = 0;
        return result;
#endif
    }
    result.pdf = result.distance * result.distance / (light.area * cosine);

    return result;
}

LightSample sample_sphere_light(Light light, vec3 surf_point, float r1, float r2) {
    vec3 light_to_point = normalize(surf_point - light.position);
    vec3 normal = sample_hemisphere_uniform(light_to_point, r1, r2);
    vec3 offset = light.radius * normal;

    LightSample result;
    result.point = light.position + offset;
    result.normal = normal;

    vec3 light_dir = result.point - surf_point;
    result.distance = length(light_dir);
    if (result.distance > 0) {
        result.pdf = result.distance * result.distance / (light.area * 0.25);
        result.direction = light_dir / result.distance;
    } else {
        result.pdf = 0;
    }

    return result;
}

LightSample sample_light(Light light, vec3 surf_point, float r1, float r2) {
    if (light.type == LightTypeRect) {
        return sample_rect_light(light, surf_point, r1, r2);
    } else if (light.type == LightTypeDisk) {
        return sample_disk_light(light, surf_point, r1, r2);
    } else if (light.type == LightTypeSphere) {
        return sample_sphere_light(light, surf_point, r1, r2);
    }

    LightSample result;
    result.pdf = 0;
    return result;
}

bool intersect_ray_light(Ray ray, Light light, inout float t) {
    if (light.type == LightTypeRect) {
        return intersect_ray_rect(ray, light.position, light.u, light.v, t);
    } else if (light.type == LightTypeDisk) {
        return intersect_ray_disk(ray, light.position, light.u, light.radius, t);
    } else if (light.type == LightTypeSphere) {
        return intersect_ray_sphere(ray, light.position, light.radius, t);
    }
    return false;
}

#endif // VALO_LIGHT_GLSL
