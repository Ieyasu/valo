#ifndef VALO_MATERIAL_GLSL
#define VALO_MATERIAL_GLSL

struct Material {
    vec3 albedo;
    float metallic;
    vec3 emission;
    float roughness;
    float subsurface;
    float specular;
    float specular_tint;
    float transmission;
    float sheen;
    float sheen_tint;
    float clearcoat;
    float clearcoat_gloss;
    float ior;
    float occlusion;
    float normal_scale;
    uint albedo_texture_idx;
    uint normal_texture_idx;
    uint material_texture_idx;
};

#endif // VALO_MATERIAL_GLSL
