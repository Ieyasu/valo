#ifndef VALO_CONSTANTS_GLSL
#define VALO_CONSTANTS_GLSL

// Parameters
const uint INVALID_INDEX = ~0;

// Epsilons
const float HIT_EPSILON = 0.001;
const float RAY_EPSILON = 0.001;
const float DIV_EPSILON = 0.000000001;

#define DEBUG_DRAW_NANS

// Lighting
// #define TWO_SIDED_AREA_LIGHTS
#define TWO_SIDED_TRIANGLES

// Firefly elimination
#define FIREFLY_ROUGHNESS_INCREASE

#endif // VALO_CONSTANTS_GLSL
