#ifndef VALO_INTERSECT_GLSL
#define VALO_INTERSECT_GLSL

#include "constants.glsl"
#include "math.glsl"
#include "ray.glsl"
#include "hit.glsl"

bool intersect_ray_rect(Ray ray, vec3 rect_pos, vec3 rect_u, vec3 rect_v, inout float t)
{
    vec3 rect_normal = normalize(cross(rect_u, rect_v));
    float det = dot(ray.direction, rect_normal);

#ifdef TWO_SIDED_AREA_LIGHTS
    if (abs(det) < DIV_EPSILON) {
        return false;
    }
#else
    if (det > -DIV_EPSILON) {
        return false;
    }
#endif

    // Intersect with plane
    float new_t = (dot(rect_normal, rect_pos) - dot(rect_normal, ray.origin)) / det;
    if (new_t > t || new_t < HIT_EPSILON) {
        return false;
    }

    // Check if resulting point falls within rectangle
    vec3 p = ray.origin + ray.direction * new_t;
    vec3 delta = p - rect_pos;

    rect_u *= 1.0f / dot(rect_u, rect_u);
    float du = dot(rect_u, delta);
    if (du < 0 || du > 1) {
        return false;
    }

    rect_v *= 1.0f / dot(rect_v, rect_v);
    float dv = dot(rect_v, delta);
    if (dv < 0 || dv > 1) {
        return false;
    }

    t = new_t;
    return true;
}

bool intersect_ray_disk(Ray ray, vec3 disk_pos, vec3 disk_normal, float disk_radius, inout float t) {
    float det = dot(ray.direction, disk_normal);

#ifdef TWO_SIDED_AREA_LIGHTS
    if (abs(det) < DIV_EPSILON) {
        return false;
    }
#else
    if (det > -DIV_EPSILON) {
        return false;
    }
#endif

    // Intersect with plane
    float new_t = (dot(disk_normal, disk_pos) - dot(disk_normal, ray.origin)) / det;
    if (new_t > t || new_t < HIT_EPSILON) {
        return false;
    }

    // Check if resulting point falls within disk
    vec3 p = ray.origin + ray.direction * new_t;
    vec3 delta = p - disk_pos;

    if (dot(delta, delta) > disk_radius * disk_radius) {
        return false;
    }

    t = new_t;
    return true;
}

bool intersect_ray_sphere(Ray ray, vec3 sphere_origin, float sphere_radius, inout float t) {
    vec3 oc = ray.origin - sphere_origin;
    float b = dot(oc, ray.direction);
    float c = dot(oc, oc) - sphere_radius * sphere_radius;

    // Origin outside of sphere and pointing away
    if (c > 0.0 && b > 0.0) {
        return false;
    }

    // Solve the second order quadratic
    // 0 solutions -> miss
    // 1 solutions -> ray grazing sphere
    // 2 solutions -> ray intersects sphere at two points
    float discriminant = b * b - c;
    if (discriminant < 0) {
        return false;
    }

    // Smallest value of t is the intersection
    float new_t = -b - sqrt(discriminant);
    if (new_t > t)
    {
        return false;
    }

    // Negative means ray is inside sphere
    if (new_t < HIT_EPSILON) {
        new_t = -b + sqrt(discriminant);
        if (new_t < HIT_EPSILON)
        {
            return false;
        }
    }

    t = new_t;
    return true;
}

bool intersect_ray_aabb(Ray ray, vec3 aabb_min, vec3 aabb_max, inout float t) {
    vec3 dirfrac = 1.0 / ray.direction;

    vec3 dmin = (aabb_min - ray.origin) * dirfrac;
    vec3 dmax = (aabb_max - ray.origin) * dirfrac;
    vec3 t0 = min(dmin, dmax);
    vec3 t1 = max(dmin, dmax);
    float tmin = max(max(t0.x, t0.y), t0.z);
    float tmax = min(min(t1.x, t1.y), t1.z);

    // AABB is behind camera
    if (tmax < HIT_EPSILON) {
        return false;
    }

    // Ray misses
    if (tmin > tmax) {
        return false;
    }

    if (tmin > t) {
        return false;
    }

    t = tmin;
    return true;
}

bool intersect_ray_triangle(Ray ray, vec3 v0, vec3 v1, vec3 v2, inout vec3 coord, inout float t) {
    vec3 edge0 = v1 - v0;
    vec3 edge1 = v2 - v0;
    vec3 pvec = cross(ray.direction, edge1);
    float det = dot(edge0, pvec);

#ifndef TWO_SIDED_TRIANGLES
    if (det < DIV_EPSILON) {
        return false;
    }
#endif

    float inv_det = 1.0 / det;
    vec3 tvec = ray.origin - v0;
    float u = dot(tvec, pvec) * inv_det;
    if (u < 0 || u > 1) {
        return false;
    }

    vec3 qvec = cross(tvec, edge0);
    float v = dot(ray.direction, qvec) * inv_det;
    if (v < 0 || u + v > 1) {
        return false;
    }

    float new_t = dot(edge1, qvec) * inv_det;
    if (new_t > t || new_t < HIT_EPSILON) {
        return false;
    }

    coord.x = u;
    coord.y = v;
    coord.z = (1 - coord.x - coord.y);
    t = new_t;
    return true;
}

bool intersect_ray_triangle(Ray ray, vec3 v0, vec3 v1, vec3 v2, inout float t) {
    vec3 coord;
    return intersect_ray_triangle(ray, v0, v1, v2, coord, t);
}

#endif // VALO_INTERSECT_GLSL
