#ifndef VALO_RAY_GLSL
#define VALO_RAY_GLSL

struct Ray {
    vec3 origin;
    vec3 direction;
};

#endif //VALO_RAY_GLSL
