#ifndef VALO_CAMERA_GLSL
#define VALO_CAMERA_GLSL

#include "ray.glsl"
#include "math.glsl"

struct Camera {
    vec3 right;
    float focal_length;
    vec3 up;
    float aperture_size;
    vec3 forward;
    float aspect;
    vec3 position;
    float fov_atan;
};

vec3 camera_position(Camera camera, vec2 screen_uv, float depth) {
    // 1. Transform the pixels to range [-1, 1]
    screen_uv = 1 - 2 * screen_uv;

    // Take into account aspect ratio and field of view
    vec2 pixel_camera = screen_uv * vec2(camera.aspect, 1) * camera.fov_atan;

    // Get the point on a camera plane that is 1.0 unit away from the camera position
    vec3 point_on_camera_plane = -camera.right * pixel_camera.x + camera.up * pixel_camera.y + camera.forward;

    return camera.position + depth * normalize(point_on_camera_plane);
}

Ray camera_ray(Camera camera, vec2 screen_uv) {

    // 1. Transform the pixels to range [-1, 1]
    screen_uv = 1 - 2 * screen_uv;

    // Take into account aspect ratio and field of view
    vec2 pixel_camera = screen_uv * vec2(camera.aspect, 1) * camera.fov_atan;

    // Get the point on a camera plane that is 1.0 unit away from the camera position
    vec3 point_on_camera_plane = -camera.right * pixel_camera.x + camera.up * pixel_camera.y + camera.forward;

    // Generate perpsective camera ray based on camera transform matrix
    Ray ray;
    ray.origin = camera.position;
    ray.direction = normalize(point_on_camera_plane);

    return ray;
}

Ray camera_ray_dof(Camera camera, vec2 screen_uv, float r1, float r2) {

    Ray ray = camera_ray(camera, screen_uv);

    // Depth of field (circular bokeh)
    vec3 focal_point = ray.origin + ray.direction * max(camera.focal_length, 0.01);

    float angle = r1 * TWO_PI;
    float radius = sqrt(r2) * camera.aperture_size;
    vec3 offset = (cos(angle) * camera.right + sin(angle) * camera.up) * radius;
    ray.origin += offset;
    ray.direction = normalize(focal_point - ray.origin);

    return ray;
}

#endif // VALO_CAMERA_GLSL
