#ifndef VALO_MATH_GLSL
#define VALO_MATH_GLSL

#include "constants.glsl"

const float PI = 3.14159265358979323846;
const float INV_PI = 1.0 / 3.14159265358979323846;
const float TWO_PI = 2.0 * PI;

float sqr(float x) {
    return x*x;
}

float deg_to_rad(float deg) {
    return deg * PI / 180.0;
}

float rad_to_deg(float rad) {
    return rad * 180.0 / PI;
}

float atan2(float y, float x)
{
  float t0, t1, t2, t3, t4;

  t3 = abs(x);
  t1 = abs(y);
  t0 = max(t3, t1);
  t1 = min(t3, t1);
  t3 = float(1) / t0;
  t3 = t1 * t3;

  t4 = t3 * t3;
  t0 =         - float(0.013480470);
  t0 = t0 * t4 + float(0.057477314);
  t0 = t0 * t4 - float(0.121239071);
  t0 = t0 * t4 + float(0.195635925);
  t0 = t0 * t4 - float(0.332994597);
  t0 = t0 * t4 + float(0.999995630);
  t3 = t0 * t3;

  t3 = (abs(y) > abs(x)) ? float(1.570796327) - t3 : t3;
  t3 = (x < 0) ?  float(3.141592654) - t3 : t3;
  t3 = (y < 0) ? -t3 : t3;

  return t3;
}

vec2 normalize_safe(vec2 v) {
    float len = length(v);
    return (len < DIV_EPSILON) ? vec2(0, 1) : v / len;
}

vec3 normalize_safe(vec3 v) {
    float len = length(v);
    return (len < DIV_EPSILON) ? vec3(0, 0, 1) : v / len;
}

vec4 normalize_safe(vec4 v) {
    float len = length(v);
    return (len < DIV_EPSILON) ? vec4(0, 0, 0, 1) : v / len;
}

#endif // VALO_MATH_GLSL
