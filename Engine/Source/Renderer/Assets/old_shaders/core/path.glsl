#ifndef VALO_PATH_GLSL
#define VALO_PATH_GLSL

#include "../brdf/brdf_sample.glsl"

struct Path {
    BrdfSample brdf_sample;
    Rng rng;
    vec3 radiance;
    uint bounce;
    vec3 throughput;
    float roughness_bias;
};

#endif // VALO_PATH_GLSL
