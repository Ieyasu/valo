#ifndef VALO_HIT_GLSL
#define VALO_HIT_GLSL

const uint HitTypeNone = 0;
const uint HitTypeEnvironment = 1;
const uint HitTypeLight = 2;
const uint HitTypeObject = 3;
const uint HitTypeBvh = 4;

struct Hit {
    vec3 point;
    uint primitive_idx;
    vec3 coord;
    uint entity_idx;
    vec3 normal;
    uint material_idx;
    vec2 uv;
    float t;
    uint type;
};

#endif // VALO_HIT_GLSL
