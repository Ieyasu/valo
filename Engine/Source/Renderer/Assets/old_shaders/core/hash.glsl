#ifndef VALO_HASH_GLSL
#define VALO_HASH_GLSL

#define HASH_PROSPECTOR

uint hash_xorshift(uint x) {
    x ^= x << 13;
    x ^= x >> 17;
    x ^= x << 5;
    return x;
}

uint hash_murmur3(uint x) {
    x ^= x >> 16;
    x *= 0x85ebca6bu;
    x ^= x >> 13;
    x *= 0xc2b2ae35u;
    x ^= x >> 16;
    return x;
}

uint hash_wang(uint x) {
    x = (x ^ 61) ^ (x >> 16);
    x *= 9;
    x = x ^ (x >> 4);
    x *= 0x27d4eb2d;
    x = x ^ (x >> 15);
    return x;
}

// https://github.com/skeeto/hash-prospector
uint hash_prospector(uint x) {
    x ^= x >> 16;
    x *= 0x7feb352d;
    x ^= x >> 15;
    x *= 0x846ca68b;
    x ^= x >> 16;
    return x;
}

uint hash(uint x) {
#if defined(HASH_XORSHIFT)
    return hash_xorshift(x);
#elif defined(HASH_MURMUR3)
    return hash_murmur3(x);
#elif defined(HASH_WANG)
    return hash_wang(x);
#elif defined(HASH_PROSPECTOR)
    return hash_prospector(x);
#endif
}

#endif // VALO_HASH_GLSL
