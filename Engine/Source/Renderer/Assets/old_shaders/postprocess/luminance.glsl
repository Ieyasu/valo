#ifndef VALO_LUMINANCE_GLSL
#define VALO_LUMINANCE_GLSL

float luminance_itu_bt601(vec3 c) {
    return dot(c, vec3(0.299, 0.587, 0.114));
}

float luminance_itu_bt709(vec3 c) {
    return dot(c, vec3(0.2126f, 0.7152f, 0.0722f));
}

float luminance(vec3 c) {
    return luminance_itu_bt601(c);
}

#endif // VALO_LUMINANCE_GLSL
