#version 450
#extension GL_GOOGLE_include_directive : enable

#include "luminance.glsl"

// Utility shader for calculating a variance map from an image

layout(local_size_x = 8, local_size_y = 8, local_size_z = 1) in;

layout(set = 0, binding = 0, rgba32f) uniform image2D _image_in;
layout(set = 0, binding = 1, r32f) uniform image2D _image_out;

const int radius = 2;

float variance(ivec2 pixel) {
    float lum = luminance(imageLoad(_image_in, pixel).rgb);

    // Calculate mean of the pixels
    float mean = 0;
    for (int i = -radius; i <= radius; ++i) {
        for (int j = -radius; j <= radius; ++j) {
            mean += luminance(imageLoad(_image_in, pixel + ivec2(i, j)).rgb);
        }
    }
    mean *= 1.0 / ((radius + 1) * (radius + 1));

    // Calculate the variance of the pixel
    float variance = 0;
    for (int i = -radius; i <= radius; ++i) {
        for (int j = -radius; j <= radius; ++j) {
            float value = luminance(imageLoad(_image_in, pixel + ivec2(i, j)).rgb);
            float delta = value - mean;
            variance += delta * delta;
        }
    }
    variance *= 1.0 / ((radius + 1) * (radius + 1) - 1);
    return variance;
}

void main() {
    ivec2 pixel = ivec2(gl_GlobalInvocationID.xy);
    float variance = variance(pixel);

    imageStore(_image_out, pixel, vec4(variance));
}
