#ifndef VALO_VIGNETTE_GLSL
#define VALO_VIGNETTE_GLSL

vec3 vignette(vec3 c, vec2 uv, float size, float intensity) {
    uv = uv * (1.0 - uv.yx);
    float vig = uv.x * uv.y * intensity;
    return c * pow(vig, size);
}

#endif // VALO_VIGNETTE_GLSL
