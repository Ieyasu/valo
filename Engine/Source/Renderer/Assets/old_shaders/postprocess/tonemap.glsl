#ifndef VALO_TONEMAPPING_GLSL
#define VALO_TONEMAPPING_GLSL

#include "luminance.glsl"

// Reinhard tonemaps

vec3 tonemap_reinhard(vec3 c) {
    return c / (1.0 + c);
}

vec3 tonemap_reinhard_extended(vec3 c) {
    const float max_white = 1.25;

    float luminance = luminance(c);
    return c * 1.0 / (1.0 + luminance / (max_white * max_white));
}

// Uncharted tonemap

vec3 tonemap_uncharted2_partial(vec3 x)
{
    const float A = 0.15;
    const float B = 0.50;
    const float C = 0.10;
    const float D = 0.20;
    const float E = 0.02;
    const float F = 0.30;

    return ((x*(A*x+C*B)+D*E)/(x*(A*x+B)+D*F))-E/F;
}

vec3 tonemap_uncharted2(vec3 v)
{
    const float max_white = 1.5;
    const float exposure_bias = 2.0;
    const vec3 w = vec3(11.2);

    vec3 curr = tonemap_uncharted2_partial(v * exposure_bias);
    vec3 white_scale = max_white / tonemap_uncharted2_partial(w);
    return curr * white_scale;
}

// ACES tonemaps

vec3 tonemap_aces_fast(vec3 x) {
    const float a = 2.51;
    const float b = 0.03;
    const float c = 2.43;
    const float d = 0.59;
    const float e = 0.14;

    x *= 0.6;
    return (x * (a * x + b)) / (x * (c * x + d) + e);
}

mat3 tonemap_aces_input_matrix = {
    { 0.59719, 0.07600, 0.02840 },
    { 0.35458, 0.90834, 0.13383 },
    { 0.04823, 0.01566, 0.83777 }
};

mat3 tonemap_aces_output_matrix = {
    { 1.60475, -0.10208, -0.00327 },
    { -0.53108, 1.10813, -0.07276 },
    { -0.07367, -0.00605, 1.07602 }
};

vec3 tonemap_aces_rtt_and_odt_fit(vec3 v)
{
    vec3 a = v * (v + 0.0245786f) - 0.000090537f;
    vec3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
    return a / b;
}

vec3 tonemap_aces(vec3 v)
{
    v = tonemap_aces_input_matrix * v;
    v = tonemap_aces_rtt_and_odt_fit(v);
    return tonemap_aces_output_matrix * v;
}

#endif // VALO_TONEMAPPING_GLSL
