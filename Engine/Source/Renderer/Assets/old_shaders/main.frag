#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

#define POSTPROCESS_VIGNETTE
#define POSTPROCESS_GAMMA_CORRECTION
#define POSTPROCESS_GAMMA_CORRECTION_FAST
// #define POSTPROCESS_TONEMAP_ACES
// #define POSTPROCESS_TONEMAP_UNCHARTED2
#define POSTPROCESS_TONEMAP_REINHARD_EXTENDED

#include "postprocess/tonemap.glsl"
#include "postprocess/vignette.glsl"

layout(location = 0) in vec2 in_uv;

layout(location = 0) out vec4 out_color;

layout(set = 0, binding = 0) uniform sampler2D image;

vec3 postprocess(vec3 c) {
    const float vignette_size = 0.2;
    const float vignette_intensity = 10;

#if defined(POSTPROCESS_TONEMAP_REINHARD)
    c.rgb = tonemap_reinhard(c.rgb);
#elif defined(POSTPROCESS_TONEMAP_REINHARD_EXTENDED)
    c.rgb = tonemap_reinhard_extended(c.rgb);
#elif defined(POSTPROCESS_TONEMAP_UNCHARTED2)
    c.rgb = tonemap_uncharted2(c.rgb);
#elif defined(POSTPROCESS_TONEMAP_ACES_FAST)
    c.rgb = tonemap_aces_fast(c.rgb);
#elif defined(POSTPROCESS_TONEMAP_ACES)
    c.rgb = tonemap_aces(c.rgb);
#endif

#if defined(POSTPROCESS_VIGNETTE)
    c = vignette(c, in_uv, vignette_size, vignette_intensity);
#endif

#if defined(POSTPROCESS_GAMMA_CORRECTION)
    c = pow(c, vec3(1.0 / 2.2));
#elif defined(POSTPROCESS_GAMMA_CORRECTION_FAST)
    c = sqrt(c);
#endif

    return c;
}

void main() {
    vec4 c = texture(image, in_uv);
    c.rgb = postprocess(c.rgb);
    c.a = 1;
    out_color = clamp(c, 0, 1);
}
