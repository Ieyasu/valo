#ifndef VALO_TONEMAP_GLSL
#define VALO_TONEMAP_GLSL

#include "../Common/Half.glsl"
#include "Luminance.glsl"

// Reinhard tonemapping

half3 tonemap_ldr_reinhard(half3 color) {
    return color / (1.0f + color);
}

half3 tonemap_ldr_reinhard_extended(half3 color) {
    const half max_white = 1.25f;
    const half luminance = luminance(color);
    return color * 1.0f / (1.0f + luminance / (max_white * max_white));
}

// Uncharted tonemapping

half3 _tonemap_ldr_uncharted2_curve(vec3 color)
{
    const half A = 0.15f;
    const half B = 0.50f;
    const half C = 0.10f;
    const half D = 0.20f;
    const half E = 0.02f;
    const half F = 0.30f;

    return ((color * (A * color + C * B) + D * E) / (color * (A * color + B) + D * F)) - E / F;
}

half3 tonemap_ldr_uncharted2(half3 color)
{
    const half max_white = 1.5f;
    const half exposure_bias = 2.0f;
    const half3 w = makeHalf3(11.2f);

    const half3 curr = _tonemap_ldr_uncharted2_curve(color * exposure_bias);
    const half3 white_scale = max_white / _tonemap_ldr_uncharted2_curve(w);
    return curr * white_scale;
}

// ACES tonemapping

halfMat3 _tonemap_ldr_aces_input_matrix = {
    { 0.59719f, 0.07600f, 0.02840f },
    { 0.35458f, 0.90834f, 0.13383f },
    { 0.04823f, 0.01566f, 0.83777f }
};

halfMat3 _tonemap_ldr_aces_output_matrix = {
    { 1.60475f, -0.10208f, -0.00327f },
    { -0.53108f, 1.10813f, -0.07276f },
    { -0.07367f, -0.00605f, 1.07602f }
};

half3 _tonemap_ldr_aces_rtt_and_odt_fit(half3 color)
{
    const half3 a = color * (color + 0.0245786f) - 0.000090537f;
    const half3 b = color * (0.983729f * color + 0.4329510f) + 0.238081f;
    return a / b;
}

half3 tonemap_ldr_aces_fast(half3 color) {
    const half a = 2.51f;
    const half b = 0.03f;
    const half c = 2.43f;
    const half d = 0.59f;
    const half e = 0.14f;

    color *= 0.6f;
    return (color * (a * color + b)) / (color * (c * color + d) + e);
}

half3 tonemap_aces(half3 color)
{
    color = _tonemap_ldr_aces_input_matrix * color;
    color = _tonemap_ldr_aces_rtt_and_odt_fit(color);
    return _tonemap_ldr_aces_output_matrix * color;
}

#endif // VALO_TONEMAP_GLSL
