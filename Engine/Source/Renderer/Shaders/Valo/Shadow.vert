#version 450
#extension GL_GOOGLE_include_directive : enable

#include "Common/Common.glsl"

invariant gl_Position;

layout(location = 0) in vec3 _in_position;

void main()
{
    const vec4 mvp_pos = _view.view_projection_matrix * vec4(_in_position, 1.0);
    gl_Position = mvp_pos;
}
