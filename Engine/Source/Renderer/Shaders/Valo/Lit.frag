#version 450
#extension GL_GOOGLE_include_directive : enable

#include "Common/MaterialParams.glsl"
#include "Surface/Surface.glsl"

#define ALBEDO_MAP 1
#define NORMAL_MAP 1
#define EMISSIVE_MAP 1
#define ORM_MAP 1

#if ALBEDO_MAP
VALO_SAMPLER2D(0, _albedo_texture);
#endif

#if NORMAL_MAP
VALO_SAMPLER2D(1, _normal_texture);
#endif

#if EMISSIVE_MAP
VALO_SAMPLER2D(2, _emissive_texture);
#endif

#if ORM_MAP
VALO_SAMPLER2D(3, _orm_texture); // occlusion (r) roughness (g) metallic (b)
#endif

// VALO_PROPERTY_BEGIN()
// VALO_PROPERTY(vec3, albedo)
// VALO_PROPERTY(float, metallic)
// VALO_PROPERTY(vec3, emission)
// VALO_PROPERTY(float, roughness)
// VALO_PROPERTY(float, occlusion_scale)
// VALO_PROPERTY(float, normal_scale)
// VALO_PROPERTY(float, alpha_cutoff)
// VALO_PROPERTY_END()

void surface(in SurfaceInput i, out SurfaceOutput o)
{
#if ALBEDO_MAP
    const half3 albedo = texture(_albedo_texture, i.uv).rgb;
#else
    const half3 albedo = makeHalf3(1.0f);
#endif

#if NORMAL_MAP
    const half3 normal = unpack_normal(texture(_normal_texture, i.uv).rgb);
#else
    const half3 normal = i.normal;
#endif

#if EMISSIVE_MAP
    const half3 emissive = texture(_emissive_texture, i.uv).rgb;
#else
    const half3 emissive = makeHalf3(0.0f);
#endif

#if ORM_MAP
    const half3 orm = texture(_orm_texture, i.uv).rgb;
#else
    const half3 orm = makeHalf3(0.0f, 1.0f, 0.0f);
#endif

    MaterialParams _material_params;
    _material_params.albedo = vec3(1.0f);
    _material_params.emissive = vec3(1.0f);
    _material_params.metallic = 1.0f;
    _material_params.roughness = 1.0f;
    _material_params.occlusion_scale = 1.0f;
    _material_params.normal_scale = 1.0f;

    o.albedo = _material_params.albedo * albedo;
    o.normal = scale_normal(normal, _material_params.normal_scale);
    o.emissive = _material_params.emissive * emissive;
    o.occlusion = scale_occlusion(orm.r, _material_params.occlusion_scale);
    o.roughness = _material_params.roughness * orm.g;
    o.metallic = _material_params.metallic * orm.b;
    o.alpha = 1.0f;
}
