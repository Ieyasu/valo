#version 450
#extension GL_GOOGLE_include_directive : enable

void main()
{
    gl_FragDepth = gl_FragCoord.z;
}
