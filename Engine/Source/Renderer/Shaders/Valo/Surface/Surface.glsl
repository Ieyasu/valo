#ifndef VALO_SURFACE_GLSL
#define VALO_SURFACE_GLSL

#include "../Common/Encode.glsl"
#include "../Common/Common.glsl"

struct SurfaceInput
{
    vec2 uv;
};

struct SurfaceOutput
{
    half3 albedo;
    half occlusion;
    half3 normal;
    half roughness;
    half3 emissive;
    half metallic;
    half alpha;
};

layout(location = 0) in vec2 _in_uv;
layout(location = 1) in vec4 _in_position;
layout(location = 2) in vec4 _in_prev_position;
layout(location = 3) in mat3 _in_tangent_to_world;

layout(location = 0) out vec4 _out_scene_color;
layout(location = 1) out vec4 _out_gbufferA; // albedo (rgb) material flags (a)
layout(location = 2) out vec4 _out_gbufferB; // encoded normal (rgb) material id (a)
layout(location = 3) out vec3 _out_gbufferC; // emissive (rgb)
layout(location = 4) out vec4 _out_gbufferD; // occlusion (r) roughness (g) metallic (b) misc material (a)
layout(location = 5) out vec2 _out_velocity; // velocity (rg)

void surface(in SurfaceInput i, out SurfaceOutput o);

half4 _encode_gbufferA(SurfaceOutput o)
{
    half4 gbuffer;
    gbuffer.rgb = o.albedo;
    gbuffer.a = 0;
    return gbuffer;
}

half4 _encode_gbufferB(SurfaceOutput o)
{
    half4 gbuffer;
    gbuffer.rg = encode_normal(o.normal);
    gbuffer.b = 0;
    gbuffer.a = 0;
    return gbuffer;
}

half3 _encode_gbufferC(SurfaceOutput o)
{
    half3 gbuffer;
    gbuffer.rgb = o.emissive;
    return gbuffer;
}

half4 _encode_gbufferD(SurfaceOutput o)
{
    half4 gbuffer;
    gbuffer.r = o.occlusion;
    gbuffer.g = o.roughness;
    gbuffer.b = o.metallic;
    gbuffer.a = 0;
    return gbuffer;
}

void main()
{
    SurfaceInput surf_in;
    surf_in.uv = _in_uv;

    SurfaceOutput surf_out;
    surface(surf_in, surf_out);

    // Transform normal from tangent space to world space
    surf_out.normal = _in_tangent_to_world * surf_out.normal;

    // Calculate velocity
    const vec3 position_ndc = _in_position.xyz / _in_position.w;
    const vec3 prev_position_ndc = _in_prev_position.xyz / _in_prev_position.w;
    const vec2 velocity = (position_ndc.xy - _view.jitter) - (prev_position_ndc.xy - _previous_view.jitter);

    // Encode values to gbuffers required by other passes
    _out_scene_color = vec4(0);
    _out_gbufferA = _encode_gbufferA(surf_out);
    _out_gbufferB = _encode_gbufferB(surf_out);
    _out_gbufferC = _encode_gbufferC(surf_out);
    _out_gbufferD = _encode_gbufferD(surf_out);
    _out_velocity = velocity;
}

#endif
