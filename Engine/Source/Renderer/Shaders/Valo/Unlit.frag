#version 450
#extension GL_GOOGLE_include_directive : enable

#include "Common/MaterialParams.glsl"
#include "Surface/Surface.glsl"

VALO_MATERIAL_SAMPLER2D(0, _albedo_texture);
// VALO_MATERIAL_UNIFORM_BUFFER(4, params) { MaterialParams _material_params; };

void surface(in SurfaceInput i, out SurfaceOutput o)
{
#if defined(ALBEDO_MAP)
    const half3 albedo = texture(_albedo_texture, i.uv).rgb;
#else
    const half3 albedo = makeHalf3(1.0f);
#endif

    MaterialParams _material_params;
    _material_params.albedo = vec3(1.0f);

    o.albedo = _material_params.albedo * albedo;
    o.normal = vec3(0.5, 0.5, 1);
    o.emissive = vec3(0);
    o.occlusion = 0;
    o.roughness = 0;
    o.metallic = 0;
    o.alpha = 0;
}
