#version 450
#extension GL_GOOGLE_include_directive : enable

#include "../Common/Common.glsl"

layout(location = 0) out vec4 _out_scene_color;
layout(input_attachment_index = 0, set = VALO_PASS_DESCRIPTOR_SET_INDEX, binding = 0) uniform subpassInput _gbufferA;
layout(input_attachment_index = 1, set = VALO_PASS_DESCRIPTOR_SET_INDEX, binding = 1) uniform subpassInput _gbufferB;
layout(input_attachment_index = 2, set = VALO_PASS_DESCRIPTOR_SET_INDEX, binding = 2) uniform subpassInput _scene_depth;

void main()
{
    _out_scene_color = _gbufferA;
}
