#version 450
#extension GL_GOOGLE_include_directive : enable

#include "Common/Common.glsl"

invariant gl_Position;

layout(location = 0) in vec3 _in_position;
layout(location = 1) in vec2 _in_uv;
layout(location = 2) in vec3 _in_normal;
layout(location = 3) in vec4 _in_tangent;

layout(location = 0) out vec2 _out_uv;
layout(location = 1) out vec4 _out_position;
layout(location = 2) out vec4 _out_prev_position;
layout(location = 3) out mat3 _out_tangent_to_world;

void main()
{
    const mat4 _model_matrix = mat4(1);

    // Calculate vertex MVP positions
    const vec4 mvp_pos = _view.view_projection_matrix * vec4(_in_position, 1.0);
    const vec4 prev_mvp_pos = _previous_view.view_projection_matrix * vec4(_in_position, 1.0);

    // Calculate matrix for converting tangent normals to world normal
    const vec3 world_normal = normalize(_model_matrix * vec4(_in_normal, 0.0)).xyz;
	const vec3 world_tangent = normalize(_model_matrix * vec4(_in_tangent.xyz, 0.0)).xyz;
	const vec3 world_bitangent = cross(world_normal, world_tangent) * _in_tangent.w;
	const mat3 tangent_to_world = mat3(world_tangent, world_bitangent, world_normal);

    _out_uv = _in_uv;
    _out_position = mvp_pos;
    _out_prev_position = prev_mvp_pos;
    _out_tangent_to_world = tangent_to_world;
    gl_Position = mvp_pos;
}
