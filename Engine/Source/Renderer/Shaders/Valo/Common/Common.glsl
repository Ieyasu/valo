#ifndef VALO_MATERIAL_GLSL
#define VALO_MATERIAL_GLSL

// Descriptor set for data that needs to be updated for each view
#define VALO_VIEW_DESCRIPTOR_SET_INDEX 0

// Descriptor set for data that needs to be updated for each render/compute pass
#define VALO_PASS_DESCRIPTOR_SET_INDEX 1

// Descriptor set for data that needs to be updated for each material
#define VALO_MATERIAL_DESCRIPTOR_SET_INDEX 2

// Descriptor set for data that needs to be updated for each draw call
#define VALO_INSTANCE_DESCRIPTOR_SET_INDEX 3

#define VALO_SAMPLER1D(BINDING, NAME)       layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING) uniform sampler1D NAME
#define VALO_SAMPLER2D(BINDING, NAME)       layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING) uniform sampler2D NAME
#define VALO_SAMPLER3D(BINDING, NAME)       layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING) uniform sampler3D NAME
#define VALO_IMAGE1D(BINDING, NAME, FORMAT) layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING, FORMAT) uniform image1D NAME
#define VALO_IMAGE2D(BINDING, NAME, FORMAT) layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING, FORMAT) uniform image2D NAME
#define VALO_IMAGE3D(BINDING, NAME, FORMAT) layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING, FORMAT) uniform image3D NAME
#define VALO_UNIFORM_BUFFER(BINDING, NAME)   layout(set = VALO_MATERIAL_DESCRIPTOR_SET_INDEX, binding = BINDING) uniform NAME

// Per-view data
struct Time
{
    float total;
    float delta;
    uint frame;
    float _padding;
};

struct View
{
    mat4x4 view_matrix;
    mat4x4 view_projection_matrix;
    mat4x4 view_projection_matrix_inverse;
    vec2 linearize_depth_params;
    vec2 jitter;
};

layout(set = VALO_VIEW_DESCRIPTOR_SET_INDEX, binding = 0) uniform _time_params
{
    Time _time;
};

layout(set = VALO_VIEW_DESCRIPTOR_SET_INDEX, binding = 1) uniform _previous_time_params
{
    Time _previous_time;
};

layout(set = VALO_VIEW_DESCRIPTOR_SET_INDEX, binding = 2) uniform _view_params
{
    View _view;
};

layout(set = VALO_VIEW_DESCRIPTOR_SET_INDEX, binding = 3) uniform _previous_view_params
{
    View _previous_view;
};

#endif // VALO_MATERIAL_GLSL
