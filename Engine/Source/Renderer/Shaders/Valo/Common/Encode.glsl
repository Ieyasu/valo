#ifndef VALO_ENCODE_GLSL
#define VALO_ENCODE_GLSL

#include "Half.glsl"
#include "Math.glsl"

#define VALO_NORMAL_ENCODING_OCTAHEDRON

#if defined(VALO_NORMAL_ENCODING_OCTAHEDRON)

// Normal encoding and decoding using Octahedron Normal Vector Encoding
// https://jcgt.org/published/0003/02/01/ (Survey of Efficient Representations for Independent Unit Vectors)

half2 encode_normal(half3 n)
{
    half2 e = n.xy /= abs(n.x) + abs(n.y) + abs(n.z);
    e = (n.z >= 0) ? e : (1 - abs(e.yx)) * sign_no_zero(e);
    e = e * 0.5f + 0.5f;
    return e;
}

half3 decode_normal(half2 e)
{
    half3 n;
    n.xy = 2 * e.xy - 1;
    n.z = 1.0f - abs(n.x) - abs(n.y);

    half t = max(-n.z, 0);
    n.xy += t * -sign_no_zero(n.xy);

    return normalize(n);
}

#elif defined(VALO_NORMAL_ENCODING_SPHEREMAP)

// Normal encoding and decoding using spheremap transform
// https://aras-p.info/texts/CompactNormalStorage.html

half2 encode_normal(half3 n)
{
    half2 e = normalize(n.xy) * sqrt(-n.z * 0.5f + 0.5f);
    e = 0.5f * e + 0.5f;
    return e;
}

half3 decode_normal(half2 e)
{
    e = 2 * e - 1;
    half l = dot(e.xy, -e.xy) + 0.5;

    half3 n;
    n.z = l;
    n.xy = e * sqrt(l + 0.5);
    return n.xyz * 2;
}

#elif defined(VALO_NORMAL_ENCODING_SPHERICAL_COORDINATES)

// Normal encoding and decoding using spherical coordinates
// https://aras-p.info/texts/CompactNormalStorage.html

half2 encode_normal(half3 n)
{
    half2 e;
    e.x = atan2(n.y, n.x) * VALO_INV_PI;
    e.y = n.z;
    e = 0.5f * e + 0.5f;
    return e;
}

half3 decode_normal(half2 e)
{
    half2 angle = 2 * e - 1;
    half sin = sin(angle.x * VALO_PI);
    half cos = cos(angle.x * VALO_PI);
    half2 phi = makeHalf2(sqrt(1.0 - angle.y * angle.y), angle.y);
    return makeHalf3(cos * phi.x, sin * phi.x, phi.y);
}

#elif defined(VALO_NORMAL_ENCODING_XY)

// Normal encoding and decoding by directly reconstructing Z
// https://aras-p.info/texts/CompactNormalStorage.html

half2 encode_normal(half3 n)
{
    half2 e = n.xy * 0.5f + 0.5f;
    return e;
}

half3 decode_normal(half2 e)
{
    half3 n;
    n.xy = 2 * e - 1;
    n.z = 1 - dot(n.xy, n.xy);
    return normalize(n);
}

#endif

half3 pack_normal(half3 normal)
{
    return 0.5f * normal + 0.5f;
}

half3 unpack_normal(half3 normal)
{
    return 2.0f * normal - 1.0f;
}

half3 scale_normal(half3 normal, float scale)
{
    const half3 scale_vector = vec3(scale, scale, 1.0f);
    return normalize(scale_vector * normal);
}

half3 unpack_normal(half3 normal, float scale)
{
    const half3 unpacked_normal = unpack_normal(normal);
    return scale_normal(unpacked_normal, scale);
}

half scale_occlusion(half occlusion, half scale)
{
    return mix(1.0f, occlusion, scale);
}

#endif // VALO_ENCODE_GLSL
