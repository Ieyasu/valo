#ifndef VALO_HALF_GLSL
#define VALO_HALF_GLSL

precision highp float;

#define half mediump float
#define half2 mediump vec2
#define half3 mediump vec3
#define half4 mediump vec4

#define halfMat2 mediump mat2
#define halfMat3 mediump mat3
#define halfMat4 mediump mat4

#define makeHalf float
#define makeHalf2 vec2
#define makeHalf3 vec3
#define makeHalf4 vec4

#define makeHalfMat2 mat2
#define makeHalfMat3 mat3
#define makeHalfMat4 mat4

#endif // VALO_HALF_GLSL
