#ifndef VALO_STANDARD_GLSL
#define VALO_STANDARD_GLSL

struct MaterialParams
{
    vec3 albedo;
    float metallic;
    vec3 emissive;
    float roughness;
    float transmission;
    float ior;
    float occlusion_scale;
    float normal_scale;
    float alpha_cutoff;
};

#endif
