
module;

#include <dylib.hpp>

module Valo.Platform;

import Valo.Std;

namespace Valo
{
    DynamicLibrary::DynamicLibrary(String name) :
        m_name(Memory::Move(name))
    {
        m_dylib = dylib(m_name.c_str());
    }

    bool DynamicLibrary::IsLoaded() const
    {
        return m_dylib.has_value();
    }

    const String& DynamicLibrary::GetName() const
    {
        return m_name;
    }
}
