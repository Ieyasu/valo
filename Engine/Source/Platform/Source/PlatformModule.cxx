module;

#include <Valo/Profile.hpp>

module Valo.Platform;

import Valo.Engine;
import Valo.Profiler;
import Valo.Std;

import :GenericPlatform;

namespace Valo
{
    bool PlatformModule::Initialize(Engine& /*engine*/)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Platform);
        Platform::SetActivePlatform(MakeUnique<GenericPlatform>());
        return true;
    }

    void PlatformModule::Deinitialize()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Platform);
        Platform::SetActivePlatform(nullptr);
    }
}
