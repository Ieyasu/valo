export module Valo.Platform:PlatformModule;

import Valo.Engine;
import Valo.Std;

import :GenericPlatform;
import :Platform;

export namespace Valo
{
    class PlatformModule final : public EngineModule
    {
    private:
        bool Initialize(Engine& engine) override;
        void Deinitialize() override;
    };
}
