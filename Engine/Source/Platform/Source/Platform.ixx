export module Valo.Platform:Platform;

import Valo.Std;

import :DynamicLibrary;

export namespace Valo
{
    class Platform
    {
    public:
        virtual ~Platform() = default;

        static DynamicLibrary LoadDynamicLibrary(String name);

        virtual DynamicLibrary LoadDynamicLibraryImpl(String name) = 0;

    private:
        friend class PlatformModule;

        static void SetActivePlatform(UniquePtr<Platform> active_platform);

        static Mutex               g_active_platform_mutex;
        static UniquePtr<Platform> g_active_platform;
    };
}
