module Valo.Platform;

import Valo.Std;

namespace Valo
{
    Mutex               Platform::g_active_platform_mutex{};
    UniquePtr<Platform> Platform::g_active_platform = nullptr;

    DynamicLibrary Platform::LoadDynamicLibrary(String name)
    {
        ScopedLock<Mutex> lock(g_active_platform_mutex);

        return g_active_platform->LoadDynamicLibraryImpl(Memory::Move(name));
    }

    void Platform::SetActivePlatform(UniquePtr<Platform> active_platform)
    {
        ScopedLock<Mutex> lock(g_active_platform_mutex);

        g_active_platform = Memory::Move(active_platform);
    }
}
