module Valo.Platform;

import Valo.Std;

namespace Valo
{
    DynamicLibrary GenericPlatform::LoadDynamicLibraryImpl(String name)
    {
        return DynamicLibrary(Memory::Move(name));
    }
}
