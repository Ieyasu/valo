module;

#include <dylib.hpp>

export module Valo.Platform:DynamicLibrary;

import Valo.Std;

export namespace Valo
{
    class DynamicLibrary final
    {
    public:
        DynamicLibrary() = default;
        DynamicLibrary(String name);

        bool IsLoaded() const;

        const String& GetName() const;

        template <typename T>
        Optional<T> GetVariable(const StringView name) const;

        template <typename T>
        Optional<Function<T>> GetFunction(const StringView name) const;

    private:
        String m_name{};

        Optional<dylib> m_dylib;
    };

    template <typename T>
    inline Optional<T> DynamicLibrary::GetVariable(const StringView name) const
    {
        if (!m_dylib.has_value())
        {
            return nullopt;
        }

        return m_dylib->get_variable<T>(name.data());
    }

    template <typename T>
    inline Optional<Function<T>> DynamicLibrary::GetFunction(const StringView name) const
    {
        if (!m_dylib.has_value())
        {
            return nullopt;
        }

        return m_dylib->get_function<T>(name.data());
    }
}
