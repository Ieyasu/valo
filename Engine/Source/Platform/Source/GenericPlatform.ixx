export module Valo.Platform:GenericPlatform;

import Valo.Std;

import :DynamicLibrary;
import :Platform;

export namespace Valo
{
    class GenericPlatform : public Platform
    {
        virtual DynamicLibrary LoadDynamicLibraryImpl(String name) override;
    };
}
