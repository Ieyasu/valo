export module Valo.RHI:ResourceHandle;

import Valo.Std;

export namespace Valo::RHI
{
    enum class ResourceType : EnumSmall
    {
        Unknown,
        Buffer,
        Image,
        Sampler,
        ComputePipeline,
        GraphicsPipeline,
        PipelineLayout,
        DescriptorSet,
        DescriptorSetLayout,
        Framebuffer,
        RenderPass,
        CommandQueue,
        CommandPool,
    };

    class BaseResourceHandle
    {
    public:
        using NativeType = UInt64;

        explicit BaseResourceHandle(ResourceType type);
        BaseResourceHandle(NativeType native, ResourceType type);

        BaseResourceHandle(const BaseResourceHandle&)            = default;
        BaseResourceHandle(BaseResourceHandle&&)                 = default;
        BaseResourceHandle& operator=(const BaseResourceHandle&) = default;
        BaseResourceHandle& operator=(BaseResourceHandle&&)      = default;

        virtual ~BaseResourceHandle() = default;

        NativeType GetNative() const;

        ResourceType GetType() const;

        bool IsValid() const;

        bool operator==(const BaseResourceHandle& other) const;

        bool operator!=(const BaseResourceHandle& other) const;

    private:
        static constexpr NativeType invalid_value = ~static_cast<NativeType>(0);

        NativeType   m_native{};
        ResourceType m_type{};
    };

    template <ResourceType TResourceType>
    class ResourceHandle : public BaseResourceHandle
    {
    public:
        using NativeType = BaseResourceHandle::NativeType;

        ResourceHandle() :
            BaseResourceHandle(TResourceType)
        {
        }

        explicit ResourceHandle(NativeType native) :
            BaseResourceHandle(native, TResourceType)
        {
        }

        ResourceHandle(const ResourceHandle&)                = default;
        ResourceHandle(ResourceHandle&&) noexcept            = default;
        ResourceHandle& operator=(const ResourceHandle&)     = default;
        ResourceHandle& operator=(ResourceHandle&&) noexcept = default;
        ~ResourceHandle() override                           = default;
    };

}
