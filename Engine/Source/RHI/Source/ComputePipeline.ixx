export module Valo.RHI:ComputePipeline;

import :PipelineLayout;
import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    struct ComputePipelineDescriptor final
    {
        PipelineLayoutHandle  layout{};
        ShaderStageDescriptor shader_stage{};
    };

    using ComputePipelineHandle = ResourceHandle<ResourceType::ComputePipeline>;
}
