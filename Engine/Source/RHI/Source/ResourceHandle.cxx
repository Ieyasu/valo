module Valo.RHI;

namespace Valo::RHI
{
    BaseResourceHandle::BaseResourceHandle(ResourceType type) :
        m_native(invalid_value),
        m_type(type)
    {
    }

    BaseResourceHandle::BaseResourceHandle(NativeType native, ResourceType type) :
        m_native(native),
        m_type(type)
    {
    }

    BaseResourceHandle::NativeType BaseResourceHandle::GetNative() const
    {
        return m_native;
    }

    ResourceType BaseResourceHandle::GetType() const
    {
        return m_type;
    }

    bool BaseResourceHandle::IsValid() const
    {
        return m_native != invalid_value;
    }

    bool BaseResourceHandle::operator==(const BaseResourceHandle& other) const
    {
        return GetNative() == other.GetNative() && IsValid() == other.IsValid() && GetType() == other.GetType();
    }

    bool BaseResourceHandle::operator!=(const BaseResourceHandle& other) const
    {
        return !(*this == other);
    }
}
