export module Valo.RHI:DescriptorSetLayout;

import Valo.Std;

import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    enum class DescriptorType : EnumSmall
    {
        Sampler = 0,
        CombinedImageSampler,
        SampledImage,
        StorageImage,
        UniformBuffer,
        StorageBuffer,
        UniformBufferDynamic,
        StorageBufferDynamic,
    };

    struct DescriptorSetLayoutBinding final
    {
        DescriptorType   type{DescriptorType::Sampler};
        ShaderStageFlags stage{ShaderStage::None};
        UInt32           descriptor_count{0};
        UInt32           binding{0};
    };

    struct DescriptorSetLayoutDescriptor final
    {
        Span<const DescriptorSetLayoutBinding> bindings{};
    };

    using DescriptorSetLayoutHandle = ResourceHandle<ResourceType::DescriptorSetLayout>;
}
