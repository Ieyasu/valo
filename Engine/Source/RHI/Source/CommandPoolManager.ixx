export module Valo.RHI:CommandPoolManager;

import Valo.Std;

import :CommandBuffer;
import :CommandPool;
import :CommandQueue;
import :Device;
import :Types;

export namespace Valo::RHI
{
    class CommandPoolManager final
    {
    public:
        static UniquePtr<CommandPoolManager> Create(DeviceHandle device);

        CommandPoolManager(const CommandPoolManager&)            = delete;
        CommandPoolManager(CommandPoolManager&&)                 = delete;
        CommandPoolManager& operator=(const CommandPoolManager&) = delete;
        CommandPoolManager& operator=(CommandPoolManager&&)      = delete;
        ~CommandPoolManager();

        Result<CommandBufferHandle> AquireCommandBuffer(CommandQueueHandle queue);

        ResultCode ResetCommandPools();

    private:
        explicit CommandPoolManager(DeviceHandle device);

        Result<CommandPoolHandle> GetOrCreateCommandPool(CommandQueueHandle queue);

        Result<CommandBufferHandle> GetOrCreateCommandBuffer(CommandPoolHandle pool);

        DeviceHandle m_device{};

        Map<ThreadId, CommandPoolHandle> m_command_pools_per_thread{};
        Vector<CommandBufferHandle>      m_aquired_command_buffers{};
        Vector<CommandBufferHandle>      m_pooled_command_buffers{};
    };
}
