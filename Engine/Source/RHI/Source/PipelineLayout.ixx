export module Valo.RHI:PipelineLayout;

import Valo.Std;

import :DescriptorSetLayout;
import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    struct ShaderStageDescriptor final
    {
        StringView       name{};
        ShaderStage      stage{ShaderStage::None};
        Span<const Byte> binary{};
    };

    struct PushConstantRangeDescriptor final
    {
        ShaderStageFlags stage{};
        UInt32           offset{0};
        UInt32           size{0};
    };

    struct PipelineLayoutDescriptor final
    {
        Span<const DescriptorSetLayoutHandle>   layouts{};
        Span<const PushConstantRangeDescriptor> push_constant_ranges{};
    };

    using PipelineLayoutHandle = ResourceHandle<ResourceType::PipelineLayout>;
}
