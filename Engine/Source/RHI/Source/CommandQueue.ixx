export module Valo.RHI:CommandQueue;

import :ResourceHandle;

export namespace Valo::RHI
{
    using CommandQueueHandle = ResourceHandle<ResourceType::CommandQueue>;
}
