export module Valo.RHI:Sampler;

import Valo.Std;

import :Types;
import :ResourceHandle;

export namespace Valo::RHI
{
    enum class SamplerAddressMode : EnumSmall
    {
        Repeat         = 0,
        MirroredRepeat = 1,
        ClampToEdge    = 2,
        ClampToBorder  = 3,
    };

    enum class SamplerFilter : EnumSmall
    {
        Nearest = 0,
        Linear  = 1,
        Cubic   = 2,
    };

    enum class SamplerMipmapMode : EnumSmall
    {
        Nearest = 0,
        Linear  = 1,
    };

    struct SamplerDescriptor final
    {
        SamplerFilter      min_filter{};
        SamplerFilter      mag_filter{};
        SamplerMipmapMode  mipmap_mode{};
        SamplerAddressMode address_mode_u{};
        SamplerAddressMode address_mode_v{};
        SamplerAddressMode address_mode_w{};
        float              mip_lod_bias{};
        bool               anisotropy_enabled{};
        float              max_anisotropy{};
        float              min_lod{};
        float              max_lod{};
    };

    using SamplerHandle = ResourceHandle<ResourceType::Sampler>;
}
