export module Valo.RHI:CommandPool;

import :CommandQueue;
import :ResourceHandle;

export namespace Valo::RHI
{
    struct CommandPoolDescriptor final
    {
        CommandQueueHandle queue{};
    };

    using CommandPoolHandle = ResourceHandle<ResourceType::CommandPool>;
}
