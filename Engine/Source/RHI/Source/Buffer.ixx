module;

#include <Valo/Macros.hpp>

export module Valo.RHI:Buffer;

import Valo.Std;

import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    VALO_FLAGS(BufferUsage, BufferUsageFlags){
        None        = 0,
        TransferSrc = Bit::Set<Flags>(0),
        TransferDst = Bit::Set<Flags>(1),
        Uniform     = Bit::Set<Flags>(4),
        Storage     = Bit::Set<Flags>(5),
        Index       = Bit::Set<Flags>(6),
        Vertex      = Bit::Set<Flags>(7),
        Indirect    = Bit::Set<Flags>(8),
    };

    enum class BufferHostAccess : EnumSmall
    {
        Auto               = 0,
        None               = 1,
        FastSequential     = 2,
        RequiredSequential = 3,
        RequiredRandom     = 4,
    };

    struct BufferDescriptor final
    {
        using SizeType = size_t;

        SizeType         size{0};
        BufferUsageFlags usage{0};
        BufferHostAccess access{BufferHostAccess::Auto};
    };

    using BufferHandle = ResourceHandle<ResourceType::Buffer>;
}
