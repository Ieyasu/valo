export module Valo.RHI:PipelineBarrier;

import Valo.Std;

import :Buffer;
import :Image;
import :Types;

export namespace Valo::RHI
{
    struct ImageMemoryBarrier final
    {
        ShaderStageFlags src_stage_flags{};
        AccessFlags      src_access_flags{};
        ImageUsageFlags  src_usage_flags{};

        ShaderStageFlags dst_stage_flags{};
        AccessFlags      dst_access_flags{};
        ImageUsageFlags  dst_usage_flags{};

        ImageHandle image{};
        UInt32      base_mip_level{};
        UInt32      level_count{};
        UInt32      base_array_layer{};
        UInt32      layer_count{};
    };

    struct BufferMemoryBarrier final

    {
        ShaderStageFlags src_stage_flags{};
        AccessFlags      src_access_flags{};
        BufferUsage      src_usage_flags{};

        ShaderStageFlags dst_stage_flags{};
        AccessFlags      dst_access_flags{};
        BufferUsage      dst_usage_flags{};

        BufferHandle   buffer{};
        DeviceSizeType offset{};
        DeviceSizeType range{};
    };
}
