export module Valo.RHI:Framebuffer;

import Valo.Std;

import :Image;
import :RenderPass;
import :ResourceHandle;

export namespace Valo::RHI
{
    struct FramebufferDescriptor final
    {
        RenderPassHandle        render_pass{};
        Span<const ImageHandle> attachments{};
        UInt32                  width{0};
        UInt32                  height{0};
        UInt32                  layers{0};
    };

    using FramebufferHandle = ResourceHandle<ResourceType::Framebuffer>;
}
