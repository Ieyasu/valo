export module Valo.RHI:DescriptorSet;

import Valo.Std;

import :Buffer;
import :DescriptorSetLayout;
import :Image;
import :ResourceHandle;
import :Sampler;
import :Types;

export namespace Valo::RHI
{
    struct DescriptorSetDescriptor final
    {
        DescriptorSetLayoutHandle layout{};
    };

    using DescriptorSetHandle = ResourceHandle<ResourceType::DescriptorSet>;

    struct DescriptorSetUpdate final
    {
        DescriptorSetHandle set{};
        DescriptorType      descriptor_type{DescriptorType::UniformBuffer};
        UInt32              binding{0};
        UInt32              array_index{0};
        UInt32              descriptor_count{0};
        BufferHandle        buffer{};
        DeviceSizeType      buffer_offset{};
        DeviceSizeType      buffer_range{};
        ImageHandle         image{};
        SamplerHandle       sampler{};
    };
}
