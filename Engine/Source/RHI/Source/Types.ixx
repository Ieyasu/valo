module;

#include <Valo/Macros.hpp>

export module Valo.RHI:Types;

import Valo.Logger;
import Valo.Std;

import :Format;

export namespace Valo::RHI
{
    enum class ResultCode : EnumSmall
    {
        Success                = 0,
        ErrorUnknown           = 1,
        ErrorOutOfHostMemory   = 2,
        ErrorOutOfDeviceMemory = 3,
        ErrorUnsupported       = 4,
        ErrorInvalidWindow     = 5,
    };

    template <typename T>
    using Result = ResultValue<ResultCode, T>;

    using DeviceSizeType = size_t;

    struct SwapchainInfo
    {
        Format format{Format::Undefined};
        UInt32 width{0};
        UInt32 height{0};
    };

    struct Rect2D final
    {
        UInt32 x{0};
        UInt32 y{0};
        UInt32 width{0};
        UInt32 height{0};
    };

    struct Viewport final
    {
        float x{0};
        float y{0};
        float width{0};
        float height{0};
        float min_depth{0};
        float max_depth{1};
    };

    struct Scissor final
    {
        UInt32 x{0};
        UInt32 y{0};
        UInt32 width{0};
        UInt32 height{0};
    };

    VALO_FLAGS(ShaderStage, ShaderStageFlags){
        None                   = 0,
        Vertex                 = Bit::Set<Flags>(0),
        TessellationControl    = Bit::Set<Flags>(1),
        TessellationEvaluation = Bit::Set<Flags>(2),
        Geometry               = Bit::Set<Flags>(3),
        Fragment               = Bit::Set<Flags>(4),
        Compute                = Bit::Set<Flags>(5),
        All                    = Bit::Set<Flags>(0, 1, 2, 3, 4, 5),
    };

    VALO_FLAGS(AccessFlagBits, AccessFlags){
        None  = 0,
        Read  = Bit::Set<Flags>(0),
        Write = Bit::Set<Flags>(1),
    };
}
