export module Valo.RHI:GraphicsPipeline;

import Valo.Std;

import :Format;
import :PipelineLayout;
import :RenderPass;
import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    enum class IndexFormat : EnumSmall
    {
        UInt16 = 0,
        UInt32 = 1,
    };

    enum class CullMode : EnumSmall
    {
        None         = 0,
        Front        = 1,
        Back         = 2,
        FrontAndBack = 3,
    };

    enum class DynamicState : EnumSmall
    {
        Viewport = 0,
        Scissors = 1,
    };

    enum class PolygonMode : EnumSmall
    {
        Fill  = 0,
        Line  = 1,
        Point = 2,
    };

    enum class PrimitiveTopology : EnumSmall
    {
        PointList    = 0,
        LineList     = 1,
        TriangleList = 2,
    };

    enum class Winding : EnumSmall
    {
        CounterClockwise = 0,
        Clockwise        = 1,
    };

    enum class BlendOp : EnumSmall
    {
        Add             = 0,
        Subtract        = 1,
        ReverseSubtract = 2,
        Min             = 3,
        Max             = 4,
    };

    enum class BlendFactor : EnumSmall
    {
        Zero                  = 0,
        One                   = 1,
        SrcColor              = 2,
        OneMinusSrcColor      = 3,
        DstColor              = 4,
        OneMinusDstColor      = 5,
        SrcAlpha              = 6,
        OneMinusSrcAlpha      = 7,
        DstAlpha              = 8,
        OneMinusDstAlpha      = 9,
        ConstantColor         = 10,
        OneMinusConstantColor = 11,
        ConstantAlpha         = 12,
        OneMinusConstantAlpha = 13,
        SrcAlphaSaturate      = 14,
        Src1Color             = 15,
        OneMinusSrc1Color     = 16,
        Src1Alpha             = 17,
        OneMinusSrc1Alpha     = 18,
    };

    enum class CompareOp : EnumSmall
    {
        Never          = 0,
        Less           = 1,
        Equal          = 2,
        LessOrEqual    = 3,
        Greater        = 4,
        NotEqual       = 5,
        GreaterOrEqual = 6,
        Always         = 7,
    };

    enum class LogicOp : EnumSmall
    {
        Clear        = 0,
        And          = 1,
        AndReverse   = 2,
        Copy         = 3,
        AndInverted  = 4,
        NoOp         = 5,
        Xor          = 6,
        Or           = 7,
        Nor          = 8,
        Equivalent   = 9,
        Invert       = 10,
        OrReverse    = 11,
        CopyInverted = 12,
        OrInverted   = 13,
        Nand         = 14,
        Set          = 15,
    };

    struct VertexAttributeDescriptor final
    {
        Format format{Format::Undefined};
        UInt32 binding{0};
        UInt32 location{0};
        UInt32 stride{0};
    };

    struct ColorBlendAttachmentDescriptor final
    {
        BlendOp     alpha_blend_op{BlendOp::Add};
        BlendFactor src_alpha_blend_factor{BlendFactor::Zero};
        BlendFactor dst_alpha_blend_factor{BlendFactor::Zero};
        BlendOp     color_blend_op{BlendOp::Add};
        BlendFactor src_color_blend_factor{BlendFactor::Zero};
        BlendFactor dst_color_blend_factor{BlendFactor::Zero};
        bool        enable_blend{false};
    };

    struct GraphicsPipelineDescriptor final
    {
        PipelineLayoutHandle layout{};

        // Shader stage info
        Span<const ShaderStageDescriptor> shader_stages{};

        // Viewport info
        Span<const Viewport> viewports{};
        Span<const Scissor>  scissors{};

        // Dynamic state info
        Span<const DynamicState> dynamic_states{};

        // Render pass info
        RenderPassHandle render_pass{};
        UInt32           render_pass_subpass{0};

        // Vertex input state info
        Span<const VertexAttributeDescriptor> vertex_attributes{};

        // Input assembly info
        PrimitiveTopology input_assembly_topology{PrimitiveTopology::TriangleList};

        // Depth/Stencil state inof
        CompareOp depth_compare_method{CompareOp::Never};
        bool      depth_enable_test{false};
        bool      depth_enable_write{false};

        // Rasterization state
        Winding     rasterizer_winding{Winding::CounterClockwise};
        PolygonMode rasterizer_polygon_mode{PolygonMode::Fill};
        CullMode    rasterizer_cull_mode{CullMode::Back};
        float       rasterizer_line_width{1.0f};
        bool        rasterizer_enable_depth_clamp{false};
        bool        rasterizer_enable_discard{false};

        // Color blending info
        Span<const ColorBlendAttachmentDescriptor> color_blend_attachments{};
        LogicOp                                    color_blend_logic_op{LogicOp::Clear};
        bool                                       color_blend_enable_logic_op{false};
    };

    using GraphicsPipelineHandle = ResourceHandle<ResourceType::GraphicsPipeline>;
}
