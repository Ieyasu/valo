module;

#include <Valo/Macros.hpp>

export module Valo.RHI:Image;

import Valo.Std;

import :Format;
import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    enum class ImageDimensions : EnumSmall
    {
        One       = 0,
        Two       = 1,
        Three     = 2,
        Cube      = 3,
        Array1D   = 4,
        Array2D   = 5,
        ArrayCube = 6,
    };

    VALO_FLAGS(ImageUsage, ImageUsageFlags){
        None                   = 0,
        TransferSrc            = Bit::Set<Flags>(0),
        TransferDst            = Bit::Set<Flags>(1),
        Sampled                = Bit::Set<Flags>(2),
        Storage                = Bit::Set<Flags>(3),
        ColorAttachment        = Bit::Set<Flags>(4),
        DepthStencilAttachment = Bit::Set<Flags>(5),
        InputAttachment        = Bit::Set<Flags>(6),
    };

    struct ImageDescriptor final
    {
        ImageUsageFlags usage{ImageUsage::None};
        Format          format{Format::Undefined};
        ImageDimensions dimensions{ImageDimensions::Two};
        UInt32          width{0};
        UInt32          height{0};
        UInt32          depth{0};
        UInt32          mip_level_count{0};
        UInt32          array_layer_count{0};
    };

    using ImageHandle = ResourceHandle<ResourceType::Image>;
}
