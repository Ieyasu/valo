module;

#include <Valo/Profile.hpp>

#include <vector>

module Valo.RHI;

import Valo.Profiler;
import Valo.Std;

namespace Valo::RHI
{
    CommandPoolManager::CommandPoolManager(DeviceHandle device) :
        m_device(device)
    {
    }

    CommandPoolManager::~CommandPoolManager()
    {
        for (const auto& thread_id_pool_pair : m_command_pools_per_thread)
        {
            const auto& pool = thread_id_pool_pair.second;
            m_device->DestroyCommandPool(pool);
        }
    }

    UniquePtr<CommandPoolManager> CommandPoolManager::Create(DeviceHandle device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        return UniquePtr<CommandPoolManager>(new CommandPoolManager(device));
    }

    Result<CommandBufferHandle> CommandPoolManager::AquireCommandBuffer(CommandQueueHandle queue)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto [command_pool_result, command_pool] = GetOrCreateCommandPool(queue);
        if (command_pool_result != ResultCode::Success)
        {
            return command_pool_result;
        }

        const auto [command_buffer_result, command_buffer] = GetOrCreateCommandBuffer(command_pool);
        if (command_buffer_result != ResultCode::Success)
        {
            return command_buffer_result;
        }

        return command_buffer;
    }

    Result<CommandPoolHandle> CommandPoolManager::GetOrCreateCommandPool(CommandQueueHandle queue)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto thread_id = GetThreadId();
        if (!m_command_pools_per_thread.contains(thread_id))
        {
            auto descriptor                   = CommandPoolDescriptor{};
            descriptor.queue                  = queue;
            const auto [result, command_pool] = m_device->CreateCommandPool(descriptor);
            if (result != ResultCode::Success)
            {
                return result;
            }
            m_command_pools_per_thread[thread_id] = command_pool;
            m_device->SetObjectName(command_pool, StringFunc::Format("Command Pool (thread_id = {})", thread_id));
            return command_pool;
        }
        return m_command_pools_per_thread[thread_id];
    }

    Result<CommandBufferHandle> CommandPoolManager::GetOrCreateCommandBuffer(CommandPoolHandle pool)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        if (!m_pooled_command_buffers.empty())
        {
            const auto command_buffer = m_pooled_command_buffers.back();
            m_aquired_command_buffers.push_back(command_buffer);
            m_pooled_command_buffers.pop_back();
            return command_buffer;
        }

        // Create a new command buffer if couldn't find an existing one
        const auto descriptor = CommandBufferDescriptor{
            .pool = pool,
        };

        const auto [result, command_buffer] = m_device->CreateCommandBuffer(descriptor);
        if (result != ResultCode::Success)
        {
            return result;
        }

        m_aquired_command_buffers.push_back(command_buffer);
        return command_buffer;
    }

    ResultCode CommandPoolManager::ResetCommandPools()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        for (auto& aquired_command_buffer : m_aquired_command_buffers)
        {
            const auto result = aquired_command_buffer->WaitUntilCompleted();
            if (result != ResultCode::Success)
            {
                return result;
            }
        }

        for (const auto& thread_id_pool_pair : m_command_pools_per_thread)
        {
            const auto pool = thread_id_pool_pair.second;
            m_device->ResetCommandPool(pool);
        }

        // All aquired command buffers return to the pool
        m_pooled_command_buffers.insert(
            m_pooled_command_buffers.end(),
            m_aquired_command_buffers.begin(),
            m_aquired_command_buffers.end());
        m_aquired_command_buffers.clear();

        return ResultCode::Success;
    }
}
