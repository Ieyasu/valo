export module Valo.RHI:CommandBuffer;

import Valo.Std;

import :Buffer;
import :CommandPool;
import :ComputePipeline;
import :DescriptorSet;
import :Framebuffer;
import :GraphicsPipeline;
import :Image;
import :PipelineBarrier;
import :PipelineLayout;
import :RenderPass;
import :Types;

export namespace Valo::RHI
{
    struct CommandBufferDescriptor final
    {
        CommandPoolHandle pool{};
    };

    class CommandBuffer
    {
    public:
        CommandBuffer()                                = default;
        CommandBuffer(const CommandBuffer&)            = delete;
        CommandBuffer(CommandBuffer&&)                 = delete;
        CommandBuffer& operator=(const CommandBuffer&) = delete;
        CommandBuffer& operator=(CommandBuffer&&)      = delete;

        virtual ~CommandBuffer() = default;

        virtual StringView GetName() = 0;

        virtual void SetName(String name) = 0;

        virtual ResultCode Begin() = 0;

        virtual ResultCode End() = 0;

        virtual ResultCode Submit() = 0;

        virtual ResultCode Reset() = 0;

        virtual ResultCode WaitUntilCompleted() = 0;

        // Barrier commands

        virtual void CmdPipelineBarrier(
            Span<const BufferMemoryBarrier> buffer_memory_barriers,
            Span<const ImageMemoryBarrier>  image_memory_barriers) = 0;

        // Blit commands
        virtual void CmdCopyBuffer(
            BufferHandle   src_buffer,
            DeviceSizeType src_offset,
            BufferHandle   dst_buffer,
            DeviceSizeType dst_offset,
            DeviceSizeType size) = 0;

        virtual void CmdCopyBufferToImage(
            BufferHandle   src_buffer,
            DeviceSizeType buffer_offset,
            ImageHandle    dst_image,
            UInt32         image_width,
            UInt32         image_height,
            UInt32         image_depth,
            UInt32         image_offset_x,
            UInt32         image_offset_y,
            UInt32         image_offset_z,
            UInt32         image_mip_level,
            UInt32         image_array_layer) = 0;

        // Compute commands

        virtual void CmdBindComputePipeline(ComputePipelineHandle pipeline) = 0;

        virtual void CmdDispatch(UInt32 group_count_x, UInt32 group_count_y, UInt32 group_count_z) = 0;

        // Render commands
        virtual void CmdBeginRenderPass(
            RenderPassHandle       render_pass,
            FramebufferHandle      framebuffer,
            const Rect2D&          render_area,
            Span<const ClearValue> clear_values) = 0;

        virtual void CmdEndRenderPass() = 0;

        virtual void CmdNextSubpass() = 0;

        virtual void CmdBindGraphicsPipeline(GraphicsPipelineHandle pipeline) = 0;

        virtual void CmdBindIndexBuffer(BufferHandle buffer, UInt32 offset, IndexFormat format) = 0;

        virtual void CmdBindVertexBuffers(
            Span<const BufferHandle>   buffers,
            Span<const DeviceSizeType> offsets,
            UInt32                     first_binding) = 0;

        virtual void CmdBindDescriptorSets(
            PipelineLayoutHandle            pipeline_layout,
            Span<const DescriptorSetHandle> descriptor_sets,
            Span<const UInt32>              dynamic_offsets,
            UInt32                          first_set) = 0;

        virtual void CmdSetScissor(const Scissor& scissor) = 0;

        virtual void CmdSetViewport(const Viewport& viewport) = 0;

        virtual void CmdPresent(ImageHandle image, UInt32 width, UInt32 height) = 0;

        virtual void CmdDraw(UInt32 vertex_count, UInt32 instance_count, UInt32 first_vertex, UInt32 first_instance) = 0;

        virtual void CmdDrawIndexed(
            UInt32 index_count,
            UInt32 instance_count,
            UInt32 first_index,
            UInt32 first_instance,
            Int32  vertex_offset) = 0;
    };

    using CommandBufferHandle = ObserverPtr<CommandBuffer>;
}
