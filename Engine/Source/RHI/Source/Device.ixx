export module Valo.RHI:Device;

import Valo.Std;
import Valo.Window;

import :Buffer;
import :CommandBuffer;
import :CommandPool;
import :CommandQueue;
import :ComputePipeline;
import :DescriptorSet;
import :DescriptorSetLayout;
import :DeviceCapabilities;
import :Framebuffer;
import :GraphicsPipeline;
import :Image;
import :PipelineLayout;
import :RenderPass;
import :ResourceHandle;
import :Sampler;
import :Types;

export namespace Valo::RHI
{
    struct DeviceDescriptor final
    {
        WindowContextHandle window_context{nullptr};
    };

    class Device
    {
    public:
        Device()                               = default;
        Device(Device&& other)                 = delete;
        Device(const Device& other)            = delete;
        Device& operator=(Device&& other)      = delete;
        Device& operator=(const Device& other) = delete;

        virtual ~Device() = default;

        virtual StringView GetName() = 0;

        virtual void SetName(String name) = 0;

        virtual DeviceCapabilities GetCapabilities() const = 0;

        virtual DeviceLimits GetLimits() const = 0;

        virtual CommandQueueHandle GetComputeQueue() const = 0;

        virtual CommandQueueHandle GetGraphicsQueue() const = 0;

        virtual CommandQueueHandle GetTransferQueue() const = 0;

        virtual ResultCode WaitIdle() const = 0;

        virtual void SetObjectName(BaseResourceHandle handle, StringView name) = 0;

        virtual Result<SwapchainInfo> AddWindow(WindowHandle window) = 0;

        virtual void RemoveWindow(WindowHandle window) = 0;

        virtual Result<void*> MapBuffer(BufferHandle buffer) const = 0;

        virtual void UnmapBuffer(BufferHandle buffer) const = 0;

        virtual Result<BufferHandle> CreateBuffer(const BufferDescriptor& descriptor) = 0;

        virtual void DestroyBuffer(BufferHandle buffer) = 0;

        virtual Result<ImageHandle> CreateImage(const ImageDescriptor& descriptor) = 0;

        virtual void DestroyImage(ImageHandle image) = 0;

        virtual Result<SamplerHandle> CreateSampler(const SamplerDescriptor& descriptor) = 0;

        virtual void DestroySampler(SamplerHandle sampler) = 0;

        virtual Result<DescriptorSetHandle> CreateDescriptorSet(const DescriptorSetDescriptor& descriptor) = 0;

        virtual void DestroyDescriptorSet(DescriptorSetHandle set) = 0;

        virtual void UpdateDescriptorSets(Span<const DescriptorSetUpdate> updates) = 0;

        virtual Result<DescriptorSetLayoutHandle> CreateDescriptorSetLayout(
            const DescriptorSetLayoutDescriptor& descriptor) = 0;

        virtual void DestroyDescriptorSetLayout(DescriptorSetLayoutHandle layout) = 0;

        virtual Result<RenderPassHandle> CreateRenderPass(const RenderPassDescriptor& descriptor) = 0;

        virtual void DestroyRenderPass(RenderPassHandle render_pass) = 0;

        virtual Result<FramebufferHandle> CreateFramebuffer(const FramebufferDescriptor& descriptor) = 0;

        virtual void DestroyFramebuffer(FramebufferHandle framebuffer) = 0;

        virtual Result<ComputePipelineHandle> CreateComputePipeline(const ComputePipelineDescriptor& descriptor) = 0;

        virtual void DestroyComputePipeline(ComputePipelineHandle pipeline) = 0;

        virtual Result<GraphicsPipelineHandle> CreateGraphicsPipeline(const GraphicsPipelineDescriptor& descriptor) = 0;

        virtual void DestroyGraphicsPipeline(GraphicsPipelineHandle pipeline) = 0;

        virtual Result<PipelineLayoutHandle> CreatePipelineLayout(const PipelineLayoutDescriptor& descriptor) = 0;

        virtual void DestroyPipelineLayout(PipelineLayoutHandle layout) = 0;

        virtual Result<CommandPoolHandle> CreateCommandPool(const CommandPoolDescriptor& descriptor) = 0;

        virtual ResultCode ResetCommandPool(CommandPoolHandle command_pool) = 0;

        virtual void DestroyCommandPool(CommandPoolHandle pool) = 0;

        virtual Result<CommandBufferHandle> CreateCommandBuffer(const CommandBufferDescriptor& descriptor) = 0;
    };

    class Device;
    using DeviceHandle = ObserverPtr<Device>;
}
