export module Valo.RHI:Format;

import Valo.Std;

export namespace Valo::RHI
{
    // All enumerations values match their Vulkan counter parts
    enum class Format : EnumSmall
    {
        Undefined = 0,

        // Depth/Stencil formats
        S8_UInt           = 127,
        D16_UNorm         = 124,
        D32_Float         = 126,
        D16_UNorm_S8_UInt = 128,
        D24_UNorm_S8_UInt = 129,
        D32_Float_S8_UInt = 130,

        // 1 component formats
        R8_UNorm  = 9,
        R8_SNorm  = 10,
        R8_UInt   = 13,
        R8_SInt   = 14,
        R8_sRGB   = 15,
        R16_UNorm = 70,
        R16_SNorm = 71,
        R16_UInt  = 74,
        R16_SInt  = 75,
        R16_Float = 76,
        R32_UInt  = 98,
        R32_SInt  = 99,
        R32_Float = 100,
        R64_UInt  = 110,
        R64_SInt  = 111,
        R64_Float = 112,

        // 2 component formats
        R8G8_UNorm   = 16,
        R8G8_SNorm   = 17,
        R8G8_UInt    = 20,
        R8G8_SInt    = 21,
        R8G8_sRGB    = 22,
        R16G16_UNorm = 77,
        R16G16_SNorm = 78,
        R16G16_UInt  = 81,
        R16G16_SInt  = 82,
        R16G16_Float = 83,
        R32G32_UInt  = 101,
        R32G32_SInt  = 102,
        R32G32_Float = 103,
        R64G64_UInt  = 113,
        R64G64_SInt  = 114,
        R64G64_Float = 115,

        // 3 component formats
        R8G8B8_UNorm    = 23,
        R8G8B8_SNorm    = 24,
        R8G8B8_UInt     = 27,
        R8G8B8_SInt     = 28,
        R8G8B8_sRGB     = 29,
        B8G8R8_UNorm    = 30,
        B8G8R8_SNorm    = 31,
        B8G8R8_UInt     = 34,
        B8G8R8_SInt     = 35,
        B8G8R8_sRGB     = 36,
        R16G16B16_UNorm = 84,
        R16G16B16_SNorm = 85,
        R16G16B16_UInt  = 88,
        R16G16B16_SInt  = 89,
        R16G16B16_Float = 90,
        R32G32B32_UInt  = 104,
        R32G32B32_SInt  = 105,
        R32G32B32_Float = 106,
        R64G64B64_UInt  = 116,
        R64G64B64_SInt  = 117,
        R64G64B64_Float = 118,
        B10G11R11_Float = 122,

        // 4 component formats
        R8G8B8A8_UNorm     = 37,
        R8G8B8A8_SNorm     = 38,
        R8G8B8A8_UInt      = 41,
        R8G8B8A8_SInt      = 42,
        R8G8B8A8_sRGB      = 43,
        B8G8R8A8_UNorm     = 44,
        B8G8R8A8_SNorm     = 45,
        B8G8R8A8_UInt      = 48,
        B8G8R8A8_SInt      = 49,
        B8G8R8A8_sRGB      = 50,
        A8B8G8R8_UNorm     = 51,
        A8B8G8R8_SNorm     = 52,
        A8B8G8R8_UInt      = 55,
        A8B8G8R8_SInt      = 56,
        A8B8G8R8_sRGB      = 57,
        A2R10G10B10_UNorm  = 58,
        A2R10G10B10_SNorm  = 59,
        A2R10G10B10_UInt   = 62,
        A2R10G10B10_SInt   = 63,
        A2B10G10R10_UNorm  = 64,
        A2B10G10R10_SNorm  = 65,
        A2B10G10R10_UInt   = 68,
        A2B10G10R10_SInt   = 69,
        R16G16B16A16_UNorm = 91,
        R16G16B16A16_SNorm = 92,
        R16G16B16A16_UInt  = 95,
        R16G16B16A16_SInt  = 96,
        R16G16B16A16_Float = 97,
        R32G32B32A32_UInt  = 107,
        R32G32B32A32_SInt  = 108,
        R32G32B32A32_Float = 109,
        R64G64B64A64_UInt  = 119,
        R64G64B64A64_SInt  = 120,
        R64G64B64A64_Float = 121,
    };

    inline bool IsDepthStencil(Format format)
    {
        switch (format)
        {
        case Format::S8_UInt:
        case Format::D16_UNorm:
        case Format::D16_UNorm_S8_UInt:
        case Format::D24_UNorm_S8_UInt:
        case Format::D32_Float:
        case Format::D32_Float_S8_UInt:
            return true;
        default:
            return false;
        }
    }

    enum class ComponentType : EnumSmall
    {
        R       = 0,
        G       = 1,
        B       = 2,
        A       = 3,
        Depth   = 4,
        Stencil = 5,
    };

    enum class ComponentPacking : EnumSmall
    {
        UNorm = 0,
        SNorm = 1,
        UInt  = 2,
        SInt  = 3,
        Float = 4,
        sRGB  = 5,
    };

    struct Component final
    {
        using SizeType = UInt8;

        ComponentType    type{ComponentType::R};
        ComponentPacking packing{ComponentPacking::UNorm};
        SizeType         size{0};

        bool operator==(const Component& other) const;
        bool operator!=(const Component& other) const;
    };

    class FormatInfo final
    {
    public:
        FormatInfo() = default;
        explicit FormatInfo(Format format);
        explicit FormatInfo(Component x);
        FormatInfo(Component x, Component y);
        FormatInfo(Component x, Component y, Component z);
        FormatInfo(Component x, Component y, Component z, Component w);

        UInt32           GetStride() const;
        bool             IsUniform() const;
        size_t           GetComponentCount() const;
        const Component& GetComponent(size_t i) const;

    private:
        static FormatInfo MakeDepth(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeStencil(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeDepthStencil(
            ComponentPacking    depth_packing,
            Component::SizeType depth_size,
            ComponentPacking    stencil_packing,
            Component::SizeType stencil_size);
        static FormatInfo Make(Format format);
        static FormatInfo MakeR(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeRG(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeRGB(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeBGR(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeRGBA(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeABGR(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeBGRA(ComponentPacking packing, Component::SizeType size);
        static FormatInfo MakeARGBPacked(ComponentPacking packing);
        static FormatInfo MakeABGRPacked(ComponentPacking packing);

        Array<Component, 4> m_components{};
        size_t              m_component_count{0};
    };
}
