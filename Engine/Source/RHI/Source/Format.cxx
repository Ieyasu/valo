module;

#include <Valo/Macros.hpp>

module Valo.RHI;

import Valo.Logger;
import Valo.Std;

namespace Valo::RHI
{
    bool Component::operator==(const Component& other) const
    {
        return type == other.type && packing == other.packing && size == other.size;
    }

    bool Component::operator!=(const Component& other) const
    {
        return !(*this == other);
    }

    FormatInfo::FormatInfo(Format format)
    {
        const auto info   = Make(format);
        m_component_count = info.m_component_count;
        m_components      = info.m_components;
    }

    FormatInfo::FormatInfo(Component x) :
        m_components{
            {x, Component{}, Component{}, Component{}}
    },
        m_component_count{1}
    {
    }

    FormatInfo::FormatInfo(Component x, Component y) :
        m_components{
            {x, y, Component{}, Component{}}
    },
        m_component_count{2}
    {
    }

    FormatInfo::FormatInfo(Component x, Component y, Component z) :
        m_components{
            {x, y, z, Component{}}
    },
        m_component_count{3}
    {
    }

    FormatInfo::FormatInfo(Component x, Component y, Component z, Component w) :
        m_components{
            {x, y, z, w}
    },
        m_component_count{4}
    {
    }

    UInt32 FormatInfo::GetStride() const
    {
        UInt32 stride = 0;
        for (Component::SizeType i = 0; i < m_component_count; ++i)
        {
            stride += m_components[i].size;
        }
        VALO_ASSERT(stride % 8 == 0);
        return stride / 8;
    }

    bool FormatInfo::IsUniform() const
    {
        const auto& lhs = m_components[0];
        for (Component::SizeType i = 1; i < m_component_count; ++i)
        {
            const auto& rhs = m_components[i];
            if (lhs.packing != rhs.packing || lhs.size != rhs.size)
            {
                return false;
            }
        }
        return true;
    }

    size_t FormatInfo::GetComponentCount() const
    {
        return m_component_count;
    }

    const Component& FormatInfo::GetComponent(size_t i) const
    {
        VALO_ASSERT(i < m_components.size());
        return m_components[i];
    }

    FormatInfo FormatInfo::MakeDepth(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo({ComponentType::Depth, packing, size});
    }

    FormatInfo FormatInfo::MakeStencil(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo({ComponentType::Stencil, packing, size});
    }

    FormatInfo FormatInfo::MakeDepthStencil(
        ComponentPacking    depth_packing,
        Component::SizeType depth_size,
        ComponentPacking    stencil_packing,
        Component::SizeType stencil_size)
    {
        auto depth   = Component{ComponentType::Depth, depth_packing, depth_size};
        auto stencil = Component{ComponentType::Stencil, stencil_packing, stencil_size};
        return {depth, stencil};
    }

    FormatInfo FormatInfo::MakeR(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo({ComponentType::R, packing, size});
    }

    FormatInfo FormatInfo::MakeRG(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo({ComponentType::R, packing, size}, {ComponentType::G, packing, size});
    }

    FormatInfo FormatInfo::MakeRGB(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo(
            {ComponentType::R, packing, size},
            {ComponentType::G, packing, size},
            {ComponentType::B, packing, size});
    }

    FormatInfo FormatInfo::MakeBGR(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo(
            {ComponentType::B, packing, size},
            {ComponentType::G, packing, size},
            {ComponentType::R, packing, size});
    }

    FormatInfo FormatInfo::MakeRGBA(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo(
            {ComponentType::R, packing, size},
            {ComponentType::G, packing, size},
            {ComponentType::B, packing, size},
            {ComponentType::A, packing, size});
    }

    FormatInfo FormatInfo::MakeABGR(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo(
            {ComponentType::A, packing, size},
            {ComponentType::B, packing, size},
            {ComponentType::G, packing, size},
            {ComponentType::R, packing, size});
    }

    FormatInfo FormatInfo::MakeBGRA(ComponentPacking packing, Component::SizeType size)
    {
        return FormatInfo(
            {ComponentType::B, packing, size},
            {ComponentType::G, packing, size},
            {ComponentType::R, packing, size},
            {ComponentType::A, packing, size});
    }

    FormatInfo FormatInfo::MakeARGBPacked(ComponentPacking packing)
    {
        return FormatInfo(
            {ComponentType::A, packing, 2},
            {ComponentType::R, packing, 10},
            {ComponentType::G, packing, 10},
            {ComponentType::B, packing, 10});
    }

    FormatInfo FormatInfo::MakeABGRPacked(ComponentPacking packing)
    {
        return FormatInfo(
            {ComponentType::A, packing, 2},
            {ComponentType::B, packing, 10},
            {ComponentType::G, packing, 10},
            {ComponentType::R, packing, 10});
    }

    FormatInfo FormatInfo::Make(Format format)
    {
        switch (format)
        {
        // Depth/Stencil formats
        case Format::S8_UInt:
            return MakeStencil(ComponentPacking::UInt, 8);
        case Format::D16_UNorm:
            return MakeDepth(ComponentPacking::UNorm, 16);
        case Format::D32_Float:
            return MakeDepth(ComponentPacking::Float, 32);
        case Format::D16_UNorm_S8_UInt:
            return MakeDepthStencil(ComponentPacking::UNorm, 16, ComponentPacking::UInt, 8);
        case Format::D24_UNorm_S8_UInt:
            return MakeDepthStencil(ComponentPacking::UNorm, 24, ComponentPacking::UInt, 8);
            break;
        case Format::D32_Float_S8_UInt:
            return MakeDepthStencil(ComponentPacking::Float, 32, ComponentPacking::UInt, 8);
        case Format::R8_UNorm:
            return MakeR(ComponentPacking::UNorm, 8);
        case Format::R8_SNorm:
            return MakeR(ComponentPacking::SNorm, 8);
        case Format::R8_UInt:
            return MakeR(ComponentPacking::UInt, 8);
        case Format::R8_SInt:
            return MakeR(ComponentPacking::SInt, 8);
        case Format::R8_sRGB:
            return MakeR(ComponentPacking::sRGB, 8);
        case Format::R16_UNorm:
            return MakeR(ComponentPacking::UNorm, 16);
        case Format::R16_SNorm:
            return MakeR(ComponentPacking::SNorm, 16);
        case Format::R16_UInt:
            return MakeR(ComponentPacking::UInt, 16);
        case Format::R16_SInt:
            return MakeR(ComponentPacking::SInt, 16);
        case Format::R16_Float:
            return MakeR(ComponentPacking::Float, 16);
        case Format::R32_UInt:
            return MakeR(ComponentPacking::UInt, 32);
        case Format::R32_SInt:
            return MakeR(ComponentPacking::SInt, 32);
        case Format::R32_Float:
            return MakeR(ComponentPacking::Float, 32);
        case Format::R64_UInt:
            return MakeR(ComponentPacking::UInt, 64);
        case Format::R64_SInt:
            return MakeR(ComponentPacking::SInt, 64);
        case Format::R64_Float:
            return MakeR(ComponentPacking::Float, 64);
        case Format::R8G8_UNorm:
            return MakeRG(ComponentPacking::UNorm, 8);
        case Format::R8G8_SNorm:
            return MakeRG(ComponentPacking::SNorm, 8);
        case Format::R8G8_UInt:
            return MakeRG(ComponentPacking::UInt, 8);
        case Format::R8G8_SInt:
            return MakeRG(ComponentPacking::SInt, 8);
        case Format::R8G8_sRGB:
            return MakeRG(ComponentPacking::sRGB, 8);
        case Format::R16G16_UNorm:
            return MakeRG(ComponentPacking::UNorm, 16);
        case Format::R16G16_SNorm:
            return MakeRG(ComponentPacking::SNorm, 16);
        case Format::R16G16_UInt:
            return MakeRG(ComponentPacking::UInt, 16);
        case Format::R16G16_SInt:
            return MakeRG(ComponentPacking::SInt, 16);
        case Format::R16G16_Float:
            return MakeRG(ComponentPacking::Float, 16);
        case Format::R32G32_UInt:
            return MakeRG(ComponentPacking::UInt, 32);
        case Format::R32G32_SInt:
            return MakeRG(ComponentPacking::SInt, 32);
        case Format::R32G32_Float:
            return MakeRG(ComponentPacking::Float, 32);
        case Format::R64G64_UInt:
            return MakeRG(ComponentPacking::UInt, 64);
        case Format::R64G64_SInt:
            return MakeRG(ComponentPacking::SInt, 64);
        case Format::R64G64_Float:
            return MakeRG(ComponentPacking::Float, 64);
        case Format::R8G8B8_UNorm:
            return MakeRGB(ComponentPacking::UNorm, 8);
        case Format::R8G8B8_SNorm:
            return MakeRGB(ComponentPacking::SNorm, 8);
        case Format::R8G8B8_UInt:
            return MakeRGB(ComponentPacking::UInt, 8);
        case Format::R8G8B8_SInt:
            return MakeRGB(ComponentPacking::SInt, 8);
        case Format::R8G8B8_sRGB:
            return MakeRGB(ComponentPacking::sRGB, 8);
        case Format::B8G8R8_UNorm:
            return MakeBGR(ComponentPacking::UNorm, 8);
        case Format::B8G8R8_SNorm:
            return MakeBGR(ComponentPacking::SNorm, 8);
        case Format::B8G8R8_UInt:
            return MakeBGR(ComponentPacking::UInt, 8);
        case Format::B8G8R8_SInt:
            return MakeBGR(ComponentPacking::SInt, 8);
        case Format::B8G8R8_sRGB:
            return MakeBGR(ComponentPacking::sRGB, 8);
        case Format::R16G16B16_UNorm:
            return MakeRGB(ComponentPacking::UNorm, 16);
        case Format::R16G16B16_SNorm:
            return MakeRGB(ComponentPacking::SNorm, 16);
        case Format::R16G16B16_UInt:
            return MakeRGB(ComponentPacking::UInt, 16);
        case Format::R16G16B16_SInt:
            return MakeRGB(ComponentPacking::SInt, 16);
        case Format::R16G16B16_Float:
            return MakeRGB(ComponentPacking::Float, 16);
        case Format::R32G32B32_UInt:
            return MakeRGB(ComponentPacking::UInt, 32);
        case Format::R32G32B32_SInt:
            return MakeRGB(ComponentPacking::SInt, 32);
        case Format::R32G32B32_Float:
            return MakeRGB(ComponentPacking::Float, 32);
        case Format::R64G64B64_UInt:
            return MakeRGB(ComponentPacking::UInt, 64);
        case Format::R64G64B64_SInt:
            return MakeRGB(ComponentPacking::SInt, 64);
        case Format::R64G64B64_Float:
            return MakeRGB(ComponentPacking::Float, 64);
        case Format::B10G11R11_Float:
            return FormatInfo(
                {ComponentType::B, ComponentPacking::Float, 10},
                {ComponentType::G, ComponentPacking::Float, 11},
                {ComponentType::R, ComponentPacking::Float, 11});
        case Format::R8G8B8A8_UNorm:
            return MakeRGBA(ComponentPacking::UNorm, 8);
        case Format::R8G8B8A8_SNorm:
            return MakeRGBA(ComponentPacking::SNorm, 8);
        case Format::R8G8B8A8_UInt:
            return MakeRGBA(ComponentPacking::UInt, 8);
        case Format::R8G8B8A8_SInt:
            return MakeRGBA(ComponentPacking::SInt, 8);
        case Format::R8G8B8A8_sRGB:
            return MakeRGBA(ComponentPacking::sRGB, 8);
        case Format::B8G8R8A8_UNorm:
            return MakeBGRA(ComponentPacking::UNorm, 8);
        case Format::B8G8R8A8_SNorm:
            return MakeBGRA(ComponentPacking::SNorm, 8);
        case Format::B8G8R8A8_UInt:
            return MakeBGRA(ComponentPacking::UInt, 8);
        case Format::B8G8R8A8_SInt:
            return MakeBGRA(ComponentPacking::SInt, 8);
        case Format::B8G8R8A8_sRGB:
            return MakeBGRA(ComponentPacking::sRGB, 8);
        case Format::A8B8G8R8_UNorm:
            return MakeABGR(ComponentPacking::UNorm, 8);
        case Format::A8B8G8R8_SNorm:
            return MakeABGR(ComponentPacking::SNorm, 8);
        case Format::A8B8G8R8_UInt:
            return MakeABGR(ComponentPacking::UInt, 8);
        case Format::A8B8G8R8_SInt:
            return MakeABGR(ComponentPacking::SInt, 8);
        case Format::A8B8G8R8_sRGB:
            return MakeABGR(ComponentPacking::sRGB, 8);
        case Format::A2R10G10B10_UNorm:
            return MakeARGBPacked(ComponentPacking::UNorm);
        case Format::A2R10G10B10_SNorm:
            return MakeARGBPacked(ComponentPacking::SNorm);
        case Format::A2R10G10B10_UInt:
            return MakeARGBPacked(ComponentPacking::UInt);
        case Format::A2R10G10B10_SInt:
            return MakeARGBPacked(ComponentPacking::SInt);
        case Format::A2B10G10R10_UNorm:
            return MakeABGRPacked(ComponentPacking::UNorm);
        case Format::A2B10G10R10_SNorm:
            return MakeABGRPacked(ComponentPacking::SNorm);
        case Format::A2B10G10R10_UInt:
            return MakeABGRPacked(ComponentPacking::UInt);
        case Format::A2B10G10R10_SInt:
            return MakeABGRPacked(ComponentPacking::SInt);
        case Format::R16G16B16A16_UNorm:
            return MakeRGBA(ComponentPacking::UNorm, 16);
        case Format::R16G16B16A16_SNorm:
            return MakeRGBA(ComponentPacking::SNorm, 16);
        case Format::R16G16B16A16_UInt:
            return MakeRGBA(ComponentPacking::UInt, 16);
        case Format::R16G16B16A16_SInt:
            return MakeRGBA(ComponentPacking::SInt, 16);
        case Format::R16G16B16A16_Float:
            return MakeRGBA(ComponentPacking::Float, 16);
        case Format::R32G32B32A32_UInt:
            return MakeRGBA(ComponentPacking::UInt, 32);
        case Format::R32G32B32A32_SInt:
            return MakeRGBA(ComponentPacking::SInt, 32);
        case Format::R32G32B32A32_Float:
            return MakeRGBA(ComponentPacking::Float, 32);
        case Format::R64G64B64A64_UInt:
            return MakeRGBA(ComponentPacking::UInt, 64);
        case Format::R64G64B64A64_SInt:
            return MakeRGBA(ComponentPacking::SInt, 64);
        case Format::R64G64B64A64_Float:
            return MakeRGBA(ComponentPacking::Float, 64);
        case Format::Undefined:
        default:
            Log::Error("Failed to get ImageFormatInfo: unknown format{}", format);
            return {};
        }
    }
}
