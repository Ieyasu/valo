export module Valo.RHI:DeviceCapabilities;

import Valo.Std;

import :Buffer;
import :Types;

export namespace Valo::RHI
{
    struct DeviceCapabilities final
    {
        bool ray_tracing{false};
        bool ray_queries{false};
    };

    struct DeviceLimits final
    {
        UInt32         max_push_constants_size{};
        size_t         min_memory_map_alignment{};
        DeviceSizeType min_storage_buffer_offset_alignment{};
        DeviceSizeType min_uniform_buffer_offset_alignment{};
    };
}
