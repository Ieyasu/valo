export module Valo.RHI:Instance;

import Valo.Platform;
import Valo.Std;
import Valo.Window;

import :Device;
import :Types;

export namespace Valo::RHI
{
    struct InstanceDescriptor final
    {
        WindowContextHandle window_context{nullptr};
    };

    class Instance
    {
    public:
        static Result<UniquePtr<Instance, Function<void(Instance*)>>> Create(const InstanceDescriptor& descriptor);

        Instance()                                 = default;
        Instance(Instance&& other)                 = delete;
        Instance(const Instance& other)            = delete;
        Instance& operator=(const Instance& other) = delete;
        Instance& operator=(Instance&& other)      = delete;
        virtual ~Instance()                        = default;

        virtual Result<DeviceHandle> CreateDevice(const DeviceDescriptor& descriptor) = 0;

        virtual void DestroyDevice(DeviceHandle device) = 0;

    private:
        DynamicLibrary m_library;
    };

    using InstanceHandle = ObserverPtr<Instance>;
}
