module;

#include <Valo/Macros.hpp>
#include <Valo/Profile.hpp>

module Valo.RHI;

import Valo.Logger;
import Valo.Platform;
import Valo.Profiler;
import Valo.Std;

namespace Valo::RHI
{
    Result<UniquePtr<Instance, Function<void(Instance*)>>> Instance::Create(const InstanceDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto library = Platform::LoadDynamicLibrary("ValoVulkanRHI");
        if (!library.IsLoaded())
        {
            Log::Error("Failed to create RHI::Instance: RHI library not found");
            return ResultCode::ErrorUnknown;
        }

        auto createInstance = library.GetFunction<Result<InstanceHandle>(const InstanceDescriptor&)>("CreateInstance");
        if (!createInstance.has_value())
        {
            Log::Error("Failed to create RHI::Instance: RHI library does not implement CreateInstance()");
            return ResultCode::ErrorUnknown;
        }

        auto destroyInstance = library.GetFunction<void(InstanceHandle)>("DestroyInstance");
        if (!destroyInstance.has_value())
        {
            Log::Error("Failed to destroy instance: RHI library does not implement DestroyInstance()");
            return ResultCode::ErrorUnknown;
        }

        const auto [result, instance] = createInstance->operator()(descriptor);
        if (result != ResultCode::Success)
        {
            Log::Error("Failed to create RHI::Instance: {}", result);
            return result;
        }

        auto self = UniquePtr<Instance, Function<void(Instance*)>>(
            instance.Get(),
            [destroyInstance](Instance* inst)
            {
                destroyInstance->operator()(inst);
            });
        self->m_library = Memory::Move(library);
        return self;
    }
}
