export module Valo.RHI:RenderPass;

import Valo.Std;

import :Format;
import :Image;
import :ResourceHandle;
import :Types;

export namespace Valo::RHI
{
    enum class AttachmentType : EnumSmall
    {
        None         = 0,
        Color        = 1,
        DepthStencil = 2,
        Input        = 3,
    };

    enum class AttachmentLoadOp : EnumSmall
    {
        Load     = 0,
        Clear    = 1,
        DontCare = 2,
    };

    enum class AttachmentStoreOp : EnumSmall
    {
        Store    = 0,
        DontCare = 1,
    };

    enum class ClearType : EnumSmall
    {
        Color,
        DepthStencil,
    };

    struct ClearValue final
    {
        Array<float, 4> color{0, 0, 0, 0};
        float           depth{1};
        UInt32          stencil{0};
        ClearType       type{ClearType::Color};
    };

    struct AttachmentDescriptor final
    {
        Format            format{Format::Undefined};
        ClearValue        clear_value{};
        AccessFlags       initial_access_flags{};
        AccessFlags       final_access_flags{};
        ImageUsageFlags   initial_usage_flags{};
        ImageUsageFlags   final_usage_flags{};
        AttachmentLoadOp  load_op{AttachmentLoadOp::DontCare};
        AttachmentStoreOp store_op{AttachmentStoreOp::DontCare};
        AttachmentLoadOp  stencil_load_op{AttachmentLoadOp::DontCare};
        AttachmentStoreOp stencil_store_op{AttachmentStoreOp::DontCare};
    };

    struct AttachmentReference final
    {
        AccessFlags access_flags{AccessFlagBits::None};
        UInt32      attachment{0};
    };

    struct SubpassDescriptor final
    {
        Span<const AttachmentReference> color_attachments{};
        Span<const AttachmentReference> input_attachments{};
        const AttachmentReference*      depth_stencil_attachment{nullptr};
    };

    struct RenderPassDescriptor final
    {
        Span<const AttachmentDescriptor> attachments{};
        Span<const SubpassDescriptor>    subpasses{};
    };

    using RenderPassHandle = ResourceHandle<ResourceType::RenderPass>;
}
