export module Valo.AssetDatabase:AssetImportContext;

import Valo.Engine;
import Valo.Logger;
import Valo.Std;

export namespace Valo
{
    struct AssetImportMessage final
    {
        ObserverPtr<const Object> Object{nullptr};
        String                    Message{};
    };

    class AssetImportContext final
    {
    public:
        void Initialize(Path AssetPath);

        const Path& GetAssetPath() const;

        const Vector<AssetImportMessage>& GetErrors() const;

        const Vector<AssetImportMessage>& GetWarnings() const;

        template <CObject T>
        void AddAsset(ObjectPoolHandle<T> Asset);

        template <CObject T>
        ObjectPoolHandle<T> GetAsset() const;

        template <CObject T>
        Vector<ObjectPoolHandle<T>> GetAssets() const;

        void AddErrorMessage(String Message);

        void AddErrorMessage(String Message, const Object& Asset);

        void AddWarningMessage(String Message);

        void AddWarningMessage(String Message, const Object& Asset);

    private:
        Path                                 m_AssetPath{};
        Vector<AssetImportMessage>           m_Errors{};
        Vector<AssetImportMessage>           m_Warnings{};
        Map<TypeIndex, Vector<ObjectHandle>> m_AssetsByType{};

        friend class AssetDatabase;
    };

    template <CObject T>
    void AssetImportContext::AddAsset(ObjectPoolHandle<T> Asset)
    {
        const auto TypeIndex = TypeIndex::Create<T>();
        m_AssetsByType[TypeIndex].push_back(Asset);
    }

    template <CObject T>
    ObjectPoolHandle<T> AssetImportContext::GetAsset() const
    {
        const auto TypeIndex = TypeIndex::Create<T>();
        const auto It        = m_AssetsByType.find(TypeIndex);
        if (It == m_AssetsByType.end())
        {
            Log::Error("Failed to get asset: no asset with the given type available");
            return {};
        }

        const auto Asset = It->second.front();
        return Asset;
    }

    template <CObject T>
    Vector<ObjectPoolHandle<T>> AssetImportContext::GetAssets() const
    {
        const auto TypeIndex = TypeIndex::Create<T>();
        const auto It        = m_AssetsByType.find(TypeIndex);
        if (It == m_AssetsByType.end())
        {
            return {};
        }

        auto Assets = Vector<ObjectPoolHandle<T>>();
        for (const auto& Asset : It->second)
        {
            Assets.push_back(static_cast<ObjectPoolHandle<T>>(Asset));
        }
        return Assets;
    }
}
