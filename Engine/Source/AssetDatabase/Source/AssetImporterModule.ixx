module;

#include <concepts>

export module Valo.AssetDatabase:AssetImporterModule;

import Valo.Engine;
import Valo.Std;

import :AssetImportContext;

export namespace Valo
{
    class AssetImporterModule : public EngineModule
    {
    public:
        AssetImporterModule()                                      = default;
        AssetImporterModule(const AssetImporterModule&)            = delete;
        AssetImporterModule& operator=(const AssetImporterModule&) = delete;
        AssetImporterModule(AssetImporterModule&&)                 = delete;
        AssetImporterModule& operator=(AssetImporterModule&&)      = delete;

        ~AssetImporterModule() override = default;

        virtual bool CanImport(const Path& AssetPath) const = 0;

        virtual void Import(AssetImportContext& Context) = 0;
    };

    using AssetImporterModuleHandle = ObserverPtr<AssetImporterModule>;

    template <typename T>
    concept CAssetImporterModule = std::derived_from<T, AssetImporterModule>;
}
