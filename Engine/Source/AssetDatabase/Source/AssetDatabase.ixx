export module Valo.AssetDatabase:AssetDatabase;

import Valo.Engine;
import Valo.Std;

import :AssetImportContext;
import :AssetImporterModule;

export namespace Valo
{
    class AssetDatabase final : public EngineModule
    {
    public:
        AssetDatabase() = default;

        AssetImportContext Import(const Path& AssetPath);

        void Import(AssetImportContext& Context);

        template <CAssetImporterModule T>
        bool RegisterImporter(T& Importer);

        template <CAssetImporterModule T>
        void DeregisterImporter(const T& Importer);

    private:
        Map<TypeIndex, AssetImporterModuleHandle> m_Importers{};
    };

    template <CAssetImporterModule T>
    bool AssetDatabase::RegisterImporter(T& Importer)
    {
        const auto TypeIndex = TypeIndex::Create<T>();
        const auto It        = m_Importers.find(TypeIndex);
        if (It != m_Importers.end())
        {
            return false;
        }

        m_Importers[TypeIndex] = &Importer;
        return true;
    }

    template <CAssetImporterModule T>
    void AssetDatabase::DeregisterImporter(const T& Importer)
    {
        const auto TypeIndex = TypeIndex::Create<T>();
        if (m_Importers[TypeIndex] == &Importer)
        {
            m_Importers.erase(TypeIndex);
        }
    }

    using AssetDatabaseHandle = ObserverPtr<AssetDatabase>;
}
