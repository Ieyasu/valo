module;

#include <Valo/Profile.hpp>

#include <vector>

module Valo.AssetDatabase;

import Valo.Logger;
import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    AssetImportContext AssetDatabase::Import(const Path& AssetPath)
    {
        VALO_PROFILE_SCOPED_FUNCTION(AssetDB);

        auto Context = AssetImportContext{};
        Context.Initialize(AssetPath);
        Import(Context);
        return Context;
    }

    void AssetDatabase::Import(AssetImportContext& Context)
    {
        VALO_PROFILE_SCOPED_FUNCTION(AssetDB);

        for (auto& Kvp : m_Importers)
        {
            auto& Importer = Kvp.second;
            if (Importer->CanImport(Context.GetAssetPath()))
            {
                Importer->Import(Context);
                break;
            }
        }

        for (const auto& Error : Context.GetErrors())
        {
            Log::Error("Importing asset '{}'. {}", Context.GetAssetPath(), Error.Message);
        }

        for (const auto& Warning : Context.GetWarnings())
        {
            Log::Warning("Importing asset '{}'. {}", Context.GetAssetPath(), Warning.Message);
        }
    }
}
