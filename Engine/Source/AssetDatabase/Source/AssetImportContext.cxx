module Valo.AssetDatabase;

import Valo.Engine;
import Valo.Std;

namespace Valo
{
    void AssetImportContext::Initialize(Path AssetPath)
    {
        m_AssetPath = Memory::Move(AssetPath);
        m_AssetsByType.clear();
        m_Errors.clear();
        m_Warnings.clear();
    }

    const Path& AssetImportContext::GetAssetPath() const
    {
        return m_AssetPath;
    }

    const Vector<AssetImportMessage>& AssetImportContext::GetErrors() const
    {
        return m_Errors;
    }

    const Vector<AssetImportMessage>& AssetImportContext::GetWarnings() const
    {
        return m_Warnings;
    }

    void AssetImportContext::AddErrorMessage(String Message)
    {
        m_Errors.emplace_back(AssetImportMessage{nullptr, Memory::Move(Message)});
    }

    void AssetImportContext::AddErrorMessage(String Message, const Object& Asset)
    {
        m_Errors.emplace_back(AssetImportMessage{&Asset, Memory::Move(Message)});
    }

    void AssetImportContext::AddWarningMessage(String Message)
    {
        m_Warnings.emplace_back(AssetImportMessage{nullptr, Memory::Move(Message)});
    }

    void AssetImportContext::AddWarningMessage(String Message, const Object& Asset)
    {
        m_Warnings.emplace_back(AssetImportMessage{&Asset, Memory::Move(Message)});
    }
}
