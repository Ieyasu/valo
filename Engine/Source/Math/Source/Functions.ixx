module;

#include <glm/gtx/hash.hpp>
#include <glm/gtx/vector_angle.hpp>

export module Valo.Math:Functions;

import :Constants;

export namespace Valo::Math
{
    // Basic functions

    template <typename T>
    inline T Abs(const T& value)
    {
        return glm::abs(value);
    }

    template <typename T>
    inline T Min(const T& lhs, const T& rhs)
    {
        return glm::min(lhs, rhs);
    }

    template <typename T>
    inline T Max(const T& lhs, const T& rhs)
    {
        return glm::max(lhs, rhs);
    }

    constexpr float DegToRad(float degrees)
    {
        return degrees * (1.0f / 180.0f) * Pi;
    }

    constexpr float RadToDeg(float radians)
    {
        return radians * (1.0f / Pi) * 180.0f;
    }

    // Trigonometry

    template <typename T>
    inline T Atan(const T& value)
    {
        return glm::atan(value);
    }

    template <typename T>
    inline T Tan(const T& value)
    {
        return glm::tan(value);
    }

    template <typename T>
    inline T Acos(const T& value)
    {
        return glm::acos(value);
    }

    template <typename T>
    inline T Cos(const T& value)
    {
        return glm::cos(value);
    }

    template <typename T>
    inline T Asin(const T& value)
    {
        return glm::asin(value);
    }

    template <typename T>
    inline T Sin(const T& value)
    {
        return glm::sin(value);
    }
}
