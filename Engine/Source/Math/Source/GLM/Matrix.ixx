module;

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

export module Valo.Math:Matrix;

import :Quaternion;
import :Vector;

export namespace Valo
{
    using DMat2 = glm::dmat2;
    using DMat3 = glm::dmat3;
    using DMat4 = glm::dmat4;

    using FMat2 = glm::mat2;
    using FMat3 = glm::mat3;
    using FMat4 = glm::mat4;

    using Mat2 = FMat2;
    using Mat3 = FMat3;
    using Mat4 = FMat4;

    template <typename T>
    concept CMatInternal = std::is_same_v<T, DMat2> || std::is_same_v<T, DMat3> || std::is_same_v<T, DMat4>
        || std::is_same_v<T, FMat2> || std::is_same_v<T, FMat3> || std::is_same_v<T, FMat4>;

    template <typename T>
    concept CMat = CMatInternal<std::remove_const_t<std::remove_reference_t<T>>>;
}

export namespace Valo::Math
{
    template <typename TMat>
    inline TMat Inverse(const TMat& matrix)
    {
        return glm::inverse(matrix);
    }

    template <typename TMat, typename TQuat>
    inline TMat Rotate(const TMat& matrix, const TQuat& rotation)
    {
        return matrix * Mat4(rotation);
    }

    template <typename TMat, typename TVec>
    inline TMat Rotate(const TMat& matrix, const typename TVec::value_type& angle, const TVec& vector)
    {
        return glm::rotate(matrix, angle, vector);
    }

    template <typename TMat, typename TVec>
    inline TMat Scale(const TMat& matrix, const TVec& scale)
    {
        return glm::scale(matrix, scale);
    }

    template <typename TMat, typename TVec>
    inline TMat Translate(const TMat& matrix, const TVec& translation)
    {
        return glm::translate(matrix, translation);
    }

    inline Mat4 ModelMatrix(const Vec3& translation, const Quaternion& rotation, const Vec3& s)
    {
        auto matrix = Mat4(1.0);
        matrix      = Translate(matrix, translation);
        matrix      = Rotate(matrix, rotation);
        matrix      = Scale(matrix, s);
        return matrix;
    }
}
