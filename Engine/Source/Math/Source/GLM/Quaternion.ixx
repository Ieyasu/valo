module;

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

export module Valo.Math:Quaternion;

export namespace Valo
{
    using DQuaternion = glm::dquat;
    using FQuaternion = glm::quat;
    using Quaternion  = FQuaternion;

    template <typename T>
    concept CQuaternionInternal = std::is_same_v<T, DQuaternion> || std::is_same_v<T, FQuaternion>;

    template <typename T>
    concept CQuaternion = CQuaternionInternal<std::remove_const_t<std::remove_reference_t<T>>>;
}
