module;

#include <glm/glm.hpp>

export module Valo.Math:GLMOperators;

import :Matrix;
import :Quaternion;
import :Vector;

export namespace Valo
{
    template <typename T>
    concept CGLM = CVec<T> || CMat<T> || CQuaternion<T>;

    template <CGLM T>
    bool operator==(const T& lhs, const T& rhs)
    {
        return glm::operator==(lhs, rhs);
    }

    template <CGLM T>
    bool operator!=(const T& lhs, const T& rhs)
    {
        return glm::operator!=(lhs, rhs);
    }

    template <CGLM T>
    T operator-(const T& lhs, const T& rhs)
    {
        return glm::operator-(lhs, rhs);
    }

    template <CGLM T>
    T operator+(const T& lhs, const T& rhs)
    {
        return glm::operator+(lhs, rhs);
    }

    template <CGLM T>
    T operator*(const T& lhs, const T& rhs)
    {
        return glm::operator*(lhs, rhs);
    }

    template <CGLM T>
    T operator/(const T& lhs, const T& rhs)
    {
        return glm::operator/(lhs, rhs);
    }

    template <CGLM T>
    T operator*(const T& lhs, typename T::value_type rhs)
    {
        return glm::operator*(lhs, rhs);
    }

    template <CGLM T>
    T operator*(typename T::value_type lhs, const T& rhs)
    {
        return glm::operator*(lhs, rhs);
    }

    template <CGLM T>
    T operator/(const T& lhs, typename T::value_type rhs)
    {
        return glm::operator/(lhs, rhs);
    }

    template <CGLM T>
    T operator/(typename T::value_type lhs, const T& rhs)
    {
        return glm::operator/(lhs, rhs);
    }
}
