
module;

#include <glm/glm.hpp>
#include <glm/gtx/vector_angle.hpp>

export module Valo.Math:Vector;

export namespace Valo
{
    using DVec2 = glm::dvec2;
    using DVec3 = glm::dvec3;
    using DVec4 = glm::dvec4;

    using FVec2 = glm::vec2;
    using FVec3 = glm::vec3;
    using FVec4 = glm::vec4;

    using IVec2 = glm::ivec2;
    using IVec3 = glm::ivec3;
    using IVec4 = glm::ivec4;

    using UVec2 = glm::uvec2;
    using UVec3 = glm::uvec3;
    using UVec4 = glm::uvec4;

    using Vec2 = FVec2;
    using Vec3 = FVec3;
    using Vec4 = FVec4;

    template <typename T>
    concept CVecInternal = std::is_same_v<T, DVec2> || std::is_same_v<T, DVec3> || std::is_same_v<T, DVec4>
        || std::is_same_v<T, FVec2> || std::is_same_v<T, FVec3> || std::is_same_v<T, FVec4> || std::is_same_v<T, IVec2>
        || std::is_same_v<T, IVec3> || std::is_same_v<T, IVec4> || std::is_same_v<T, UVec2> || std::is_same_v<T, UVec3>
        || std::is_same_v<T, UVec4>;

    template <typename T>
    concept CVec = CVecInternal<std::remove_const_t<std::remove_reference_t<T>>>;
}

export namespace Valo::Math
{
    template <CVec TVec>
    inline TVec::value_type Angle(const TVec& v1, const TVec& v2)
    {
        return glm::angle(v1, v2);
    }

    template <CVec TVec>
    inline TVec::value_type Dot(const TVec& v1, const TVec& v2)
    {
        return glm::dot(v1, v2);
    }

    template <CVec TVec>
    inline TVec Cross(const TVec& v1, const TVec& v2)
    {
        return glm::cross(v1, v2);
    }

    template <CVec TVec>
    inline TVec Normalize(const TVec& v)
    {
        return glm::normalize(v);
    }
}
