module;

#include <numbers>

export module Valo.Math:Constants;

export namespace Valo::Math
{
    constexpr float Pi = static_cast<float>(std::numbers::pi);
}
