module Valo.Math;

namespace Valo
{
    Sphere::Sphere(Vec3 origin, float radius) noexcept :
        m_packed(origin, radius)
    {
    }

    Vec3 Sphere::GetOrigin() const noexcept
    {
        return {m_packed};
    }

    void Sphere::SetOrigin(Vec3 origin) noexcept
    {
        m_packed = Vec4(origin, m_packed[3]);
    }

    float Sphere::GetRadius() const noexcept
    {
        return m_packed[3];
    }

    void Sphere::SetRadius(float radius) noexcept
    {
        m_packed[3] = radius;
    }

    const Vec4& Sphere::GetPacked() const noexcept
    {
        return m_packed;
    }

    void Sphere::SetPacked(Vec4 packed) noexcept
    {
        m_packed = packed;
    }
}
