export module Valo.Math:Sphere;

import :Vector;

export namespace Valo
{
    struct Sphere final
    {
    public:
        Sphere() noexcept = default;

        Sphere(Vec3 origin, float radius) noexcept;

        [[nodiscard]] Vec3 GetOrigin() const noexcept;

        void SetOrigin(Vec3 origin) noexcept;

        [[nodiscard]] float GetRadius() const noexcept;

        void SetRadius(float radius) noexcept;

        [[nodiscard]] const Vec4& GetPacked() const noexcept;

        void SetPacked(Vec4 packed) noexcept;

    private:
        Vec4 m_packed{};
    };
}
