export module Valo.Math:AABB;

import :Vector;

export namespace Valo
{
    class AABB final
    {
    public:
        AABB() noexcept = default;

        AABB(Vec3 min, Vec3 max) noexcept;

        [[nodiscard]] const Vec3& GetMin() const noexcept;

        void SetMin(Vec3 min) noexcept;

        [[nodiscard]] const Vec3& GetMax() const noexcept;

        void SetMax(Vec3 max) noexcept;

        [[nodiscard]] float GetArea() const noexcept;

        [[nodiscard]] Vec3 GetCenter() const noexcept;

        [[nodiscard]] Vec3 GetSize() const noexcept;

        [[nodiscard]] Vec3 GetExtents() const noexcept;

        void Expand(const Vec3& vertex) noexcept;

        void Expand(const AABB& other) noexcept;

        [[nodiscard]] AABB GetExpanded(const AABB& other) const noexcept;

    private:
        Vec3 m_min{};
        Vec3 m_max{};
    };
}
