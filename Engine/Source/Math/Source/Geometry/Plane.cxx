module Valo.Math;

namespace Valo
{
    Plane::Plane(Vec3 normal, float distance) noexcept :
        m_packed(normal, distance)
    {
    }

    Vec3 Plane::GetNormal() const noexcept
    {
        return {m_packed};
    }

    void Plane::SetNormal(Vec3 normal) noexcept
    {
        m_packed = Vec4(normal, m_packed[3]);
    }

    float Plane::GetDistance() const noexcept
    {
        return m_packed[3];
    }

    void Plane::SetDistance(float distance) noexcept
    {
        m_packed[3] = distance;
    }

    const Vec4& Plane::GetPacked() const noexcept
    {
        return m_packed;
    }

    void Plane::SetPacked(Vec4 packed) noexcept
    {
        m_packed = packed;
    }
}
