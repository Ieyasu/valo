export module Valo.Math:Plane;

import :Vector;

export namespace Valo
{
    class Plane final
    {
    public:
        Plane() noexcept = default;

        Plane(Vec3 normal, float distance) noexcept;

        [[nodiscard]] Vec3 GetNormal() const noexcept;

        void SetNormal(Vec3 normal) noexcept;

        [[nodiscard]] float GetDistance() const noexcept;

        void SetDistance(float distance) noexcept;

        [[nodiscard]] const Vec4& GetPacked() const noexcept;

        void SetPacked(Vec4 packed) noexcept;

    private:
        Vec4 m_packed{0, 1, 0, 0};
    };
}
