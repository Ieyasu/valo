module;

#include <glm/glm.hpp>

module Valo.Math;

namespace Valo
{
    AABB::AABB(Vec3 min, Vec3 max) noexcept :
        m_min(min),
        m_max(max)
    {
    }

    Vec3 AABB::GetCenter() const noexcept
    {
        return 0.5f * (m_min + m_max);
    }

    Vec3 AABB::GetSize() const noexcept
    {
        return m_max - m_min;
    }

    Vec3 AABB::GetExtents() const noexcept
    {
        return 0.5f * GetSize();
    }

    float AABB::GetArea() const noexcept
    {
        Vec3 delta = m_max - m_min;
        return delta.x * delta.y * delta.z;
    }

    void AABB::Expand(const Vec3& vertex) noexcept
    {
        m_min = Math::Min(m_min, vertex);
        m_max = Math::Max(m_max, vertex);
    }

    void AABB::Expand(const AABB& other) noexcept
    {
        m_min = Math::Min(m_min, other.m_min);
        m_max = Math::Max(m_max, other.m_max);
    }

    AABB AABB::GetExpanded(const AABB& other) const noexcept
    {
        auto min = Math::Min(m_min, other.m_min);
        auto max = Math::Max(m_max, other.m_max);
        return {min, max};
    }
}
