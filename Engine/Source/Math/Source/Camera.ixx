module;

#include <glm/ext/matrix_clip_space.hpp>

export module Valo.Math:Camera;

import Valo.Std;

import :Functions;
import :Plane;
import :Matrix;

export namespace Valo::Math
{
    inline Mat4 CameraProjectionMatrix(float fovy, float aspect, float z_near, float z_far) noexcept
    {
        const auto h = 1.0f / Math::Tan(0.5f * fovy);

        auto projection_matrix  = Mat4(0);
        projection_matrix[0][0] = h / aspect;
        projection_matrix[1][1] = -h;
        projection_matrix[2][2] = z_near / (z_far - z_near);
        projection_matrix[2][3] = -1.0f;
        projection_matrix[3][2] = (z_far * z_near) / (z_far - z_near);
        return projection_matrix;
    }

    inline Mat4 CameraProjectionMatrix(float fovy, float aspect, float z_near) noexcept
    {
        const auto h = 1.0f / Math::Tan(0.5f * fovy);
        const auto w = h / aspect;

        auto projection_matrix  = Mat4(0);
        projection_matrix[0][0] = w;
        projection_matrix[1][1] = -h;
        projection_matrix[2][2] = 0.0f;
        projection_matrix[2][3] = -1.0f;
        projection_matrix[3][2] = z_near;
        return projection_matrix;
    }

    inline Array<Plane, 4> CameraFrustumPlanes(float /*fovy*/, float /*aspect*/) noexcept
    {
        Array<Plane, 4> planes;
        return planes;
    }

    inline Array<Plane, 6> CameraFrustumPlanes(float /*fovy*/, float /*aspect*/, float /*z_near*/) noexcept
    {
        Array<Plane, 6> planes;
        return planes;
    }
}
