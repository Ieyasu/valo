module;

#include <Valo/Profile.hpp>

#include <chrono>

module Valo.Engine;

import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    bool Time::Initialize(Engine& /*engine*/)
    {
        VALO_PROFILE_SCOPED_FUNCTION(Core);

        Reset();
        return true;
    }

    void Time::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Core);

        UpdateTime();
        UpdateFrame();
    }

    UInt64 Time::GetFrame() const noexcept
    {
        return m_frame;
    }

    TimePoint Time::GetStartTime() const noexcept
    {
        return m_start_time;
    }

    TimePoint Time::GetFrameTime() const noexcept
    {
        return m_frame_time;
    }

    Duration Time::GetDeltaTime() const noexcept
    {
        return m_delta_time;
    }

    Duration Time::GetTimeSinceStart() const noexcept
    {
        return m_time_since_start;
    }

    void Time::Reset() noexcept
    {
        m_frame      = 0;
        m_start_time = std::chrono::system_clock::now();
        m_frame_time = std::chrono::system_clock::now();
        UpdateTime();
    }

    void Time::UpdateTime() noexcept
    {
        auto new_frame_time = std::chrono::system_clock::now();
        m_delta_time        = new_frame_time - m_frame_time;
        m_frame_time        = new_frame_time;
        m_time_since_start  = new_frame_time - m_start_time;
    }

    void Time::UpdateFrame() noexcept
    {
        m_frame = m_frame + 1;
    }
}
