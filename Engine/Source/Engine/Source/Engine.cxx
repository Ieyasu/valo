module;

#include <Valo/Profile.hpp>

module Valo.Engine;

import Valo.Logger;
import Valo.Profiler;
import Valo.Std;

namespace Valo
{
    bool Engine::IsRunning() const
    {
        return m_is_running;
    }

    void Engine::Start()
    {
        if (m_is_running)
        {
            Log::Warning("Failed to run Engine::start(): engine already running.");
            return;
        }

        VALO_PROFILE_BEGIN_THREAD("Main Thread");
        m_is_running = true;
    }

    void Engine::Stop()
    {
        if (!m_is_running)
        {
            Log::Warning("Failed to run Engine::stop(): engine not running.");
            return;
        }

        VALO_PROFILE_END_THREAD();
        m_is_running = false;
    }

    void Engine::Update()
    {
        VALO_PROFILE_SCOPED_FRAME(Frame);
        VALO_PROFILE_SCOPED_FUNCTION(Frame);

        if (!m_is_running)
        {
            Log::Warning("Failed to run Engine::update(): engine not running. Call Engine::start() first.");
            return;
        }

        for (auto& module : m_modules)
        {
            module.second->Update();
        }
    }
}
