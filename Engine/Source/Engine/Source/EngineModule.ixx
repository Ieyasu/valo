export module Valo.Engine:EngineModule;

import Valo.Logger;
import Valo.Std;

import :Engine;

export namespace Valo
{
    class EngineModule
    {
    public:
        EngineModule() = default;

        EngineModule(const EngineModule& other)            = delete;
        EngineModule& operator=(const EngineModule& other) = delete;

        EngineModule(EngineModule&& other)            = default;
        EngineModule& operator=(EngineModule&& other) = default;

        virtual ~EngineModule() = default;

    protected:
        [[nodiscard]] virtual bool Initialize(Engine& /*engine*/)
        {
            return true;
        }

        virtual void Deinitialize()
        {
        }

        virtual void Update()
        {
        }

    private:
        friend class Engine;
    };

    template <CEngineModule T>
    ObserverPtr<T> Engine::Require()
    {
        const auto type_index = TypeIndex::Create<T>();
        const auto it         = m_modules.find(type_index);
        if (it != m_modules.end())
        {
            return static_cast<T*>(it->second.get());
        }

        m_modules[type_index] = Valo::MakeShared<T>();
        if (!m_modules[type_index]->Initialize(*this))
        {
            Log::Fatal("Failed to initialize module! Shutting down.");
        }
        return static_cast<T*>(m_modules[type_index].get());
    }

    template <CEngineModule T>
    ObserverPtr<T> Engine::Optional()
    {
        const auto type_index = TypeIndex::Create<T>();
        const auto it         = m_modules.find(type_index);
        if (it != m_modules.end())
        {
            return static_cast<T*>(it->second.get());
        }
        return nullptr;
    }

    template <CEngineModule T>
    bool Engine::RemoveModule()
    {
        const auto type_index = TypeIndex::Create<T>();
        if (m_modules.contains(type_index))
        {
            return false;
        }

        m_modules.erase(type_index);
        return true;
    }
}
