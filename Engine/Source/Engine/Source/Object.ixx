module;

#include <concepts>

export module Valo.Engine:Object;

import Valo.Serialization;
import Valo.Std;

export namespace Valo
{
    class Object
    {
    public:
        explicit Object(String name);

        Object(const Object& other)            = delete;
        Object& operator=(const Object& other) = delete;

        Object(Object&& other)            = default;
        Object& operator=(Object&& other) = default;

        virtual ~Object() = default;

        [[nodiscard]] virtual const String& GetName() const;

        virtual void SetName(String name);

    protected:
        template <typename OutputStream>
        void Serialize(OutputStream stream) const;

        template <typename InputStream>
        void Deserialize(InputStream stream);

    private:
        String m_name{};
    };

    template <typename T>
    concept CObject = std::derived_from<T, Object>;

    using ObjectHandle = BasePoolHandle<Object>;

    template <CObject T>
    using ObjectPool = Pool<T, Object>;

    template <CObject T>
    using ObjectPoolHandle = PoolHandle<T, Object>;

    template <typename OutputStream>
    void Object::Serialize(OutputStream stream) const
    {
        stream.SerializeProperty("name", m_name);
    }

    template <typename InputStream>
    void Object::Deserialize(InputStream stream)
    {
        stream.DeserializeProperty("name", m_name);
    }
}
