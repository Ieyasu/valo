module;

#include <concepts>

export module Valo.Engine:Engine;

import Valo.Profiler;
import Valo.Std;

export namespace Valo
{
    class EngineModule;

    template <typename T>
    concept CEngineModule = std::derived_from<T, EngineModule>;

    class Engine final
    {
    public:
        [[nodiscard]] bool IsRunning() const;

        void Start();

        void Stop();

        void Update();

        template <CEngineModule T>
        ObserverPtr<T> Require();

        template <CEngineModule T>
        ObserverPtr<T> Optional();

        template <CEngineModule T>
        bool RemoveModule();

    private:
        Map<TypeIndex, SharedPtr<EngineModule>> m_modules{};
        bool                                    m_is_running{};
    };
}
