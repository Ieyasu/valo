module;

#include <chrono>

export module Valo.Engine:Time;

import Valo.Std;

import :Engine;
import :EngineModule;

export namespace Valo
{
    using Duration  = std::chrono::duration<float>;
    using TimePoint = std::chrono::system_clock::time_point;

    class Time final : public EngineModule
    {
    public:
        [[nodiscard]] UInt64  GetFrame() const noexcept;
        [[nodiscard]] TimePoint GetStartTime() const noexcept;
        [[nodiscard]] TimePoint GetFrameTime() const noexcept;
        [[nodiscard]] Duration  GetDeltaTime() const noexcept;
        [[nodiscard]] Duration  GetTimeSinceStart() const noexcept;

    private:
        bool Initialize(Engine& engine) override;
        void Update() override;

        void Reset() noexcept;
        void UpdateTime() noexcept;
        void UpdateFrame() noexcept;

        UInt64  m_frame{0};
        TimePoint m_start_time{};
        TimePoint m_frame_time{};
        Duration  m_delta_time{};
        Duration  m_time_since_start{};
    };

    using TimeHandle = ObserverPtr<Time>;
}
