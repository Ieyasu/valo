module Valo.Engine;

import Valo.Std;

namespace Valo
{
    Object::Object(String name) :
        m_name(Memory::Move(name))
    {
    }

    const String& Object::GetName() const
    {
        return m_name;
    }

    void Object::SetName(String name)
    {
        m_name = name;
    }
}
