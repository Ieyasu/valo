export module Valo.ShaderCompiler:ShaderCompilerOptions;

import Valo.Std;

export namespace Valo
{
    enum class ShaderLanguage : EnumSmall
    {
        GLSL,
        HLSL,
        Custom,
    };

    enum class ShaderIR : EnumSmall
    {
        DXIL,
        SPIRV,
        Custom,
    };

    struct ShaderCompilerOptions final
    {
        Path Filepath{};

        StringView EntryPoint{};

        ShaderLanguage Language{ShaderLanguage::GLSL};

        ShaderIR IR{ShaderIR::SPIRV};

        Set<StringView> Defines{};

        Set<StringView> Includes{};
    };
}
