module;

#include <Valo/Macros.hpp>

#include <spirv_cross.hpp>

module Valo.ShaderCompiler;

import Valo.Logger;
import Valo.Std;

namespace Valo
{
    Optional<ShaderReflection> ShaderReflectionBuilder::Build(StringView name, const Vector<UInt32>& spirv_binary)
    {
        const auto spirv     = spirv_cross::Compiler(spirv_binary.data(), spirv_binary.size());
        const auto resources = spirv.get_shader_resources();

        // Find entry point and ensure there is only one
        const auto entry_points_and_stages = spirv.get_entry_points_and_stages();
        const auto entry_point_count       = entry_points_and_stages.size();

        if (entry_point_count == 0)
        {
            LogReflectionError(name, "failed to find entry point.");
            return nullopt;
        }

        if (entry_point_count > 1)
        {
            const auto message = StringFunc::Format("only one entry point is allowed (found {}).", entry_point_count);
            LogReflectionError(name, message);
            return nullopt;
        }

        const auto& entry_point     = entry_points_and_stages.front();
        const auto  execution_model = entry_point.execution_model;
        const auto  stage           = ShaderReflectionBuilder::ConvertShaderStage(execution_model);
        if (stage == ShaderStageReflection::None)
        {
            const auto message = StringFunc::Format("unknown shader stage {}.", execution_model);
            LogReflectionError(name, message);
            return nullopt;
        }

        auto shader_reflection          = ShaderReflection{};
        shader_reflection.m_name        = name;
        shader_reflection.m_stage       = stage;
        shader_reflection.m_entry_point = entry_point.name;

        // Create reflections for all supported binding types.
        for (const auto& resource : resources.uniform_buffers)
        {
            ShaderReflectionBuilder::BuildBindingReflection(
                shader_reflection,
                spirv,
                resource,
                DescriptorTypeReflection::UniformBuffer);
        }

        for (const auto& resource : resources.storage_buffers)
        {
            ShaderReflectionBuilder::BuildBindingReflection(
                shader_reflection,
                spirv,
                resource,
                DescriptorTypeReflection::StorageBuffer);
        }

        for (const auto& resource : resources.sampled_images)
        {
            ShaderReflectionBuilder::BuildBindingReflection(
                shader_reflection,
                spirv,
                resource,
                DescriptorTypeReflection::CombinedImageSampler);
        }

        for (const auto& resource : resources.separate_images)
        {
            ShaderReflectionBuilder::BuildBindingReflection(
                shader_reflection,
                spirv,
                resource,
                DescriptorTypeReflection::SampledImage);
        }

        for (const auto& resource : resources.storage_images)
        {
            ShaderReflectionBuilder::BuildBindingReflection(
                shader_reflection,
                spirv,
                resource,
                DescriptorTypeReflection::StorageImage);
        }

        for (const auto& resource : resources.stage_inputs)
        {
            ShaderReflectionBuilder::BuildStageInputReflection(shader_reflection, spirv, resource);
        }

        for (const auto& resource : resources.stage_outputs)
        {
            ShaderReflectionBuilder::BuildStageOutputReflection(shader_reflection, spirv, resource);
        }

        // Sort the descriptor sets in ascending order
        const auto sort = [](const DescriptorSetReflection& a, const DescriptorSetReflection& b)
        {
            return a.set < b.set;
        };
        std::ranges::sort(shader_reflection.m_descriptor_sets, sort);

        // Create reflections for push constants
        for (const auto& resource : resources.push_constant_buffers)
        {
            const auto& ranges = spirv.get_active_buffer_ranges(resource.id);
            const auto& type   = spirv.get_type(resource.type_id);

            shader_reflection.m_push_constant_ranges.reserve(ranges.size());
            for (const auto& range : ranges)
            {
                const auto& member_name = spirv.get_member_name(resource.base_type_id, range.index);
                const auto& member_type = type.member_types[range.index];
                ShaderReflectionBuilder::BuildConstantReflection(
                    shader_reflection,
                    spirv,
                    member_type,
                    member_name,
                    static_cast<UInt32>(range.offset));

                auto& push_constants_range  = shader_reflection.m_push_constant_ranges.emplace_back();
                push_constants_range.offset = static_cast<UInt32>(range.offset);
                push_constants_range.size   = static_cast<UInt32>(range.range);

                Log::Trace("Parsed {} bytes of push constant data", push_constants_range.size);
            }
        }

        return shader_reflection;
    }

    void ShaderReflectionBuilder::BuildStageInputReflection(
        ShaderReflection&            shader_reflection,
        const spirv_cross::Compiler& spirv,
        const spirv_cross::Resource& resource)
    {
        auto& stage_reflection    = shader_reflection.m_stage_inputs.emplace_back();
        stage_reflection.name     = spirv.get_name(resource.id);
        stage_reflection.location = spirv.get_decoration(resource.id, spv::Decoration::DecorationLocation);
    }

    void ShaderReflectionBuilder::BuildStageOutputReflection(
        ShaderReflection&            shader_reflection,
        const spirv_cross::Compiler& spirv,
        const spirv_cross::Resource& resource)
    {
        auto& stage_reflection    = shader_reflection.m_stage_outputs.emplace_back();
        stage_reflection.name     = spirv.get_name(resource.id);
        stage_reflection.type     = ConvertType(spirv.get_type(resource.type_id));
        stage_reflection.location = spirv.get_decoration(resource.id, spv::Decoration::DecorationLocation);
    }

    void ShaderReflectionBuilder::BuildBindingReflection(
        ShaderReflection&            shader_reflection,
        const spirv_cross::Compiler& spirv,
        const spirv_cross::Resource& resource,
        DescriptorTypeReflection     descriptor_type)

    {
        const bool is_buffer = descriptor_type == DescriptorTypeReflection::UniformBuffer
            || descriptor_type == DescriptorTypeReflection::StorageBuffer;

        auto& binding      = shader_reflection.m_descriptor_set_bindings.emplace_back();
        binding.name       = is_buffer ? spirv.get_name(resource.base_type_id) : spirv.get_name(resource.id);
        binding.set        = spirv.get_decoration(resource.id, spv::DecorationDescriptorSet);
        binding.binding    = spirv.get_decoration(resource.id, spv::DecorationBinding);
        binding.type       = descriptor_type;
        binding.array_size = 0;

        VALO_ASSERT(!binding.name.empty());
        VALO_ASSERT(!shader_reflection.m_property_ids.contains(binding.name));
        auto& property_id = shader_reflection.m_property_ids[binding.name];
        property_id.type  = PropertyType::DescriptorSetBinding;
        property_id.m_id  = static_cast<UInt32>(shader_reflection.m_descriptor_set_bindings.size()) - 1;

        // Add the set to the reflection data if it does not exist yet
        auto& descriptor_sets = shader_reflection.m_descriptor_sets;

        const auto exists = [&binding](const DescriptorSetReflection& reflection)
        {
            return reflection.set == binding.set;
        };

        if (std::find_if(descriptor_sets.begin(), descriptor_sets.end(), exists) == descriptor_sets.end())
        {
            auto& descriptor_set = shader_reflection.m_descriptor_sets.emplace_back();
            descriptor_set.set   = binding.set;
        }
    }

    UInt32 ShaderReflectionBuilder::BuildConstantReflection(
        ShaderReflection&            shader_reflection,
        const spirv_cross::Compiler& spirv,
        const spirv_cross::TypeID&   type_id,
        StringView                   name,
        UInt32                       offset)
    {
        auto spirv_type = spirv.get_type(type_id);
        auto base_type  = spirv_type.basetype;
        auto size       = UInt32{0};

        switch (base_type)
        {
        case spirv_cross::SPIRType::Struct:
        {
            for (UInt32 i = 0; i < static_cast<UInt32>(spirv_type.member_types.size()); ++i)
            {
                const auto& member_name = spirv.get_member_name(type_id, i);
                const auto& member_type = spirv_type.member_types[i];

                auto child_name = String(name);
                child_name.append(".");
                child_name.append(member_name);

                offset += BuildConstantReflection(shader_reflection, spirv, member_type, child_name, offset);
            }
            return static_cast<UInt32>(spirv.get_declared_struct_size(spirv_type));
        }
        case spirv_cross::SPIRType::Int:
        {
            size = spirv_type.columns * spirv_type.vecsize * static_cast<UInt32>(sizeof(Int32));
            break;
        }
        case spirv_cross::SPIRType::UInt:
        {
            size = spirv_type.columns * spirv_type.vecsize * static_cast<UInt32>(sizeof(UInt32));
            break;
        }
        case spirv_cross::SPIRType::Float:
        {
            size = spirv_type.columns * spirv_type.vecsize * static_cast<UInt32>(sizeof(float));
            break;
        }
        case spirv_cross::SPIRType::Double:
        {
            size = spirv_type.columns * spirv_type.vecsize * static_cast<UInt32>(sizeof(double));
            break;
        }
        default:
        {
            Log::Fatal("failed to build shader reflection: unsupported SPIR type {}", base_type);
            return 0;
        }
        }

        auto& constant  = shader_reflection.m_push_constant_fields.emplace_back();
        constant.name   = name;
        constant.offset = offset;
        constant.size   = size;

        VALO_ASSERT(!shader_reflection.m_property_ids.contains(name));
        auto& property_id = shader_reflection.m_property_ids[String(name)];
        property_id.type  = PropertyType::DescriptorSetBinding;
        property_id.m_id  = static_cast<UInt32>(shader_reflection.m_push_constant_fields.size()) - 1;

        return size;
    }

    TypeReflection ShaderReflectionBuilder::ConvertType(const spirv_cross::SPIRType& type)
    {
        auto reflection     = TypeReflection{};
        reflection.baseType = ConvertBaseType(type.basetype);
        reflection.columns  = type.vecsize;
        reflection.rows     = type.columns;
        return reflection;
    }

    BaseTypeReflection ShaderReflectionBuilder::ConvertBaseType(spirv_cross::SPIRType::BaseType baseType)
    {
        switch (baseType)
        {
        case spirv_cross::SPIRType::UInt:
            return BaseTypeReflection::UInt;
        case spirv_cross::SPIRType::Int:
            return BaseTypeReflection::Int;
        case spirv_cross::SPIRType::Double:
            return BaseTypeReflection::Double;
        case spirv_cross::SPIRType::Float:
            return BaseTypeReflection::Float;
        default:
            Log::Fatal("failed to get type reflection: unsupported SPIRType {}", baseType);
            return {};
        }
    }

    ShaderStageReflection ShaderReflectionBuilder::ConvertShaderStage(spv::ExecutionModel execution_model)
    {
        switch (execution_model)
        {
        case spv::ExecutionModelVertex:
            return ShaderStageReflection::Vertex;
        case spv::ExecutionModelTessellationControl:
            return ShaderStageReflection::TessellationControl;
        case spv::ExecutionModelTessellationEvaluation:
            return ShaderStageReflection::TessellationEvaluation;
        case spv::ExecutionModelGeometry:
            return ShaderStageReflection::Geometry;
        case spv::ExecutionModelFragment:
            return ShaderStageReflection::Fragment;
        case spv::ExecutionModelGLCompute:
            return ShaderStageReflection::Compute;
        case spv::ExecutionModelKernel:
        case spv::ExecutionModelTaskNV:
        case spv::ExecutionModelMeshNV:
        case spv::ExecutionModelRayGenerationKHR:
        case spv::ExecutionModelIntersectionKHR:
        case spv::ExecutionModelAnyHitKHR:
        case spv::ExecutionModelClosestHitKHR:
        case spv::ExecutionModelMissKHR:
        case spv::ExecutionModelCallableKHR:
        default:
            return ShaderStageReflection::None;
        }
    }

    void ShaderReflectionBuilder::LogReflectionError(StringView filepath, StringView message)
    {
        Log::Error("Failed to build reflection data for shader '{}': {}", filepath, message);
    }
}
