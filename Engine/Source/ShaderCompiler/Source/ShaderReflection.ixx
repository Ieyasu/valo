export module Valo.ShaderCompiler:ShaderReflection;

import Valo.Std;

export namespace Valo
{
    enum class BaseTypeReflection : EnumSmall
    {
        UInt,
        Int,
        Float,
        Double,
    };

    enum class ShaderStageReflection : EnumSmall
    {
        None,
        Vertex,
        TessellationControl,
        TessellationEvaluation,
        Geometry,
        Fragment,
        Compute,
    };

    enum class DescriptorTypeReflection : EnumSmall
    {
        Sampler,
        CombinedImageSampler,
        SampledImage,
        StorageImage,
        UniformBuffer,
        StorageBuffer,
    };

    struct TypeReflection final
    {
        BaseTypeReflection baseType{BaseTypeReflection::UInt};
        UInt32             columns{0};
        UInt32             rows{0};
    };

    struct LayoutReflection final
    {
        String         name{};
        TypeReflection type{};
        UInt32         location{0};
    };

    struct DescriptorSetReflection final
    {
        UInt32 set{0};
    };

    struct DescriptorSetBindingReflection final
    {
        String                   name{};
        UInt32                   set{0};
        UInt32                   binding{0};
        UInt32                   array_size{0};
        DescriptorTypeReflection type{DescriptorTypeReflection::UniformBuffer};
    };

    struct PushConstantRangeReflection final
    {
        UInt32 offset{0};
        UInt32 size{0};
    };

    struct PushConstantFieldReflection final
    {
        String name{};
        UInt32 offset{0};
        UInt32 size{0};
    };

    enum class PropertyType : EnumSmall
    {
        DescriptorSetBinding,
        PushConstantField,
        StageInput,
        StageOutput,
    };

    struct PropertyId
    {
    public:
        PropertyType type;

    private:
        friend class ShaderReflection;
        friend class ShaderReflectionBuilder;

        UInt32 m_id;
    };

    class ShaderReflection final
    {
    public:
        ShaderStageReflection GetStage() const;
        const String&         GetName() const;
        const String&         GetEntryPoint() const;

        Optional<PropertyId> GetPropertyID(const String& name) const;

        const Vector<DescriptorSetReflection>&        GetDescriptorSets() const;
        const Vector<DescriptorSetBindingReflection>& GetDescriptorSetBindings() const;
        const DescriptorSetBindingReflection&         GetDescriptorSetBinding(const PropertyId& property_id) const;

        const Vector<PushConstantRangeReflection>& GetPushConstantRanges() const;
        const Vector<PushConstantFieldReflection>& GetPushConstantFields() const;
        const PushConstantFieldReflection&         GetPushConstantField(const PropertyId& property_id) const;

        const Vector<LayoutReflection>& GetStageInputs() const;
        const Vector<LayoutReflection>& GetStageOutputs() const;
        const LayoutReflection&         GetStageInput(const PropertyId& property_id) const;
        const LayoutReflection&         GetStageOutput(const PropertyId& property_id) const;

    private:
        friend class ShaderReflectionBuilder;

        ShaderStageReflection m_stage{};
        String                m_name{};
        String                m_entry_point{};

        // Mapping from property name to property index
        Map<String, PropertyId, Less<>> m_property_ids{};

        // Reflection data
        Vector<LayoutReflection>               m_stage_inputs{};
        Vector<LayoutReflection>               m_stage_outputs{};
        Vector<DescriptorSetReflection>        m_descriptor_sets{};
        Vector<DescriptorSetBindingReflection> m_descriptor_set_bindings{};
        Vector<PushConstantRangeReflection>    m_push_constant_ranges{};
        Vector<PushConstantFieldReflection>    m_push_constant_fields{};
    };
}
