module Valo.ShaderCompiler;

namespace Valo
{
    ShaderCompilerHandle ShaderCompilerModule::GetShaderCompiler()
    {
        return &m_compiler;
    }
}
