module;

#include <algorithm>
#include <vector>

module Valo.ShaderCompiler;

import Valo.Logger;
import Valo.Std;

namespace Valo
{
    Optional<BinaryBlob> ShaderCompiler::Compile(const ShaderCompilerOptions& Options)
    {
        LogCompilerOptions(Options);

        // Find the first shader compiler backend supporting the input and output format
        for (auto& Backend : Backends)
        {
            if (Backend->IsShaderLanguageSupported(Options.Language) && Backend->IsShaderIRSupported(Options.IR))
            {
                return Backend->CompileBase(Options);
            }
        }

        Log::Error("Failed to compile shader '{}': no supported backend found.", Options.Filepath);
        return nullopt;
    }

    Optional<ShaderReflection> ShaderCompiler::Reflect(StringView Name, const BinaryBlob& Binary)
    {
        Log::Debug("Building reflection data for shader '{}'", Name);

        if (Binary.size() % sizeof(UInt32) != 0 || Binary.empty())
        {
            const auto Message = StringFunc::Format("invalid shader binary (length {}).", Binary.size());
            ShaderReflectionBuilder::LogReflectionError(Name, Message);
            return nullopt;
        }

        SpirvBuffer.resize(Binary.size() / sizeof(UInt32));
        Memory::Memcpy(SpirvBuffer.data(), Binary.data(), Binary.size());

        return ShaderReflectionBuilder::Build(Name, SpirvBuffer);
    }

    void ShaderCompiler::RegisterBackend(ShaderCompilerBackend& Backend)
    {
        const auto It = std::find(Backends.begin(), Backends.end(), &Backend);
        if (It != Backends.end())
        {
            Log::Warning("Trying to register an already registered ShaderCompilerBackend.");
            return;
        }

        Backends.push_back(&Backend);
    }

    void ShaderCompiler::DeregisterBackend(ShaderCompilerBackend& Backend)
    {
        const auto It = std::find(Backends.begin(), Backends.end(), &Backend);
        if (It == Backends.end())
        {
            Log::Warning("Trying to deregister a not registererd ShaderCompilerBackend.");
            return;
        }

        Backends.erase(It);
    }

    void ShaderCompiler::LogCompilerOptions(const ShaderCompilerOptions& Options)
    {
        Log::Trace("Options:");
        Log::Trace("  File Path: {}", Options.Filepath);
        Log::Trace("  Language: {}", Options.Language);
        Log::Trace("  IR: {}", Options.IR);

        if (!Options.Defines.empty())
        {
            Log::Trace("  Defines:");
            for (const auto& Define : Options.Defines)
            {
                Log::Trace("  - {}", Define);
            }
        }

        if (!Options.Includes.empty())
        {
            Log::Trace("  Includes:");
            for (const auto& Include : Options.Includes)
            {
                Log::Trace("  - {}", Include);
            }
        }
    }
}
