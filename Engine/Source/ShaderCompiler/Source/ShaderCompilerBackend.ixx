export module Valo.ShaderCompiler:ShaderCompilerBackend;

import Valo.Std;

import :ShaderCompilerOptions;
import :ShaderReflection;

export namespace Valo
{
    class ShaderCompilerBackend
    {
    public:
        virtual ~ShaderCompilerBackend() = default;

        Optional<BinaryBlob> CompileBase(const ShaderCompilerOptions& params);

        virtual bool IsShaderLanguageSupported(ShaderLanguage language) const = 0;

        virtual bool IsShaderIRSupported(ShaderIR ir) const = 0;

    protected:
        virtual Optional<BinaryBlob> Compile(const ShaderCompilerOptions& params) = 0;

        template <typename... Args>
        void AddCompilerError(const StringView message, Args&&... args);

        template <typename... Args>
        void AddCompilerWarning(const StringView message, Args&&... args);

    private:
        void LogCompilerResult(const ShaderCompilerOptions& params);

        Vector<String> CompileErrors{};
        Vector<String> CompileWarnings{};
    };

    using ShaderCompilerBackendHandle = ObserverPtr<ShaderCompilerBackend>;

    template <typename... Args>
    void ShaderCompilerBackend::AddCompilerError(const StringView message, Args&&... args)
    {
        CompileErrors.push_back(StringFunc::Format(message, Memory::Forward<Args>(args)...));
    }

    template <typename... Args>
    void ShaderCompilerBackend::AddCompilerWarning(const StringView message, Args&&... args)
    {
        CompileWarnings.push_back(StringFunc::Format(message, Memory::Forward<Args>(args)...));
    }
}
