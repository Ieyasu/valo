module;

#include <spirv_cross.hpp>

export module Valo.ShaderCompiler:ShaderReflectionBuilder;

import Valo.Std;

import :ShaderReflection;

export namespace Valo
{
    class ShaderReflectionBuilder final
    {
    public:
        static Optional<ShaderReflection> Build(StringView name, const Vector<UInt32>& spirv_binary);

        static void LogReflectionError(StringView filepath, StringView message);

    private:
        static void BuildStageInputReflection(
            ShaderReflection&            shader_reflection,
            const spirv_cross::Compiler& spirv,
            const spirv_cross::Resource& resource);

        static void BuildStageOutputReflection(
            ShaderReflection&            shader_reflection,
            const spirv_cross::Compiler& spirv,
            const spirv_cross::Resource& resource);

        static void BuildBindingReflection(
            ShaderReflection&            shader_reflection,
            const spirv_cross::Compiler& spirv,
            const spirv_cross::Resource& resource,
            DescriptorTypeReflection     type);

        static UInt32 BuildConstantReflection(
            ShaderReflection&            shader_reflection,
            const spirv_cross::Compiler& spirv,
            const spirv_cross::TypeID&   type_id,
            StringView                   name,
            UInt32                       offset);

        static TypeReflection        ConvertType(const spirv_cross::SPIRType& type);
        static BaseTypeReflection    ConvertBaseType(spirv_cross::SPIRType::BaseType baseType);
        static ShaderStageReflection ConvertShaderStage(spv::ExecutionModel execution_model);
    };
}
