export module Valo.ShaderCompiler:ShaderCompiler;

import Valo.Engine;
import Valo.Std;

import :ShaderCompilerBackend;
import :ShaderCompilerOptions;
import :ShaderReflection;

export namespace Valo
{
    class ShaderCompiler final
    {
    public:
        Optional<BinaryBlob> Compile(const ShaderCompilerOptions& Params);

        Optional<ShaderReflection> Reflect(StringView Name, const BinaryBlob& Binary);

        void RegisterBackend(ShaderCompilerBackend& Backend);

        void DeregisterBackend(ShaderCompilerBackend& Backend);

    private:
        void LogCompilerOptions(const ShaderCompilerOptions& Params);

        Vector<ShaderCompilerBackendHandle> Backends{};
        Vector<UInt32>                      SpirvBuffer{};
    };

    using ShaderCompilerHandle = ObserverPtr<ShaderCompiler>;
}
