export module Valo.ShaderCompiler:ShaderCompilerModule;

import Valo.Engine;

import :ShaderCompiler;

export namespace Valo
{
    class ShaderCompilerModule final : public EngineModule
    {
    public:
        ShaderCompilerHandle GetShaderCompiler();

    private:
        ShaderCompiler m_compiler{};
    };
}
