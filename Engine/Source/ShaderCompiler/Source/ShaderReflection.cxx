module;

#include <Valo/Macros.hpp>

#include <string>

module Valo.ShaderCompiler;

import Valo.Logger;
import Valo.Std;

import :ShaderReflection;
import :ShaderReflectionBuilder;

namespace Valo
{
    ShaderStageReflection ShaderReflection::GetStage() const
    {
        return m_stage;
    }

    const String& ShaderReflection::GetName() const
    {
        return m_name;
    }

    const String& ShaderReflection::GetEntryPoint() const
    {
        return m_entry_point;
    }

    Optional<PropertyId> ShaderReflection::GetPropertyID(const String& property_name) const
    {
        auto it = m_property_ids.find(property_name);
        if (it == m_property_ids.end())
        {
            return nullopt;
        }
        return it->second;
    }

    const Vector<DescriptorSetReflection>& ShaderReflection::GetDescriptorSets() const
    {
        return m_descriptor_sets;
    }

    const Vector<DescriptorSetBindingReflection>& ShaderReflection::GetDescriptorSetBindings() const
    {
        return m_descriptor_set_bindings;
    }

    const DescriptorSetBindingReflection& ShaderReflection::GetDescriptorSetBinding(const PropertyId& property_id) const
    {
        VALO_ASSERT(property_id.type == PropertyType::DescriptorSetBinding);
        VALO_ASSERT(property_id.m_id < m_descriptor_set_bindings.size());
        return m_descriptor_set_bindings[property_id.m_id];
    }

    const Vector<PushConstantRangeReflection>& ShaderReflection::GetPushConstantRanges() const
    {
        return m_push_constant_ranges;
    }

    const Vector<PushConstantFieldReflection>& ShaderReflection::GetPushConstantFields() const
    {
        return m_push_constant_fields;
    }

    const PushConstantFieldReflection& ShaderReflection::GetPushConstantField(const PropertyId& property_id) const
    {
        VALO_ASSERT(property_id.type == PropertyType::PushConstantField);
        VALO_ASSERT(property_id.m_id < m_push_constant_fields.size());
        return m_push_constant_fields[property_id.m_id];
    }

    const Vector<LayoutReflection>& ShaderReflection::GetStageInputs() const
    {
        return m_stage_inputs;
    }

    const Vector<LayoutReflection>& ShaderReflection::GetStageOutputs() const
    {
        return m_stage_outputs;
    }

    const LayoutReflection& ShaderReflection::GetStageInput(const PropertyId& property_id) const
    {
        VALO_ASSERT(property_id.type == PropertyType::StageInput);
        VALO_ASSERT(property_id.m_id < m_stage_inputs.size());
        return m_stage_inputs[property_id.m_id];
    }

    const LayoutReflection& ShaderReflection::GetStageOutput(const PropertyId& property_id) const
    {
        VALO_ASSERT(property_id.type == PropertyType::StageOutput);
        VALO_ASSERT(property_id.m_id < m_stage_outputs.size());
        return m_stage_inputs[property_id.m_id];
    }
}
