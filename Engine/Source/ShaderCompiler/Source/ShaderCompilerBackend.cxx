module;

#include <Valo/Macros.hpp>

#include <vector>

module Valo.ShaderCompiler;

import Valo.Logger;
import Valo.Std;

namespace Valo
{
    Optional<BinaryBlob> ShaderCompilerBackend::CompileBase(const ShaderCompilerOptions& Options)
    {
        CompileErrors.clear();
        CompileWarnings.clear();

        auto result = Compile(Options);
        LogCompilerResult(Options);
        return result;
    }

    void ShaderCompilerBackend::LogCompilerResult(const ShaderCompilerOptions& Options)
    {
        if (!CompileErrors.empty())
        {
            Log::Error("Shader '{}' compiled with errors:", Options.Filepath);
            for (const auto& Error : CompileErrors)
            {
                Log::Error(Error);
            }
        }

        if (!CompileWarnings.empty())
        {
            Log::Warning("Shader '{}' compiled with warnings:", Options.Filepath);
            for (const auto& Warning : CompileWarnings)
            {
                Log::Warning(Warning);
            }
        }
    }
}
