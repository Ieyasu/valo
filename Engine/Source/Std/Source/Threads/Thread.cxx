module;

#include <mutex>
#include <thread>
#include <unordered_map>

module Valo.Std;

namespace Valo
{
    ThreadId GetThreadId() noexcept
    {
        static ThreadId                                      thread_index{};
        static std::mutex                                    thread_mutex{};
        static std::unordered_map<std::thread::id, ThreadId> thread_ids{};

        const auto lock = std::lock_guard<std::mutex>(thread_mutex);
        const auto id   = std::this_thread::get_id();
        auto       iter = thread_ids.find(id);
        if (iter == thread_ids.end())
        {
            iter = thread_ids.insert(std::pair<std::thread::id, ThreadId>(id, thread_index++)).first;
        }

        return iter->second;
    }
}
