export module Valo.Std:Thread;

import :Types;

export namespace Valo
{
    using ThreadId = size_t;

    [[nodiscard]] ThreadId GetThreadId() noexcept;
}
