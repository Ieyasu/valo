module;

#include <mutex>

export module Valo.Std:Mutex;

export namespace Valo
{
    using Mutex = std::mutex;

    template <typename MutexTypes>
    using ScopedLock = std::scoped_lock<MutexTypes>;
}
