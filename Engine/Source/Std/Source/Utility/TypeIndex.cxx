module Valo.Std;

namespace Valo
{
    TypeIndex::ValueType TypeIndex::type_index_counter = 0;

    bool TypeIndex::operator==(const TypeIndex& rhs) const noexcept
    {
        return m_value == rhs.m_value;
    }

    bool TypeIndex::operator!=(const TypeIndex& rhs) const noexcept
    {
        return m_value != rhs.m_value;
    }

    bool TypeIndex::operator<(const TypeIndex& rhs) const noexcept
    {
        return m_value < rhs.m_value;
    }

    bool TypeIndex::operator>(const TypeIndex& rhs) const noexcept
    {
        return m_value > rhs.m_value;
    }

    bool TypeIndex::operator>=(const TypeIndex& rhs) const noexcept
    {
        return m_value >= rhs.m_value;
    }

    bool TypeIndex::operator<=(const TypeIndex& rhs) const noexcept
    {
        return m_value <= rhs.m_value;
    }
}
