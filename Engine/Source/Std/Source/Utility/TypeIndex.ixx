module;

#include <functional>

export module Valo.Std:TypeIndex;

import :Std;

namespace Valo
{
    export class TypeIndex final
    {
    public:
        template <typename... Ts>
        [[nodiscard]] static TypeIndex Create() noexcept;

        [[nodiscard]] bool operator==(const TypeIndex& rhs) const noexcept;
        [[nodiscard]] bool operator!=(const TypeIndex& rhs) const noexcept;
        [[nodiscard]] bool operator<(const TypeIndex& rhs) const noexcept;
        [[nodiscard]] bool operator>(const TypeIndex& rhs) const noexcept;
        [[nodiscard]] bool operator>=(const TypeIndex& rhs) const noexcept;
        [[nodiscard]] bool operator<=(const TypeIndex& rhs) const noexcept;

    private:
        friend Hash<TypeIndex>;
        using ValueType = size_t;

        static ValueType type_index_counter;

        ValueType m_value{~ValueType{0}};
    };

    template <typename... Ts>
    TypeIndex TypeIndex::Create() noexcept
    {
        // Remove const from types to ensure correct type index resolution
        if constexpr (std::disjunction_v<std::is_const<Ts>...>)
        {
            return TypeIndex::Create<std::remove_const_t<Ts>...>();
        }
        else
        {
            static TypeIndex::ValueType type_index = type_index_counter++;

            TypeIndex value;
            value.m_value = type_index;
            return value;
        }
    }
}

namespace std
{
    export template <>
    struct hash<Valo::TypeIndex>
    {
        [[nodiscard]] size_t operator()(const Valo::TypeIndex& type_index) const noexcept
        {
            return type_index.m_value;
        }
    };
}
