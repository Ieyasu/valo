export module Valo.Std:Result;

import :Memory;
import :Pointers;

export namespace Valo
{
    template <typename TResult, typename TValue>
    class ResultValue final
    {
    public:
        ResultValue() = default;
        ResultValue(const TResult& res);  // NOLINT
        ResultValue(const TValue& val);   // NOLINT
        ResultValue(TResult&& res);       // NOLINT
        ResultValue(TValue&& val);        // NOLINT

        TResult result{};
        TValue  value{};
    };

    template <typename TResult, typename TValue>
    ResultValue<TResult, TValue>::ResultValue(const TResult& res) :
        result(res)
    {
    }

    template <typename TResult, typename TValue>
    ResultValue<TResult, TValue>::ResultValue(const TValue& val) :
        value(val)
    {
    }

    template <typename TResult, typename TValue>
    ResultValue<TResult, TValue>::ResultValue(TResult&& res) :
        result(Memory::Move(res))
    {
    }

    template <typename TResult, typename TValue>
    ResultValue<TResult, TValue>::ResultValue(TValue&& val) :
        value(Memory::Move(val))
    {
    }

    template <typename TResult, typename TValue>
    using UniqueResult = ResultValue<TResult, UniquePtr<TValue>>;
}
