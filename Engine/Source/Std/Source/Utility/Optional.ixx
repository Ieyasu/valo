module;

#include <optional>

export module Valo.Std:Optional;

import :Memory;

export namespace Valo
{
    template <typename T>
    using Optional = std::optional<T>;

    using NullOpt = std::nullopt_t;

    constexpr NullOpt nullopt = std::nullopt;

    template <typename T, typename... Args>
    [[nodiscard]] inline Optional<T> MakeOptional(Args&&... args)
    {
        return std::make_optional<T, Args...>(Memory::Forward<Args>(args)...);
    }
}
