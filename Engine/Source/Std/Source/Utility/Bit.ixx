export module Valo.Std:Bit;

export namespace Valo::Bit
{
    template <typename T, typename... Args>
    [[nodiscard]] inline constexpr T Set(Args... bits) noexcept
    {
        return static_cast<T>(((T(1) << static_cast<T>(bits)) | ...));
    }
}
