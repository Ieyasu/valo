export module Valo.Std:Enum;

import :Types;

export namespace Valo
{
    using Flags32 = UInt32;
    using Flags64 = UInt64;
    using Flags   = Flags32;

    using Enum8     = UInt8;
    using Enum16    = UInt16;
    using Enum32    = UInt32;
    using EnumSmall = Enum8;
    using Enum      = Enum32;

    using SEnum8     = Int8;
    using SEnum16    = Int16;
    using SEnum32    = Int32;
    using SEnumSmall = SEnum8;
    using SEnum      = SEnum32;

    template <typename BitType>
    class BaseFlags final
    {
    public:
        BaseFlags() = default;

        BaseFlags(BitType bit) :  // NOLINT allow implicit conversion
            m_value(static_cast<Flags>(bit))
        {
        }

        BaseFlags(Flags flags) :  // NOLINT allow implicit conversion
            m_value(flags)
        {
        }

        [[nodiscard]] Flags Value() const noexcept
        {
            return m_value;
        }

        [[nodiscard]] inline operator bool() const noexcept  // NOLINT allow implicit conversion
        {
            return m_value != 0;
        }

        [[nodiscard]] inline BaseFlags operator==(BitType bit) const noexcept
        {
            return m_value == static_cast<Flags>(bit);
        }

        [[nodiscard]] inline BaseFlags operator!=(BitType bit) const noexcept
        {
            return m_value != static_cast<Flags>(bit);
        }

        [[nodiscard]] inline BaseFlags operator|(BaseFlags flags) const noexcept
        {
            return m_value | flags.Value();
        }

        [[nodiscard]] inline BaseFlags operator&(BaseFlags flags) const noexcept
        {
            return m_value & flags.Value();
        }

        [[nodiscard]] inline BaseFlags operator|(BitType bit) const noexcept
        {
            return m_value | static_cast<Flags>(bit);
        }

        [[nodiscard]] inline BaseFlags operator&(BitType bit) const noexcept
        {
            return m_value & static_cast<Flags>(bit);
        }

        inline BaseFlags& operator|=(BaseFlags flags) noexcept
        {
            m_value |= flags.Value();
            return *this;
        }

        inline BaseFlags& operator&=(BaseFlags flags) noexcept
        {
            m_value &= flags.Value();
            return *this;
        }

        inline BaseFlags& operator|=(BitType bit) noexcept
        {
            m_value |= static_cast<Flags>(bit);
            return *this;
        }

        inline BaseFlags& operator&=(BitType bit) noexcept
        {
            m_value |= static_cast<Flags>(bit);
            return *this;
        }

    private:
        Flags m_value{};
    };
}
