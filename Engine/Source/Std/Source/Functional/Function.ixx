module;

#include <functional>

export module Valo.Std:Function;

export namespace Valo
{
    template <typename T>
    using Function = std::function<T>;
}
