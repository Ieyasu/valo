module;

#include <cstring>
#include <type_traits>

export module Valo.Std:Memory;

export namespace Valo::Memory
{
    template <typename T>
    [[nodiscard]] constexpr T&& Forward(typename std::remove_reference<T>::type& value) noexcept
    {
        return static_cast<T&&>(value);
    }

    template <typename T>
    [[nodiscard]] constexpr typename std::remove_reference<T>::type&& Move(T&& value) noexcept
    {
        return static_cast<typename std::remove_reference<T>::type&&>(value);
    }

    [[nodiscard]] std::size_t Align(std::size_t size, std::size_t alignment) noexcept
    {
        if (alignment == 0)
        {
            return size;
        }

        return (size + alignment - 1) & ~(alignment - 1);
    }

    void* Memcpy(void* dest, const void* src, std::size_t count)
    {
        return std::memcpy(dest, src, count);
    }
}
