
module;

#include <memory>

export module Valo.Std:Pointers;

import :Memory;
import :Types;

export namespace Valo
{
    using Byte = UInt8;

    template <typename T, typename Deleter = std::default_delete<T>>
    using UniquePtr = std::unique_ptr<T, Deleter>;

    template <typename T>
    using SharedPtr = std::shared_ptr<T>;

    template <typename T>
    using WeakPtr = std::weak_ptr<T>;

    template <typename T, typename... Args>
    [[nodiscard]] inline UniquePtr<T> MakeUnique(Args&&... args)
    {
        return std::make_unique<T>(Memory::Forward<Args>(args)...);
    }

    template <typename T, typename... Args>
    [[nodiscard]] inline SharedPtr<T> MakeShared(Args&&... args)
    {
        return std::make_shared<T>(Memory::Forward<Args>(args)...);
    }

    template <typename T>
    class ObserverPtr final
    {
    public:
        ObserverPtr() = default;

        ObserverPtr(T* ptr) :  // NOLINT allow implicit conversions
            m_ptr(ptr)
        {
        }

        [[nodiscard]] bool IsValid() const noexcept
        {
            return m_ptr != nullptr;
        }

        [[nodiscard]] T* Get() const noexcept
        {
            return m_ptr;
        }

        [[nodiscard]] T* operator->() const noexcept
        {
            return m_ptr;
        }

        [[nodiscard]] T& operator*() const noexcept
        {
            return *m_ptr;
        }

        [[nodiscard]] operator T*() const noexcept  // NOLINT allow implicit conversions
        {
            return m_ptr;
        }

    private:
        T* m_ptr{nullptr};
    };

    template <typename T>
    class UniqueHandle final
    {
    public:
        UniqueHandle() = default;

        UniqueHandle(T* ptr, void (*del)(T*)) :
            m_ptr(ptr, del)
        {
        }

        [[nodiscard]] T* Get() const noexcept
        {
            return m_ptr.get();
        }

        [[nodiscard]] T* operator->() const noexcept
        {
            return m_ptr.get();
        }

        [[nodiscard]] T& operator*() const noexcept
        {
            return *m_ptr;
        }

    private:
        UniquePtr<T, void (*)(T*)> m_ptr{nullptr, nullptr};
    };
}
