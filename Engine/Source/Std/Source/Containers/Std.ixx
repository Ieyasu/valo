module;

#include <array>
#include <deque>
#include <map>
#include <queue>
#include <set>
#include <span>
#include <stack>
#include <unordered_map>
#include <unordered_set>

export module Valo.Std:Std;

import :Pointers;

export namespace Valo
{
    template <typename T, size_t N>
    using Array = std::array<T, N>;

    template <typename T>
    using Deque = std::deque<T>;

    template <typename T = void>
    using Less = std::less<T>;

    template <typename T>
    using Hash = std::hash<T>;

    template <typename TKey, typename T, typename TCompare = Less<TKey>>
    using Map = std::map<TKey, T, TCompare>;

    template <typename T>
    using Queue = std::queue<T>;

    template <typename T>
    using Set = std::set<T>;

    template <typename T>
    using Span = std::span<T>;

    template <typename T, size_t N = 8>
    using SmallVector = std::vector<T>;

    template <typename T>
    using Stack = std::stack<T>;

    template <typename TKey, typename T, typename THash = Hash<TKey>>
    using UnorderedMap = std::unordered_map<TKey, T, THash>;

    template <typename T>
    using UnorderedSet = std::unordered_set<T>;

    template <typename T>
    using Vector = std::vector<T>;

    using BinaryBlob = Vector<Byte>;
}
