module;

#include <Valo/Macros.hpp>

#include <climits>
#include <functional>
#include <variant>

export module Valo.Std:Pool;

import :Function;
import :Memory;
import :Std;
import :Types;

export namespace Valo
{
    class BasePool;

    template <typename T, typename TBase>
    class Pool;

    class BasePool
    {
    };

    template <typename T>
    class BasePoolHandle
    {
    public:
        using IdType      = UInt64;
        using IndexType   = UInt32;
        using CounterType = UInt32;

        BasePoolHandle() = default;

        [[nodiscard]] IdType GetId() const noexcept
        {
            return GetId(GetIndex(), GetCounter());
        }

        [[nodiscard]] IndexType GetIndex() const noexcept
        {
            return m_index;
        }

    protected:
        BasePoolHandle(BasePool& pool, IndexType index, CounterType counter) :
            m_pool(&pool),
            m_index(index),
            m_counter(counter)
        {
        }

        [[nodiscard]] CounterType GetCounter() const noexcept
        {
            return m_counter;
        }

        [[nodiscard]] BasePool* GetBasePool() noexcept
        {
            return m_pool;
        }

        [[nodiscard]] const BasePool* GetBasePool() const noexcept
        {
            return m_pool;
        }

        [[nodiscard]] static IdType GetId(IndexType index, CounterType counter) noexcept
        {
            constexpr IdType BitsToShift = CHAR_BIT * sizeof(IndexType);
            return (static_cast<IdType>(counter) << BitsToShift) | static_cast<IdType>(index);
        }

        [[nodiscard]] static IndexType GetIndex(IdType id) noexcept
        {
            return static_cast<IndexType>(id & static_cast<IdType>(~static_cast<IndexType>(0)));
        }

        [[nodiscard]] static CounterType GetCounter(IdType id) noexcept
        {
            return static_cast<CounterType>(id >> static_cast<IdType>(CHAR_BIT * sizeof(IndexType)));
        }

    private:
        BasePool*   m_pool{nullptr};
        IndexType   m_index{0};
        CounterType m_counter{0};
    };

    template <typename T, typename TBase = T>
    class PoolHandle final : public BasePoolHandle<TBase>
    {
    public:
        using IdType      = typename BasePoolHandle<TBase>::IdType;
        using IndexType   = typename BasePoolHandle<TBase>::IndexType;
        using CounterType = typename BasePoolHandle<TBase>::CounterType;

        PoolHandle() = default;

        explicit PoolHandle(BasePoolHandle<TBase> handle) :
            BasePoolHandle<TBase>(handle)
        {
        }

        [[nodiscard]] bool IsValid() const;

        [[nodiscard]] T* Get();

        [[nodiscard]] const T* Get() const;

        [[nodiscard]] T* operator->();

        [[nodiscard]] const T* operator->() const;

        [[nodiscard]] T& operator*();

        [[nodiscard]] const T& operator*() const;

        [[nodiscard]] bool operator<(const PoolHandle<T, TBase>& other) const;

        [[nodiscard]] bool operator==(const PoolHandle<T, TBase>& other) const;

        [[nodiscard]] bool operator!=(const PoolHandle<T, TBase>& other) const;

    private:
        PoolHandle(Pool<T, TBase>& pool, IndexType index, CounterType counter);

        [[nodiscard]] Pool<T, TBase>* GetPool()
        {
            return static_cast<Pool<T, TBase>*>(this->GetBasePool());
        }

        [[nodiscard]] const Pool<T, TBase>* GetPool() const
        {
            return static_cast<const Pool<T, TBase>*>(this->GetBasePool());
        }

        friend class Pool<T, TBase>;
    };

    template <typename T, typename IndexType, typename DataType, typename Reference>
    class PoolIterator final
    {
    public:
        using iterator_category = std::forward_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using pointer           = void;

        PoolIterator() = default;

        PoolIterator(DataType* ptr, DataType* end) :
            m_ptr(ptr),
            m_end(end)
        {
            // Make sure the initial pointer is valid
            while (m_ptr != m_end && std::holds_alternative<IndexType>(*m_ptr))
            {
                ++m_ptr;
            }
        }

        Reference operator*() const
        {
            return std::get<T>(*m_ptr);
        }

        PoolIterator& operator++()
        {
            ++m_ptr;
            while (m_ptr != m_end && std::holds_alternative<IndexType>(*m_ptr))
            {
                ++m_ptr;
            }
            return *this;
        }

        PoolIterator operator++(int)
        {
            auto tmp = *this;
            ++(*this);
            return tmp;
        }

        friend bool operator==(const PoolIterator& lhs, const PoolIterator& rhs)
        {
            return lhs.m_ptr == rhs.m_ptr;
        }

        friend bool operator!=(const PoolIterator& lhs, const PoolIterator& rhs)
        {
            return !(lhs == rhs);
        }

    private:
        DataType* m_ptr{nullptr};
        DataType* m_end{nullptr};
    };

    template <typename T, typename TBase = T>
    class Pool final : public BasePool
    {
    private:
        using IndexType   = typename PoolHandle<T, TBase>::IndexType;
        using CounterType = typename PoolHandle<T, TBase>::CounterType;
        using DataType    = std::variant<T, IndexType>;

    public:
        using IdType   = typename PoolHandle<T, TBase>::IdType;
        using SizeType = typename Vector<T>::size_type;

        using IteratorType      = PoolIterator<T, IndexType, DataType, T&>;
        using ConstIteratorType = PoolIterator<T, IndexType, const DataType, const T&>;

        void Clear() noexcept;

        [[nodiscard]] bool IsEmpty() const noexcept;

        [[nodiscard]] SizeType GetSize() const noexcept;

        [[nodiscard]] T& Get(const PoolHandle<T, TBase>& handle);

        [[nodiscard]] const T& Get(const PoolHandle<T, TBase>& handle) const;

        [[nodiscard]] T& Get(IdType id);

        [[nodiscard]] const T& Get(IdType id) const;

        template <typename... Args>
        [[nodiscard]] PoolHandle<T, TBase> Emplace(Args&&... args);

        [[nodiscard]] PoolHandle<T, TBase> Insert(const T& value);

        [[nodiscard]] PoolHandle<T, TBase> Insert(T&& value);

        bool Erase(const PoolHandle<T, TBase>& handle);

        bool Erase(IdType id);

        [[nodiscard]] PoolHandle<T, TBase> Find(Function<bool(const T&)> predicate);

        [[nodiscard]] IteratorType begin();

        [[nodiscard]] IteratorType end();

        [[nodiscard]] ConstIteratorType begin() const;

        [[nodiscard]] ConstIteratorType end() const;

    private:
        // Invalid index is used to mark an index that does not point to any data.
        static constexpr IndexType m_invalid_index = std::numeric_limits<IndexType>::max();

        [[nodiscard]] PoolHandle<T, TBase> InsertIndex();

        [[nodiscard]] bool Erase(IndexType index, CounterType counter);

        Vector<DataType>    m_data{};
        Vector<CounterType> m_counters{};
        SizeType            m_open_slots{0};

        // Index to the next data slot that should be populated when
        // a new value is inserted. Each empty data slot saves an index
        // to the next free slot, allowing us to efficiently mark all
        // empty slots without spending extra memory.
        IndexType m_next_free_index{m_invalid_index};

        friend class PoolHandle<T, TBase>;
    };

    template <typename T, typename TBase>
    PoolHandle<T, TBase>::PoolHandle(Pool<T, TBase>& pool, IndexType index, CounterType counter) :
        BasePoolHandle<TBase>(pool, index, counter)
    {
    }

    template <typename T, typename TBase>
    bool PoolHandle<T, TBase>::IsValid() const
    {
        return GetPool() != nullptr && this->GetCounter() == this->GetPool()->m_counters[this->GetIndex()];
    }

    template <typename T, typename TBase>
    T* PoolHandle<T, TBase>::Get()
    {
        VALO_ASSERT(IsValid());

        return &GetPool()->Get(*this);
    }

    template <typename T, typename TBase>
    const T* PoolHandle<T, TBase>::Get() const
    {
        VALO_ASSERT(IsValid());

        return &GetPool()->Get(*this);
    }

    template <typename T, typename TBase>
    T* PoolHandle<T, TBase>::operator->()
    {
        VALO_ASSERT(IsValid());

        return Get();
    }

    template <typename T, typename TBase>
    const T* PoolHandle<T, TBase>::operator->() const
    {
        VALO_ASSERT(IsValid());

        return Get();
    }

    template <typename T, typename TBase>
    T& PoolHandle<T, TBase>::operator*()
    {
        return *Get();
    }

    template <typename T, typename TBase>
    const T& PoolHandle<T, TBase>::operator*() const
    {
        VALO_ASSERT(IsValid());

        return *Get();
    }

    template <typename T, typename TBase>
    bool PoolHandle<T, TBase>::operator<(const PoolHandle<T, TBase>& other) const
    {
        if (GetPool() < other.GetPool())
        {
            return true;
        }
        if (GetPool() > other.GetPool())
        {
            return false;
        }
        return this->GetCounter() < other.GetCounter()
            || (this->GetCounter() == other.GetCounter() && this->GetIndex() < other.GetIndex());
    }

    template <typename T, typename TBase>
    bool PoolHandle<T, TBase>::operator==(const PoolHandle<T, TBase>& other) const
    {
        return GetPool() == other.GetPool() && this->GetCounter() == other.GetCounter()
            && this->GetIndex() == other.GetIndex();
    }

    template <typename T, typename TBase>
    bool PoolHandle<T, TBase>::operator!=(const PoolHandle<T, TBase>& other) const
    {
        return GetPool() != other.GetPool() || this->GetCounter() != other.GetCounter()
            || this->GetIndex() != other.GetIndex();
    }

    template <typename T, typename TBase>
    void Pool<T, TBase>::Clear() noexcept
    {
        m_data.clear();
        m_counters.clear();
    }

    template <typename T, typename TBase>
    bool Pool<T, TBase>::IsEmpty() const noexcept
    {
        return m_data.size() == m_open_slots;
    }

    template <typename T, typename TBase>
    typename Pool<T, TBase>::SizeType Pool<T, TBase>::GetSize() const noexcept
    {
        return m_data.size();
    }

    template <typename T, typename TBase>
    T& Pool<T, TBase>::Get(const PoolHandle<T, TBase>& handle)
    {
        VALO_ASSERT(handle.GetIndex() < m_data.size());
        VALO_ASSERT(m_counters[handle.GetIndex()] == handle.GetCounter());
        VALO_ASSERT(std::holds_alternative<T>(m_data[handle.GetIndex()]));

        return std::get<T>(m_data[handle.GetIndex()]);
    }

    template <typename T, typename TBase>
    const T& Pool<T, TBase>::Get(const PoolHandle<T, TBase>& handle) const
    {
        VALO_ASSERT(handle.GetIndex() < m_data.size());
        VALO_ASSERT(m_counters[handle.GetIndex()] == handle.GetCounter());
        VALO_ASSERT(std::holds_alternative<T>(m_data[handle.GetIndex()]));

        return std::get<T>(m_data[handle.GetIndex()]);
    }

    template <typename T, typename TBase>
    T& Pool<T, TBase>::Get(IdType id)
    {
        const auto index = PoolHandle<T>::GetIndex(id);

        VALO_ASSERT(index < m_data.size());
        VALO_ASSERT(m_counters[index] == PoolHandle<T>::GetCounter(id));
        VALO_ASSERT(std::holds_alternative<T>(m_data[index]));

        return std::get<T>(m_data[index]);
    }

    template <typename T, typename TBase>
    const T& Pool<T, TBase>::Get(IdType id) const
    {
        const auto index = PoolHandle<T>::GetIndex(id);

        VALO_ASSERT(index < m_data.size());
        VALO_ASSERT(m_counters[index] == PoolHandle<T>::GetCounter(id));
        VALO_ASSERT(std::holds_alternative<T>(m_data[index]));

        return std::get<T>(m_data[index]);
    }

    template <typename T, typename TBase>
    template <typename... Args>
    PoolHandle<T, TBase> Pool<T, TBase>::Emplace(Args&&... args)
    {
        auto handle               = InsertIndex();
        m_data[handle.GetIndex()] = T(Memory::Forward<Args>(args)...);
        return handle;
    }

    template <typename T, typename TBase>
    PoolHandle<T, TBase> Pool<T, TBase>::Insert(const T& value)
    {
        auto handle               = InsertIndex();
        m_data[handle.GetIndex()] = value;
        return handle;
    }

    template <typename T, typename TBase>
    PoolHandle<T, TBase> Pool<T, TBase>::Insert(T&& value)
    {
        auto handle               = InsertIndex();
        m_data[handle.GetIndex()] = Memory::Move(value);
        return handle;
    }

    template <typename T, typename TBase>
    PoolHandle<T, TBase> Pool<T, TBase>::InsertIndex()
    {
        // If there are no free slots left for index mapping, need to allocate more
        if (m_next_free_index == m_invalid_index)
        {
            m_data.emplace_back(m_invalid_index);
            m_counters.emplace_back();
            m_next_free_index = static_cast<IndexType>(m_data.size() - 1);
        }

        const auto index  = m_next_free_index;
        m_next_free_index = std::get<IndexType>(m_data[index]);
        --m_open_slots;
        return PoolHandle<T, TBase>(*this, index, m_counters[index]);
    }

    template <typename T, typename TBase>
    bool Pool<T, TBase>::Erase(const PoolHandle<T, TBase>& handle)
    {
        return Erase(handle.GetIndex(), handle.GetCounter());
    }

    template <typename T, typename TBase>
    bool Pool<T, TBase>::Erase(IdType id)
    {
        // Extract counter and index from the ID
        const auto index   = PoolHandle<T>::GetIndex(id);
        const auto counter = PoolHandle<T>::GetCounter(id);
        return Erase(index, counter);
    }

    template <typename T, typename TBase>
    bool Pool<T, TBase>::Erase(IndexType index, CounterType counter)
    {
        (void)counter;

        VALO_ASSERT(index < m_data.size());
        VALO_ASSERT(m_counters[index] == counter);
        VALO_ASSERT(std::holds_alternative<T>(m_data[index]));

        // Update the "linked list" of free indices
        m_data[index]     = m_next_free_index;
        m_next_free_index = index;

        // Invalidate handles by increasing counter
        ++m_counters[index];
        ++m_open_slots;
        return true;
    }

    template <typename T, typename TBase>
    PoolHandle<T, TBase> Pool<T, TBase>::Find(Function<bool(const T&)> predicate)
    {
        for (IndexType index = 0; index < m_data.size(); ++index)
        {
            if (std::holds_alternative<T>(m_data[index]) && predicate(std::get<T>(m_data[index])))
            {
                return PoolHandle<T, TBase>(*this, index, m_counters[index]);
            }
        }
        return {};
    }

    template <typename T, typename TBase>
    typename Pool<T, TBase>::IteratorType Pool<T, TBase>::begin()
    {
        return IteratorType(&m_data.front(), &m_data.back() + 1);
    }

    template <typename T, typename TBase>
    typename Pool<T, TBase>::IteratorType Pool<T, TBase>::end()
    {
        return IteratorType(&m_data.back() + 1, &m_data.back() + 1);
    }

    template <typename T, typename TBase>
    typename Pool<T, TBase>::ConstIteratorType Pool<T, TBase>::begin() const
    {
        return ConstIteratorType(&m_data.front(), &m_data.back() + 1);
    }

    template <typename T, typename TBase>
    typename Pool<T, TBase>::ConstIteratorType Pool<T, TBase>::end() const
    {
        return ConstIteratorType(&m_data.back() + 1, &m_data.back() + 1);
    }
}

template <typename T, typename TBase>
struct std::hash<Valo::PoolHandle<T, TBase>>
{
    std::size_t operator()(const Valo::PoolHandle<T, TBase>& k) const
    {
        return std::hash<typename Valo::PoolHandle<T, TBase>::IdType>()(k.GetId());
    }
};
