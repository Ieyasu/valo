module;

#include <algorithm>
#include <cctype>
#include <string>

module Valo.Std;

import :String;

namespace Valo::StringFunc
{
    void ToLower(String& str)
    {
        const auto transform = [](unsigned char c)
        {
            return static_cast<unsigned char>(std::tolower(static_cast<int>(c)));
        };
        std::ranges::transform(str, str.begin(), transform);
    }

    void ToUpper(String& str)
    {
        const auto transform = [](unsigned char c)
        {
            return static_cast<unsigned char>(std::toupper(static_cast<int>(c)));
        };
        std::ranges::transform(str, str.begin(), transform);
    }

    String ToLowerCopy(const StringView str)
    {
        auto copy = String(str);
        ToLower(copy);
        return copy;
    }

    String ToUpperCopy(const StringView str)
    {
        auto copy = String(str);
        ToUpper(copy);
        return copy;
    }

    void LTrim(String& str, const StringView chars)
    {
        str.erase(0, str.find_first_not_of(chars));
    }

    void RTrim(String& str, const StringView chars)
    {
        str.erase(str.find_last_not_of(chars) + 1);
    }

    void Trim(String& str, const StringView chars)
    {
        RTrim(str, chars);
        LTrim(str, chars);
    }

    String LTrimCopy(const StringView str, const StringView chars)
    {
        auto copy = String(str);
        LTrim(copy, chars);
        return copy;
    }

    String RTrimCopy(const StringView str, const StringView chars)
    {
        auto copy = String(str);
        RTrim(copy, chars);
        return copy;
    }

    String TrimCopy(const StringView str, const StringView chars)
    {
        auto copy = String(str);
        Trim(copy, chars);
        return copy;
    }

    void Replace(String& str, const StringView old_value, const StringView new_value)
    {
        if (old_value.empty())
        {
            return;
        }

        size_t start_pos = 0;
        while ((start_pos = str.find(old_value, start_pos)) != String::npos)
        {
            str.replace(start_pos, old_value.length(), new_value);
            start_pos += new_value.length();
        }
    }

    String ReplaceCopy(const String& str, const StringView old_value, const StringView new_value)
    {
        auto copy = String(str);
        Replace(copy, old_value, new_value);
        return copy;
    }
}
