module;

#include <fmt/format.h>
#include <magic_enum.hpp>

#include <string>
#include <string_view>

export module Valo.Std:String;

import :Memory;

export namespace Valo
{
    using String     = std::string;
    using StringView = std::string_view;
}

export namespace Valo::StringFunc
{
    void ToLower(String& str);
    void ToUpper(String& str);

    [[nodiscard]] String ToLowerCopy(const StringView str);
    [[nodiscard]] String ToUpperCopy(const StringView str);

    void LTrim(String& str, const StringView chars = "\t\n\v\f\r ");
    void RTrim(String& str, const StringView chars = "\t\n\v\f\r ");
    void Trim(String& str, const StringView chars = "\t\n\v\f\r ");

    [[nodiscard]] String LTrimCopy(const StringView str, const StringView chars = "\t\n\v\f\r ");
    [[nodiscard]] String RTrimCopy(const StringView str, const StringView chars = "\t\n\v\f\r ");
    [[nodiscard]] String TrimCopy(const StringView str, const StringView chars = "\t\n\v\f\r ");

    void                 Replace(String& str, const StringView old_value, const StringView new_value);
    [[nodiscard]] String ReplaceCopy(const String& str, const StringView old_value, const StringView new_value);

    // Helper function for turning strings into enums.
    // This is useful when formatting templated types.
    template <typename T>
    [[nodiscard]] inline auto ToStringIfEnum(T&& arg)
    {
        if constexpr (std::is_enum_v<std::remove_reference_t<T>>)
        {
            return magic_enum::enum_name(arg);
        }
        else
        {
            return Memory::Forward<T>(arg);
        }
    }

    template <typename... Args>
    [[nodiscard]] inline String Format(const StringView str, Args&&... args)
    {
        return fmt::format(fmt::runtime(str), ToStringIfEnum(Memory::Forward<Args>(args))...);
    }
}
