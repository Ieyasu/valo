module;

#include <fmt/base.h>

#include <filesystem>

export module Valo.Std:Path;

import :String;

export namespace Valo
{
    class Path final
    {
    public:
        Path() = default;

        Path(std::filesystem::path Path);

        Path(const char* Str);

        Path(String Str);

        Path(StringView Str);

        const char* GetCString() const;

        const String& GetString() const;

        void Clear();

        bool IsAbsolute() const;

        bool IsRelative() const;

        Path GetRootDirectory() const;

        Path GetParentDirectory() const;

        Path GetExtension() const;

        Path GetFilename() const;

        Path& RemoveExtension();

        Path& RemoveFilename();

        Path& ReplaceExtension(const Path& Path);

        Path& ReplaceFilename(const Path& Path);

        operator String() const;

        operator StringView() const;

        Path operator/(const Path& Path) const;

    private:
        std::filesystem::path StdPath{};
    };
}

export template <>
class fmt::formatter<Valo::Path>
{
public:
    constexpr auto parse(format_parse_context& Ctx)
    {
        return Ctx.begin();
    }

    template <typename Context>
    constexpr auto format(const Valo::Path& Path, Context& Ctx) const
    {
        return format_to(Ctx.out(), "{}", Valo::StringView(Path));  // --== KEY LINE ==--
    }
};
