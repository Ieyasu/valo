module;

#include <filesystem>

module Valo.Std;

import :Memory;
import :String;

namespace Valo
{
    Path::Path(std::filesystem::path Path) :
        StdPath(Memory::Move(Path))
    {
    }

    Path::Path(const char* Str) :
        StdPath(Str)
    {
    }

    Path::Path(String Str) :
        StdPath(Str)
    {
    }

    Path::Path(StringView Str) :
        StdPath(Str)
    {
    }

    const char* Path::GetCString() const
    {
        return StdPath.c_str();
    }

    const String& Path::GetString() const
    {
        return StdPath.native();
    }

    void Path::Clear()
    {
        StdPath.clear();
    }

    bool Path::IsAbsolute() const
    {
        return StdPath.is_absolute();
    }

    bool Path::IsRelative() const
    {
        return StdPath.is_relative();
    }

    Path Path::GetRootDirectory() const
    {
        return StdPath.root_path();
    }

    Path Path::GetParentDirectory() const
    {
        return StdPath.parent_path();
    }

    Path Path::GetExtension() const
    {
        return StdPath.extension();
    }

    Path Path::GetFilename() const
    {
        return StdPath.filename();
    }

    Path& Path::RemoveExtension()
    {
        StdPath.replace_extension("");
        return *this;
    }

    Path& Path::RemoveFilename()
    {
        StdPath.remove_filename();
        return *this;
    }

    Path& Path::ReplaceExtension(const Path& Path)
    {
        StdPath.replace_extension(Path.StdPath);
        return *this;
    }

    Path& Path::ReplaceFilename(const Path& Path)
    {
        StdPath.replace_filename(Path.StdPath);
        return *this;
    }

    Path::operator String() const
    {
        return StdPath;
    }

    Path::operator StringView() const
    {
        return StdPath.c_str();
    }

    Path Path::operator/(const Path& Path) const
    {
        return StdPath / Path.StdPath;
    }
}
