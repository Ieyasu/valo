module;

#include <filesystem>
#include <fstream>

export module Valo.Std:File;

import :Bit;
import :Enum;
import :Memory;
import :Path;
import :Pointers;
import :String;
import :Std;
import :Types;

export namespace Valo
{
    enum class FileAccess : EnumSmall
    {
        Read       = Bit::Set<Enum>(0),
        Write      = Bit::Set<Enum>(1),
        Append     = Bit::Set<Enum>(2),
        ReadWrite  = Bit::Set<Enum>(0, 1),
        ReadAppend = Bit::Set<Enum>(0, 2),
    };

    enum class FileMode : EnumSmall
    {
        Binary = 0,
        Text   = 1,
    };

    class File final
    {
    public:
        using SizeType = UInt64;

        File() = default;
        File(Path Path, FileMode Mode, FileAccess Access);

        File(const File& Other)            = delete;
        File& operator=(const File& Other) = delete;

        File(File&& Other)            = default;
        File& operator=(File&& Other) = default;

        ~File();

        [[nodiscard]] bool Open(Path Path, FileMode Mode, FileAccess Access);

        void Close();

        void Flush();

        [[nodiscard]] bool IsOpen() const;

        [[nodiscard]] SizeType GetSize() const;

        [[nodiscard]] bool ReadLine(String& Line);

        [[nodiscard]] bool ReadText(String& Text);

        [[nodiscard]] bool Read(BinaryBlob& Binary);

        [[nodiscard]] SizeType Read(Byte* Data, SizeType Size);

        void WriteLine(StringView Line);

        void WriteText(StringView Text);

    private:
        std::fstream m_Stream{};
        Path         m_Path{};
        FileAccess   m_Access{};
        FileMode     m_Mode{};
    };
}
