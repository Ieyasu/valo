module;

#include <Valo/Macros.hpp>

#include <filesystem>
#include <ostream>

module Valo.Std;

import :File;
import :Std;

namespace Valo
{

    File::File(Path Path, FileMode Mode, FileAccess Access)
    {
        const bool IsOpen = Open(Path, Mode, Access);
        VALO_UNUSED(IsOpen);
    }

    File::~File()
    {
        Close();
    }

    bool File::IsOpen() const
    {
        return m_Stream.is_open();
    }

    File::SizeType File::GetSize() const
    {
        return static_cast<SizeType>(std::filesystem::file_size(m_Path.GetString()));
    }

    bool File::Open(Path Path, FileMode Mode, FileAccess Access)
    {
        Close();

        m_Path   = Path;
        m_Mode   = Mode;
        m_Access = Access;

        std::ios_base::openmode IoMode{};
        switch (Access)
        {
        case FileAccess::Read:
            IoMode |= std::ios_base::in;
            break;
        case FileAccess::Append:
            IoMode |= std::ios_base::app;
            break;
        case FileAccess::Write:
            IoMode |= std::ios_base::out;
            break;
        case FileAccess::ReadWrite:
            IoMode |= std::ios_base::in | std::ios_base::out;
            break;
        case FileAccess::ReadAppend:
            IoMode |= std::ios_base::in | std::ios_base::app;
            break;
        default:
            VALO_ASSERT(false && "Unknown file access flags");
            return false;
        }

        switch (Mode)
        {
        case FileMode::Binary:
            IoMode |= std::ios::binary;
            break;
        case FileMode::Text:
            break;
        default:
            VALO_ASSERT(false && "Unknown file mode");
            return false;
        }

        m_Stream.open(Path.GetCString(), IoMode);
        return m_Stream.is_open();
    }

    void File::Close()
    {
        if (IsOpen())
        {
            m_Stream.close();
        }
    }

    void File::Flush()
    {
        m_Stream << std::flush;
    }

    bool File::ReadLine(String& Line)
    {
        return !!std::getline(m_Stream, Line);
    }

    bool File::ReadText(String& Text)
    {
        if (m_Mode == FileMode::Binary)
        {
            return false;
        }

        // Get the file size
        m_Stream.seekg(0);
        m_Stream.seekg(0, std::ios::end);
        const auto Size = m_Stream.tellg();
        m_Stream.seekg(0);

        // Read data
        Text.resize(Size);
        if (Size > 0)
        {
            m_Stream.read(Text.data(), Size);
        }

        return true;
    }

    bool File::Read(BinaryBlob& Binary)
    {
        const auto FileSize = GetSize();
        Binary.resize(FileSize);
        if (FileSize > 0)
        {
            return Read(Binary.data(), FileSize);
        }

        return true;
    }

    File::SizeType File::Read(Byte* Data, SizeType Size)
    {
        m_Stream.read(reinterpret_cast<std::fstream::char_type*>(Data), Size);
        return m_Stream.gcount();
    }

    void File::WriteLine(StringView Line)
    {
        m_Stream << Line << std::endl;
    }

    void File::WriteText(StringView Text)
    {
        m_Stream << Text;
    }
}
