#include <valo/string.hpp>

#include <catch2/catch_test_macros.hpp>

namespace Valo
{
    TEST_CASE("String case", "[common][string]")
    {
        const auto empty_str      = String("");
        const auto lower_case_str = String("casetestingstring");
        const auto upper_case_str = String("CASETESTINGSTRING");
        const auto camel_case_str = String("CaseTestingString");

        SECTION("string::to_lower_copy()")
        {
            REQUIRE(StringFunc::ToLowerCopy(empty_str) == empty_str);
            REQUIRE(StringFunc::ToLowerCopy(lower_case_str) == lower_case_str);
            REQUIRE(StringFunc::ToLowerCopy(upper_case_str) == lower_case_str);
            REQUIRE(StringFunc::ToLowerCopy(camel_case_str) == lower_case_str);
        }

        SECTION("string::to_upper_copy()")
        {
            REQUIRE(StringFunc::ToUpperCopy(empty_str) == empty_str);
            REQUIRE(StringFunc::ToUpperCopy(lower_case_str) == upper_case_str);
            REQUIRE(StringFunc::ToUpperCopy(upper_case_str) == upper_case_str);
            REQUIRE(StringFunc::ToUpperCopy(camel_case_str) == upper_case_str);
        }
    }

    TEST_CASE("String trim", "[common][string]")
    {
        const auto empty_str           = String("");
        const auto only_whitespace_str = String(" \f\t\v   \r\r\n \t\v \f\f");
        const auto no_whitespace_str   = String("StringWithNoWhiteSpace");
        const auto no_trim_str         = String("String with nothing to trim");
        const auto newlines_str        = String("\r\n\r\n\nString with newlines\n  \n\r\n");
        const auto vertical_tab_str    = String("  \v  \v\v String with vertical tabs and spaces\v  \v\v ");
        const auto horizontal_tab_str  = String("  \t\t String with horizontal tabs and spaces\t  \t\t ");
        const auto feed_str            = String("  \f\f String with feeds and spaces\f  \f\f ");
        const auto mixed_str           = String("\f\t \n\v\rString with mixture of all whitespaces\n\r\f\f  \t \v");

        SECTION("string::trim_copy()")
        {
            REQUIRE(StringFunc::TrimCopy(empty_str) == empty_str);
            REQUIRE(StringFunc::TrimCopy(only_whitespace_str) == empty_str);
            REQUIRE(StringFunc::TrimCopy(no_whitespace_str) == no_whitespace_str);
            REQUIRE(StringFunc::TrimCopy(no_trim_str) == no_trim_str);
            REQUIRE(StringFunc::TrimCopy(newlines_str) == "String with newlines");
        }

        SECTION("string::ltrim_copy()")
        {
            REQUIRE(StringFunc::LTrimCopy(empty_str) == empty_str);
            REQUIRE(StringFunc::LTrimCopy(only_whitespace_str) == empty_str);
            REQUIRE(StringFunc::LTrimCopy(no_whitespace_str) == no_whitespace_str);
            REQUIRE(StringFunc::LTrimCopy(no_trim_str) == no_trim_str);
            REQUIRE(StringFunc::LTrimCopy(newlines_str) == "String with newlines\n  \n\r\n");
            REQUIRE(StringFunc::LTrimCopy(vertical_tab_str) == "String with vertical tabs and spaces\v  \v\v ");
            REQUIRE(StringFunc::LTrimCopy(horizontal_tab_str) == "String with horizontal tabs and spaces\t  \t\t ");
            REQUIRE(StringFunc::LTrimCopy(feed_str) == "String with feeds and spaces\f  \f\f ");
            REQUIRE(StringFunc::LTrimCopy(mixed_str) == "String with mixture of all whitespaces\n\r\f\f  \t \v");
        }

        SECTION("string::rtrim_copy()")
        {
            REQUIRE(StringFunc::RTrimCopy(empty_str) == empty_str);
            REQUIRE(StringFunc::RTrimCopy(only_whitespace_str) == empty_str);
            REQUIRE(StringFunc::RTrimCopy(no_whitespace_str) == no_whitespace_str);
            REQUIRE(StringFunc::RTrimCopy(no_trim_str) == no_trim_str);
            REQUIRE(StringFunc::RTrimCopy(newlines_str) == "\r\n\r\n\nString with newlines");
            REQUIRE(StringFunc::RTrimCopy(vertical_tab_str) == "  \v  \v\v String with vertical tabs and spaces");
            REQUIRE(StringFunc::RTrimCopy(horizontal_tab_str) == "  \t\t String with horizontal tabs and spaces");
            REQUIRE(StringFunc::RTrimCopy(feed_str) == "  \f\f String with feeds and spaces");
            REQUIRE(StringFunc::RTrimCopy(mixed_str) == "\f\t \n\v\rString with mixture of all whitespaces");
        }
    }

    TEST_CASE("String replace", "[common][string]")
    {
        const auto empty_str        = String("");
        const auto x                = String("x");
        const auto y                = String("y");
        const auto one              = String("one");
        const auto three            = String("three");
        const auto one_race_horse   = String("one one was a race horse, two two was one too.");
        const auto three_race_horse = String("three three was a race horse, two two was three too.");

        SECTION("string::replace_copy()")
        {
            REQUIRE(StringFunc::ReplaceCopy(empty_str, empty_str, empty_str) == empty_str);
            REQUIRE(StringFunc::ReplaceCopy(empty_str, x, y) == empty_str);
            REQUIRE(StringFunc::ReplaceCopy(x, x, y) == y);
            REQUIRE(StringFunc::ReplaceCopy(x, x, y) == y);
            REQUIRE(StringFunc::ReplaceCopy(one_race_horse, one, three) == three_race_horse);
        }
    }
}
