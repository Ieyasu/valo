#include <valo/type_index.hpp>

#include <catch2/catch_test_macros.hpp>

namespace Valo
{
    TEST_CASE("Valo::TypeIndex", "[common]")
    {
        auto type_index1 = TypeIndex::Create<int>();
        auto type_index2 = TypeIndex::Create<float>();
        auto type_index3 = TypeIndex::Create<int>();

        SECTION("TypeIndex::operator==() returns true for same types")
        {
            REQUIRE(type_index1 == type_index3);
        }

        SECTION("TypeIndex::operator==() returns false for different types")
        {
            REQUIRE(!(type_index1 == type_index2));
        }

        SECTION("TypeIndex::operator!=() returns true for different types")
        {
            REQUIRE(type_index1 != type_index2);
        }

        SECTION("TypeIndex::operator!= returns false for same types")
        {
            REQUIRE(!(type_index1 != type_index3));
        }

        SECTION("TypeIndex::operator<() can be used to compare type indices")
        {
            REQUIRE((type_index1 < type_index2 || type_index2 < type_index1));
            REQUIRE(!(type_index1 < type_index2 && type_index2 < type_index1));
        }

        SECTION("TypeIndex::operator>() can be used to compare type indices")
        {
            REQUIRE((type_index1 > type_index2 || type_index2 > type_index1));
            REQUIRE(!(type_index1 > type_index2 && type_index2 > type_index1));
        }

        SECTION("TypeIndex::operator>() and TypeIndex::operator<() can't be true at the same time")
        {
            REQUIRE(!(type_index1 > type_index2 && type_index1 < type_index2));
            REQUIRE(!(type_index2 > type_index1 && type_index2 < type_index1));
        }

        SECTION("TypeIndex::operator>() and TypeIndex::operator<() are both false for the same types")
        {
            REQUIRE(!(type_index1 > type_index3 || type_index1 < type_index3));
            REQUIRE(!(type_index3 > type_index1 || type_index3 < type_index1));
        }

        SECTION("TypeIndex::operator<=() can be used to compare type indices")
        {
            REQUIRE((type_index1 <= type_index2 || type_index2 <= type_index1));
            REQUIRE(!(type_index1 <= type_index2 && type_index2 <= type_index1));
        }

        SECTION("TypeIndex::operator>=() can be used to compare type indices")
        {
            REQUIRE((type_index1 >= type_index2 || type_index2 >= type_index1));
            REQUIRE(!(type_index1 >= type_index2 && type_index2 >= type_index1));
        }

        SECTION("TypeIndex::operator>=() and TypeIndex::operator<=() can't be false at the same time")
        {
            REQUIRE((type_index1 >= type_index2 || type_index1 <= type_index2));
            REQUIRE((type_index1 >= type_index3 || type_index1 <= type_index3));
        }

        SECTION("TypeIndex::operator>=() and TypeIndex::operator<=() are both true for the same types")
        {
            REQUIRE((type_index1 >= type_index3 && type_index1 <= type_index3));
            REQUIRE((type_index3 >= type_index1 && type_index3 <= type_index1));
        }
    }
}
