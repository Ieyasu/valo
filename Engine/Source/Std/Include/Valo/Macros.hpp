#pragma once

#include <cassert>

#define VALO_ASSERT(X) assert((X))

#define VALO_UNUSED(x) (void)(x)

#ifdef _WIN32
    #ifdef WIN_EXPORT
        #define VALO_LIBRARY_API __declspec(dllexport)
    #else
        #define VALO_LIBRARY_API __declspec(dllimport)
    #endif
#else
    #define VALO_LIBRARY_API
#endif

#define VALO_FLAGS(BIT_NAME, FLAG_NAME)                                            \
    enum class BIT_NAME : Enum;                                                    \
                                                                                   \
    using FLAG_NAME = BaseFlags<BIT_NAME>;                                         \
                                                                                   \
    [[nodiscard]] inline FLAG_NAME operator|(BIT_NAME lhs, BIT_NAME rhs) noexcept  \
    {                                                                              \
        return FLAG_NAME(lhs) | FLAG_NAME(rhs);                                    \
    }                                                                              \
                                                                                   \
    [[nodiscard]] inline FLAG_NAME operator|(BIT_NAME lhs, FLAG_NAME rhs) noexcept \
    {                                                                              \
        return FLAG_NAME(lhs) | rhs;                                               \
    }                                                                              \
                                                                                   \
    [[nodiscard]] inline FLAG_NAME operator&(BIT_NAME lhs, BIT_NAME rhs) noexcept  \
    {                                                                              \
        return FLAG_NAME(lhs) & FLAG_NAME(rhs);                                    \
    }                                                                              \
                                                                                   \
    [[nodiscard]] inline FLAG_NAME operator&(BIT_NAME lhs, FLAG_NAME rhs) noexcept \
    {                                                                              \
        return FLAG_NAME(lhs) & rhs;                                               \
    }                                                                              \
                                                                                   \
    enum class BIT_NAME : Enum
