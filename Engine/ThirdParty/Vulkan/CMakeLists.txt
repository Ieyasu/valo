cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(Vulkan VALO_THIRDPARTY_VULKAN_TARGET)

target_sources(${target}
    PRIVATE
        Source/Vulkan.cpp
)

find_package(VulkanHeaders CONFIG REQUIRED)
target_link_libraries(${target} PUBLIC Vulkan::Headers)

target_compile_definitions(${target} PUBLIC VULKAN_HPP_NO_STRUCT_CONSTRUCTORS)
target_compile_definitions(${target} PUBLIC VULKAN_HPP_NO_EXCEPTIONS)
target_compile_definitions(${target} PUBLIC VULKAN_HPP_DISPATCH_LOADER_DYNAMIC)
target_compile_definitions(${target} PUBLIC VULKAN_HPP_NO_SMART_HANDLE)

find_package(VulkanMemoryAllocator CONFIG REQUIRED)
target_link_libraries(${target} PUBLIC GPUOpen::VulkanMemoryAllocator)
target_compile_definitions(${target} PUBLIC VMA_STATIC_VULKAN_FUNCTIONS=0)
target_compile_definitions(${target} PUBLIC VMA_DYNAMIC_VULKAN_FUNCTIONS=1)
