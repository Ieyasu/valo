module Valo.DirectXShaderCompiler;

import Valo.Engine;
import Valo.ShaderCompiler;

namespace Valo
{
    bool DirectXShaderCompilerModule::Initialize(Engine& Engine)
    {
        Compiler = Engine.Require<ShaderCompilerModule>()->GetShaderCompiler();
        Compiler->RegisterBackend(CompilerBackend);
        return true;
    }

    void DirectXShaderCompilerModule::Deinitialize()
    {
        Compiler->DeregisterBackend(CompilerBackend);
    }
}
