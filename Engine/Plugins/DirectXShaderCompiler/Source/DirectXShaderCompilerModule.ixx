export module Valo.DirectXShaderCompiler:DirectXShaderCompilerModule;

import Valo.Engine;
import Valo.ShaderCompiler;

import :DirectXShaderCompilerBackend;

export namespace Valo
{
    class DirectXShaderCompilerModule final : public EngineModule
    {
    public:
        bool Initialize(Engine& engine) override;

        void Deinitialize() override;

    private:
        ShaderCompilerHandle Compiler{};

        DirectXShaderCompilerBackend CompilerBackend{};
    };
}
