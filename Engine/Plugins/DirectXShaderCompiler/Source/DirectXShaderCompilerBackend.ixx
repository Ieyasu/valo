module;

#include <dxcapi.h>

export module Valo.DirectXShaderCompiler:DirectXShaderCompilerBackend;

import Valo.ShaderCompiler;
import Valo.Std;

export namespace Valo
{
    class DirectXShaderCompilerBackend final : public ShaderCompilerBackend
    {
    public:
        DirectXShaderCompilerBackend();

    protected:
        bool IsShaderLanguageSupported(ShaderLanguage language) const override;

        bool IsShaderIRSupported(ShaderIR ir) const override;

        Optional<BinaryBlob> Compile(const ShaderCompilerOptions& options) override;

    private:
        CComPtr<IDxcCompiler3> DxcCompiler{};
        CComPtr<IDxcUtils>     DxcUtils{};
        Vector<LPCWSTR>        DxcArguments{};
    };
}
