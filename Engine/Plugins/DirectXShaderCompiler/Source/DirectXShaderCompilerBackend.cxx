module;

#include <dxcapi.h>

#include <codecvt>

module Valo.DirectXShaderCompiler;

import Valo.Logger;
import Valo.ShaderCompiler;
import Valo.Std;

namespace Valo
{
    DirectXShaderCompilerBackend::DirectXShaderCompilerBackend()
    {
        DxcCreateInstance(CLSID_DxcCompiler, IID_PPV_ARGS(&DxcCompiler));
        DxcCreateInstance(CLSID_DxcUtils, IID_PPV_ARGS(&DxcUtils));
    }

    bool DirectXShaderCompilerBackend::IsShaderLanguageSupported(ShaderLanguage Language) const
    {
        return Language == ShaderLanguage::HLSL;
    }

    bool DirectXShaderCompilerBackend::IsShaderIRSupported(ShaderIR IR) const
    {
        return IR == ShaderIR::SPIRV || IR == ShaderIR::DXIL;
    }

    Optional<BinaryBlob> DirectXShaderCompilerBackend::Compile(const ShaderCompilerOptions& Options)
    {
        static std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> Converter;

        // Load shader source
        auto SourceFile = File{};
        if (!SourceFile.Open(Options.Filepath, FileMode::Text, FileAccess::Read))
        {
            AddCompilerError("File not found.");
            return nullopt;
        }

        auto Source = String();
        if (!SourceFile.ReadText(Source))
        {
            AddCompilerError("Failed to read file.");
            return nullopt;
        }

        auto DxcSource = CComPtr<IDxcBlobEncoding>{};
        DxcUtils->CreateBlob(Source.data(), static_cast<UInt32>(Source.size()), CP_UTF8, &DxcSource);

        // Convert arguments to wide strings
        auto EntryPoint = Converter.from_bytes(Options.EntryPoint.data());
        auto Defines    = Vector<std::wstring>();
        for (const auto& Define : Options.Defines)
        {
            Defines.push_back(Converter.from_bytes(Define.data()));
        }

        // Build arguments list
        DxcArguments.clear();

        DxcArguments.push_back(L"-E");
        DxcArguments.push_back(EntryPoint.c_str());

        DxcArguments.push_back(L"-T");
        DxcArguments.push_back(L"cs_6_6");

        DxcArguments.push_back(L"-Qstrip_debug");
        DxcArguments.push_back(L"-Qstrip_reflect");

        DxcArguments.push_back(DXC_ARG_WARNINGS_ARE_ERRORS);
        DxcArguments.push_back(DXC_ARG_OPTIMIZATION_LEVEL3);
        DxcArguments.push_back(DXC_ARG_ALL_RESOURCES_BOUND);

        for (const auto& Define : Defines)
        {
            DxcArguments.push_back(L"-D");
            DxcArguments.push_back(Define.c_str());
        }

        // Build source buffer
        auto DxcSourceBuffer = DxcBuffer{
            .Ptr      = DxcSource->GetBufferPointer(),
            .Size     = DxcSource->GetBufferSize(),
            .Encoding = 0,
        };

        // Compile
        auto DxcCompileResult = CComPtr<IDxcResult>{};
        auto DxcHr            = DxcCompiler->Compile(
            &DxcSourceBuffer,
            DxcArguments.data(),
            static_cast<UInt32>(DxcArguments.size()),
            nullptr,
            IID_PPV_ARGS(&DxcCompileResult));
        if (DxcHr != S_OK)
        {
            AddCompilerError("IDxcCompiler3::Compile failed with HRESULT={}", DxcHr);
            return nullopt;
        }

        // Handle errors
        auto DxcErrors = CComPtr<IDxcBlobUtf8>{};
        DxcCompileResult->GetOutput(DXC_OUT_ERRORS, IID_PPV_ARGS(&DxcErrors), nullptr);
        if (DxcErrors && DxcErrors->GetStringLength() > 0)
        {
            AddCompilerError(static_cast<const char*>(DxcErrors->GetBufferPointer()));
            return nullopt;
        }

        return nullopt;
    }
}
