cmake_minimum_required(VERSION 3.30)

valo_add_shared_library(Input VALO_INPUT_TARGET)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/_Module.ixx
        Source/Enums.ixx
        Source/Input.ixx
    PRIVATE
        Source/Input.cxx
)

target_link_libraries(${target} PUBLIC
    ${VALO_LIBRARIES}
)

find_package(glfw3 CONFIG REQUIRED)
target_link_libraries(${target} PRIVATE glfw)
