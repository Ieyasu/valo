export module Valo.Input:Input;

import Valo.Engine;
import Valo.Math;
import Valo.Std;

import :Enums;

export namespace Valo
{
    class Input final : public EngineModule
    {
    public:
        [[nodiscard]] Vec2 GetMousePos() const;
        [[nodiscard]] Vec2 GetMouseDelta() const;

        [[nodiscard]] bool IsKeyHeld(KeyCode key) const;
        [[nodiscard]] bool IsKeyPressed(KeyCode key) const;
        [[nodiscard]] bool IsKeyReleased(KeyCode key) const;

        [[nodiscard]] bool IsMouseHeld(MouseButton button) const;
        [[nodiscard]] bool IsMousePressed(MouseButton button) const;
        [[nodiscard]] bool IsMouseReleased(MouseButton button) const;

    private:
        void Update() override;

        void SetKeyState(KeyCode key, KeyAction action);
        void SetKeyPressed(KeyCode key);
        void SetKeyReleased(KeyCode key);

        void SetMousePos(Vec2 pos);
        void SetMouseState(MouseButton button, KeyAction action);
        void SetMousePressed(MouseButton button);
        void SetMouseReleased(MouseButton button);

        void Poll();

        Set<MouseButton> m_buttons_held{};
        Set<MouseButton> m_buttons_pressed{};
        Set<MouseButton> m_buttons_released{};

        Set<KeyCode> m_keys_held{};
        Set<KeyCode> m_keys_pressed{};
        Set<KeyCode> m_keys_released{};

        Vec2 m_mouse_pos{};
        Vec2 m_mouse_pos_prev{};
    };
}
