module;

#include <Valo/Profile.hpp>

#include <GLFW/glfw3.h>

module Valo.Input;

import Valo.Math;
import Valo.Profiler;

namespace Valo
{
    void Input::Update()
    {
        VALO_PROFILE_SCOPED_FUNCTION(Input);

        Poll();
    }

    Vec2 Input::GetMousePos() const
    {
        return m_mouse_pos;
    }

    Vec2 Input::GetMouseDelta() const
    {
        return m_mouse_pos - m_mouse_pos_prev;
    }

    bool Input::IsKeyPressed(KeyCode key) const
    {
        return m_keys_pressed.contains(key);
    }

    bool Input::IsKeyReleased(KeyCode key) const
    {
        return m_keys_released.contains(key);
    }

    bool Input::IsKeyHeld(KeyCode key) const
    {
        return m_keys_held.contains(key);
    }

    bool Input::IsMousePressed(MouseButton button) const
    {
        return m_buttons_pressed.contains(button);
    }

    bool Input::IsMouseReleased(MouseButton button) const
    {
        return m_buttons_released.contains(button);
    }

    bool Input::IsMouseHeld(MouseButton button) const
    {
        return m_buttons_held.contains(button);
    }

    void Input::SetKeyPressed(KeyCode key)
    {
        m_keys_held.insert(key);
        m_keys_pressed.insert(key);
    }

    void Input::SetKeyReleased(KeyCode key)
    {
        m_keys_held.erase(key);
        m_keys_released.insert(key);
    }

    void Input::SetKeyState(KeyCode key, KeyAction action)
    {
        switch (action)
        {
        case KeyAction::Press:
            SetKeyPressed(key);
            break;
        case KeyAction::Release:
            SetKeyReleased(key);
            break;
        default:
            break;
        }
    }

    void Input::SetMousePos(Vec2 pos)
    {
        m_mouse_pos_prev = m_mouse_pos;
        m_mouse_pos      = pos;
    }

    void Input::SetMousePressed(MouseButton button)
    {
        m_buttons_held.insert(button);
        m_buttons_pressed.insert(button);
    }

    void Input::SetMouseReleased(MouseButton button)
    {
        m_buttons_held.erase(button);
        m_buttons_released.insert(button);
    }

    void Input::SetMouseState(MouseButton button, KeyAction action)
    {
        switch (action)
        {
        case KeyAction::Press:
            SetMousePressed(button);
            break;
        case KeyAction::Release:
            SetMouseReleased(button);
            break;
        default:
            break;
        }
    }

    void Input::Poll()
    {
        m_mouse_pos_prev = m_mouse_pos;
        m_keys_pressed.clear();
        m_keys_released.clear();
        m_buttons_pressed.clear();
        m_buttons_released.clear();
        glfwPollEvents();
    }
}
