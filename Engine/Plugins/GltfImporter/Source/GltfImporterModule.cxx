module;

#include <filesystem>

module Valo.GltfImporter;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Renderer;
import Valo.Std;

import :CGltfImporter;

namespace Valo
{
    bool GltfImporterModule::Initialize(Engine& Engine)
    {
        m_AssetDatabase = Engine.Require<AssetDatabase>();
        m_AssetDatabase->RegisterImporter(*this);

        auto RendererModule = Engine.Require<Renderer>();
        m_Importer          = MakeUnique<CGltfImporter>(
            RendererModule->m_material_store.get(),
            RendererModule->m_mesh_store.get(),
            RendererModule->m_texture_store.get());

        return true;
    }

    void GltfImporterModule::Deinitialize()
    {
        m_AssetDatabase->DeregisterImporter(*this);
        m_Importer.reset();
    }

    bool GltfImporterModule::CanImport(const Path& AssetPath) const
    {
        const auto Extension = AssetPath.GetExtension();
        return Extension.GetString() == ".glb" || Extension.GetString() == ".gltf";
    }

    void GltfImporterModule::Import(AssetImportContext& Context)
    {
        m_Importer->Import(Context);
    }
}
