module;

#include <cgltf.h>

export module Valo.GltfImporter:CGltfTextureImporter;

import Valo.AssetDatabase;
import Valo.ImageImporter;
import Valo.Renderer;
import Valo.Std;

import :CGltfData;

export namespace Valo
{
    class CGltfTextureImporter final
    {
    public:
        explicit CGltfTextureImporter(TextureStoreHandle texture_store);

        void Import(AssetImportContext& context, CGltfData& gltf);

    private:
        void ImportTexture(CGltfData& gltf, const cgltf_texture& gltf_texture);

        bool LoadImage(const cgltf_texture& gltf_texture, TextureFormat format, ImageData& out_data);

        void DeduceImageFormats(CGltfData& gltf, const cgltf_material& gltf_material);

        void SetImageFormats(CGltfData& data, const cgltf_texture& texture, TextureFormat format);

        static StringView GetTextureName(const cgltf_texture& gltf_texture);

        static StringView GetImageName(const cgltf_image* gltf_image);

        void LogFormatError(const String& message, const cgltf_texture& texture);

        void LogTextureError(const String& message, const cgltf_texture& texture);

        ObserverPtr<AssetImportContext> m_context{nullptr};
        TextureStoreHandle              m_texture_store{};
        Path                            m_asset_directory{};
    };
}
