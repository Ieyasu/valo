module;

#include <cgltf.h>

export module Valo.GltfImporter:CGltfMeshImporter;

import Valo.AssetDatabase;
import Valo.Math;
import Valo.Renderer;
import Valo.RHI;
import Valo.Std;

import :CGltfData;

export namespace Valo
{
    class CGltfMeshImporter final
    {
    public:
        explicit CGltfMeshImporter(MeshStoreHandle mesh_store);

        void Import(AssetImportContext& context, const CGltfData& gltf);

    private:
        static String cgltf_attribute_type_to_string(cgltf_attribute_type type);

        static RHI::PrimitiveTopology cgltf_primitive_type_to_topology(cgltf_primitive_type primitive_type);

        void ImportMesh(const CGltfData& gltf, const cgltf_mesh& mesh);

        bool ImportIndices(const CGltfData& gltf, const cgltf_primitive& primitive);

        bool ImportAttribute(const CGltfData& gltf, const cgltf_attribute& attribute);

        template <typename T>
        bool ImportAttribute(const CGltfData& gltf, const cgltf_attribute& attribute, Vector<T>& out_data);

        void LogMeshError(const String& message);

        void LogAccessorError(const cgltf_accessor& accessor, const String& message);

        void LogMeshAttributeError(const cgltf_attribute& attribute, const String& message);

        void LogMeshAttributeWarning(const cgltf_attribute& attribute, const String& message);

        ObserverPtr<AssetImportContext> m_context{nullptr};
        MeshStoreHandle                 m_mesh_store{};
        MeshHandle                      m_mesh{};
        UInt32                          m_current_primitive{0};
        UInt32                          m_current_uv{0};

        BinaryBlob     m_accessor_data_buffer{};
        Vector<Vec3>   m_position_buffer{};
        Vector<Vec3>   m_normal_buffer{};
        Vector<Vec4>   m_tangent_buffer{};
        Vector<Vec2>   m_uv_buffer{};
        Vector<Vec3>   m_color_rgb_buffer{};
        Vector<Vec4>   m_color_rgba_buffer{};
        Vector<UInt32> m_index_buffer{};
    };

    template <typename T>
    bool CGltfMeshImporter::ImportAttribute(const CGltfData& gltf, const cgltf_attribute& attribute, Vector<T>& out_data)
    {
        out_data.clear();
        if (!gltf.LoadAccessorFloatData(*attribute.data, out_data))
        {
            LogMeshAttributeError(attribute, "failed to load accessor data.");
            return false;
        }
        return true;
    }
}
