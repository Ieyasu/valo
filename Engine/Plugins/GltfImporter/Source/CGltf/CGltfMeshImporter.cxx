module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

module Valo.GltfImporter;

import Valo.AssetDatabase;
import Valo.Logger;
import Valo.ImageImporter;
import Valo.Renderer;
import Valo.RHI;
import Valo.Std;

import :CGltfData;
import :CGltfMeshImporter;

namespace Valo
{
    CGltfMeshImporter::CGltfMeshImporter(MeshStoreHandle mesh_store) :
        m_mesh_store(mesh_store)
    {
    }

    void CGltfMeshImporter::Import(AssetImportContext& context, const CGltfData& gltf)
    {
        m_context = &context;

        for (size_t i = 0; i < gltf.data->meshes_count; ++i)
        {
            ImportMesh(gltf, gltf.data->meshes[i]);
        }
    }

    void CGltfMeshImporter::ImportMesh(const CGltfData& gltf, const cgltf_mesh& mesh)
    {
        m_mesh = m_mesh_store->CreateMesh();
        m_context->AddAsset(m_mesh);

        if (mesh.name != nullptr)
        {
            m_mesh->SetName(mesh.name);
        }

        // Load all primitives
        m_current_primitive = 0;
        for (size_t primitive_idx = 0; primitive_idx < mesh.primitives_count; ++primitive_idx)
        {
            const auto& primitive = mesh.primitives[primitive_idx];

            m_current_uv = 0;
            m_position_buffer.clear();
            m_normal_buffer.clear();
            m_tangent_buffer.clear();
            m_uv_buffer.clear();
            m_color_rgb_buffer.clear();
            m_color_rgba_buffer.clear();
            m_index_buffer.clear();

            // Load indices
            if (!ImportIndices(gltf, primitive))
            {
                LogMeshError("failed to load indices");
                continue;
            }
            // Load all supported vertex attributes
            for (size_t attribute_idx = 0; attribute_idx < primitive.attributes_count; ++attribute_idx)
            {
                const auto& attribute = primitive.attributes[attribute_idx];
                if (!ImportAttribute(gltf, attribute))
                {
                    continue;
                }
            }

            // If positions are not available, can't do anything
            if (m_position_buffer.empty())
            {
                LogMeshError("failed to load positions");
                continue;
            }
            m_mesh->SetPositions(m_position_buffer);

            // glTF spec allows empty indices, in which case they should be generated accorgind to spec
            if (m_index_buffer.empty())
            {
                MeshUtility::GenerateIndices(RHI::PrimitiveTopology::TriangleList, m_position_buffer, m_index_buffer);
            }
            m_mesh->SetIndices(m_current_primitive, m_index_buffer);

            // If uvs are not avalable,
            if (m_uv_buffer.size() != m_position_buffer.size())
            {
                m_uv_buffer.resize(m_position_buffer.size());
            }
            m_mesh->SetUVs(m_uv_buffer);

            // If normals are not available, calculate them
            if (m_normal_buffer.size() != m_position_buffer.size())
            {
                MeshUtility::CalculateNormals(
                    RHI::PrimitiveTopology::TriangleList,
                    m_index_buffer,
                    m_position_buffer,
                    MeshNormalWeightAlgorithm::AreaAndAngle,
                    m_normal_buffer);
                VALO_ASSERT(m_normal_buffer.size() == m_position_buffer.size());
            }
            m_mesh->SetNormals(m_normal_buffer);

            // If tangents are not available, calculate them
            if (m_tangent_buffer.size() != m_position_buffer.size())
            {
                MeshUtility::CalculateTangents(
                    RHI::PrimitiveTopology::TriangleList,
                    m_index_buffer,
                    m_position_buffer,
                    m_normal_buffer,
                    m_uv_buffer,
                    m_tangent_buffer);
                VALO_ASSERT(m_tangent_buffer.size() == m_position_buffer.size());
            }
            m_mesh->SetTangents(m_tangent_buffer);
        }
    }

    bool CGltfMeshImporter::ImportIndices(const CGltfData& gltf, const cgltf_primitive& primitive)
    {
        // glTF spec doesn't require indices to be present. If they are not defined,
        // MeshDataWriter will handle generating the correct indices base on the mesh
        // topology.
        if (primitive.indices == nullptr)
        {
            return true;
        }

        // Load the index data from the accessor
        const auto& indices = *primitive.indices;
        if (!gltf.LoadAccessorData(indices, m_accessor_data_buffer))
        {
            return false;
        }

        switch (indices.component_type)
        {
        case cgltf_component_type_r_8u:
        {
            m_index_buffer.resize(indices.count);

            UInt8 index{};
            for (size_t i = 0; i < indices.count; ++i)
            {
                Memory::Memcpy(&index, m_accessor_data_buffer.data() + i * sizeof(UInt8), sizeof(UInt8));
                m_index_buffer[i] = index;
            }
            break;
        }
        case cgltf_component_type_r_16u:
        {
            m_index_buffer.resize(indices.count);

            UInt16 index{};
            for (size_t i = 0; i < indices.count; ++i)
            {
                Memory::Memcpy(&index, m_accessor_data_buffer.data() + i * sizeof(UInt16), sizeof(UInt16));
                m_index_buffer[i] = index;
            }
            break;
        }
        case cgltf_component_type_r_32u:
        {
            m_index_buffer.resize(indices.count);
            Memory::Memcpy(&m_index_buffer.front(), m_accessor_data_buffer.data(), indices.count * sizeof(UInt32));
            break;
        }
        default:
            Log::Fatal("Failed to load indices: unsupported component type {}", indices.component_type);
            return false;
        }

        return true;
    }

    bool CGltfMeshImporter::ImportAttribute(const CGltfData& gltf, const cgltf_attribute& attribute)
    {
        if (attribute.data == nullptr)
        {
            LogMeshAttributeError(attribute, "data is null");
            return false;
        }

        switch (attribute.type)
        {
        case cgltf_attribute_type_position:
        {
            return ImportAttribute(gltf, attribute, m_position_buffer);
        }
        case cgltf_attribute_type_color:
        {
            // Colors are alsow allowed to be vec3, make sure to convert to vec4 in this case
            if (attribute.data->type == cgltf_type_vec3)
            {
                if (!ImportAttribute(gltf, attribute, m_color_rgb_buffer))
                {
                    return false;
                }

                m_color_rgba_buffer.resize(m_color_rgb_buffer.size());
                for (size_t i = 0; i < m_color_rgb_buffer.size(); ++i)
                {
                    m_color_rgba_buffer[i].r = m_color_rgb_buffer[i].r;
                    m_color_rgba_buffer[i].g = m_color_rgb_buffer[i].g;
                    m_color_rgba_buffer[i].b = m_color_rgb_buffer[i].b;
                    m_color_rgba_buffer[i].a = 1.0;
                }

                return true;
            }

            return ImportAttribute(gltf, attribute, m_color_rgba_buffer);
        }
        case cgltf_attribute_type_normal:
        {
            return ImportAttribute(gltf, attribute, m_normal_buffer);
        }
        case cgltf_attribute_type_tangent:
        {
            return ImportAttribute(gltf, attribute, m_tangent_buffer);
        }
        case cgltf_attribute_type_texcoord:
        {
            if (m_current_uv == 0)
            {
                return ImportAttribute(gltf, attribute, m_uv_buffer);
            }

            LogMeshAttributeWarning(attribute, "more than one UV set is not supported");
            return true;
        }
        case cgltf_attribute_type_weights:
        case cgltf_attribute_type_joints:
        {
            LogMeshAttributeWarning(attribute, "attribute type is not supported");
            return true;
        }
        case cgltf_attribute_type_invalid:
        {
            LogMeshAttributeError(attribute, "invalid attribute type");
            return false;
        }
        default:
        {
            LogMeshAttributeError(attribute, "unknown attribute type");
            return false;
        }
        }
    }

    RHI::PrimitiveTopology CGltfMeshImporter::cgltf_primitive_type_to_topology(cgltf_primitive_type primitive_type)
    {
        switch (primitive_type)
        {
        case cgltf_primitive_type_points:
            return RHI::PrimitiveTopology::PointList;
        case cgltf_primitive_type_line_loop:
            // return RHI::PrimitiveTopology::LineLoop;
            // case cgltf_primitive_type_line_strip:
            // return RHI::PrimitiveTopology::LineStrip;
            // case cgltf_primitive_type_lines:
            return RHI::PrimitiveTopology::LineList;
        case cgltf_primitive_type_triangle_fan:
            // return RHI::PrimitiveTopology::TriangleFan;
            // case cgltf_primitive_type_triangle_strip:
            // return RHI::PrimitiveTopology::TriangleStrip;
            // case cgltf_primitive_type_triangles:
            return RHI::PrimitiveTopology::TriangleList;
        default:
            return {};
        }
    }

    String CGltfMeshImporter::cgltf_attribute_type_to_string(cgltf_attribute_type type)
    {
        switch (type)
        {
        case cgltf_attribute_type_invalid:
            return c_attribute_name_invalid;
        case cgltf_attribute_type_position:
            return c_attribute_name_position;
        case cgltf_attribute_type_normal:
            return c_attribute_name_normal;
        case cgltf_attribute_type_tangent:
            return c_attribute_name_tangent;
        case cgltf_attribute_type_texcoord:
            return c_attribute_name_uv;
        case cgltf_attribute_type_color:
            return c_attribute_name_color_rgba;
        case cgltf_attribute_type_joints:
            return c_attribute_name_joints;
        case cgltf_attribute_type_weights:
            return c_attribute_name_weights;
        default:
            Log::Fatal("Failed to get attribute type string: unknown attribute type {}", type);
            return c_attribute_name_unknown;
        }
    }

    void CGltfMeshImporter::LogMeshError(const String& message)
    {
        const auto formatted_message = StringFunc::Format(
            "Failed to import mesh (name={}): {}.",
            m_mesh->GetName(),
            message);
        m_context->AddErrorMessage(formatted_message, *m_mesh);
    }

    void CGltfMeshImporter::LogAccessorError(const cgltf_accessor& accessor, const String& message)
    {
        const auto formatted_message = StringFunc::Format(
            "Failed to import mesh accessor (name={}, type={}): {}.",
            accessor.name ? accessor.name : "???",
            accessor.component_type,
            message);
        m_context->AddErrorMessage(formatted_message, *m_mesh);
    }

    void CGltfMeshImporter::LogMeshAttributeError(const cgltf_attribute& attribute, const String& message)
    {
        const auto formatted_message = StringFunc::Format(
            "Failed to import mesh attribute (name={}, type={}): {}.",
            attribute.name ? attribute.name : c_attribute_name_unknown,
            cgltf_attribute_type_to_string(attribute.type),
            message);
        m_context->AddErrorMessage(formatted_message, *m_mesh);
    }

    void CGltfMeshImporter::LogMeshAttributeWarning(const cgltf_attribute& attribute, const String& message)
    {
        const auto formatted_message = StringFunc::Format(
            "Failed to import mesh attribute (name={}, type={}): {}.",
            attribute.name ? attribute.name : c_attribute_name_unknown,
            cgltf_attribute_type_to_string(attribute.type),
            message);
        m_context->AddWarningMessage(formatted_message, *m_mesh);
    }

}
