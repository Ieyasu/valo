module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

#include <cstring>

module Valo.GltfImporter;

import Valo.AssetDatabase;
import Valo.ImageImporter;
import Valo.Math;
import Valo.RHI;
import Valo.Renderer;
import Valo.Std;

import :CGltfData;
import :CGltfTextureImporter;

namespace Valo
{
    CGltfTextureImporter::CGltfTextureImporter(TextureStoreHandle texture_store) :
        m_texture_store(texture_store)
    {
    }

    void CGltfTextureImporter::Import(AssetImportContext& context, CGltfData& gltf)
    {
        if (gltf.data->textures == nullptr)
        {
            return;
        }

        m_context         = &context;
        m_asset_directory = context.GetAssetPath().GetParentDirectory();

        // Deduce image formats
        for (size_t i = 0; i < gltf.data->materials_count; ++i)
        {
            DeduceImageFormats(gltf, gltf.data->materials[i]);
        }

        // Import textures
        for (size_t i = 0; i < gltf.data->textures_count; ++i)
        {
            ImportTexture(gltf, gltf.data->textures[i]);
        }
    }

    void CGltfTextureImporter::ImportTexture(CGltfData& gltf, const cgltf_texture& gltf_texture)
    {
        if (!gltf_texture.image)
        {
            LogTextureError("image is null", gltf_texture);
            return;
        }

        auto texture_descriptor = TextureDescriptor{};

        // Find if the image and sampler for this texture have already been created
        const auto image_it = gltf.images.find(gltf_texture.image);
        if (image_it != gltf.images.end())
        {
            texture_descriptor.image = image_it->second;
        }

        const auto sampler_it = gltf.samplers.find(gltf_texture.sampler);
        if (sampler_it != gltf.samplers.end())
        {
            texture_descriptor.sampler = sampler_it->second;
        }

        // If the image has not been created previously, fill in the image info
        auto image_data = ImageData{};
        if (image_it == gltf.images.end())
        {
            const auto image_format_it = gltf.image_formats.find(gltf_texture.image);
            if (image_format_it == gltf.image_formats.end())
            {
                LogTextureError("failed to deduce image format", gltf_texture);
                return;
            }

            const auto format = image_format_it->second;
            if (!LoadImage(gltf_texture, format, image_data))
            {
                return;
            }

            texture_descriptor.width             = image_data.info.width;
            texture_descriptor.height            = image_data.info.height;
            texture_descriptor.depth             = image_data.info.depth;
            texture_descriptor.dimensions        = TextureDimensions::Two;
            texture_descriptor.array_layer_count = image_data.info.array_layer_count;
            texture_descriptor.mip_level_count   = image_data.info.mip_level_count;
            texture_descriptor.format            = format;
            texture_descriptor.usage             = TextureUsage::Sampled | TextureUsage::TransferDst;
        }

        // If the sampler has not been created previously, create it
        if (sampler_it == gltf.samplers.end())
        {
            texture_descriptor.sampler = m_texture_store->GetDefaultSamplerLinearRepeat();
        }

        // Create the texture
        auto texture = m_texture_store->CreateTexture(texture_descriptor);
        if (!texture.IsValid())
        {
            LogTextureError("failed to create resource", gltf_texture);
            return;
        }
        texture->SetName(String(GetTextureName(gltf_texture)));
        m_context->AddAsset(texture);

        // Upload the image data
        VALO_ASSERT(!image_data.data.empty());
        for (UInt32 mip_level = 0; mip_level < image_data.info.mip_level_count; ++mip_level)
        {
            m_texture_store->UploadTexture(image_data.data[mip_level], texture, mip_level, 0);
        }

        // Cache texture information
        if (image_it == gltf.images.end())
        {
            gltf.images[gltf_texture.image] = texture->GetImage();
        }

        if (sampler_it == gltf.samplers.end())
        {
            gltf.samplers[gltf_texture.sampler] = texture->GetSampler();
        }

        gltf.textures[&gltf_texture] = texture;
    }

    bool CGltfTextureImporter::LoadImage(const cgltf_texture& gltf_texture, TextureFormat format, ImageData& out_data)
    {
        // First, try to load directly from buffer
        if (gltf_texture.image->buffer_view && gltf_texture.image->buffer_view->buffer)
        {
            const auto* buffer_view = gltf_texture.image->buffer_view;
            const auto* buffer_ptr  = static_cast<const Byte*>(buffer_view->buffer->data);
            const auto  data        = Span<const Byte>(buffer_ptr + buffer_view->offset, buffer_view->size);
            if (!ImageLoader::LoadImage(data, format, out_data))
            {
                LogTextureError("failed to load image from buffer view", gltf_texture);
                return false;
            }
            return true;
        }

        // Next, try to load from a data URI
        if (gltf_texture.image->uri && std::strncmp(gltf_texture.image->uri, "data:", 5) == 0)
        {
            LogTextureError("data URIs not implemented", gltf_texture);
            return false;
        }

        // Last, try to load from a file URI
        if (gltf_texture.image->uri)
        {
            const auto image_path = m_asset_directory / Path(gltf_texture.image->uri);
            if (!ImageLoader::LoadImage(image_path, format, out_data))
            {
                LogTextureError("failed to load image from URI", gltf_texture);
                return false;
            }
            return true;
        }

        LogTextureError("image contains invalid data", gltf_texture);
        return false;
    }

    void CGltfTextureImporter::DeduceImageFormats(CGltfData& gltf, const cgltf_material& gltf_material)
    {
        // Sampled from RGB channels, sRGB
        const auto* emissive_texture = gltf_material.emissive_texture.texture;
        if (emissive_texture)
        {
            SetImageFormats(gltf, *emissive_texture, TextureFormat::R8G8B8A8_sRGB);
        }

        // Sampled from R channel, linear
        const auto* occlusion_texture = gltf_material.occlusion_texture.texture;
        if (occlusion_texture)
        {
            SetImageFormats(gltf, *occlusion_texture, TextureFormat::R8_UNorm);
        }

        // Sampled from RGB channel, linear
        const auto* normal_texture = gltf_material.normal_texture.texture;
        if (normal_texture)
        {
            SetImageFormats(gltf, *normal_texture, TextureFormat::R16G16B16A16_UNorm);
        }

        if (gltf_material.has_pbr_metallic_roughness)
        {
            // Sampled from RGB channels, sRGB
            const auto* albedo_texture = gltf_material.pbr_metallic_roughness.base_color_texture.texture;
            if (albedo_texture)
            {
                SetImageFormats(gltf, *albedo_texture, TextureFormat::R8G8B8A8_sRGB);
            }

            // Sampled from GB channels, linear
            const auto* rm_texture = gltf_material.pbr_metallic_roughness.metallic_roughness_texture.texture;
            if (rm_texture)
            {
                SetImageFormats(gltf, *rm_texture, TextureFormat::R8G8B8A8_UNorm);
            }
        }

        if (gltf_material.has_specular)
        {
            // Sampled from RGB channels, sRGB
            const auto* specular_color_texture = gltf_material.specular.specular_color_texture.texture;
            if (specular_color_texture)
            {
                SetImageFormats(gltf, *specular_color_texture, TextureFormat::R8G8B8A8_sRGB);
            }

            // Sampled from A channel, linear
            const auto* specular_texture = gltf_material.specular.specular_texture.texture;
            if (specular_texture)
            {
                SetImageFormats(gltf, *specular_texture, TextureFormat::R8G8B8A8_UNorm);
            }
        }

        if (gltf_material.has_transmission)
        {
            // Sampled from R channel, linear
            const auto* transmission_texture = gltf_material.transmission.transmission_texture.texture;
            if (transmission_texture)
            {
                SetImageFormats(gltf, *transmission_texture, TextureFormat::R8_UNorm);
            }
        }

        if (gltf_material.has_volume)
        {
            // Sampled from G channel, linear
            const auto* thickness_texture = gltf_material.volume.thickness_texture.texture;
            if (thickness_texture)
            {
                SetImageFormats(gltf, *thickness_texture, TextureFormat::R8G8_UNorm);
            }
        }

        if (gltf_material.has_sheen)
        {
            // Sampled from RGB channels, sRGB
            const auto* sheen_color_texture = gltf_material.sheen.sheen_color_texture.texture;
            if (sheen_color_texture)
            {
                SetImageFormats(gltf, *sheen_color_texture, TextureFormat::R8G8B8A8_sRGB);
            }

            // Sampled from A channel, linear
            const auto* sheen_roughness_texture = gltf_material.sheen.sheen_roughness_texture.texture;
            if (sheen_roughness_texture)
            {
                SetImageFormats(gltf, *sheen_roughness_texture, TextureFormat::R8G8B8A8_UNorm);
            }
        }

        if (gltf_material.has_iridescence)
        {
            // Sampled from R channel, linear
            const auto* iridescence_texture = gltf_material.iridescence.iridescence_texture.texture;
            if (iridescence_texture)
            {
                SetImageFormats(gltf, *iridescence_texture, TextureFormat::R8_UNorm);
            }

            // Sampled from G channel, linear
            const auto* iridescence_thickness_texture = gltf_material.iridescence.iridescence_thickness_texture.texture;
            if (iridescence_thickness_texture)
            {
                SetImageFormats(gltf, *iridescence_thickness_texture, TextureFormat::R8G8_UNorm);
            }
        }

        if (gltf_material.has_clearcoat)
        {
            // Sampled from R channel, linear
            const auto* clearcoat_texture = gltf_material.clearcoat.clearcoat_texture.texture;
            if (clearcoat_texture)
            {
                SetImageFormats(gltf, *clearcoat_texture, TextureFormat::R8_UNorm);
            }

            // Sampled from RGB channel, linear
            const auto* clearcoat_normal_texture = gltf_material.clearcoat.clearcoat_normal_texture.texture;
            if (clearcoat_normal_texture)
            {
                SetImageFormats(gltf, *clearcoat_normal_texture, TextureFormat::R16G16B16A16_UNorm);
            }

            // Sampled from G channel, linear
            const auto* clearcoat_roughness_texture = gltf_material.clearcoat.clearcoat_roughness_texture.texture;
            if (clearcoat_roughness_texture)
            {
                SetImageFormats(gltf, *clearcoat_roughness_texture, TextureFormat::R8G8_UNorm);
            }
        }
    }

    void CGltfTextureImporter::SetImageFormats(CGltfData& data, const cgltf_texture& texture, TextureFormat format)
    {
        if (!texture.image)
        {
            LogFormatError("image is null", texture);
            return;
        }

        // Make sure existing format is compatible
        const auto format_it = data.image_formats.find(texture.image);
        if (format_it != data.image_formats.end())
        {
            const auto old_format          = format_it->second;
            const auto old_format_info     = RHI::FormatInfo(old_format);
            const auto new_format_info     = RHI::FormatInfo(format);
            const auto old_component_count = old_format_info.GetComponentCount();
            const auto new_component_count = new_format_info.GetComponentCount();
            const auto max_component_count = Math::Max(old_component_count, new_component_count);

            for (UInt32 i = 0; i < max_component_count; ++i)
            {
                if (old_format_info.GetComponent(i) != new_format_info.GetComponent(i))
                {
                    LogFormatError(
                        StringFunc::Format("conflicting format requirements ({} and {})", old_format, format),
                        texture);
                    return;
                }
            }

            // The old format already encompasses this one
            if (new_component_count <= old_component_count)
            {
                return;
            }
        }

        data.image_formats[texture.image] = format;
    }

    StringView CGltfTextureImporter::GetTextureName(const cgltf_texture& gltf_texture)
    {
        if (gltf_texture.name)
        {
            return gltf_texture.name;
        }

        if (gltf_texture.image->name)
        {
            return gltf_texture.image->name;
        }

        if (gltf_texture.image->uri)
        {
            return gltf_texture.image->uri;
        }

        return "<Texture>";
    }

    StringView CGltfTextureImporter::GetImageName(const cgltf_image* gltf_image)
    {
        if (gltf_image == nullptr)
        {
            return "<null>";
        }

        if (gltf_image->name)
        {
            return gltf_image->name;
        }

        if (gltf_image->uri)
        {
            return gltf_image->uri;
        }

        return "<Image>";
    }

    void CGltfTextureImporter::LogFormatError(const String& message, const cgltf_texture& gltf_texture)
    {
        const auto texture_name      = GetTextureName(gltf_texture);
        const auto image_name        = GetImageName(gltf_texture.image);
        const auto formatted_message = StringFunc::Format(
            "Failed to deduce format for texture (name = '{}', image = '{}'): {}.",
            texture_name,
            image_name,
            message);
        m_context->AddErrorMessage(formatted_message);
    }

    void CGltfTextureImporter::LogTextureError(const String& message, const cgltf_texture& gltf_texture)
    {
        const auto texture_name      = GetTextureName(gltf_texture);
        const auto image_name        = GetImageName(gltf_texture.image);
        const auto formatted_message = StringFunc::Format(
            "Failed to import texture (name = '{}', image = '{}'): {}.",
            texture_name,
            image_name,
            message);
        m_context->AddErrorMessage(formatted_message);
    }

}
