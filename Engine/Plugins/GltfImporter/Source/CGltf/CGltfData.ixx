module;

#include <cgltf.h>

#include <type_traits>

export module Valo.GltfImporter:CGltfData;

import Valo.Logger;
import Valo.Renderer;
import Valo.RHI;
import Valo.Std;

export namespace Valo
{
    struct CGltfData final
    {
        CGltfData() = default;

        CGltfData(const CGltfData& other)            = delete;
        CGltfData& operator=(const CGltfData& other) = delete;

        CGltfData(CGltfData&& other)            = delete;
        CGltfData& operator=(CGltfData&& other) = delete;

        ~CGltfData();

        static size_t GetComponentSize(cgltf_component_type component_type);

        bool LoadAccessorData(const cgltf_accessor& accessor, BinaryBlob& out_data) const;

        template <typename T>
        bool LoadAccessorFloatData(const cgltf_accessor& accessor, Vector<T>& out_data) const;

        cgltf_data*                                        data{nullptr};
        UnorderedMap<const cgltf_image*, RHI::ImageHandle> images{};
        UnorderedMap<const cgltf_sampler*, SamplerHandle>  samplers{};
        UnorderedMap<const cgltf_texture*, TextureHandle>  textures{};
        UnorderedMap<const cgltf_image*, TextureFormat>    image_formats{};
    };

    template <typename T>
    bool CGltfData::LoadAccessorFloatData(const cgltf_accessor& accessor, Vector<T>& out_data) const
    {
        static_assert(sizeof(T) % sizeof(cgltf_float) == 0, "Type can't be unpacked from floats.");

        constexpr auto expected_component_count = sizeof(T) / sizeof(cgltf_float);
        const auto     read_component_count     = cgltf_num_components(accessor.type);
        const auto     expected_float_count     = accessor.count * read_component_count;

        out_data.clear();

        if (read_component_count != expected_component_count)
        {
            Log::Error(
                "Failed to parse accessor (name={}): incorrect number of components (expected {}, got {}).",
                accessor.name ? accessor.name : "???",
                expected_component_count,
                read_component_count);
            return false;
        }

        size_t read_float_count = 0;
        if constexpr (std::is_same<T, cgltf_float>::value)
        {
            out_data.resize(expected_float_count);
            read_float_count = cgltf_accessor_unpack_floats(&accessor, out_data.data(), out_data.size());
        }
        else
        {
            Vector<cgltf_float> floats(expected_float_count);
            read_float_count = cgltf_accessor_unpack_floats(&accessor, floats.data(), floats.size());

            out_data.resize(accessor.count);
            Memory::Memcpy(out_data.data(), floats.data(), expected_float_count * sizeof(cgltf_float));
        }

        if (read_float_count != expected_float_count)
        {
            Log::Error(
                "Failed to parse accessor (name={}): incorrect number of floats (expected {}, got {}).",
                accessor.name ? accessor.name : "???",
                expected_float_count,
                read_float_count);
            return false;
        }

        return true;
    }
}
