module;

#include <Valo/Macros.hpp>

#define CGLTF_IMPLEMENTATION
#include <cgltf.h>

module Valo.GltfImporter;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Renderer;
import Valo.Std;

import :CGltfData;
import :CGltfImporter;

namespace Valo
{
    CGltfImporter::CGltfImporter(
        MaterialStoreHandle MaterialStore,
        MeshStoreHandle     MeshStore,
        TextureStoreHandle  TextureStore) :
        m_MaterialImporter(MaterialStore),
        m_MeshImporter(MeshStore),
        m_TextureImporter(TextureStore)
    {
        ConfigureOptions();
    }

    void CGltfImporter::Import(AssetImportContext& Context)
    {
        auto Gltf = CGltfData();

        const auto ParseResult = cgltf_parse_file(&m_Options, Context.GetAssetPath().GetCString(), &Gltf.data);
        if (ParseResult != cgltf_result_success)
        {
            const auto ErrorInfo = CGltfResultToString(ParseResult);
            Context.AddErrorMessage(StringFunc::Format("Failed to parse glTF: {}.", ErrorInfo));
            return;
        }

        const auto LoadBuffersResult = cgltf_load_buffers(&m_Options, Gltf.data, Context.GetAssetPath().GetCString());
        if (LoadBuffersResult != cgltf_result_success)
        {
            const auto ErrorInfo = CGltfResultToString(LoadBuffersResult);
            Context.AddErrorMessage(StringFunc::Format("Failed to load glTF buffers: {}.", ErrorInfo));
            return;
        }

        const auto ValidateResult = cgltf_validate(Gltf.data);
        if (ValidateResult != cgltf_result_success)
        {
            const auto ErrorInfo = CGltfResultToString(ValidateResult);
            Context.AddErrorMessage(StringFunc::Format("Failed to validate glTF: {}.", ErrorInfo));
            return;
        }

        m_TextureImporter.Import(Context, Gltf);
        m_MaterialImporter.Import(Context, Gltf);
        m_MeshImporter.Import(Context, Gltf);
    }

    String CGltfImporter::CGltfResultToString(cgltf_result Result)
    {
        switch (Result)
        {
        case cgltf_result_success:
            return "";
        case cgltf_result_invalid_gltf:
            return "invalid gltf";
        case cgltf_result_invalid_json:
            return "invalid json";
        case cgltf_result_data_too_short:
            return "data too short";
        case cgltf_result_unknown_format:
            return "unknown format";
        case cgltf_result_legacy_gltf:
            return "legacy gltf";
        case cgltf_result_io_error:
            return "io error";
        case cgltf_result_file_not_found:
            return "file not found";
        case cgltf_result_out_of_memory:
            return "out of memory";
        case cgltf_result_invalid_options:
            return "invalid options";
        default:
            VALO_ASSERT(false);
            return "unknown error";
        }
    }

    void CGltfImporter::ConfigureOptions()
    {
        // Auto-detect file type (.glb/.gltf)
        m_Options.type = cgltf_file_type_invalid;

        // Auto-detect token count
        m_Options.json_token_count = 0;

        // Use C++ new/delete for allocations
        m_Options.memory.alloc_func = [](void* User, cgltf_size Size)
        {
            VALO_UNUSED(User);
            return operator new(Size);
        };

        m_Options.memory.free_func = [](void* User, void* Ptr)
        {
            VALO_UNUSED(User);
            operator delete(Ptr);
        };

        // Use Valo file API for reading files
        m_Options.file.read = [](const struct cgltf_memory_options* MemoryOptions,
                                 const struct cgltf_file_options*   FileOptions,
                                 const char*                        Filepath,
                                 cgltf_size*                        Size,
                                 void**                             Data)
        {
            VALO_UNUSED(FileOptions);

            // Try to open the file
            auto File = Valo::File(Filepath, FileMode::Binary, FileAccess::Read);
            if (!File.IsOpen())
            {
                return cgltf_result_file_not_found;
            }

            // Reserve data for the read contents
            const auto  FileSize = (Size != nullptr && *Size != 0) ? *Size : File.GetSize();
            auto* const FileData = MemoryOptions->alloc_func(MemoryOptions->user_data, FileSize);
            if (FileData == nullptr)
            {
                return cgltf_result_out_of_memory;
            }

            // Read data from file
            const auto ReadSize = File.Read(static_cast<Byte*>(FileData), FileSize);
            if (ReadSize != FileSize)
            {
                MemoryOptions->free_func(MemoryOptions->user_data, FileData);
                return cgltf_result_io_error;
            }

            // Set return values
            if (Size != nullptr)
            {
                *Size = FileSize;
            }

            if (Data != nullptr && FileData != nullptr)
            {
                *Data = FileData;
            }

            return cgltf_result_success;
        };

        m_Options.file.release = [](const struct cgltf_memory_options* MemoryOptions,
                                    const struct cgltf_file_options*   FileOptions,
                                    void*                              Data)
        {
            VALO_UNUSED(FileOptions);
            MemoryOptions->free_func(MemoryOptions->user_data, Data);
        };
    }
}
