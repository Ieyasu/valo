module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

module Valo.GltfImporter;

import Valo.AssetDatabase;
import Valo.Logger;
import Valo.Math;
import Valo.Renderer;
import Valo.Std;

import :CGltfData;
import :CGltfMaterialImporter;

namespace Valo
{
    CGltfMaterialImporter::CGltfMaterialImporter(MaterialStoreHandle material_store) :
        m_material_store(material_store)
    {
    }

    void CGltfMaterialImporter::Import(AssetImportContext& context, const CGltfData& gltf)
    {
        m_context = &context;

        for (size_t i = 0; i < gltf.data->materials_count; ++i)
        {
            ImportMaterial(gltf, gltf.data->materials[i]);
        }
    }

    void CGltfMaterialImporter::ImportMaterial(const CGltfData& gltf, const cgltf_material& cgltf_material)
    {
        (void)gltf;

        // Create material
        const auto material_name = GetMaterialName(cgltf_material);
        m_material               = m_material_store->CreateMaterial(material_name);

        if (cgltf_material.name != nullptr)
        {
            m_material->SetName(cgltf_material.name);
        }

        m_context->AddAsset(m_material);

        // Configure properties

        if (cgltf_material.alpha_mode == cgltf_alpha_mode_mask)
        {
            m_material->SetFloat("_materials.alpha_cutoff", cgltf_material.alpha_cutoff);
        }

        if (cgltf_material.occlusion_texture.texture)
        {
            const auto occlusion_scale = cgltf_material.occlusion_texture.scale;
            m_material->SetFloat("_material_params.occlusion_scale", occlusion_scale);
        }

        // Occlusion texture is batched with metalness-roughness.
        // If we don't set the metalness-roughness textures, we need to set the occlusion.
        if (cgltf_material.occlusion_texture.texture && !cgltf_material.has_pbr_metallic_roughness)
        {
            const auto* occlusion_texture = cgltf_material.occlusion_texture.texture;
            SetTexture(MaterialTexture::ORM, gltf, occlusion_texture);
        }

        if (cgltf_material.normal_texture.texture)
        {
            const auto* normal_texture = cgltf_material.normal_texture.texture;
            SetTexture(MaterialTexture::Normal, gltf, normal_texture);

            const auto normal_scale = cgltf_material.normal_texture.scale;
            m_material->SetFloat("_material_params.normal_scale", normal_scale);
        }

        if (cgltf_material.emissive_texture.texture)
        {
            const auto* emissive_texture = cgltf_material.emissive_texture.texture;
            SetTexture(MaterialTexture::Emissive, gltf, emissive_texture);

            // Get HDR emissive scale
            auto scale = 1.0f;
            if (cgltf_material.has_emissive_strength)
            {
                scale *= cgltf_material.emissive_strength.emissive_strength;
            }

            // Get HDR emissive color
            const auto* gltf_emissive = cgltf_material.emissive_factor;
            const auto  emissive      = scale * Vec3{gltf_emissive[0], gltf_emissive[1], gltf_emissive[2]};
            m_material->SetVec3("_material_params.emissive", emissive);
        }

        if (cgltf_material.has_ior)
        {
            const auto ior = cgltf_material.ior.ior;
            m_material->SetFloat("_material_params.ior", ior);
        }

        if (cgltf_material.has_transmission)
        {
            const auto transmission = cgltf_material.transmission.transmission_factor;
            m_material->SetFloat("_material_params.transmission", transmission);
        }

        if (cgltf_material.has_transmission && cgltf_material.transmission.transmission_texture.texture)
        {
            const auto* transmission_texture = cgltf_material.transmission.transmission_texture.texture;
            SetTexture(MaterialTexture::Transmission, gltf, transmission_texture);
        }

        const auto& cgltf_metallic_roughness = cgltf_material.pbr_metallic_roughness;
        if (cgltf_material.has_pbr_metallic_roughness)
        {
            const auto* gltf_albedo = cgltf_metallic_roughness.base_color_factor;
            const auto  albedo      = Vec4(gltf_albedo[0], gltf_albedo[1], gltf_albedo[2], gltf_albedo[3]);
            m_material->SetVec4("_material_params.albedo", albedo);

            const auto roughness = cgltf_metallic_roughness.roughness_factor;
            m_material->SetFloat("_material_params.roughness", roughness);

            const auto metallic = cgltf_metallic_roughness.metallic_factor;
            m_material->SetFloat("_material_params.metallic", metallic);

            const auto* albedo_texture = cgltf_metallic_roughness.base_color_texture.texture;
            SetTexture(MaterialTexture::Albedo, gltf, albedo_texture);

            const auto* orm_texture = cgltf_metallic_roughness.metallic_roughness_texture.texture;
            SetTexture(MaterialTexture::ORM, gltf, orm_texture);
        }
    }

    String CGltfMaterialImporter::GetMaterialName(const cgltf_material& cgltf_material) const
    {
        switch (cgltf_material.alpha_mode)
        {
        case cgltf_alpha_mode_opaque:
        case cgltf_alpha_mode_mask:
            if (cgltf_material.unlit)
            {
                return "valo/unlit";
            }
            if (cgltf_material.has_transmission)
            {
                return "Valo/LitTranslucent";
            }
            return "Valo/Lit";
        case cgltf_alpha_mode_blend:
            if (cgltf_material.unlit)
            {
                return "Valo/UnlitTransparent";
            }
            return "Valo/LitTransparent";
        default:
            Log::Fatal("Unknown cgltf_alpha_mode: {}", cgltf_material.alpha_mode);
            return "";
        }
    }

    void CGltfMaterialImporter::SetTexture(
        MaterialTexture      type,
        const CGltfData&     gltf,
        const cgltf_texture* cgltf_texture)
    {
        // Set the texture if available, otherwise use a default texture
        const auto& texture_it = gltf.textures.find(cgltf_texture);
        if (texture_it != gltf.textures.end())
        {
            VALO_ASSERT(cgltf_texture != nullptr);
            m_material->SetTexture(type, texture_it->second);
        }
        else
        {
            const auto default_texture = m_material_store->GetDefaultTexture(type);
            m_material->SetTexture(type, default_texture);
        }
    }

    void CGltfMaterialImporter::LogMaterialError(const String& message)
    {
        const auto formatted_message = StringFunc::Format(
            "Failed to import material (name={}): {}.",
            m_material->GetName(),
            message);
        m_context->AddErrorMessage(formatted_message, *m_material);
    }
}
