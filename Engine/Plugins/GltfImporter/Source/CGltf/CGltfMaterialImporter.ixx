module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

export module Valo.GltfImporter:CGltfMaterialImporter;

import Valo.AssetDatabase;
import Valo.Renderer;
import Valo.Std;

import :CGltfData;

export namespace Valo
{
    class CGltfMaterialImporter final
    {
    public:
        explicit CGltfMaterialImporter(MaterialStoreHandle material_store);

        void Import(AssetImportContext& context, const CGltfData& gltf);

    private:
        void ImportMaterial(const CGltfData& gltf, const cgltf_material& cgltf_material);

        String GetMaterialName(const cgltf_material& cgltf_material) const;

        void SetTexture(MaterialTexture type, const CGltfData& gltf, const cgltf_texture* cgltf_texture);

        void LogMaterialError(const String& message);

        ObserverPtr<AssetImportContext> m_context{nullptr};
        MaterialStoreHandle             m_material_store{};
        MaterialHandle                  m_material{};
    };
}
