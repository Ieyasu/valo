module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

export module Valo.GltfImporter:CGltfImporter;

import Valo.AssetDatabase;
import Valo.Renderer;
import Valo.Std;

import :CGltfMaterialImporter;
import :CGltfMeshImporter;
import :CGltfTextureImporter;

export namespace Valo
{
    class CGltfImporter final
    {
    public:
        CGltfImporter(MaterialStoreHandle MaterialStore, MeshStoreHandle MeshStore, TextureStoreHandle TextureStore);

        void Import(AssetImportContext& Context);

    private:
        static String CGltfResultToString(cgltf_result result);

        void ConfigureOptions();

        AssetDatabaseHandle m_AssetDatabase{};

        CGltfMaterialImporter m_MaterialImporter;
        CGltfMeshImporter     m_MeshImporter;
        CGltfTextureImporter  m_TextureImporter;
        cgltf_options         m_Options{};
    };
}
