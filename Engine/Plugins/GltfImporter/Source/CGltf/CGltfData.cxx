module;

#include <Valo/Macros.hpp>

#include <cgltf.h>

module Valo.GltfImporter;

import Valo.Logger;
import Valo.Std;

import :CGltfData;

namespace Valo
{
    CGltfData::~CGltfData()
    {
        if (data != nullptr)
        {
            cgltf_free(data);
        }
    }

    bool CGltfData::LoadAccessorData(const cgltf_accessor& accessor, BinaryBlob& out_data) const  // NOLINT
    {
        out_data.clear();

        if (accessor.component_type == cgltf_component_type_invalid)
        {
            Log::Error(
                "Failed to parse accessor (name={}): component type is invalid.",
                accessor.name ? accessor.name : "???");
            return false;
        }

        // Calculate the size of the total data
        const auto component_count = cgltf_num_components(accessor.type);
        const auto component_size  = GetComponentSize(accessor.component_type);
        const auto element_size    = component_count * component_size;
        const auto total_size      = element_size * accessor.count;

        // According to spec, null buffer view should be populated with default values
        if (accessor.buffer_view == nullptr)
        {
            out_data.resize(total_size);
            return true;
        }

        if (accessor.buffer_view->buffer == nullptr)
        {
            Log::Error(
                "Failed to parse accessor (name={}): buffer view has no valid buffer.",
                accessor.name ? accessor.name : "???");
            return false;
        }

        if (accessor.buffer_view->buffer->data == nullptr)
        {
            Log::Error(
                "Failed to parse accessor (name={}): buffer has no valid data.",
                accessor.name ? accessor.name : "???");
            return false;
        }

        // Reserve space for the data
        out_data.resize(total_size);

        // If stride matches with the element size, we can just copy entire data chunk
        const auto  offset        = accessor.buffer_view->offset + accessor.offset;
        const auto* accessor_data = static_cast<UInt8*>(accessor.buffer_view->buffer->data) + offset;
        if (accessor.stride == 0 || accessor.stride == element_size)
        {
            Memory::Memcpy(out_data.data(), accessor_data, total_size);
            return true;
        }

        // If stride does not match, we need to copy elements one by one
        for (size_t i = 0; i < accessor.count; ++i)
        {
            Memory::Memcpy(out_data.data() + i, accessor_data, element_size);
        }

        return true;
    }

    size_t CGltfData::GetComponentSize(cgltf_component_type component_type)
    {
        switch (component_type)
        {
        case cgltf_component_type_r_8:
        case cgltf_component_type_r_8u:
            return 1;
        case cgltf_component_type_r_16:
        case cgltf_component_type_r_16u:
            return 2;
        case cgltf_component_type_r_32u:
        case cgltf_component_type_r_32f:
            return 4;
        case cgltf_component_type_invalid:
            return 0;
        default:
            VALO_ASSERT(false);
            return 0;
        }
    }
}
