export module Valo.GltfImporter:GltfImporterModule;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Std;

import :CGltfImporter;

export namespace Valo
{
    class GltfImporterModule final : public AssetImporterModule
    {
    public:
        bool CanImport(const Path& AssetPath) const override;

        void Import(AssetImportContext& Context) override;

    private:
        bool Initialize(Engine& Engine) override;

        void Deinitialize() override;

        AssetDatabaseHandle      m_AssetDatabase{};
        UniquePtr<CGltfImporter> m_Importer;
    };
}
