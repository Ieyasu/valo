export module Valo.ImageImporter:ImageLoader;

import Valo.Renderer;
import Valo.RHI;
import Valo.Std;

import :ImageData;

export namespace Valo
{
    class ImageLoader final
    {
    public:
        static bool LoadImageInfo(StringView filepath, ImageInfo& out_info);

        static bool LoadImageInfo(Span<const Byte> data, ImageInfo& out_info);

        static bool LoadImage(StringView filepath, TextureFormat format, ImageData& out_data);

        static bool LoadImage(Span<const Byte> data, TextureFormat format, ImageData& out_data);

        static bool LoadDDSImageInfo(StringView filepath, ImageInfo& out_info);

        static bool LoadDDSImageInfo(Span<const Byte> data, ImageInfo& out_info);

        static bool LoadDDSImage(StringView filepath, ImageData& out_data);

        static bool LoadDDSImage(Span<const Byte> data, ImageData& out_data);

    private:
        struct StbImageInfo final
        {
            size_t element_size{};
            Int32  component_count{};
            Int32  width{};
            Int32  height{};
            Int32  bin{};
        };

        static bool StbIsValidFormat(const RHI::FormatInfo& format_info);

        static bool StbLoadImageInfo(StringView filepath, ImageInfo& out_info);

        static bool StbLoadImageInfo(Span<const Byte> data, ImageInfo& out_info);

        static bool StbLoadImageUInt8(StringView filepath, size_t component_count, ImageData& out_data);

        static bool StbLoadImageUInt8(Span<const Byte> data, size_t component_count, ImageData& out_data);

        static bool StbLoadImageUInt16(StringView filepath, size_t component_count, ImageData& out_data);

        static bool StbLoadImageUInt16(Span<const Byte> data, size_t component_count, ImageData& out_data);

        static bool StbLoadImageFloat(StringView filepath, size_t component_count, ImageData& out_data);

        static bool StbLoadImageFloat(Span<const Byte> data, size_t component_count, ImageData& out_data);

        static void StbFillimageInfo(const StbImageInfo& info, ImageInfo& out_info);

        static void StbFillImageData(void* data, const StbImageInfo& info, ImageData& out_data);
    };
}
