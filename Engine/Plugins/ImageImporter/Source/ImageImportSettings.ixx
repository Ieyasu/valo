export module Valo.ImageImporter:ImageImportSettings;

import Valo.RHI;

namespace Valo
{
    struct ImageImportSettings final
    {
        RHI::Format image_format{RHI::Format::R8G8B8A8_sRGB};
    };
}
