module;

#define STBI_ONLY_JPEG
#define STBI_ONLY_PNG
#define STBI_ONLY_BMP
#define STBI_ONLY_PSD
#define STBI_ONLY_TGA
#define STBI_ONLY_HDR
#define STBI_FAILURE_USERMSG
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

module Valo.ImageImporter;

import Valo.Logger;
import Valo.Renderer;
import Valo.RHI;
import Valo.Std;

import :ImageData;
import :ImageLoader;

namespace Valo
{
    bool ImageLoader::LoadImageInfo(StringView filepath, ImageInfo& out_info)
    {
        return StbLoadImageInfo(filepath, out_info);
    }

    bool ImageLoader::LoadImageInfo(Span<const Byte> data, ImageInfo& out_info)
    {
        return StbLoadImageInfo(data, out_info);
    }

    bool ImageLoader::LoadImage(StringView filepath, TextureFormat format, ImageData& out_data)
    {
        const auto format_info = RHI::FormatInfo(format);
        if (!StbIsValidFormat(format_info))
        {
            Log::Error("Failed to load image from file '{}': format {} not supported", filepath, format);
            return false;
        }

        const auto component       = format_info.GetComponent(0);
        const auto component_count = format_info.GetComponentCount();
        const auto packing         = component.packing;
        switch (packing)
        {
        case RHI::ComponentPacking::Float:
            if (!StbLoadImageFloat(filepath, component_count, out_data))
            {
                return false;
            }
            break;
        case RHI::ComponentPacking::UNorm:
        case RHI::ComponentPacking::UInt:
        case RHI::ComponentPacking::SNorm:
        case RHI::ComponentPacking::SInt:
        case RHI::ComponentPacking::sRGB:
            if (component.size == 8 && !StbLoadImageUInt8(filepath, component_count, out_data))
            {
                return false;
            }
            if (component.size == 16 && !StbLoadImageUInt16(filepath, component_count, out_data))
            {
                return false;
            }
            break;
        default:
            Log::Error("Failed to load image from file '{}': component packing {} not supported", filepath, packing);
            return false;
        }

        return true;
    }

    bool ImageLoader::LoadImage(Span<const Byte> data, TextureFormat format, ImageData& out_data)
    {
        const auto format_info = RHI::FormatInfo(format);
        if (!StbIsValidFormat(format_info))
        {
            Log::Error("Failed to load image from memory: format {} not supported", format);
            return false;
        }

        const auto component       = format_info.GetComponent(0);
        const auto component_count = format_info.GetComponentCount();
        const auto packing         = component.packing;
        switch (packing)
        {
        case RHI::ComponentPacking::Float:
            if (!StbLoadImageFloat(data, component_count, out_data))
            {
                return false;
            }
            break;
        case RHI::ComponentPacking::UNorm:
        case RHI::ComponentPacking::UInt:
        case RHI::ComponentPacking::SNorm:
        case RHI::ComponentPacking::SInt:
        case RHI::ComponentPacking::sRGB:
            if (component.size == 8 && !StbLoadImageUInt8(data, component_count, out_data))
            {
                return false;
            }
            if (component.size == 16 && !StbLoadImageUInt16(data, component_count, out_data))
            {
                return false;
            }
            break;
        default:
            Log::Fatal("Failed to load image from memory: component packing {} not supported", packing);
            return false;
        }

        return true;
    }

    bool ImageLoader::LoadDDSImageInfo(StringView filepath, ImageInfo& out_info)
    {
        Log::Error("Failed to load DDS image info from file '{}': not implemented.", filepath);
        (void)out_info;
        return false;
    }

    bool ImageLoader::LoadDDSImageInfo(Span<const Byte> data, ImageInfo& out_info)
    {
        Log::Error("Failed to load DDS image info from memory: not implemented.");
        (void)data;
        (void)out_info;
        return false;
    }

    bool ImageLoader::LoadDDSImage(StringView filepath, ImageData& out_data)
    {
        Log::Error("Failed to load DDS image from file '{}': not implemented.", filepath);
        (void)out_data;
        return false;
    }

    bool ImageLoader::LoadDDSImage(Span<const Byte> data, ImageData& out_data)
    {
        Log::Error("Failed to load DDS image from memory: not implemented.");
        (void)data;
        (void)out_data;
        return false;
    }

    bool ImageLoader::StbIsValidFormat(const RHI::FormatInfo& format_info)
    {
        if (!format_info.IsUniform())
        {
            Log::Error("Failed to load image: all components of the format must have the same type.");
            return false;
        }

        const auto component = format_info.GetComponent(0);
        if (component.packing == RHI::ComponentPacking::Float && component.size != 32)
        {
            Log::Error("Failed to load image: floats other than 32-bit are not supported.");
            return false;
        }

        if (component.packing != RHI::ComponentPacking::Float && component.size != 8 && component.size != 16)
        {
            Log::Error("Failed to load image: uints other than 8-bit or 16-bit are not supported.");
            return false;
        }

        return true;
    }

    bool ImageLoader::StbLoadImageInfo(StringView filepath, ImageInfo& out_info)
    {
        auto info    = StbImageInfo{};
        auto success = stbi_info(filepath.data(), &info.width, &info.height, &info.bin);
        StbFillimageInfo(info, out_info);
        return success;
    }

    bool ImageLoader::StbLoadImageInfo(Span<const Byte> data, ImageInfo& out_info)
    {
        auto info    = StbImageInfo{};
        auto success = stbi_info_from_memory(
            data.data(),
            static_cast<int>(data.size()),
            &info.width,
            &info.height,
            &info.bin);
        StbFillimageInfo(info, out_info);
        return success;
    }

    bool ImageLoader::StbLoadImageUInt8(StringView filepath, size_t component_count, ImageData& out_data)
    {
        // Load using STB
        auto info = StbImageInfo{
            .element_size    = sizeof(stbi_uc),
            .component_count = static_cast<int>(component_count),
        };

        auto* data_ptr = stbi_load(filepath.data(), &info.width, &info.height, &info.bin, info.component_count);
        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load 8-bit image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    bool ImageLoader::StbLoadImageUInt8(Span<const Byte> data, size_t component_count, ImageData& out_data)
    {
        // Load using STB
        auto info = StbImageInfo{
            .element_size    = sizeof(stbi_uc),
            .component_count = static_cast<int>(component_count),
        };

        auto* data_ptr = stbi_load_from_memory(
            data.data(),
            static_cast<int>(data.size()),
            &info.width,
            &info.height,
            &info.bin,
            info.component_count);

        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load 8-bit image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    bool ImageLoader::StbLoadImageUInt16(StringView filepath, size_t component_count, ImageData& out_data)
    {
        auto info = StbImageInfo{
            .element_size    = sizeof(stbi_us),
            .component_count = static_cast<int>(component_count),
        };

        auto* data_ptr = stbi_load_16(filepath.data(), &info.width, &info.height, &info.bin, info.component_count);
        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load 16-bit image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    bool ImageLoader::StbLoadImageUInt16(Span<const Byte> data, size_t component_count, ImageData& out_data)
    {
        auto info = StbImageInfo{
            .element_size    = sizeof(stbi_us),
            .component_count = static_cast<int>(component_count),
        };

        auto* data_ptr = stbi_load_16_from_memory(
            data.data(),
            static_cast<int>(data.size()),
            &info.width,
            &info.height,
            &info.bin,
            info.component_count);

        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load 16-bit image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    bool ImageLoader::StbLoadImageFloat(StringView filepath, size_t component_count, ImageData& out_data)
    {
        auto info = StbImageInfo{
            .element_size    = sizeof(float),
            .component_count = static_cast<int>(component_count),
        };
        auto* data_ptr = stbi_loadf(filepath.data(), &info.width, &info.height, &info.bin, info.component_count);

        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load float image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    bool ImageLoader::StbLoadImageFloat(Span<const Byte> data, size_t component_count, ImageData& out_data)
    {
        auto info = StbImageInfo{
            .element_size    = sizeof(float),
            .component_count = static_cast<int>(component_count),
        };

        auto* data_ptr = stbi_loadf_from_memory(
            data.data(),
            static_cast<int>(data.size()),
            &info.width,
            &info.height,
            &info.bin,
            info.component_count);

        if (data_ptr == nullptr)
        {
            Log::Error("Failed to load float image using stb: {}", stbi_failure_reason());
            return false;
        }

        StbFillImageData(data_ptr, info, out_data);
        return true;
    }

    void ImageLoader::StbFillimageInfo(const StbImageInfo& info, ImageInfo& out_info)
    {
        out_info.width             = static_cast<UInt32>(info.width);
        out_info.height            = static_cast<UInt32>(info.height);
        out_info.depth             = 1;
        out_info.mip_level_count   = 1;
        out_info.array_layer_count = 1;
    }

    void ImageLoader::StbFillImageData(void* data, const StbImageInfo& info, ImageData& out_data)
    {
        StbFillimageInfo(info, out_data.info);

        const auto pixel_count = out_data.info.width * out_data.info.height;
        const auto size        = pixel_count * static_cast<size_t>(info.component_count) * info.element_size;

        // Only one mip level is loaded from regular files
        out_data.data.resize(1);
        out_data.data[0].resize(size);
        Memory::Memcpy(out_data.data[0].data(), data, size);

        // Free STB memory
        stbi_image_free(data);
    }
}
