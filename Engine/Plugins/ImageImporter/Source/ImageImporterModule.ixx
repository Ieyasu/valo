export module Valo.ImageImporter:ImageImporterModule;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Std;

export namespace Valo
{
    class StbImporter;

    class ImageImporterModule final : public AssetImporterModule
    {
    public:
        bool CanImport(const Path& AssetPath) const override;

        void Import(AssetImportContext& Context) override;

    private:
        bool Initialize(Engine& Engine) override;

        void Deinitialize() override;

        AssetDatabaseHandle      m_AssetDatabase{};
        ObserverPtr<StbImporter> m_Importer{};
    };
}
