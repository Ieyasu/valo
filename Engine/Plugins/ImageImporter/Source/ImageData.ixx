export module Valo.ImageImporter:ImageData;

import Valo.Std;

export namespace Valo
{
    struct ImageInfo final
    {
        UInt32 width{};
        UInt32 height{};
        UInt32 depth{};
        UInt32 mip_level_count{};
        UInt32 array_layer_count{};
    };

    struct ImageData final
    {
        ImageInfo          info{};
        Vector<BinaryBlob> data{};
    };
}
