module;

#include <algorithm>
#include <ranges>
#include <vector>

module Valo.ImageImporter;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Std;

import :ImageImporterModule;

namespace Valo
{
    bool ImageImporterModule::Initialize(Engine& Engine)
    {
        m_AssetDatabase = Engine.Require<AssetDatabase>();
        m_AssetDatabase->RegisterImporter(*this);

        return true;
    }

    void ImageImporterModule::Deinitialize()
    {
        m_AssetDatabase->DeregisterImporter(*this);
    }

    bool ImageImporterModule::CanImport(const Path& AssetPath) const
    {
        static SmallVector<String> SupportedFormats = {".jpeg", ".jpg", ".png", ".bmp", ".psd", ".tga", ".hdr"};
        return std::ranges::find(SupportedFormats, String(AssetPath.GetExtension())) != SupportedFormats.end();
    }

    void ImageImporterModule::Import(AssetImportContext& Context)
    {
        (void)Context;
    }
}
