export module Valo.ImageImporter:MeshAttributeNames;

import Valo.Std;

export namespace Valo
{
    inline const String c_attribute_name_unknown    = "???";
    inline const String c_attribute_name_invalid    = "invalid";
    inline const String c_attribute_name_position   = "position";
    inline const String c_attribute_name_color_rgb  = "color (RGB)";
    inline const String c_attribute_name_color_rgba = "color (RGBA)";
    inline const String c_attribute_name_normal     = "normal";
    inline const String c_attribute_name_tangent    = "tangent";
    inline const String c_attribute_name_uv         = "uv";
    inline const String c_attribute_name_weights    = "weights";
    inline const String c_attribute_name_joints     = "joints";
}
