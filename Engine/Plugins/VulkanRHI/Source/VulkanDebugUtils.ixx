module;

#include <Valo/Vulkan.hpp>

#include <sstream>

export module Valo.VulkanRHI:DebugUtils;

import Valo.Logger;
import Valo.Std;

export namespace Valo::RHI::VulkanDebugUtils
{
    vk::DebugUtilsMessengerEXT CreateMessenger(vk::Instance instance);

    vk::DebugUtilsMessengerCreateInfoEXT GetMessengerDescriptor();

    VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
        VkDebugUtilsMessageTypeFlagsEXT             message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        void*                                       user_data);

    void ComposeSimpleDebugMessage(
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        std::ostringstream&                         stream);

    void ComposeDetailedDebugMessage(
        VkDebugUtilsMessageTypeFlagsEXT             message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        std::ostringstream&                         stream);
}
