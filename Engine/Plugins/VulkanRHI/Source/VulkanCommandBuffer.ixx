module;

#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:CommandBuffer;

import Valo.RHI;
import Valo.Std;

import :Image;
import :Queue;

export namespace Valo::RHI
{
    class VulkanDevice;
    class VulkanCommandPool;

    class VulkanCommandBuffer final : public CommandBuffer
    {
    public:
        static ResultValue<vk::Result, UniquePtr<VulkanCommandBuffer>> Create(
            const VulkanDevice&      device,
            const VulkanQueue&       queue,
            const VulkanCommandPool& pool);

        VulkanCommandBuffer(const VulkanDevice& device, const VulkanQueue& queue);
        VulkanCommandBuffer(VulkanCommandBuffer&& other)                 = delete;
        VulkanCommandBuffer(const VulkanCommandBuffer& other)            = delete;
        VulkanCommandBuffer& operator=(VulkanCommandBuffer&& other)      = delete;
        VulkanCommandBuffer& operator=(const VulkanCommandBuffer& other) = delete;

        ~VulkanCommandBuffer() override;

        StringView GetName() override;

        void SetName(String name) override;

        ResultCode Begin() override;

        ResultCode End() override;

        ResultCode Submit() override;

        ResultCode Reset() override;

        ResultCode WaitUntilCompleted() override;

        ResultCode OnReset();

        // Barrier commands
        void CmdPipelineBarrier(
            Span<const BufferMemoryBarrier> buffer_memory_barriers,
            Span<const ImageMemoryBarrier>  image_memory_barriers) override;

        // Blit commands
        void CmdCopyBuffer(
            BufferHandle   src_buffer,
            DeviceSizeType src_offset,
            BufferHandle   dst_buffer,
            DeviceSizeType dst_offset,
            DeviceSizeType size) override;

        void CmdCopyBufferToImage(
            BufferHandle   src_buffer,
            DeviceSizeType buffer_offset,
            ImageHandle    dst_image,
            UInt32         image_width,
            UInt32         image_height,
            UInt32         image_depth,
            UInt32         image_offset_x,
            UInt32         image_offset_y,
            UInt32         image_offset_z,
            UInt32         image_mip_level,
            UInt32         image_array_layer) override;

        // Compute commands

        void CmdBindComputePipeline(ComputePipelineHandle pipeline) override;

        void CmdDispatch(UInt32 group_count_x, UInt32 group_count_y, UInt32 group_count_z) override;

        // Render commands
        void CmdBeginRenderPass(
            RenderPassHandle       render_pass,
            FramebufferHandle      framebuffer,
            const Rect2D&          render_area,
            Span<const ClearValue> clear_values) override;

        void CmdEndRenderPass() override;

        void CmdNextSubpass() override;

        void CmdBindGraphicsPipeline(GraphicsPipelineHandle pipeline) override;

        void CmdBindIndexBuffer(BufferHandle buffer, UInt32 offset, IndexFormat format) override;

        void CmdBindVertexBuffers(
            Span<const BufferHandle>   buffers,
            Span<const DeviceSizeType> offsets,
            UInt32                     first_binding) override;

        void CmdBindDescriptorSets(
            PipelineLayoutHandle            pipeline_layout,
            Span<const DescriptorSetHandle> descriptor_sets,
            Span<const UInt32>              dynamic_offsets,
            UInt32                          first_set) override;

        void CmdSetScissor(const Scissor& scissor) override;

        void CmdSetViewport(const Viewport& viewport) override;

        void CmdPresent(ImageHandle image, UInt32 image_width, UInt32 image_height) override;

        void CmdDraw(UInt32 vertex_count, UInt32 instance_count, UInt32 first_vertex, UInt32 first_instance) override;

        void CmdDrawIndexed(
            UInt32 index_count,
            UInt32 instance_count,
            UInt32 first_index,
            UInt32 first_instance,
            Int32  vertex_offset) override;

    private:
        ResultValue<vk::Result, vk::Semaphore> GetOrCreateSemaphore();

        void SetFenceName(vk::Fence vk_fence);
        void SetSemaphoreName(vk::Semaphore vk_semaphore, size_t index);

        const VulkanDevice& m_device;
        const VulkanQueue&  m_queue;
        String              m_name{};

        vk::CommandBuffer     m_vk_command_buffer{};
        vk::Fence             m_vk_fence{};
        Vector<vk::Semaphore> m_available_semaphores{};
        Vector<vk::Semaphore> m_vk_semaphores{};

        // Present
        vk::Semaphore    m_aquire_image_semaphore{};
        vk::Semaphore    m_ready_for_present_semaphore{};
        Optional<UInt32> m_swapchain_image_index{};

        friend class VulkanCommandPool;
    };
}
