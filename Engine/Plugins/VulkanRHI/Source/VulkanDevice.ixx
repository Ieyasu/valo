module;

#include <Valo/Vulkan.hpp>

#include <vk_mem_alloc.h>

export module Valo.VulkanRHI:Device;

import Valo.RHI;
import Valo.Std;
import Valo.Window;

import :Buffer;
import :CommandBuffer;
import :CommandPool;
import :DescriptorPoolAllocator;
import :Image;
import :Queue;
import :Swapchain;

export namespace Valo::RHI
{
    class VulkanInstance;

    class VulkanDevice final : public Device
    {
    public:
        VulkanDevice(
            const VulkanInstance& instance,
            vk::Device            vk_device,
            vk::PhysicalDevice    vk_physical_device,
            VulkanQueue           compute_queue,
            VulkanQueue           graphics_queue,
            VulkanQueue           transfer_queue,
            VmaAllocator          allocator);

        VulkanDevice(const VulkanDevice& other)            = delete;
        VulkanDevice& operator=(const VulkanDevice& other) = delete;

        VulkanDevice(VulkanDevice&& other)            = delete;
        VulkanDevice& operator=(VulkanDevice&& other) = delete;

        ~VulkanDevice() override;

        vk::Device GetVkDevice() const;

        vk::PhysicalDevice GetVkPhysicalDevice() const;

        StringView GetName() override;

        void SetName(String name) override;

        DeviceCapabilities GetCapabilities() const override;

        DeviceLimits GetLimits() const override;

        CommandQueueHandle GetComputeQueue() const override;

        CommandQueueHandle GetGraphicsQueue() const override;

        CommandQueueHandle GetTransferQueue() const override;

        ResultCode WaitIdle() const override;

        void SetObjectName(BaseResourceHandle handle, StringView name) override;

        Result<void*> MapBuffer(BufferHandle buffer) const override;

        void UnmapBuffer(BufferHandle buffer) const override;

        Result<SwapchainInfo> AddWindow(WindowHandle window) override;

        void RemoveWindow(WindowHandle window) override;

        Result<BufferHandle> CreateBuffer(const BufferDescriptor& descriptor) override;

        void DestroyBuffer(BufferHandle buffer) override;

        Result<ImageHandle> CreateImage(const ImageDescriptor& descriptor) override;

        void DestroyImage(ImageHandle image) override;

        Result<SamplerHandle> CreateSampler(const SamplerDescriptor& descriptor) override;

        void DestroySampler(SamplerHandle sampler) override;

        Result<DescriptorSetHandle> CreateDescriptorSet(const DescriptorSetDescriptor& descriptor) override;

        void DestroyDescriptorSet(DescriptorSetHandle set) override;

        void UpdateDescriptorSets(Span<const DescriptorSetUpdate> updates) override;

        Result<DescriptorSetLayoutHandle> CreateDescriptorSetLayout(
            const DescriptorSetLayoutDescriptor& descriptor) override;

        void DestroyDescriptorSetLayout(DescriptorSetLayoutHandle layout) override;

        Result<FramebufferHandle> CreateFramebuffer(const FramebufferDescriptor& descriptor) override;

        void DestroyFramebuffer(FramebufferHandle framebuffer) override;

        Result<RenderPassHandle> CreateRenderPass(const RenderPassDescriptor& descriptor) override;

        void DestroyRenderPass(RenderPassHandle render_pass) override;

        Result<PipelineLayoutHandle> CreatePipelineLayout(const PipelineLayoutDescriptor& descriptor) override;

        void DestroyPipelineLayout(PipelineLayoutHandle layout) override;

        Result<ComputePipelineHandle> CreateComputePipeline(const ComputePipelineDescriptor& descriptor) override;

        void DestroyComputePipeline(ComputePipelineHandle pipeline) override;

        Result<GraphicsPipelineHandle> CreateGraphicsPipeline(const GraphicsPipelineDescriptor& descriptor) override;

        void DestroyGraphicsPipeline(GraphicsPipelineHandle pipeline) override;

        Result<CommandPoolHandle> CreateCommandPool(const CommandPoolDescriptor& descriptor) override;

        ResultCode ResetCommandPool(CommandPoolHandle pool) override;

        void DestroyCommandPool(CommandPoolHandle pool) override;

        Result<CommandBufferHandle> CreateCommandBuffer(const CommandBufferDescriptor& descriptor) override;

        template <typename T>
        void SetObjectName(T handle, StringView name) const;
        void SetObjectName(UInt64 handle, vk::ObjectType type, StringView name) const;

        const VulkanInstance& m_instance;
        vk::Device            m_vk_device;
        vk::PhysicalDevice    m_vk_physical_device;

        String             m_name{};
        DeviceCapabilities m_capabilities{};
        DeviceLimits       m_limits{};

        VulkanQueue m_compute_queue;
        VulkanQueue m_graphics_queue;
        VulkanQueue m_transfer_queue;

        VmaAllocator                             m_vma_allocator;
        UniquePtr<VulkanDescriptorPoolAllocator> m_descriptor_pool_allocator;

        // Resources that manage some other resources
        Vector<UniquePtr<VulkanCommandPool>> m_command_pools{};
        Vector<UniquePtr<VulkanSwapchain>>   m_swapchains{};

        // Resources that do not manage other resources
        Pool<VulkanImage>  m_images{};
        Pool<VulkanBuffer> m_buffers{};

        friend class VulkanCommandBuffer;
        friend class VulkanSwapchain;
    };

    template <typename T>
    inline void VulkanDevice::SetObjectName(T handle, StringView name) const
    {
        const auto handleValue = UInt64(static_cast<typename T::NativeType>(handle));
        SetObjectName(handleValue, handle.objectType, name);
    }
}
