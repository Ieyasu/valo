module;

#include <Valo/Profile.hpp>
#include <Valo/Vulkan.hpp>

module Valo.VulkanRHI;

import Valo.Profiler;
import Valo.Std;

import :Conversions;
import :Device;

namespace Valo::RHI
{
    VulkanCommandPool::VulkanCommandPool(const VulkanDevice& device, const VulkanQueue& queue, vk::CommandPool vk_pool) :
        m_device(device),
        m_queue(queue),
        m_vk_pool(vk_pool)
    {
    }

    VulkanCommandPool::~VulkanCommandPool()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        if (m_vk_pool)
        {
            m_device.GetVkDevice().destroyCommandPool(m_vk_pool);
        }
    }

    UniqueResult<vk::Result, VulkanCommandPool> VulkanCommandPool::Create(
        const VulkanDevice& device,
        const VulkanQueue&  queue)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto vk_command_pool_info             = vk::CommandPoolCreateInfo{};
        vk_command_pool_info.queueFamilyIndex = queue.family_index;
        vk_command_pool_info.flags            = vk::CommandPoolCreateFlagBits::eResetCommandBuffer;

        const auto [vk_result, vk_command_pool] = device.GetVkDevice().createCommandPool(vk_command_pool_info);
        if (vk_result != vk::Result::eSuccess)
        {
            return vk_result;
        }

        auto* native_command_pool = new VulkanCommandPool(device, queue, vk_command_pool);
        return UniquePtr<VulkanCommandPool>(native_command_pool);
    }

    vk::CommandPool VulkanCommandPool::GetVkPool() const
    {
        return m_vk_pool;
    }

    const VulkanQueue& VulkanCommandPool::GetQueue() const
    {
        return m_queue;
    }

    ResultValue<vk::Result, VulkanCommandBuffer*> VulkanCommandPool::CreateCommandBuffer()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto [result, command_buffer] = VulkanCommandBuffer::Create(m_device, m_queue, *this);
        if (result != vk::Result::eSuccess)
        {
            return result;
        }
        return m_command_buffers.emplace_back(Memory::Move(command_buffer)).get();
    }

    vk::Result VulkanCommandPool::Reset()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        for (const auto& command_buffer : m_command_buffers)
        {
            command_buffer->OnReset();
        }
        return m_device.GetVkDevice().resetCommandPool(m_vk_pool);
    }

}
