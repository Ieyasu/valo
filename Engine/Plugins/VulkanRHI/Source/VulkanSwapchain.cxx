module;

#include <Valo/Profile.hpp>
#include <Valo/Vulkan.hpp>

#include <algorithm>
#include <limits>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.Math;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;
import Valo.Window;

import :Conversions;
import :DebugUtils;
import :Device;
import :Instance;

namespace Valo::RHI
{
    VulkanSwapchain::~VulkanSwapchain()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        CleanUp();

        if (m_vk_surface)
        {
            m_instance->GetVkInstance().destroySurfaceKHR(m_vk_surface);
        }
    }

    ResultValue<vk::Result, UniqueVulkanSwapchain> VulkanSwapchain::Create(
        const VulkanInstance& instance,
        VulkanDevice&         device,
        WindowHandle          window)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto swapchain        = Valo::MakeUnique<VulkanSwapchain>();
        swapchain->m_instance = &instance;
        swapchain->m_device   = &device;
        swapchain->m_window   = window;

        // Create surface
        auto* const window_context                 = static_cast<WindowVulkanContextHandle>(window->GetContext());
        const auto [vk_surface_result, vk_surface] = window_context->CreateSurface(instance.GetVkInstance());
        if (vk_surface_result != CreateVulkanSurfaceResult::Success)
        {
            Log::Error("Failed to create window surface for swapchain");
            return vk::Result::eErrorUnknown;
        }
        swapchain->m_vk_surface = vk_surface;
        device.SetObjectName(vk_surface, StringFunc::Format("{} Surface", window->GetTitle()));

        // Find suitable present mode and surface format
        const auto vsync                                     = true;
        const auto [vk_present_mode_result, vk_present_mode] = SelectPresentMode(
            device.GetVkPhysicalDevice(),
            vk_surface,
            vsync);
        if (vk_present_mode_result != vk::Result::eSuccess)
        {
            return vk_present_mode_result;
        }
        swapchain->m_vk_present_mode = vk_present_mode;

        const auto [vk_surface_format_result, vk_surface_format] = SelectSurfaceFormat(
            device.GetVkPhysicalDevice(),
            vk_surface);
        if (vk_surface_format_result != vk::Result::eSuccess)
        {
            return vk_surface_format_result;
        }
        swapchain->m_vk_surface_format = vk_surface_format;

        auto vk_recreate_result = swapchain->Recreate();
        if (vk_recreate_result != vk::Result::eSuccess)
        {
            return vk_recreate_result;
        }

        return swapchain;
    }

    vk::Result VulkanSwapchain::Recreate()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        constexpr auto desired_image_count  = UInt32{3};
        constexpr auto desired_array_layers = UInt32{1};

        m_device->WaitIdle();

        CleanUp();

        auto vk_device          = m_device->GetVkDevice();
        auto vk_physical_device = m_device->GetVkPhysicalDevice();

        // Validate input
        const auto [caps_result, caps] = vk_physical_device.getSurfaceCapabilitiesKHR(m_vk_surface);
        if (caps_result != vk::Result::eSuccess)
        {
            Log::Error("vk::PhysicalDevice::getSurfaceCapabilitiesKHR() failed ({})", caps_result);
            return caps_result;
        }

        const auto width  = std::clamp(m_window->GetWidth(), caps.minImageExtent.width, caps.maxImageExtent.width);
        const auto height = std::clamp(m_window->GetHeight(), caps.minImageExtent.height, caps.maxImageExtent.height);

        // Create the swapchain
        auto swapchain_info       = vk::SwapchainCreateInfoKHR{};
        swapchain_info.surface    = m_vk_surface;
        swapchain_info.imageUsage = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eColorAttachment;
        swapchain_info.imageSharingMode   = vk::SharingMode::eExclusive;
        swapchain_info.compositeAlpha     = vk::CompositeAlphaFlagBitsKHR::eOpaque;
        swapchain_info.presentMode        = m_vk_present_mode;
        swapchain_info.clipped            = VK_TRUE;
        swapchain_info.oldSwapchain       = VK_NULL_HANDLE;
        swapchain_info.imageFormat        = m_vk_surface_format.format;
        swapchain_info.imageColorSpace    = m_vk_surface_format.colorSpace;
        swapchain_info.preTransform       = caps.currentTransform;
        swapchain_info.imageArrayLayers   = Math::Min(desired_array_layers, caps.maxImageArrayLayers);
        swapchain_info.minImageCount      = std::clamp(desired_image_count, caps.minImageCount, caps.maxImageCount);
        swapchain_info.imageExtent.width  = width;
        swapchain_info.imageExtent.height = height;

        const auto [vk_swapchain_result, vk_swapchain] = vk_device.createSwapchainKHR(swapchain_info, nullptr);
        if (vk_swapchain_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createSwapchainKHR() failed ({})", vk_swapchain_result);
            return vk_swapchain_result;
        }
        m_vk_swapchain = vk_swapchain;
        m_device->SetObjectName(vk_swapchain, StringFunc::Format("{} Swapchain", m_window->GetTitle()));

        // Get the swapchain images
        const auto [vk_images_result, vk_images] = vk_device.getSwapchainImagesKHR(vk_swapchain);
        if (vk_images_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::getSwapchainImagesKHR() failed ({})", vk_images_result);
            return vk_images_result;
        }

        // Create image view for swapchain images
        m_image_handles.resize(vk_images.size());
        for (size_t i = 0; i < vk_images.size(); ++i)
        {
            m_device->SetObjectName(vk_images[i], StringFunc::Format("{} Swapchain Image {}", m_window->GetTitle(), i));

            auto image         = m_device->m_images.Emplace();
            m_image_handles[i] = image;

            // Create the swapchain image view
            auto image_view_info                            = vk::ImageViewCreateInfo{};
            image_view_info.image                           = vk_images[i];
            image_view_info.viewType                        = vk::ImageViewType::e2D;
            image_view_info.format                          = m_vk_surface_format.format;
            image_view_info.components.r                    = vk::ComponentSwizzle::eIdentity;
            image_view_info.components.g                    = vk::ComponentSwizzle::eIdentity;
            image_view_info.components.b                    = vk::ComponentSwizzle::eIdentity;
            image_view_info.components.a                    = vk::ComponentSwizzle::eIdentity;
            image_view_info.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
            image_view_info.subresourceRange.baseMipLevel   = 0;
            image_view_info.subresourceRange.levelCount     = 1;
            image_view_info.subresourceRange.baseArrayLayer = 0;
            image_view_info.subresourceRange.layerCount     = 1;

            const auto [vk_image_view_result, vk_image_view] = vk_device.createImageView(image_view_info, nullptr);
            if (vk_image_view_result != vk::Result::eSuccess)
            {
                Log::Error("vk::Device::createImageView() failed ({})", vk_image_view_result);
                return vk_image_view_result;
            }
            image->vk_image      = vk_images[i];
            image->vk_image_view = vk_image_view;
            m_device->SetObjectName(
                vk_image_view,
                StringFunc::Format("{} Swapchain Image View {}", m_window->GetTitle(), i));
        }

        // Update info
        m_info.format = FromVkFormat(m_vk_surface_format.format);
        m_info.width  = width;
        m_info.height = height;
        return vk::Result::eSuccess;
    }

    void VulkanSwapchain::CleanUp()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        for (const auto& image : m_image_handles)
        {
            // Only vk::ImageView is destroyed, as vk::Image is managed by swapchain
            if (image->vk_image_view)
            {
                m_device->GetVkDevice().destroyImageView(image->vk_image_view);
            }

            m_device->m_images.Erase(image);
        }

        if (m_vk_swapchain)
        {
            m_device->GetVkDevice().destroySwapchainKHR(m_vk_swapchain);
            m_vk_swapchain = nullptr;
        }

        m_info = {};
        m_image_handles.clear();
        m_current_image_index = 0;
    }

    const SwapchainInfo& VulkanSwapchain::GetInfo() const
    {
        return m_info;
    }

    vk::SwapchainKHR VulkanSwapchain::GetVkSwapchain() const
    {
        return m_vk_swapchain;
    }

    UInt32 VulkanSwapchain::GetCurrentImageIndex() const
    {
        return m_current_image_index;
    }

    PoolHandle<VulkanImage> VulkanSwapchain::GetImage(UInt32 index) const
    {
        return m_image_handles[index];
    }

    ResultValue<vk::Result, UInt32> VulkanSwapchain::AquireNextImageIndex(vk::Semaphore semaphore)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto [result, index] = m_device->GetVkDevice().acquireNextImageKHR(
            m_vk_swapchain,
            std::numeric_limits<UInt64>::max(),
            semaphore,
            nullptr);

        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::acquireNextImageKHR() failed ({})", result);
            return result;
        }

        m_current_image_index = index;
        return m_current_image_index;
    }

    ResultValue<vk::Result, vk::SurfaceFormatKHR> VulkanSwapchain::SelectSurfaceFormat(
        vk::PhysicalDevice physical_device,
        vk::SurfaceKHR     surface)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto [result, surface_formats] = physical_device.getSurfaceFormatsKHR(surface);
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::PhysicalDevice::getSurfaceFormatsKHR() failed ({})", result);
            return result;
        }

        for (const auto& surface_format : surface_formats)
        {
            if (surface_format.format == vk::Format::eB8G8R8A8Srgb
                && surface_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            {
                return surface_format;
            }
        }

        return surface_formats.front();
    }

    ResultValue<vk::Result, vk::PresentModeKHR> VulkanSwapchain::SelectPresentMode(
        vk::PhysicalDevice physical_device,
        vk::SurfaceKHR     surface,
        bool               vsync)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto [result, surface_present_modes] = physical_device.getSurfacePresentModesKHR(surface);
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::PhysicalDevice::getSurfacePresentModesKHR() failed ({})", result);
            return result;
        }

        for (const auto& present_mode : surface_present_modes)
        {
            if ((!vsync && present_mode == vk::PresentModeKHR::eImmediate)
                || (vsync && present_mode == vk::PresentModeKHR::eMailbox))
            {
                return present_mode;
            }
        }

        return vk::PresentModeKHR::eFifo;
    }
}
