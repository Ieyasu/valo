module;

#include <Valo/Profile.hpp>
#include <Valo/Vulkan.hpp>

#include <vk_mem_alloc.h>

#include <algorithm>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;
import Valo.Window;

import :Conversions;

namespace Valo::RHI
{
    VulkanInstance::VulkanInstance(vk::Instance instance, UInt32 api_version) :
        m_vk_instance(instance),
        m_api_version(api_version),
        m_vk_debug_messenger(VulkanDebugUtils::CreateMessenger(instance))
    {
    }

    VulkanInstance::~VulkanInstance()
    {
        m_vk_instance.destroyDebugUtilsMessengerEXT(m_vk_debug_messenger, nullptr);
        m_vk_instance.destroy();
    }

    vk::Instance VulkanInstance::GetVkInstance() const
    {
        return m_vk_instance;
    }

    Result<InstanceHandle> VulkanInstance::Create(const InstanceDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        Log::Info("Creating Vulkan instance");

        VULKAN_HPP_DEFAULT_DISPATCHER.init();

        const auto api_version = GetDefaultApiVersion();

        Log::Info(
            "Using Vulkan API version {}.{}.{}",
            VK_API_VERSION_MAJOR(api_version),
            VK_API_VERSION_MINOR(api_version),
            VK_API_VERSION_PATCH(api_version));

        const auto layers          = GetInstanceLayers();
        const auto vk_layer_result = ValidateInstanceLayers(layers);
        if (vk_layer_result != vk::Result::eSuccess)
        {
            Log::Error("{}: required instance layers not supported", create_instance_error);
            return FromVkResultCode(vk_layer_result);
        }

        const auto extensions          = GetInstanceExtensions(descriptor);
        const auto vk_extension_result = ValidateInstanceExtensions(extensions);
        if (vk_extension_result != vk::Result::eSuccess)
        {
            Log::Error("{}: required instance extensions not supported", create_instance_error);
            return FromVkResultCode(vk_extension_result);
        }

        auto clayers = Vector<const char*>(layers.size());
        for (size_t i = 0; i < layers.size(); ++i)
        {
            clayers[i] = layers[i].c_str();
        }

        auto cextensions = Vector<const char*>(extensions.size());
        for (size_t i = 0; i < extensions.size(); ++i)
        {
            cextensions[i] = extensions[i].c_str();
        }

        auto vk_application_info               = vk::ApplicationInfo{};
        vk_application_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
        vk_application_info.pApplicationName   = "Valo";
        vk_application_info.engineVersion      = VK_MAKE_VERSION(1, 0, 0);
        vk_application_info.pEngineName        = "Valo";
        vk_application_info.apiVersion         = api_version;

        const auto vk_messenger_info = VulkanDebugUtils::GetMessengerDescriptor();

        auto vk_instance_info             = vk::InstanceCreateInfo{};
        vk_instance_info.pApplicationInfo = &vk_application_info;
        vk_instance_info.pNext            = &vk_messenger_info;
        vk_instance_info.setPEnabledExtensionNames(cextensions);
        vk_instance_info.setPEnabledLayerNames(clayers);

        const auto [vk_instance_result, vk_instance] = vk::createInstance(vk_instance_info);
        if (vk_instance_result != vk::Result::eSuccess)
        {
            Log::Error("{}: vk::createInstance() failed ({})", create_instance_error, vk_instance_result);
            return FromVkResultCode(vk_instance_result);
        }

        VULKAN_HPP_DEFAULT_DISPATCHER.init(vk_instance);

        return InstanceHandle(new VulkanInstance(vk_instance, api_version));
    }

    Result<DeviceHandle> VulkanInstance::CreateDevice(const DeviceDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        Log::Info("Creating Vulkan device");

        // Create a physical device
        const auto [result, physical_devices] = m_vk_instance.enumeratePhysicalDevices();
        if (result != vk::Result::eSuccess)
        {
            Log::Error("{}: vk::Instance::enumeratePhysicalDevices() failed ({})", create_device_error, result);
            return FromVkResultCode(result);
        }

        if (physical_devices.empty())
        {
            Log::Error("{}: no GPUs with Vulkan support", create_device_error);
            return ResultCode::ErrorUnsupported;
        }

        const auto physical_device = physical_devices.front();
        Log::Info("Using physical device '{}'", physical_device.getProperties().deviceName.data());

        // Get device extensions
        const auto extensions       = GetDeviceExtensions(descriptor);
        const auto extension_result = ValidateDeviceExtensions(physical_device, extensions);
        if (extension_result != vk::Result::eSuccess)
        {
            Log::Error("{}: required device extensions not supported", create_device_error);
            return FromVkResultCode(extension_result);
        }

        auto cextensions = Vector<const char*>(extensions.size());
        for (size_t i = 0; i < extensions.size(); ++i)
        {
            cextensions[i] = extensions[i].c_str();
        }

        // Get queues
        const auto compute_family_index = GetComputeQueue(physical_device);
        if (!compute_family_index.has_value())
        {
            Log::Error("{}: suitable compute queue not found", create_device_error);
            return ResultCode::ErrorUnsupported;
        }

        const auto graphics_family_index = GetGraphicsQueue(physical_device);
        if (!graphics_family_index.has_value())
        {
            Log::Error("{}: suitable graphics queue not found", create_device_error);
            return ResultCode::ErrorUnsupported;
        }

        const auto transfer_family_index = GetTransferQueue(physical_device);
        if (!transfer_family_index.has_value())
        {
            Log::Error("{}: suitable transfer queue not found", create_device_error);
            return ResultCode::ErrorUnsupported;
        }

        auto queue_families = Map<UInt32, UInt32>();
        queue_families[*compute_family_index]++;
        queue_families[*graphics_family_index]++;
        queue_families[*transfer_family_index]++;

        const auto queue_priority = Vector<float>(4, 1.0f);
        auto       queue_infos    = Vector<vk::DeviceQueueCreateInfo>();
        for (const auto& pair : queue_families)
        {
            auto queue_info             = vk::DeviceQueueCreateInfo{};
            queue_info.queueFamilyIndex = pair.first;
            queue_info.queueCount       = pair.second;
            queue_info.pQueuePriorities = queue_priority.data();
            queue_infos.push_back(queue_info);
        }

        // Create the device
        const auto device_features = physical_device.getFeatures();

        // Enable synchronization2
        auto synchronization2_features             = vk::PhysicalDeviceSynchronization2Features{};
        synchronization2_features.synchronization2 = VK_TRUE;

        auto device_info                    = vk::DeviceCreateInfo{};
        device_info.pNext                   = &synchronization2_features;
        device_info.pEnabledFeatures        = &device_features;
        device_info.ppEnabledExtensionNames = cextensions.data();
        device_info.enabledExtensionCount   = static_cast<UInt32>(cextensions.size());
        device_info.pQueueCreateInfos       = queue_infos.data();
        device_info.queueCreateInfoCount    = static_cast<UInt32>(queue_infos.size());

        auto [device_result, device] = physical_device.createDevice(device_info);
        if (device_result != vk::Result::eSuccess)
        {
            Log::Error("{}: vk::PhysicalDevice::createDevice() failed ({})", create_device_error, device_result);
            return FromVkResultCode(device_result);
        }
        VULKAN_HPP_DEFAULT_DISPATCHER.init(device);

        // Create memory allocator
        const auto dl = vk::DynamicLoader{};

        auto vulkanFunctions                  = VmaVulkanFunctions{};
        vulkanFunctions.vkGetInstanceProcAddr = dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
        vulkanFunctions.vkGetDeviceProcAddr   = dl.getProcAddress<PFN_vkGetDeviceProcAddr>("vkGetDeviceProcAddr");

        auto allocatorCreateInfo             = VmaAllocatorCreateInfo{};
        allocatorCreateInfo.vulkanApiVersion = m_api_version;
        allocatorCreateInfo.physicalDevice   = physical_device;
        allocatorCreateInfo.device           = device;
        allocatorCreateInfo.instance         = m_vk_instance;
        allocatorCreateInfo.pVulkanFunctions = &vulkanFunctions;

        auto allocator = VmaAllocator{};  // NOLINT
        vmaCreateAllocator(&allocatorCreateInfo, &allocator);

        const auto compute_queue  = device.getQueue(*compute_family_index, --queue_families[*compute_family_index]);
        const auto graphics_queue = device.getQueue(*graphics_family_index, --queue_families[*graphics_family_index]);
        const auto transfer_queue = device.getQueue(*transfer_family_index, --queue_families[*transfer_family_index]);

        return DeviceHandle(new VulkanDevice{
            *this,
            device,
            physical_device,
            { compute_queue,  *compute_family_index},
            {graphics_queue, *graphics_family_index},
            {transfer_queue, *transfer_family_index},
            allocator
        });
    }

    void VulkanInstance::DestroyDevice(DeviceHandle device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        delete device.Get();
    }

    UInt32 VulkanInstance::GetDefaultApiVersion()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto dl = vk::DynamicLoader{};

        if (dl.getProcAddress<PFN_vkEnumerateInstanceVersion>("vkEnumerateInstanceVersion") == nullptr)
        {
            return VK_API_VERSION_1_0;
        }

        const auto [result, apiVersion] = vk::enumerateInstanceVersion();
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::enumerateInstanceVersion() failed ({})", result);
            return VK_API_VERSION_1_0;
        }

        return apiVersion;
    }

    Vector<String> VulkanInstance::GetInstanceLayers()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto layers = Vector<String>();
#if !defined(NDEBUG)
        layers.emplace_back("VK_LAYER_KHRONOS_validation");
#endif
        return layers;
    }

    Vector<String> VulkanInstance::GetInstanceExtensions(const InstanceDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto extensions = Vector<String>();

        if (descriptor.window_context != nullptr)
        {
            auto* const context           = static_cast<WindowVulkanContextHandle>(descriptor.window_context);
            const auto  window_extensions = context->GetInstanceExtensions();
            extensions.insert(extensions.end(), window_extensions.begin(), window_extensions.end());
        }

        extensions.emplace_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        extensions.emplace_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);

        return extensions;
    }

    Vector<String> VulkanInstance::GetDeviceExtensions(const DeviceDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto extensions = Vector<String>();

        if (descriptor.window_context != nullptr)
        {
            extensions.emplace_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
        }

        extensions.emplace_back(VK_KHR_SYNCHRONIZATION_2_EXTENSION_NAME);

        return extensions;
    }

    vk::Result VulkanInstance::ValidateInstanceLayers(const Vector<String>& layers)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        Log::Debug("Validating instance layers");

        const auto [enumerate_result, available_layers] = vk::enumerateInstanceLayerProperties();
        if (enumerate_result != vk::Result::eSuccess)
        {
            Log::Error("vk::enumerateInstanceLayerProperties() failed ({})", enumerate_result);
            return enumerate_result;
        }

        auto result = vk::Result::eSuccess;
        for (const auto& layer : layers)
        {
            const auto predicate = [&layer](const auto& available_layer)
            {
                return std::strcmp(layer.c_str(), available_layer.layerName) == 0;
            };

            if (!std::ranges::any_of(available_layers, predicate))
            {
                Log::Error("Required Vulkan instance layer '{}' not supported", layer);
                result = vk::Result::eErrorLayerNotPresent;
            }
        }
        return result;
    }

    vk::Result VulkanInstance::ValidateInstanceExtensions(const Vector<String>& extensions)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        Log::Debug("Validating instance extensions");

        const auto [enumerate_result, available_extensions] = vk::enumerateInstanceExtensionProperties();
        if (enumerate_result != vk::Result::eSuccess)
        {
            Log::Error("vk::enumerateInstanceExtensionProperties() failed ({})", enumerate_result);
            return enumerate_result;
        }

        auto result = vk::Result::eSuccess;
        for (const auto& extension : extensions)
        {
            const auto predicate = [&extension](const auto& available_extension)
            {
                return std::strcmp(extension.c_str(), available_extension.extensionName) == 0;
            };

            if (!std::ranges::any_of(available_extensions, predicate))
            {
                Log::Error("Required Vulkan instance extension '{}' not supported", extension);
                result = vk::Result::eErrorExtensionNotPresent;
            }
        }
        return result;
    }

    vk::Result VulkanInstance::ValidateDeviceExtensions(
        vk::PhysicalDevice    physical_device,
        const Vector<String>& extensions)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        Log::Debug("Validating device extensions");

        const auto [enumerate_result, available_extensions] = physical_device.enumerateDeviceExtensionProperties();
        if (enumerate_result != vk::Result::eSuccess)
        {
            Log::Error("vk::PhysicalDevice::enumerateDeviceExtensionProperties() failed ({})", enumerate_result);
            return enumerate_result;
        }

        auto result = vk::Result::eSuccess;
        for (const auto& extension : extensions)
        {
            const auto predicate = [&extension](const auto& available_extension)
            {
                return std::strcmp(extension.c_str(), available_extension.extensionName) == 0;
            };

            if (!std::ranges::any_of(available_extensions, predicate))
            {
                Log::Warning("Required Vulkan device extension '{}' not supported", extension);
                result = vk::Result::eErrorExtensionNotPresent;
            }
        }
        return result;
    }

    Optional<UInt32> VulkanInstance::GetGraphicsQueue(vk::PhysicalDevice physical_device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto include = vk::QueueFlagBits::eGraphics & vk::QueueFlagBits::eCompute & vk::QueueFlagBits::eTransfer;
        const auto exclude = vk::QueueFlags(0);
        return GetQueueFamilyIndex(include, exclude, physical_device);
    }

    Optional<UInt32> VulkanInstance::GetComputeQueue(vk::PhysicalDevice physical_device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto include            = vk::QueueFlagBits::eCompute;
        auto exclude            = vk::QueueFlagBits::eGraphics;
        auto queue_family_index = GetQueueFamilyIndex(include, exclude, physical_device);

        if (!queue_family_index.has_value())
        {
            include            = vk::QueueFlagBits::eCompute;
            exclude            = static_cast<vk::QueueFlagBits>(0);
            queue_family_index = GetQueueFamilyIndex(include, exclude, physical_device);
        }

        return queue_family_index;
    }

    Optional<UInt32> VulkanInstance::GetTransferQueue(vk::PhysicalDevice physical_device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto include            = vk::QueueFlagBits::eTransfer;
        auto exclude            = vk::QueueFlagBits::eGraphics & vk::QueueFlagBits::eCompute;
        auto queue_family_index = GetQueueFamilyIndex(include, exclude, physical_device);

        if (!queue_family_index.has_value())
        {
            include            = vk::QueueFlagBits::eTransfer;
            exclude            = vk::QueueFlags(0);
            queue_family_index = GetQueueFamilyIndex(include, exclude, physical_device);
        }

        return queue_family_index;
    }

    Optional<UInt32> VulkanInstance::GetQueueFamilyIndex(
        vk::QueueFlags     include_flags,
        vk::QueueFlags     exclude_flags,
        vk::PhysicalDevice physical_device)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto queue_families = physical_device.getQueueFamilyProperties();
        for (UInt32 i = 0; i < static_cast<UInt32>(queue_families.size()); ++i)
        {
            const bool all_include_flags = (queue_families[i].queueFlags & include_flags) == include_flags;
            const bool no_exclude_flags  = (queue_families[i].queueFlags & exclude_flags) == vk::QueueFlags(0);
            if (all_include_flags && no_exclude_flags)
            {
                return i;
            }
        }
        return Valo::nullopt;
    }
}
