module;

#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:CommandPool;

import Valo.RHI;
import Valo.Std;

import :CommandBuffer;
import :Queue;

export namespace Valo::RHI
{
    class VulkanDevice;

    class VulkanCommandPool final
    {
    public:
        VulkanCommandPool(const VulkanCommandPool&)            = delete;
        VulkanCommandPool(VulkanCommandPool&&)                 = delete;
        VulkanCommandPool& operator=(const VulkanCommandPool&) = delete;
        VulkanCommandPool& operator=(VulkanCommandPool&&)      = delete;
        ~VulkanCommandPool();

        static UniqueResult<vk::Result, VulkanCommandPool> Create(const VulkanDevice& device, const VulkanQueue& queue);

        vk::CommandPool GetVkPool() const;

        const VulkanQueue& GetQueue() const;

        ResultValue<vk::Result, VulkanCommandBuffer*> CreateCommandBuffer();

        vk::Result Reset();

    private:
        explicit VulkanCommandPool(const VulkanDevice& device, const VulkanQueue& queue, vk::CommandPool vk_pool);

        const VulkanDevice& m_device;
        const VulkanQueue&  m_queue;
        vk::CommandPool     m_vk_pool{nullptr};

        Vector<UniquePtr<VulkanCommandBuffer>> m_command_buffers{};
    };
}
