module;

#include <Valo/Profile.hpp>
#include <Valo/Vulkan.hpp>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;

import :CommandPool;
import :Conversions;
import :Device;

namespace Valo::RHI
{
    VulkanCommandBuffer::VulkanCommandBuffer(const VulkanDevice& device, const VulkanQueue& queue) :
        m_device(device),
        m_queue(queue)
    {
    }

    VulkanCommandBuffer::~VulkanCommandBuffer()
    {
        if (m_vk_fence)
        {
            m_device.GetVkDevice().destroyFence(m_vk_fence);
        }

        for (const auto& vk_semaphore : m_vk_semaphores)
        {
            m_device.GetVkDevice().destroySemaphore(vk_semaphore);
        }
    }

    ResultValue<vk::Result, UniquePtr<VulkanCommandBuffer>> VulkanCommandBuffer::Create(
        const VulkanDevice&      device,
        const VulkanQueue&       queue,
        const VulkanCommandPool& pool)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto self = Valo::MakeUnique<VulkanCommandBuffer>(device, queue);

        // Create fence used for wait_until_completed()
        auto vk_fence_info                     = vk::FenceCreateInfo{};
        const auto [vk_fence_result, vk_fence] = device.GetVkDevice().createFence(vk_fence_info);
        if (vk_fence_result != vk::Result::eSuccess)
        {
            return vk_fence_result;
        }
        self->m_vk_fence = vk_fence;
        self->SetFenceName(self->m_vk_fence);

        // Allocate command buffer
        auto vk_alloc_info               = vk::CommandBufferAllocateInfo{};
        vk_alloc_info.commandPool        = pool.GetVkPool();
        vk_alloc_info.commandBufferCount = 1;
        vk_alloc_info.level              = vk::CommandBufferLevel::ePrimary;

        const auto [vk_alloc_result, vk_cmd_buffers] = device.GetVkDevice().allocateCommandBuffers(vk_alloc_info);
        if (vk_alloc_result != vk::Result::eSuccess)
        {
            return vk_alloc_result;
        }
        self->m_vk_command_buffer = vk_cmd_buffers[0];

        return self;
    }

    StringView VulkanCommandBuffer::GetName()
    {
        return m_name;
    }

    void VulkanCommandBuffer::SetName(String name)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        m_name = Memory::Move(name);
        m_device.SetObjectName(m_vk_command_buffer, m_name);

        SetFenceName(m_vk_fence);
        for (size_t i = 0; i < m_vk_semaphores.size(); ++i)
        {
            SetSemaphoreName(m_vk_semaphores[i], i);
        }
    }

    ResultCode VulkanCommandBuffer::Begin()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        // Begin the Vulkan command buffer
        auto begin_info  = vk::CommandBufferBeginInfo{};
        begin_info.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

        const auto vk_begin_result = m_vk_command_buffer.begin(begin_info);
        if (vk_begin_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to begin Vulkan command buffer: {}", vk_begin_result);
        }
        return FromVkResultCode(vk_begin_result);
    }

    ResultCode VulkanCommandBuffer::End()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto vk_end_result = m_vk_command_buffer.end();
        if (vk_end_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to end Vulkan command buffer: {}", vk_end_result);
        }
        return FromVkResultCode(vk_end_result);
    }

    ResultCode VulkanCommandBuffer::Reset()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto result_reSetBuffer = m_vk_command_buffer.reset();
        if (result_reSetBuffer != vk::Result::eSuccess)
        {
            Log::Error("Failed to reset command buffer: {}", result_reSetBuffer);
            return FromVkResultCode(result_reSetBuffer);
        }

        return OnReset();
    }

    ResultCode VulkanCommandBuffer::OnReset()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        m_swapchain_image_index       = nullopt;
        m_aquire_image_semaphore      = nullptr;
        m_ready_for_present_semaphore = nullptr;

        // When reset, all semaphores become available
        m_available_semaphores.assign(m_vk_semaphores.begin(), m_vk_semaphores.end());

        auto result_reset_fences = m_device.GetVkDevice().resetFences(1, &m_vk_fence);
        if (result_reset_fences != vk::Result::eSuccess)
        {
            Log::Error("Failed to reset command buffer fence: {}", result_reset_fences);
            return FromVkResultCode(result_reset_fences);
        }

        return FromVkResultCode(result_reset_fences);
    }

    ResultCode VulkanCommandBuffer::WaitUntilCompleted()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto vk_result = m_device.GetVkDevice().waitForFences(1, &m_vk_fence, VK_TRUE, UINT64_MAX);
        if (vk_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to wait for Vulkan command buffer fence: {}", vk_result);
        }

        return FromVkResultCode(vk_result);
    }

    ResultCode VulkanCommandBuffer::Submit()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        // Submit Vulkan command buffer
        auto command_buffer_info          = vk::CommandBufferSubmitInfo{};
        command_buffer_info.commandBuffer = m_vk_command_buffer;

        auto wait_semaphore_info      = vk::SemaphoreSubmitInfo{};
        wait_semaphore_info.semaphore = m_aquire_image_semaphore;
        wait_semaphore_info.stageMask = vk::PipelineStageFlagBits2::eBlit;

        auto signal_semaphore_info      = vk::SemaphoreSubmitInfo{};
        signal_semaphore_info.semaphore = m_ready_for_present_semaphore;
        signal_semaphore_info.stageMask = vk::PipelineStageFlagBits2::eBlit;

        auto vk_submit_info                     = vk::SubmitInfo2{};
        vk_submit_info.commandBufferInfoCount   = 1;
        vk_submit_info.pCommandBufferInfos      = &command_buffer_info;
        vk_submit_info.pWaitSemaphoreInfos      = &wait_semaphore_info;
        vk_submit_info.waitSemaphoreInfoCount   = m_aquire_image_semaphore ? 1 : 0;
        vk_submit_info.pSignalSemaphoreInfos    = &signal_semaphore_info;
        vk_submit_info.signalSemaphoreInfoCount = m_ready_for_present_semaphore ? 1 : 0;

        const auto vk_submit_result = m_queue.vk_queue.submit2(vk_submit_info, m_vk_fence);
        if (vk_submit_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to submit Vulkan command buffer: {}", vk_submit_result);
            return FromVkResultCode(vk_submit_result);
        }

        // Present the image
        if (m_swapchain_image_index.has_value())
        {
            const auto& swapchain    = m_device.m_swapchains.back();
            const auto  vk_swapchain = swapchain->GetVkSwapchain();

            auto present_info               = vk::PresentInfoKHR{};
            present_info.waitSemaphoreCount = 1;
            present_info.pWaitSemaphores    = &m_ready_for_present_semaphore;
            present_info.pImageIndices      = &m_swapchain_image_index.value();
            present_info.swapchainCount     = 1;
            present_info.pSwapchains        = &vk_swapchain;

            const auto present_result = m_queue.vk_queue.presentKHR(present_info);

            if (present_result == vk::Result::eErrorOutOfDateKHR || present_result == vk::Result::eSuboptimalKHR)
            {
                Log::Debug("Recreating vk::Swapchain after present: {}", present_result);
                swapchain->Recreate();
                return ResultCode::Success;
            }

            if (present_result != vk::Result::eSuccess)
            {
                Log::Error("vk::Queue::presentKHR() failed ({})", present_result);
                return FromVkResultCode(present_result);
            }
        }

        return ResultCode::Success;
    }

    void VulkanCommandBuffer::CmdPipelineBarrier(
        Span<const BufferMemoryBarrier> buffer_memory_barriers,
        Span<const ImageMemoryBarrier>  image_memory_barriers)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_buffer_barriers = SmallVector<vk::BufferMemoryBarrier2>(buffer_memory_barriers.size());
        for (size_t i = 0; i < vk_buffer_barriers.size(); ++i)
        {
            vk_buffer_barriers[i]        = ToVkBufferMemoryBarrier(buffer_memory_barriers[i]);
            vk_buffer_barriers[i].buffer = m_device.m_buffers.Get(buffer_memory_barriers[i].buffer.GetNative())
                                               .vk_buffer;
        }

        auto vk_image_barriers = SmallVector<vk::ImageMemoryBarrier2>(image_memory_barriers.size());
        for (size_t i = 0; i < vk_image_barriers.size(); ++i)
        {
            vk_image_barriers[i]       = ToVkImageMemoryBarrier(image_memory_barriers[i]);
            vk_image_barriers[i].image = m_device.m_images.Get(image_memory_barriers[i].image.GetNative()).vk_image;
        }

        auto dependency_info1 = vk::DependencyInfo{
            .bufferMemoryBarrierCount = static_cast<UInt32>(vk_buffer_barriers.size()),
            .pBufferMemoryBarriers    = vk_buffer_barriers.data(),
            .imageMemoryBarrierCount  = static_cast<UInt32>(vk_image_barriers.size()),
            .pImageMemoryBarriers     = vk_image_barriers.data(),
        };
        m_vk_command_buffer.pipelineBarrier2(dependency_info1);
    }

    void VulkanCommandBuffer::CmdCopyBuffer(
        BufferHandle   src_buffer,
        DeviceSizeType src_offset,
        BufferHandle   dst_buffer,
        DeviceSizeType dst_offset,
        DeviceSizeType size)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_src_buffer = m_device.m_buffers.Get(src_buffer.GetNative()).vk_buffer;
        auto vk_dst_buffer = m_device.m_buffers.Get(dst_buffer.GetNative()).vk_buffer;

        const auto vk_region = vk::BufferCopy{
            .srcOffset = src_offset,
            .dstOffset = dst_offset,
            .size      = size,
        };

        m_vk_command_buffer.copyBuffer(vk_src_buffer, vk_dst_buffer, 1, &vk_region);
    }

    void VulkanCommandBuffer::CmdCopyBufferToImage(
        BufferHandle   src_buffer,
        DeviceSizeType buffer_offset,
        ImageHandle    dst_image,
        UInt32         image_width,
        UInt32         image_height,
        UInt32         image_depth,
        UInt32         image_offset_x,
        UInt32         image_offset_y,
        UInt32         image_offset_z,
        UInt32         image_mip_level,
        UInt32         image_array_layer)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_src_buffer = m_device.m_buffers.Get(src_buffer.GetNative()).vk_buffer;
        auto vk_dst_image  = m_device.m_images.Get(dst_image.GetNative()).vk_image;

        const auto vk_image_subresource = vk::ImageSubresourceLayers{
            .aspectMask     = vk::ImageAspectFlagBits::eColor,
            .mipLevel       = image_mip_level,
            .baseArrayLayer = image_array_layer,
            .layerCount     = 1,
        };

        const auto vk_offset = vk::Offset3D{
            .x = static_cast<Int32>(image_offset_x),
            .y = static_cast<Int32>(image_offset_y),
            .z = static_cast<Int32>(image_offset_z),
        };

        const auto vk_region = vk::BufferImageCopy{
            .bufferOffset     = buffer_offset,
            .imageSubresource = vk_image_subresource,
            .imageOffset      = vk_offset,
            .imageExtent      = {image_width, image_height, image_depth},
        };

        m_vk_command_buffer
            .copyBufferToImage(vk_src_buffer, vk_dst_image, vk::ImageLayout::eTransferDstOptimal, 1, &vk_region);
    }

    void VulkanCommandBuffer::CmdBindComputePipeline(ComputePipelineHandle pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_pipeline = ToVkHandle<vk::Pipeline>(pipeline);
        m_vk_command_buffer.bindPipeline(vk::PipelineBindPoint::eCompute, vk_pipeline);
    }

    void VulkanCommandBuffer::CmdDispatch(UInt32 group_count_x, UInt32 group_count_y, UInt32 group_count_z)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_vk_command_buffer.dispatch(group_count_x, group_count_y, group_count_z);
    }

    void VulkanCommandBuffer::CmdBeginRenderPass(
        RenderPassHandle       render_pass,
        FramebufferHandle      framebuffer,
        const Rect2D&          render_area,
        Span<const ClearValue> clear_values)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_clear_values = SmallVector<vk::ClearValue>(clear_values.size());
        for (size_t i = 0; i < vk_clear_values.size(); ++i)
        {
            vk_clear_values[i] = ToVkClearValue(clear_values[i]);
        }

        const auto begin_info = vk::RenderPassBeginInfo{
            .renderPass      = ToVkHandle<vk::RenderPass>(render_pass),
            .framebuffer     = ToVkHandle<vk::Framebuffer>(framebuffer),
            .renderArea      = ToVkRect2D(render_area),
            .clearValueCount = static_cast<UInt32>(vk_clear_values.size()),
            .pClearValues    = vk_clear_values.data(),
        };

        m_vk_command_buffer.beginRenderPass(begin_info, vk::SubpassContents::eInline);
    }

    void VulkanCommandBuffer::CmdNextSubpass()
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_vk_command_buffer.nextSubpass(vk::SubpassContents::eInline);
    }

    void VulkanCommandBuffer::CmdEndRenderPass()
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_vk_command_buffer.endRenderPass();
    }

    void VulkanCommandBuffer::CmdBindGraphicsPipeline(GraphicsPipelineHandle pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_pipeline = ToVkHandle<vk::Pipeline>(pipeline);
        m_vk_command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, vk_pipeline);
    }

    void VulkanCommandBuffer::CmdDraw(
        UInt32 vertex_count,
        UInt32 instance_count,
        UInt32 first_vertex,
        UInt32 first_instance)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_vk_command_buffer.draw(vertex_count, instance_count, first_vertex, first_instance);
    }

    void VulkanCommandBuffer::CmdDrawIndexed(
        UInt32 index_count,
        UInt32 instance_count,
        UInt32 first_index,
        UInt32 first_instance,
        Int32  vertex_offset)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_vk_command_buffer.drawIndexed(index_count, instance_count, first_index, vertex_offset, first_instance);
    }

    void VulkanCommandBuffer::CmdBindIndexBuffer(BufferHandle buffer, UInt32 offset, IndexFormat format)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_buffer  = m_device.m_buffers.Get(static_cast<UInt64>(buffer.GetNative())).vk_buffer;
        const auto index_type = ToVkIndexType(format);

        m_vk_command_buffer.bindIndexBuffer(vk_buffer, offset, index_type);
    }

    void VulkanCommandBuffer::CmdBindVertexBuffers(
        Span<const BufferHandle>   buffers,
        Span<const DeviceSizeType> offsets,
        UInt32                     first_binding)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_buffers = SmallVector<vk::Buffer>(buffers.size());
        for (size_t i = 0; i < buffers.size(); ++i)
        {
            vk_buffers[i] = m_device.m_buffers.Get(static_cast<UInt64>(buffers[i].GetNative())).vk_buffer;
        }

        m_vk_command_buffer.bindVertexBuffers(
            first_binding,
            static_cast<UInt32>(vk_buffers.size()),
            vk_buffers.data(),
            offsets.data());
    }

    void VulkanCommandBuffer::CmdBindDescriptorSets(
        PipelineLayoutHandle            pipeline_layout,
        Span<const DescriptorSetHandle> descriptor_sets,
        Span<const UInt32>              dynamic_offsets,
        UInt32                          first_set)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_descriptor_sets = SmallVector<vk::DescriptorSet>(descriptor_sets.size());
        for (size_t i = 0; i < descriptor_sets.size(); ++i)
        {
            vk_descriptor_sets[i] = ToVkHandle<vk::DescriptorSet>(descriptor_sets[i]);
        }

        m_vk_command_buffer.bindDescriptorSets(
            vk::PipelineBindPoint::eGraphics,
            ToVkHandle<vk::PipelineLayout>(pipeline_layout),
            first_set,
            static_cast<UInt32>(vk_descriptor_sets.size()),
            vk_descriptor_sets.data(),
            static_cast<UInt32>(dynamic_offsets.size()),
            dynamic_offsets.data());
    }

    void VulkanCommandBuffer::CmdSetScissor(const Scissor& scissor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_scissor = ToVkScissor(scissor);
        m_vk_command_buffer.setScissor(0, 1, &vk_scissor);
    }

    void VulkanCommandBuffer::CmdSetViewport(const Viewport& viewport)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_viewport = ToVkViewport(viewport);
        m_vk_command_buffer.setViewport(0, 1, &vk_viewport);
    }

    void VulkanCommandBuffer::CmdPresent(ImageHandle image, UInt32 image_width, UInt32 image_height)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        // Get semaphore used to wait on image aquire
        const auto [semaphore_result1, aquire_image_semaphore] = GetOrCreateSemaphore();
        if (semaphore_result1 != vk::Result::eSuccess)
        {
            Log::Error("Failed to present image: failed to create semaphore ({})", semaphore_result1);
            return;
        }
        m_aquire_image_semaphore = aquire_image_semaphore;

        // Get semaphore used to signal that frame is ready to present
        const auto [semaphore_result2, ready_for_present_semaphore] = GetOrCreateSemaphore();
        if (semaphore_result2 != vk::Result::eSuccess)
        {
            Log::Error("Failed to present image: failed to create semaphore ({})", semaphore_result2);
            return;
        }
        m_ready_for_present_semaphore = ready_for_present_semaphore;

        // Aquire swapchain image
        const auto& swapchain                                       = m_device.m_swapchains.back();
        const auto [swapchain_aquire_result, swapchain_image_index] = swapchain->AquireNextImageIndex(
            m_aquire_image_semaphore);

        // If swapchain is out of date, recreate and skip presenting for this frame
        if (swapchain_aquire_result == vk::Result::eErrorOutOfDateKHR)
        {
            Log::Debug("Recreating vk::Swapchain after aquire: {}", swapchain_aquire_result);
            swapchain->Recreate();
            return;
        }

        if (swapchain_aquire_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to present image: failed to aquire swapchain image: {}", swapchain_aquire_result);
            return;
        }

        m_swapchain_image_index = swapchain_image_index;
        auto swapchain_image    = swapchain->GetImage(m_swapchain_image_index.value());

        // Transition swapchain image for blitting
        const auto subresource_range = vk::ImageSubresourceRange{
            .aspectMask     = vk::ImageAspectFlagBits::eColor,
            .baseMipLevel   = 0,
            .levelCount     = 1,
            .baseArrayLayer = 0,
            .layerCount     = 1,
        };

        const auto imageBarrier1 = vk::ImageMemoryBarrier2{
            .srcStageMask     = vk::PipelineStageFlagBits2::eNone,
            .srcAccessMask    = vk::AccessFlagBits2::eNone,
            .dstStageMask     = vk::PipelineStageFlagBits2::eBlit,
            .dstAccessMask    = vk::AccessFlagBits2::eTransferWrite,
            .oldLayout        = vk::ImageLayout::eUndefined,
            .newLayout        = vk::ImageLayout::eTransferDstOptimal,
            .image            = swapchain_image->vk_image,
            .subresourceRange = subresource_range,
        };

        auto dependency_info1 = vk::DependencyInfo{
            .imageMemoryBarrierCount = 1,
            .pImageMemoryBarriers    = &imageBarrier1,
        };
        m_vk_command_buffer.pipelineBarrier2(dependency_info1);

        auto src_image = m_device.m_images.Get(image.GetNative());

        // Blit the source image to the swapchain image
        auto src_region          = vk::ImageBlit{};
        src_region.srcOffsets[0] = vk::Offset3D{0, 0, 0};
        src_region.srcOffsets[1] = vk::Offset3D{static_cast<Int32>(image_width), static_cast<Int32>(image_height), 1};
        src_region.dstOffsets[0] = vk::Offset3D{0, 0, 0};
        src_region.dstOffsets[1] = vk::Offset3D{
            static_cast<Int32>(swapchain->GetInfo().width),
            static_cast<Int32>(swapchain->GetInfo().height),
            1};
        src_region.dstSubresource.baseArrayLayer = 0;
        src_region.dstSubresource.layerCount     = 1;
        src_region.dstSubresource.mipLevel       = 0;
        src_region.dstSubresource.aspectMask     = vk::ImageAspectFlagBits::eColor;
        src_region.srcSubresource.baseArrayLayer = 0;
        src_region.srcSubresource.layerCount     = 1;
        src_region.srcSubresource.mipLevel       = 0;
        src_region.srcSubresource.aspectMask     = vk::ImageAspectFlagBits::eColor;

        // Transfer swapchain image for presentation
        m_vk_command_buffer.blitImage(
            src_image.vk_image,
            vk::ImageLayout::eTransferSrcOptimal,
            swapchain_image->vk_image,
            vk::ImageLayout::eTransferDstOptimal,
            1,
            &src_region,
            vk::Filter::eLinear);

        const auto imageBarrier2 = vk::ImageMemoryBarrier2{
            .srcStageMask     = vk::PipelineStageFlagBits2::eBlit,
            .srcAccessMask    = vk::AccessFlagBits2::eTransferWrite,
            .dstStageMask     = vk::PipelineStageFlagBits2::eNone,
            .dstAccessMask    = vk::AccessFlagBits2::eNone,
            .oldLayout        = vk::ImageLayout::eTransferDstOptimal,
            .newLayout        = vk::ImageLayout::ePresentSrcKHR,
            .image            = swapchain_image->vk_image,
            .subresourceRange = subresource_range,
        };

        const auto dependency_info2 = vk::DependencyInfo{
            .imageMemoryBarrierCount = 1,
            .pImageMemoryBarriers    = &imageBarrier2,
        };
        m_vk_command_buffer.pipelineBarrier2(dependency_info2);
    }

    ResultValue<vk::Result, vk::Semaphore> VulkanCommandBuffer::GetOrCreateSemaphore()
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        if (m_available_semaphores.empty())
        {
            auto vk_semaphore_info                         = vk::SemaphoreCreateInfo{};
            const auto [vk_semaphore_result, vk_semaphore] = m_device.GetVkDevice().createSemaphore(vk_semaphore_info);
            if (vk_semaphore_result != vk::Result::eSuccess)
            {
                return vk_semaphore_result;
            }
            SetSemaphoreName(vk_semaphore, m_vk_semaphores.size());
            m_vk_semaphores.push_back(vk_semaphore);
            return vk_semaphore;
        }

        const auto semaphore = m_available_semaphores.back();
        m_available_semaphores.pop_back();
        return semaphore;
    }

    void VulkanCommandBuffer::SetFenceName(vk::Fence vk_fence)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_device.SetObjectName(vk_fence, StringFunc::Format("{} Fence", GetName()));
    }

    void VulkanCommandBuffer::SetSemaphoreName(vk::Semaphore vk_semaphore, size_t index)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        m_device.SetObjectName(vk_semaphore, StringFunc::Format("{} Semaphore {}", GetName(), index));
    }
}
