module;

#include <Valo/Vulkan.hpp>

#include <memory>

export module Valo.VulkanRHI:Conversions;

import Valo.Logger;
import Valo.RHI;
import Valo.Std;

export namespace Valo::RHI
{
    ResultCode FromVkResultCode(vk::Result value);

    vk::ObjectType ToVkObjectType(ResourceType value);

    vk::ClearValue ToVkClearValue(const ClearValue& value);

    vk::Rect2D ToVkRect2D(const Rect2D& value);

    vk::ShaderStageFlags ToVkShaderStageFlags(ShaderStageFlags flags);

    vk::AttachmentLoadOp ToVkAttachmentLoadOp(AttachmentLoadOp value);

    vk::AttachmentStoreOp ToVkAttachmentStoreOp(AttachmentStoreOp value);

    vk::BufferUsageFlags ToVkBufferUsageFlags(BufferUsageFlags flags);

    vk::ImageUsageFlags ToVkImageUsageFlags(ImageUsageFlags flags);

    vk::ImageType ToVkImageType(ImageDimensions value);

    vk::ShaderStageFlagBits ToVkShaderStageFlagBits(ShaderStage value);

    Format FromVkFormat(vk::Format value);

    vk::Format ToVkFormat(Format value);

    vk::ImageAspectFlags ToVkAspectFlags(Format value);

    vk::DescriptorType ToVkDescriptorType(DescriptorType value);

    vk::ImageViewType ToVkImageViewType(ImageDimensions value);

    vk::IndexType ToVkIndexType(IndexFormat value);

    vk::PrimitiveTopology ToVkPrimitiveTopology(PrimitiveTopology value);

    vk::PolygonMode ToVkPolygonMode(PolygonMode value);

    vk::CullModeFlagBits ToVkCullMode(CullMode value);

    vk::FrontFace ToVkFrontFace(Winding value);

    vk::DynamicState ToVkDynamicState(DynamicState value);

    vk::CompareOp ToVkCompareOp(CompareOp value);

    vk::LogicOp ToVkLogicOp(LogicOp value);

    vk::BlendOp ToVkBlendOp(BlendOp value);

    vk::BlendFactor ToVkBlendFactor(BlendFactor value);

    vk::Filter ToVkFilter(SamplerFilter value);

    vk::SamplerMipmapMode ToVkSamplerMipmapMode(SamplerMipmapMode value);

    vk::SamplerAddressMode ToVkSamplerAddressMode(SamplerAddressMode value);

    vk::Viewport ToVkViewport(const Viewport& value);

    vk::Rect2D ToVkScissor(const Scissor& value);

    vk::PipelineStageFlags2 ToVkPipelineStageFlags(ShaderStageFlags stage_flags, vk::AccessFlags2 access_flags);

    vk::AccessFlags2 ToVkAccessFlags(BufferUsageFlags usage_flags, AccessFlags access_flags);

    vk::AccessFlags2 ToVkAccessFlags(ImageUsageFlags usage_flags, AccessFlags access_flags);

    vk::ImageLayout ToVkImageLayout(DescriptorType descriptor_type);

    vk::ImageLayout ToVkImageLayout(ImageUsageFlags usage_flags, AccessFlags access_flags);

    vk::ImageLayout ToVkImageLayout(ImageUsageFlags usage_flags, AccessFlags access_flags, Format format);

    vk::BufferMemoryBarrier2 ToVkBufferMemoryBarrier(const BufferMemoryBarrier& barrier);

    vk::ImageMemoryBarrier2 ToVkImageMemoryBarrier(const ImageMemoryBarrier& barrier);

    template <typename TResourceHandle, typename TVulkanHandle>
    inline TResourceHandle ToResourceHandle(const TVulkanHandle& vk_handle)
    {
        static_assert(
            sizeof(typename TResourceHandle::NativeType) == sizeof(typename TVulkanHandle::NativeType),
            "Handle size used by Graphics API must match handle size used by the engine");

        const auto vk_native_type       = typename TVulkanHandle::NativeType(vk_handle);
        const auto resource_native_type = reinterpret_cast<typename TResourceHandle::NativeType>(vk_native_type);
        return TResourceHandle(resource_native_type);
    }

    template <typename TVulkanHandle, typename TResourceHandle>
    inline TVulkanHandle ToVkHandle(TResourceHandle resource_handle)
    {
        static_assert(
            sizeof(typename TResourceHandle::NativeType) == sizeof(typename TVulkanHandle::NativeType),
            "Handle size used by Graphics API must match handle size used by the engine");

        const auto resource_native_type = resource_handle.GetNative();
        const auto vk_native_type       = reinterpret_cast<typename TVulkanHandle::NativeType>(resource_native_type);
        return TVulkanHandle{vk_native_type};
    }

    template <typename T>
    inline void ToVkConversionError(const String& type, const String& vk_type, T value)
    {
        Log::Fatal("Failed to convert Valo::{}::{} to vk::{}", type, value, vk_type);
    }

    template <typename T>
    inline void FromVkConversionError(const String& vk_type, const String& type, T vk_value)
    {
        Log::Fatal("Failed to convert vk::{}::{} to Valo::RHI::{}", vk_type, vk_value, type);
    }
}
