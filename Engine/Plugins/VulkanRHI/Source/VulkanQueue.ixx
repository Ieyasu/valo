module;

#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:Queue;

import Valo.Std;

export namespace Valo::RHI
{
    struct VulkanQueue
    {
        vk::Queue vk_queue{};
        UInt32    family_index{0};
    };
}
