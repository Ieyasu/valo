module;

#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:DescriptorPoolAllocator;

import Valo.RHI;
import Valo.Std;

export namespace Valo::RHI
{
    class VulkanDevice;

    class VulkanDescriptorPoolAllocator final
    {
    public:
        explicit VulkanDescriptorPoolAllocator(VulkanDevice& device);

        VulkanDescriptorPoolAllocator(const VulkanDescriptorPoolAllocator& other)            = delete;
        VulkanDescriptorPoolAllocator& operator=(const VulkanDescriptorPoolAllocator& other) = delete;

        VulkanDescriptorPoolAllocator(VulkanDescriptorPoolAllocator&& other)            = delete;
        VulkanDescriptorPoolAllocator& operator=(VulkanDescriptorPoolAllocator&& other) = delete;

        ~VulkanDescriptorPoolAllocator();

        void AddLayout(vk::DescriptorSetLayout layout, Span<const DescriptorSetLayoutBinding> bindings);

        void RemoveLayout(vk::DescriptorSetLayout layout);

        ResultValue<vk::Result, vk::DescriptorPool> GetOrCreatePool(vk::DescriptorSetLayout layout);

    private:
        VulkanDevice&              m_device;
        Vector<vk::DescriptorPool> m_descriptor_pools{};

        UnorderedMap<VkDescriptorSetLayout, Vector<vk::DescriptorPoolSize>> m_descriptor_pool_sizes{};
    };
}
