module;

#include <Valo/Vulkan.hpp>

#include <algorithm>
#include <ranges>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.RHI;
import Valo.Std;

import :Conversions;
import :DescriptorPoolAllocator;
import :Device;

namespace Valo::RHI
{
    VulkanDescriptorPoolAllocator::VulkanDescriptorPoolAllocator(VulkanDevice& device) :
        m_device(device)
    {
    }

    VulkanDescriptorPoolAllocator::~VulkanDescriptorPoolAllocator()
    {
        for (const auto pool : m_descriptor_pools)
        {
            m_device.GetVkDevice().destroyDescriptorPool(pool);
        }
    }

    void VulkanDescriptorPoolAllocator::AddLayout(
        vk::DescriptorSetLayout                layout,
        Span<const DescriptorSetLayoutBinding> bindings)
    {
        auto& pool_sizes = m_descriptor_pool_sizes[static_cast<VkDescriptorSetLayout>(layout)];
        pool_sizes.clear();
        pool_sizes.reserve(bindings.size());

        // Get the pool size for each descriptor type
        for (const auto& binding : bindings)
        {
            const auto predicate = [&binding](const vk::DescriptorPoolSize& pool_size)
            {
                return pool_size.type == ToVkDescriptorType(binding.type);
            };

            auto it = std::ranges::find_if(pool_sizes, predicate);
            if (it != pool_sizes.end())
            {
                it->descriptorCount += 1;
            }
            else
            {
                auto& pool_size           = pool_sizes.emplace_back();
                pool_size.type            = ToVkDescriptorType(binding.type);
                pool_size.descriptorCount = 1;
            }
        }
    }

    void VulkanDescriptorPoolAllocator::RemoveLayout(vk::DescriptorSetLayout layout)
    {
        m_descriptor_pool_sizes.erase(layout);
    }

    ResultValue<vk::Result, vk::DescriptorPool> VulkanDescriptorPoolAllocator::GetOrCreatePool(
        vk::DescriptorSetLayout layout)
    {
        const auto& pool_sizes = m_descriptor_pool_sizes[static_cast<VkDescriptorSetLayout>(layout)];

        auto pool_info = vk::DescriptorPoolCreateInfo{};
        pool_info.setPoolSizes(pool_sizes);
        pool_info.maxSets = 1;

        const auto [result, pool] = m_device.GetVkDevice().createDescriptorPool(pool_info);
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createDescriptorPool() failed ({})", result);
            return result;
        }

        m_descriptor_pools.push_back(pool);
        return pool;
    }
}
