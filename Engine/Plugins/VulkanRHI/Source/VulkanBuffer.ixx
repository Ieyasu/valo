module;

#include <Valo/Vulkan.hpp>

#include <vk_mem_alloc.h>

export module Valo.VulkanRHI:Buffer;

export namespace Valo::RHI
{
    struct VulkanBuffer final
    {
        vk::Buffer    vk_buffer{nullptr};
        VmaAllocation vma_allocation{nullptr};
    };
}
