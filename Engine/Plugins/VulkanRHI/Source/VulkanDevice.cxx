module;

#include <Valo/Profile.hpp>
#include <Valo/Vulkan.hpp>

#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.Profiler;
import Valo.RHI;
import Valo.Std;
import Valo.Window;

import :CommandBuffer;
import :Conversions;
import :DebugUtils;
import :DescriptorPoolAllocator;
import :Instance;
import :Swapchain;

namespace Valo::RHI
{
    VulkanDevice::VulkanDevice(
        const VulkanInstance& instance,
        vk::Device            device,
        vk::PhysicalDevice    physical_device,
        VulkanQueue           compute_queue,
        VulkanQueue           graphics_queue,
        VulkanQueue           transfer_queue,
        VmaAllocator          vma_allocator) :
        m_instance(instance),
        m_vk_device(device),
        m_vk_physical_device(physical_device),
        m_compute_queue(compute_queue),
        m_graphics_queue(graphics_queue),
        m_transfer_queue(transfer_queue),
        m_vma_allocator(vma_allocator),
        m_descriptor_pool_allocator(new VulkanDescriptorPoolAllocator(*this))
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        const auto device_name = StringFunc::Format("Device ({})", physical_device.getProperties().deviceName.data());
        SetName(device_name);
        SetObjectName(compute_queue.vk_queue, "Compute Queue");
        SetObjectName(graphics_queue.vk_queue, "Graphics Queue");
        SetObjectName(transfer_queue.vk_queue, "Transfer Queue");

        // Get device capabilities and limits
        m_capabilities.ray_queries = false;
        m_capabilities.ray_tracing = false;

        const auto& properties                       = physical_device.getProperties();
        m_limits.max_push_constants_size             = properties.limits.maxPushConstantsSize;
        m_limits.min_memory_map_alignment            = properties.limits.minMemoryMapAlignment;
        m_limits.min_storage_buffer_offset_alignment = properties.limits.minStorageBufferOffsetAlignment;
        m_limits.min_uniform_buffer_offset_alignment = properties.limits.minUniformBufferOffsetAlignment;
    }

    VulkanDevice::~VulkanDevice()
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        m_descriptor_pool_allocator.reset();
        m_command_pools.clear();
        m_swapchains.clear();
        m_vk_device.destroy();
    }

    vk::Device VulkanDevice::GetVkDevice() const
    {
        return m_vk_device;
    }

    vk::PhysicalDevice VulkanDevice::GetVkPhysicalDevice() const
    {
        return m_vk_physical_device;
    }

    StringView VulkanDevice::GetName()
    {
        return m_name;
    }

    void VulkanDevice::SetName(String name)
    {
        m_name = Memory::Move(name);
        SetObjectName(m_vk_device, m_name);
    }

    DeviceCapabilities VulkanDevice::GetCapabilities() const
    {
        return m_capabilities;
    }

    DeviceLimits VulkanDevice::GetLimits() const
    {
        return m_limits;
    }

    CommandQueueHandle VulkanDevice::GetComputeQueue() const
    {
        const auto native_queue = reinterpret_cast<CommandQueueHandle::NativeType>(&m_compute_queue);
        return CommandQueueHandle(native_queue);
    }

    CommandQueueHandle VulkanDevice::GetGraphicsQueue() const
    {
        const auto native_queue = reinterpret_cast<CommandQueueHandle::NativeType>(&m_graphics_queue);
        return CommandQueueHandle(native_queue);
    }

    CommandQueueHandle VulkanDevice::GetTransferQueue() const
    {
        const auto native_queue = reinterpret_cast<CommandQueueHandle::NativeType>(&m_transfer_queue);
        return CommandQueueHandle(native_queue);
    }

    ResultCode VulkanDevice::WaitIdle() const
    {
        const auto result = m_vk_device.waitIdle();
        return FromVkResultCode(result);
    }

    void VulkanDevice::SetObjectName(BaseResourceHandle handle, StringView name)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        switch (handle.GetType())
        {
        case ResourceType::Buffer:
        {
            const auto buffer = m_buffers.Get(handle.GetNative());
            SetObjectName(buffer.vk_buffer, name);
            if (buffer.vma_allocation->GetType() == VmaAllocation_T::ALLOCATION_TYPE_DEDICATED)
            {
                SetObjectName(vk::DeviceMemory(buffer.vma_allocation->GetMemory()), name);
            }
            break;
        }
        case ResourceType::Image:
        {
            const auto image = m_images.Get(handle.GetNative());
            SetObjectName(image.vk_image, name);
            SetObjectName(image.vk_image_view, name);
            if (image.vma_allocation->GetType() == VmaAllocation_T::ALLOCATION_TYPE_DEDICATED)
            {
                SetObjectName(vk::DeviceMemory(image.vma_allocation->GetMemory()), name);
            }
            break;
        }
        case ResourceType::CommandPool:
        {
            const auto* native_pool = reinterpret_cast<VulkanCommandPool*>(handle.GetNative());
            SetObjectName(native_pool->GetVkPool(), name);
            break;
        }
        default:
        {
            SetObjectName(handle.GetNative(), ToVkObjectType(handle.GetType()), name);
            break;
        }
        }
    }

    Result<void*> VulkanDevice::MapBuffer(BufferHandle buffer) const
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        void*       memory{nullptr};
        const auto& native_buffer = m_buffers.Get(buffer.GetNative());
        const auto  vma_result    = vmaMapMemory(m_vma_allocator, native_buffer.vma_allocation, &memory);
        const auto  result        = static_cast<vk::Result>(vma_result);
        if (result != vk::Result::eSuccess)
        {
            return FromVkResultCode(result);
        }

        return memory;
    }

    void VulkanDevice::UnmapBuffer(BufferHandle buffer) const
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto& native_buffer = m_buffers.Get(buffer.GetNative());
        vmaUnmapMemory(m_vma_allocator, native_buffer.vma_allocation);
    }

    Result<SwapchainInfo> VulkanDevice::AddWindow(WindowHandle window)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        auto [vk_result, swapchain] = VulkanSwapchain::Create(m_instance, *this, window);
        if (vk_result != vk::Result::eSuccess)
        {
            Log::Error("Failed to create swapchain");
            return FromVkResultCode(vk_result);
        }

        m_swapchains.push_back(Memory::Move(swapchain));
        return m_swapchains.back()->GetInfo();
    }

    void VulkanDevice::RemoveWindow(WindowHandle window)
    {
        VALO_PROFILE_SCOPED_FUNCTION(RHI);

        // TODO
        (void)window;
    }

    Result<BufferHandle> VulkanDevice::CreateBuffer(const BufferDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto buffer_info                  = vk::BufferCreateInfo{};
        buffer_info.usage                 = ToVkBufferUsageFlags(descriptor.usage);
        buffer_info.size                  = descriptor.size;
        buffer_info.sharingMode           = vk::SharingMode::eExclusive;
        buffer_info.pQueueFamilyIndices   = nullptr;
        buffer_info.queueFamilyIndexCount = 0;

        // Configure optimal allocation type
        VmaAllocationCreateInfo allocation_info = {};

        allocation_info.usage = VMA_MEMORY_USAGE_AUTO;

        switch (descriptor.access)
        {
        case BufferHostAccess::FastSequential:
            allocation_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT
                | VMA_ALLOCATION_CREATE_HOST_ACCESS_ALLOW_TRANSFER_INSTEAD_BIT;
            break;
        case BufferHostAccess::RequiredSequential:
            allocation_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT;
            break;
        case BufferHostAccess::RequiredRandom:
            allocation_info.flags = VMA_ALLOCATION_CREATE_HOST_ACCESS_RANDOM_BIT;
            break;
        case BufferHostAccess::None:
            allocation_info.flags = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT;
            break;
        case BufferHostAccess::Auto:
            break;
        default:
            Log::Fatal("Failed to create buffer: unkown BufferHostAccess flags");
            return ResultCode::ErrorUnknown;
        }

        // Create buffer using VMA
        auto native_buffer  = m_buffers.Emplace();
        auto vk_buffer      = VkBuffer();                       // NOLINT
        auto vk_buffer_info = VkBufferCreateInfo(buffer_info);  // NOLINT

        const auto vma_result = vmaCreateBuffer(
            m_vma_allocator,
            &vk_buffer_info,
            &allocation_info,
            &vk_buffer,
            &native_buffer->vma_allocation,
            nullptr);

        const auto result = static_cast<vk::Result>(vma_result);
        if (result != vk::Result::eSuccess)
        {
            DestroyBuffer(BufferHandle(native_buffer.GetId()));
            Log::Error("vk::Device::createBuffer() failed ({})", result);
            return FromVkResultCode(result);
        }
        native_buffer->vk_buffer = vk::Buffer(vk_buffer);

        const auto* alloc = native_buffer->vma_allocation;
        SetObjectName(
            vk::DeviceMemory(alloc->GetMemory()),
            StringFunc::Format("Memory (offset = {}, size = {})", alloc->GetOffset(), alloc->GetSize()));

        return BufferHandle(native_buffer.GetId());
    }

    void VulkanDevice::DestroyBuffer(BufferHandle buffer)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto& native_buffer = m_buffers.Get(buffer.GetNative());

        if (native_buffer.vma_allocation != nullptr)
        {
            vmaDestroyBuffer(
                m_vma_allocator,
                static_cast<VkBuffer>(native_buffer.vk_buffer),
                native_buffer.vma_allocation);
        }

        m_buffers.Erase(buffer.GetNative());
    }

    Result<ImageHandle> VulkanDevice::CreateImage(const ImageDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        // Create image
        auto image_info                  = vk::ImageCreateInfo{};
        image_info.format                = ToVkFormat(descriptor.format);
        image_info.usage                 = ToVkImageUsageFlags(descriptor.usage);
        image_info.arrayLayers           = descriptor.array_layer_count;
        image_info.initialLayout         = vk::ImageLayout::eUndefined;
        image_info.mipLevels             = descriptor.mip_level_count;
        image_info.samples               = vk::SampleCountFlagBits::e1;
        image_info.tiling                = vk::ImageTiling::eOptimal;
        image_info.sharingMode           = vk::SharingMode::eExclusive;
        image_info.imageType             = ToVkImageType(descriptor.dimensions);
        image_info.pQueueFamilyIndices   = nullptr;
        image_info.queueFamilyIndexCount = 0;
        image_info.extent                = vk::Extent3D{
            static_cast<UInt32>(descriptor.width),
            static_cast<UInt32>(descriptor.height),
            static_cast<UInt32>(descriptor.depth)};

        // Allocate image using VMA
        VmaAllocationCreateInfo allocation_info = {};
        allocation_info.usage                   = VMA_MEMORY_USAGE_AUTO;

        auto native_image  = m_images.Emplace();
        auto vk_image      = VkImage();                      // NOLINT
        auto vk_image_info = VkImageCreateInfo(image_info);  // NOLINT

        const auto vma_result = vmaCreateImage(
            m_vma_allocator,
            &vk_image_info,
            &allocation_info,
            &vk_image,
            &native_image->vma_allocation,
            nullptr);

        const auto image_result = static_cast<vk::Result>(vma_result);
        if (image_result != vk::Result::eSuccess)
        {
            // Make sure to destroy the already created image if something went wrong
            DestroyImage(ImageHandle(native_image.GetId()));
            Log::Error("vmaCreateImage() failed ({})", image_result);
            return FromVkResultCode(image_result);
        }
        native_image->vk_image = vk::Image(vk_image);

        const auto* alloc = native_image->vma_allocation;
        SetObjectName(
            vk::DeviceMemory(alloc->GetMemory()),
            StringFunc::Format("Memory (offset = {}, size = {})", alloc->GetOffset(), alloc->GetSize()));

        // Create image view
        auto image_view_info                            = vk::ImageViewCreateInfo{};
        image_view_info.image                           = native_image->vk_image;
        image_view_info.format                          = ToVkFormat(descriptor.format);
        image_view_info.viewType                        = ToVkImageViewType(descriptor.dimensions);
        image_view_info.components.r                    = vk::ComponentSwizzle::eIdentity;
        image_view_info.components.g                    = vk::ComponentSwizzle::eIdentity;
        image_view_info.components.b                    = vk::ComponentSwizzle::eIdentity;
        image_view_info.components.a                    = vk::ComponentSwizzle::eIdentity;
        image_view_info.subresourceRange.aspectMask     = ToVkAspectFlags(descriptor.format);
        image_view_info.subresourceRange.baseMipLevel   = 0;
        image_view_info.subresourceRange.levelCount     = descriptor.mip_level_count;
        image_view_info.subresourceRange.baseArrayLayer = 0;
        image_view_info.subresourceRange.layerCount     = descriptor.array_layer_count;

        const auto [image_view_result, image_view] = m_vk_device.createImageView(image_view_info);
        if (image_view_result != vk::Result::eSuccess)
        {
            // Make sure to destroy the already created image if something went wrong
            DestroyImage(ImageHandle(native_image.GetId()));
            Log::Error("vk::Device::createImageView() failed ({})", image_view_result);
            return FromVkResultCode(image_view_result);
        }
        native_image->vk_image_view = image_view;

        return ImageHandle(native_image.GetId());
    }

    void VulkanDevice::DestroyImage(ImageHandle image)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto& vulkan_image = m_images.Get(image.GetNative());

        if (vulkan_image.vk_image_view)
        {
            m_vk_device.destroyImageView(vulkan_image.vk_image_view);
        }

        if (vulkan_image.vma_allocation != nullptr)
        {
            vmaDestroyImage(m_vma_allocator, static_cast<VkImage>(vulkan_image.vk_image), vulkan_image.vma_allocation);
        }

        m_images.Erase(image.GetNative());
    }

    Result<SamplerHandle> VulkanDevice::CreateSampler(const SamplerDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_sampler_info = vk::SamplerCreateInfo{
            .magFilter        = ToVkFilter(descriptor.mag_filter),
            .minFilter        = ToVkFilter(descriptor.min_filter),
            .mipmapMode       = ToVkSamplerMipmapMode(descriptor.mipmap_mode),
            .addressModeU     = ToVkSamplerAddressMode(descriptor.address_mode_u),
            .addressModeV     = ToVkSamplerAddressMode(descriptor.address_mode_v),
            .addressModeW     = ToVkSamplerAddressMode(descriptor.address_mode_w),
            .mipLodBias       = descriptor.mip_lod_bias,
            .anisotropyEnable = descriptor.anisotropy_enabled,
            .maxAnisotropy    = descriptor.max_anisotropy,
            .minLod           = descriptor.min_lod,
            .maxLod           = descriptor.max_lod,
        };

        const auto [vk_result, vk_sampler] = m_vk_device.createSampler(vk_sampler_info);
        if (vk_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createSampler() failed ({})", vk_result);
            return FromVkResultCode(vk_result);
        }

        return ToResourceHandle<SamplerHandle>(vk_sampler);
    }

    void VulkanDevice::DestroySampler(SamplerHandle sampler)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_sampler = ToVkHandle<vk::Sampler>(sampler);
        m_vk_device.destroySampler(vk_sampler);
    }

    Result<DescriptorSetHandle> VulkanDevice::CreateDescriptorSet(const DescriptorSetDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_layout = ToVkHandle<vk::DescriptorSetLayout>(descriptor.layout);

        const auto [vk_pool_result, vk_pool] = m_descriptor_pool_allocator->GetOrCreatePool(vk_layout);
        if (vk_pool_result != vk::Result::eSuccess)
        {
            return FromVkResultCode(vk_pool_result);
        }

        auto alloc_info               = vk::DescriptorSetAllocateInfo{};
        alloc_info.descriptorPool     = vk_pool;
        alloc_info.descriptorSetCount = 1;
        alloc_info.pSetLayouts        = &vk_layout;

        const auto [vk_result, vk_set] = m_vk_device.allocateDescriptorSets(alloc_info);
        if (vk_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::allocateDescriptorSets() failed ({})", vk_result);
            return FromVkResultCode(vk_result);
        }

        return ToResourceHandle<DescriptorSetHandle>(vk_set.front());
    }

    void VulkanDevice::DestroyDescriptorSet(DescriptorSetHandle set)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        (void)set;
        // TODO: Destroy descriptor pool once all descriptors are destroyed
    }

    void VulkanDevice::UpdateDescriptorSets(Span<const DescriptorSetUpdate> updates)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto writes       = Vector<vk::WriteDescriptorSet>(updates.size());
        auto buffer_infos = Vector<vk::DescriptorBufferInfo>(updates.size());
        auto image_infos  = Vector<vk::DescriptorImageInfo>(updates.size());
        for (size_t i = 0; i < writes.size(); ++i)
        {
            const auto& update = updates[i];
            auto&       write  = writes[i];

            write.descriptorType  = ToVkDescriptorType(update.descriptor_type);
            write.descriptorCount = update.descriptor_count;
            write.dstBinding      = update.binding;
            write.dstSet          = ToVkHandle<vk::DescriptorSet>(update.set);
            write.dstArrayElement = update.array_index;

            if (update.buffer.IsValid())
            {
                const auto buffer      = m_buffers.Get(update.buffer.GetNative());
                buffer_infos[i].buffer = buffer.vk_buffer;
                buffer_infos[i].offset = update.buffer_offset;
                buffer_infos[i].range  = update.buffer_range;
                write.pBufferInfo      = &buffer_infos[i];
            }

            if (update.image.IsValid())
            {
                const auto image           = m_images.Get(update.image.GetNative());
                const auto vk_sampler      = ToVkHandle<vk::Sampler>(update.sampler);
                image_infos[i].imageView   = image.vk_image_view;
                image_infos[i].imageLayout = ToVkImageLayout(update.descriptor_type);
                image_infos[i].sampler     = vk_sampler;
                write.pImageInfo           = &image_infos[i];
            }
        }

        m_vk_device.updateDescriptorSets(writes, {});
    }

    Result<DescriptorSetLayoutHandle> VulkanDevice::CreateDescriptorSetLayout(
        const DescriptorSetLayoutDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto bindings = Vector<vk::DescriptorSetLayoutBinding>(descriptor.bindings.size());
        for (size_t i = 0; i < descriptor.bindings.size(); ++i)
        {
            auto binding = descriptor.bindings[i];

            auto& vk_binding              = bindings[i];
            vk_binding.binding            = binding.binding;
            vk_binding.descriptorCount    = binding.descriptor_count;
            vk_binding.descriptorType     = ToVkDescriptorType(binding.type);
            vk_binding.stageFlags         = ToVkShaderStageFlags(binding.stage);
            vk_binding.pImmutableSamplers = nullptr;
        }

        auto vk_layout_info         = vk::DescriptorSetLayoutCreateInfo{};
        vk_layout_info.pBindings    = bindings.data();
        vk_layout_info.bindingCount = static_cast<UInt32>(bindings.size());

        const auto [vk_result, vk_layout] = m_vk_device.createDescriptorSetLayout(vk_layout_info);
        if (vk_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createDescriptorSetLayout() failed ({})", vk_result);
            return FromVkResultCode(vk_result);
        }

        m_descriptor_pool_allocator->AddLayout(vk_layout, descriptor.bindings);
        return ToResourceHandle<DescriptorSetLayoutHandle>(vk_layout);
    }

    void VulkanDevice::DestroyDescriptorSetLayout(DescriptorSetLayoutHandle layout)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_layout = ToVkHandle<vk::DescriptorSetLayout>(layout);
        m_descriptor_pool_allocator->RemoveLayout(vk_layout);
        m_vk_device.destroyDescriptorSetLayout(vk_layout);
    }

    Result<FramebufferHandle> VulkanDevice::CreateFramebuffer(const FramebufferDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto attachments = Vector<vk::ImageView>(descriptor.attachments.size());
        for (size_t i = 0; i < attachments.size(); ++i)
        {
            attachments[i] = m_images.Get(descriptor.attachments[i].GetNative()).vk_image_view;
        }

        auto framebuffer_info       = vk::FramebufferCreateInfo();
        framebuffer_info.width      = descriptor.width;
        framebuffer_info.height     = descriptor.height;
        framebuffer_info.layers     = descriptor.layers;
        framebuffer_info.renderPass = ToVkHandle<vk::RenderPass>(descriptor.render_pass);
        framebuffer_info.setAttachments(attachments);

        const auto [result, framebuffer] = m_vk_device.createFramebuffer(framebuffer_info);
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createFramebuffer() failed ({})", result);
            return FromVkResultCode(result);
        }

        return ToResourceHandle<FramebufferHandle>(framebuffer);
    }

    void VulkanDevice::DestroyFramebuffer(FramebufferHandle framebuffer)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_framebuffer = ToVkHandle<vk::Framebuffer>(framebuffer);
        m_vk_device.destroyFramebuffer(vk_framebuffer);
    }

    Result<RenderPassHandle> VulkanDevice::CreateRenderPass(const RenderPassDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        // Create attachments
        auto vk_attachments = Vector<vk::AttachmentDescription>(descriptor.attachments.size());
        for (size_t i = 0; i < descriptor.attachments.size(); ++i)
        {
            const auto& attachment = descriptor.attachments[i];

            auto& vk_attachment         = vk_attachments[i];
            vk_attachment.format        = ToVkFormat(attachment.format);
            vk_attachment.initialLayout = ToVkImageLayout(
                attachment.initial_usage_flags,
                attachment.initial_access_flags,
                attachment.format);
            vk_attachment.finalLayout = ToVkImageLayout(
                attachment.final_usage_flags,
                attachment.final_access_flags,
                attachment.format);
            vk_attachment.loadOp         = ToVkAttachmentLoadOp(attachment.load_op);
            vk_attachment.storeOp        = ToVkAttachmentStoreOp(attachment.store_op);
            vk_attachment.stencilLoadOp  = ToVkAttachmentLoadOp(attachment.stencil_load_op);
            vk_attachment.stencilStoreOp = ToVkAttachmentStoreOp(attachment.stencil_store_op);
            vk_attachment.samples        = vk::SampleCountFlagBits::e1;
        }

        // Create subpasses with references to the attachments
        auto vk_subpasses                     = Vector<vk::SubpassDescription>(descriptor.subpasses.size());
        auto vk_color_refs_per_subpass        = Vector<Vector<vk::AttachmentReference>>(descriptor.subpasses.size());
        auto vk_input_refs_per_subpass        = Vector<Vector<vk::AttachmentReference>>(descriptor.subpasses.size());
        auto vk_depth_stencil_ref_per_subpass = Vector<vk::AttachmentReference>(descriptor.subpasses.size());
        for (UInt32 i = 0; i < descriptor.subpasses.size(); ++i)
        {
            const auto& subpass = descriptor.subpasses[i];

            auto& vk_subpass = vk_subpasses[i];

            // Create reference to depth attachment
            if (subpass.depth_stencil_attachment != nullptr)
            {
                const auto& depth_stencil_ref = *subpass.depth_stencil_attachment;

                auto& vk_depth_stencil_ref      = vk_depth_stencil_ref_per_subpass[i];
                vk_depth_stencil_ref.attachment = depth_stencil_ref.attachment;
                vk_depth_stencil_ref.layout     = ToVkImageLayout(
                    ImageUsage::DepthStencilAttachment,
                    depth_stencil_ref.access_flags,
                    descriptor.attachments[depth_stencil_ref.attachment].format);
                vk_subpass.pDepthStencilAttachment = &vk_depth_stencil_ref_per_subpass[i];
            }

            // Create references to color attachments
            auto& vk_color_refs = vk_color_refs_per_subpass[i];
            vk_color_refs.resize(subpass.color_attachments.size());
            for (size_t j = 0; j < subpass.color_attachments.size(); ++j)
            {
                const auto& color_ref = subpass.color_attachments[j];

                auto& vk_color_ref      = vk_color_refs[j];
                vk_color_ref.attachment = color_ref.attachment;
                vk_color_ref.layout     = ToVkImageLayout(
                    ImageUsage::ColorAttachment,
                    color_ref.access_flags,
                    descriptor.attachments[color_ref.attachment].format);
            }
            vk_subpass.setColorAttachments(vk_color_refs);

            // Create references to input attachments
            auto& vk_input_refs = vk_input_refs_per_subpass[i];
            vk_input_refs.resize(subpass.input_attachments.size());
            for (size_t j = 0; j < subpass.input_attachments.size(); ++j)
            {
                const auto& input_ref = subpass.input_attachments[j];

                auto& vk_input_ref      = vk_input_refs[j];
                vk_input_ref.attachment = input_ref.attachment;
                vk_input_ref.layout     = ToVkImageLayout(
                    ImageUsage::InputAttachment,
                    input_ref.access_flags,
                    descriptor.attachments[input_ref.attachment].format);
            }
            vk_subpass.setInputAttachments(vk_input_refs);
        }

        // TODO: Create dependencies between subpasses
        auto vk_subpass_dependencies = Vector<vk::SubpassDependency>();
        // for (size_t i = 0; i < descriptor.subpass_dependencies.size(); ++i)
        // {
        //     const auto& subpass_dependency = descriptor.subpass_dependencies[i];
        // auto& vk_subpass_dependency         = vk_subpass_dependencies[i];
        // vk_subpass_dependency.srcSubpass    = subpass_dependency.dst_subpass;
        // vk_subpass_dependency.dstSubpass    = subpass_dependency.src_subpass;
        // vk_subpass_dependency.srcStageMask  = to_vk_shader_stage_flags(subpass_dependency.src_stage_mask);
        // vk_subpass_dependency.dstStageMask  = to_vk_shader_stage_flags(subpass_dependency.dst_stage_mask);
        // vk_subpass_dependency.srcAccessMask = to_vk_access_flags(subpass_dependency.src_access_mask);
        // vk_subpass_dependency.dstAccessMask = to_vk_access_flags(subpass_dependency.dst_access_mask);
        // }

        auto vk_render_pass_info = vk::RenderPassCreateInfo{};
        vk_render_pass_info.setDependencies(vk_subpass_dependencies);
        vk_render_pass_info.setSubpasses(vk_subpasses);
        vk_render_pass_info.setAttachments(vk_attachments);

        auto [result, render_pass] = m_vk_device.createRenderPass(vk_render_pass_info);
        if (result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createRenderPass() failed ({})", result);
            return FromVkResultCode(result);
        }

        return ToResourceHandle<RenderPassHandle>(render_pass);
    }

    void VulkanDevice::DestroyRenderPass(RenderPassHandle render_pass)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_render_pass = ToVkHandle<vk::RenderPass>(render_pass);
        m_vk_device.destroyRenderPass(vk_render_pass);
    }

    Result<PipelineLayoutHandle> VulkanDevice::CreatePipelineLayout(const PipelineLayoutDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_push_constant_ranges = Vector<vk::PushConstantRange>(descriptor.push_constant_ranges.size());
        for (size_t i = 0; i < descriptor.push_constant_ranges.size(); ++i)
        {
            auto  range_info                  = descriptor.push_constant_ranges[i];
            auto& vk_push_constant_range      = vk_push_constant_ranges[i];
            vk_push_constant_range.stageFlags = ToVkShaderStageFlags(range_info.stage);
            vk_push_constant_range.offset     = range_info.offset;
            vk_push_constant_range.size       = range_info.size;
        }

        auto vk_descriptor_layouts = Vector<vk::DescriptorSetLayout>(descriptor.layouts.size());
        for (size_t i = 0; i < vk_descriptor_layouts.size(); ++i)
        {
            vk_descriptor_layouts[i] = ToVkHandle<vk::DescriptorSetLayout>(descriptor.layouts[i]);
        }

        auto vk_layout_info                   = vk::PipelineLayoutCreateInfo{};
        vk_layout_info.pSetLayouts            = vk_descriptor_layouts.data();
        vk_layout_info.setLayoutCount         = static_cast<UInt32>(vk_descriptor_layouts.size());
        vk_layout_info.pPushConstantRanges    = vk_push_constant_ranges.data();
        vk_layout_info.pushConstantRangeCount = static_cast<UInt32>(vk_push_constant_ranges.size());

        const auto [vk_result, vk_layout] = m_vk_device.createPipelineLayout(vk_layout_info);
        if (vk_result != vk::Result::eSuccess)
        {
            return FromVkResultCode(vk_result);
        }

        return ToResourceHandle<PipelineLayoutHandle>(vk_layout);
    }

    void VulkanDevice::DestroyPipelineLayout(PipelineLayoutHandle layout)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_layout = ToVkHandle<vk::PipelineLayout>(layout);
        m_vk_device.destroyPipelineLayout(vk_layout);
    }

    Result<ComputePipelineHandle> VulkanDevice::CreateComputePipeline(const ComputePipelineDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto& shader_stage = descriptor.shader_stage;

        // Create shader stage
        auto vk_module_info     = vk::ShaderModuleCreateInfo{};
        vk_module_info.pCode    = reinterpret_cast<const UInt32*>(shader_stage.binary.data());
        vk_module_info.codeSize = static_cast<UInt32>(shader_stage.binary.size());

        const auto [vk_module_result, vk_module] = m_vk_device.createShaderModule(vk_module_info, nullptr);
        if (vk_module_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createShaderModule() failed ({})", vk_module_result);
            return FromVkResultCode(vk_module_result);
        }

        auto vk_stage_info   = vk::PipelineShaderStageCreateInfo{};
        vk_stage_info.stage  = ToVkShaderStageFlagBits(shader_stage.stage);
        vk_stage_info.module = vk_module;
        vk_stage_info.pName  = shader_stage.name.data();

        // Create pipeline
        auto vk_pipeline_info   = vk::ComputePipelineCreateInfo{};
        vk_pipeline_info.layout = ToVkHandle<vk::PipelineLayout>(descriptor.layout);
        vk_pipeline_info.stage  = vk_stage_info;

        auto [vk_pipeline_result, vk_pipeline] = m_vk_device.createComputePipeline(nullptr, vk_pipeline_info);

        // Destroy module regardless whether we succeeded or not
        m_vk_device.destroyShaderModule(vk_pipeline_info.stage.module);

        if (vk_pipeline_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createComputePipeline() failed ({})", vk_pipeline_result);
            return FromVkResultCode(vk_pipeline_result);
        }

        return ToResourceHandle<ComputePipelineHandle>(vk_pipeline);
    }

    void VulkanDevice::DestroyComputePipeline(ComputePipelineHandle pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_pipeline = ToVkHandle<vk::Pipeline>(pipeline);
        m_vk_device.destroyPipeline(vk_pipeline);
    }

    Result<GraphicsPipelineHandle> VulkanDevice::CreateGraphicsPipeline(const GraphicsPipelineDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto vk_shader_stages = Vector<vk::PipelineShaderStageCreateInfo>(descriptor.shader_stages.size());
        for (size_t i = 0; i < descriptor.shader_stages.size(); ++i)
        {
            const auto& shader_stage = descriptor.shader_stages[i];

            const auto vk_module_info = vk::ShaderModuleCreateInfo{
                .codeSize = static_cast<UInt32>(shader_stage.binary.size()),
                .pCode    = reinterpret_cast<const UInt32*>(shader_stage.binary.data()),
            };

            const auto [vk_module_result, vk_module] = m_vk_device.createShaderModule(vk_module_info, nullptr);
            if (vk_module_result != vk::Result::eSuccess)
            {
                // Make sure to clear already created resources in case of error
                for (size_t j = 0; j < i; ++j)
                {
                    m_vk_device.destroyShaderModule(vk_shader_stages[j].module);
                }

                Log::Error("vk::Device::createShaderModule() failed ({})", vk_module_result);
                return FromVkResultCode(vk_module_result);
            }

            vk_shader_stages[i] = vk::PipelineShaderStageCreateInfo{
                .stage  = ToVkShaderStageFlagBits(shader_stage.stage),
                .module = vk_module,
                .pName  = shader_stage.name.data(),
            };
        }

        // Create viewport state
        auto vk_viewports = Vector<vk::Viewport>(descriptor.viewports.size());
        auto vk_scissors  = Vector<vk::Rect2D>(descriptor.viewports.size());
        for (size_t i = 0; i < descriptor.viewports.size(); ++i)
        {
            vk_viewports[i] = ToVkViewport(descriptor.viewports[i]);
            vk_scissors[i]  = ToVkScissor(descriptor.scissors[i]);
        }

        auto vk_viewport_state = vk::PipelineViewportStateCreateInfo{
            .viewportCount = static_cast<UInt32>(vk_viewports.size()),
            .pViewports    = vk_viewports.data(),
            .scissorCount  = static_cast<UInt32>(vk_scissors.size()),
            .pScissors     = vk_scissors.data(),
        };

        // Create dynamic states
        auto vk_dynamic_states = Vector<vk::DynamicState>(descriptor.dynamic_states.size());
        for (size_t i = 0; i < descriptor.dynamic_states.size(); ++i)
        {
            vk_dynamic_states[i] = ToVkDynamicState(descriptor.dynamic_states[i]);
        }

        const auto vk_dynamic_state = vk::PipelineDynamicStateCreateInfo{
            .dynamicStateCount = static_cast<UInt32>(vk_dynamic_states.size()),
            .pDynamicStates    = vk_dynamic_states.data(),
        };

        // Create MSAA state
        const auto vk_multisampling_state = vk::PipelineMultisampleStateCreateInfo{
            .rasterizationSamples  = vk::SampleCountFlagBits::e1,
            .sampleShadingEnable   = VK_FALSE,
            .pSampleMask           = nullptr,
            .alphaToCoverageEnable = VK_FALSE,
            .alphaToOneEnable      = VK_FALSE,
        };

        // Create input assembly
        const auto vk_input_assembly_state = vk::PipelineInputAssemblyStateCreateInfo{
            .topology               = ToVkPrimitiveTopology(descriptor.input_assembly_topology),
            .primitiveRestartEnable = VK_FALSE,
        };

        // Create depth stencil state
        const auto vk_depth_stencil_state = vk::PipelineDepthStencilStateCreateInfo{
            .depthTestEnable       = static_cast<vk::Bool32>(descriptor.depth_enable_test),
            .depthWriteEnable      = static_cast<vk::Bool32>(descriptor.depth_enable_write),
            .depthCompareOp        = ToVkCompareOp(descriptor.depth_compare_method),
            .depthBoundsTestEnable = VK_FALSE,
            .stencilTestEnable     = VK_FALSE,
        };

        // Create color blend state
        auto vk_color_blend = Vector<vk::PipelineColorBlendAttachmentState>(descriptor.color_blend_attachments.size());
        for (size_t i = 0; i < descriptor.color_blend_attachments.size(); ++i)
        {
            const auto& attachment = descriptor.color_blend_attachments[i];
            vk_color_blend[i]      = vk::PipelineColorBlendAttachmentState{
                     .blendEnable         = static_cast<vk::Bool32>(attachment.enable_blend),
                     .srcColorBlendFactor = ToVkBlendFactor(attachment.src_alpha_blend_factor),
                     .dstColorBlendFactor = ToVkBlendFactor(attachment.dst_alpha_blend_factor),
                     .colorBlendOp        = ToVkBlendOp(attachment.color_blend_op),
                     .srcAlphaBlendFactor = ToVkBlendFactor(attachment.src_alpha_blend_factor),
                     .dstAlphaBlendFactor = ToVkBlendFactor(attachment.dst_alpha_blend_factor),
                     .alphaBlendOp        = ToVkBlendOp(attachment.alpha_blend_op),
                     .colorWriteMask      = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG
                    | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA,
            };
        }

        const auto vk_color_blend_state = vk::PipelineColorBlendStateCreateInfo{
            .logicOpEnable   = static_cast<vk::Bool32>(descriptor.color_blend_enable_logic_op),
            .logicOp         = ToVkLogicOp(descriptor.color_blend_logic_op),
            .attachmentCount = static_cast<UInt32>(vk_color_blend.size()),
            .pAttachments    = vk_color_blend.data(),
            .blendConstants  = {{0.0f, 0.0f, 0.0f, 0.0f}},
        };

        // Create rasterization state
        const auto vk_rasterization_state = vk::PipelineRasterizationStateCreateInfo{
            .depthClampEnable        = static_cast<vk::Bool32>(descriptor.rasterizer_enable_depth_clamp),
            .rasterizerDiscardEnable = static_cast<vk::Bool32>(descriptor.rasterizer_enable_discard),
            .polygonMode             = ToVkPolygonMode(descriptor.rasterizer_polygon_mode),
            .cullMode                = ToVkCullMode(descriptor.rasterizer_cull_mode),
            .frontFace               = ToVkFrontFace(descriptor.rasterizer_winding),
            .depthBiasEnable         = VK_FALSE,
            .depthBiasConstantFactor = 0.0f,
            .depthBiasClamp          = 0.0f,
            .depthBiasSlopeFactor    = 0.0f,
            .lineWidth               = descriptor.rasterizer_line_width,
        };

        // Create vertex input state
        auto vk_bindings   = Vector<vk::VertexInputBindingDescription>(descriptor.vertex_attributes.size());
        auto vk_attributes = Vector<vk::VertexInputAttributeDescription>(descriptor.vertex_attributes.size());
        for (size_t i = 0; i < descriptor.vertex_attributes.size(); ++i)
        {
            const auto& attribute = descriptor.vertex_attributes[i];

            vk_bindings[i] = vk::VertexInputBindingDescription{
                .binding   = attribute.binding,
                .stride    = attribute.stride,
                .inputRate = vk::VertexInputRate::eVertex,
            };

            vk_attributes[i] = vk::VertexInputAttributeDescription{
                .location = attribute.location,
                .binding  = attribute.binding,
                .format   = ToVkFormat(attribute.format),
                .offset   = 0,
            };
        }

        const auto vk_vertex_input_state = vk::PipelineVertexInputStateCreateInfo{
            .vertexBindingDescriptionCount   = static_cast<UInt32>(vk_bindings.size()),
            .pVertexBindingDescriptions      = vk_bindings.data(),
            .vertexAttributeDescriptionCount = static_cast<UInt32>(vk_attributes.size()),
            .pVertexAttributeDescriptions    = vk_attributes.data(),
        };

        // Create the pipeline
        const auto vk_pipeline_info = vk::GraphicsPipelineCreateInfo{
            .stageCount          = static_cast<UInt32>(vk_shader_stages.size()),
            .pStages             = vk_shader_stages.data(),
            .pVertexInputState   = &vk_vertex_input_state,
            .pInputAssemblyState = &vk_input_assembly_state,
            .pViewportState      = &vk_viewport_state,
            .pRasterizationState = &vk_rasterization_state,
            .pMultisampleState   = &vk_multisampling_state,
            .pDepthStencilState  = &vk_depth_stencil_state,
            .pColorBlendState    = &vk_color_blend_state,
            .pDynamicState       = &vk_dynamic_state,
            .layout              = ToVkHandle<vk::PipelineLayout>(descriptor.layout),
            .renderPass          = ToVkHandle<vk::RenderPass>(descriptor.render_pass),
            .subpass             = descriptor.render_pass_subpass,
        };

        const auto [vk_pipeline_result, vk_pipeline] = m_vk_device.createGraphicsPipeline(nullptr, vk_pipeline_info);

        // Destroy modules regardless whether we succeeded or not
        for (size_t i = 0; i < descriptor.shader_stages.size(); ++i)
        {
            m_vk_device.destroyShaderModule(vk_pipeline_info.pStages[i].module);
        }

        if (vk_pipeline_result != vk::Result::eSuccess)
        {
            Log::Error("vk::Device::createGraphicsPipeline() failed ({})", vk_pipeline_result);
            return FromVkResultCode(vk_pipeline_result);
        }

        return ToResourceHandle<GraphicsPipelineHandle>(vk_pipeline);
    }

    void VulkanDevice::DestroyGraphicsPipeline(GraphicsPipelineHandle pipeline)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto vk_pipeline = ToVkHandle<vk::Pipeline>(pipeline);
        m_vk_device.destroyPipeline(vk_pipeline);
    }

    Result<CommandPoolHandle> VulkanDevice::CreateCommandPool(const CommandPoolDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto* native_queue   = reinterpret_cast<const VulkanQueue*>(descriptor.queue.GetNative());
        auto [result, native_pool] = VulkanCommandPool::Create(*this, *native_queue);
        if (result != vk::Result::eSuccess)
        {
            return FromVkResultCode(result);
        }
        m_command_pools.emplace_back(Memory::Move(native_pool));

        const auto resource_handle = reinterpret_cast<CommandPoolHandle::NativeType>(m_command_pools.back().get());
        return CommandPoolHandle(resource_handle);
    }

    ResultCode VulkanDevice::ResetCommandPool(CommandPoolHandle pool)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto*      native_pool = reinterpret_cast<VulkanCommandPool*>(pool.GetNative());
        const auto vk_result   = native_pool->Reset();
        return FromVkResultCode(vk_result);
    }

    void VulkanDevice::DestroyCommandPool(CommandPoolHandle pool)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        const auto* native_pool = reinterpret_cast<const VulkanCommandPool*>(pool.GetNative());
        for (size_t i = 0; i < m_command_pools.size(); ++i)
        {
            if (m_command_pools[i].get() == native_pool)
            {
                m_command_pools.erase(m_command_pools.begin() + i);
                break;
            }
        }
    }

    Result<CommandBufferHandle> VulkanDevice::CreateCommandBuffer(const CommandBufferDescriptor& descriptor)
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        auto* native_pool            = reinterpret_cast<VulkanCommandPool*>(descriptor.pool.GetNative());
        auto [result, native_buffer] = native_pool->CreateCommandBuffer();
        if (result != vk::Result::eSuccess)
        {
            return FromVkResultCode(result);
        }
        return CommandBufferHandle(native_buffer);
    }

    void VulkanDevice::SetObjectName(UInt64 handle, vk::ObjectType type, StringView name) const
    {
        VALO_PROFILE_SCOPED_FUNCTION_DETAILED(RHI);

        if (name.empty())
        {
            return;
        }

        const auto object_name = StringFunc::Format("{} [{}]", name, vk::to_string(type));
        const auto name_info   = vk::DebugUtilsObjectNameInfoEXT{
              .objectType   = type,
              .objectHandle = handle,
              .pObjectName  = object_name.c_str(),
        };

        const auto result = m_vk_device.setDebugUtilsObjectNameEXT(name_info);
        if (result != vk::Result::eSuccess)
        {
            Log::Warning("vk::Device::setDebugUtilsObjectNameEXT() failed ({})", result);
        }
    }
}
