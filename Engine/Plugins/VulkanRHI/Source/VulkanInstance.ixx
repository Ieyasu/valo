module;

#include <Valo/Macros.hpp>
#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:Instance;

import Valo.RHI;
import Valo.Std;

import :DebugUtils;
import :Device;

export namespace Valo::RHI
{
    class VulkanInstance final : public Instance
    {
    public:
        VulkanInstance(vk::Instance instance, UInt32 api_version);
        VulkanInstance(const VulkanInstance&)            = delete;
        VulkanInstance(VulkanInstance&&)                 = delete;
        VulkanInstance& operator=(const VulkanInstance&) = delete;
        VulkanInstance& operator=(VulkanInstance&&)      = delete;
        ~VulkanInstance() override;

        static Result<InstanceHandle> Create(const InstanceDescriptor& descriptor);

        vk::Instance GetVkInstance() const;

        Result<DeviceHandle> CreateDevice(const DeviceDescriptor& descriptor) override;

        void DestroyDevice(DeviceHandle device) override;

    private:
        inline static const String create_instance_error = "Failed to create Vulkan instance";
        inline static const String create_device_error   = "Failed to create Vulkan device";

        static UInt32         GetDefaultApiVersion();
        static Vector<String> GetInstanceLayers();
        static Vector<String> GetInstanceExtensions(const InstanceDescriptor& descriptor);
        static Vector<String> GetDeviceExtensions(const DeviceDescriptor& descriptor);

        static vk::Result ValidateInstanceLayers(const Vector<String>& layers);
        static vk::Result ValidateInstanceExtensions(const Vector<String>& extensions);
        static vk::Result ValidateDeviceExtensions(vk::PhysicalDevice physical_device, const Vector<String>& extensions);

        static Optional<UInt32> GetGraphicsQueue(vk::PhysicalDevice physical_device);
        static Optional<UInt32> GetComputeQueue(vk::PhysicalDevice physical_device);
        static Optional<UInt32> GetTransferQueue(vk::PhysicalDevice physical_device);
        static Optional<UInt32> GetQueueFamilyIndex(
            vk::QueueFlags     include_flags,
            vk::QueueFlags     exclude_flags,
            vk::PhysicalDevice physical_device);

        vk::Instance               m_vk_instance;
        UInt32                     m_api_version;
        vk::DebugUtilsMessengerEXT m_vk_debug_messenger;
    };
}

export namespace Valo::RHI
{
    extern "C"
    {
        extern VALO_LIBRARY_API Result<InstanceHandle> CreateInstance(const InstanceDescriptor& descriptor)
        {
            return VulkanInstance::Create(descriptor);
        }

        extern VALO_LIBRARY_API void DestroyInstance(InstanceHandle instance)
        {
            delete instance.Get();
        }
    }
}
