module;

#include <Valo/Macros.hpp>
#include <Valo/Vulkan.hpp>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.RHI;
import Valo.Std;

import :Conversions;

namespace Valo::RHI
{
    ResultCode FromVkResultCode(vk::Result value)
    {
        switch (value)
        {
        case vk::Result::eSuccess:
            return ResultCode::Success;
        case vk::Result::eErrorOutOfDeviceMemory:
            return ResultCode::ErrorOutOfDeviceMemory;
        case vk::Result::eErrorOutOfHostMemory:
            return ResultCode::ErrorOutOfHostMemory;
        case vk::Result::eErrorLayerNotPresent:
        case vk::Result::eErrorExtensionNotPresent:
        case vk::Result::eErrorFeatureNotPresent:
        case vk::Result::eErrorIncompatibleDriver:
            return ResultCode::ErrorUnsupported;
        default:
            return ResultCode::ErrorUnknown;
        }
    }

    vk::ObjectType ToVkObjectType(ResourceType value)
    {
        switch (value)
        {
        case ResourceType::Buffer:
            return vk::ObjectType::eBuffer;
        case ResourceType::Image:
            return vk::ObjectType::eImage;
        case ResourceType::Sampler:
            return vk::ObjectType::eSampler;
        case ResourceType::Framebuffer:
            return vk::ObjectType::eFramebuffer;
        case ResourceType::RenderPass:
            return vk::ObjectType::eRenderPass;
        case ResourceType::DescriptorSet:
            return vk::ObjectType::eDescriptorSet;
        case ResourceType::DescriptorSetLayout:
            return vk::ObjectType::eDescriptorSetLayout;
        case ResourceType::GraphicsPipeline:
        case ResourceType::ComputePipeline:
            return vk::ObjectType::ePipeline;
        case ResourceType::PipelineLayout:
            return vk::ObjectType::ePipelineLayout;
        case ResourceType::CommandPool:
            return vk::ObjectType::eCommandPool;
        case ResourceType::CommandQueue:
            return vk::ObjectType::eQueue;
        default:
            ToVkConversionError("ResourceType", "ObjectType", value);
            return vk::ObjectType::eUnknown;
        }
    }

    vk::ClearValue ToVkClearValue(const ClearValue& value)
    {
        auto vk_value = vk::ClearValue{};

        switch (value.type)
        {
        case ClearType::Color:
            vk_value.color.setFloat32({value.color[0], value.color[1], value.color[2], value.color[3]});
            break;
        case ClearType::DepthStencil:
            vk_value.setDepthStencil({value.depth, value.stencil});
            break;
        default:
            ToVkConversionError("ClearValue", "ClearValue", value.type);
            break;
        }

        return vk_value;
    }

    vk::Rect2D ToVkRect2D(const Rect2D& value)
    {
        auto vk_rect          = vk::Rect2D{};
        vk_rect.offset.x      = value.x;
        vk_rect.offset.y      = value.y;
        vk_rect.extent.width  = value.width;
        vk_rect.extent.height = value.height;
        return vk_rect;
    }

    vk::ShaderStageFlags ToVkShaderStageFlags(ShaderStageFlags flags)
    {
        auto vk_flags = vk::ShaderStageFlags{0};
        if (flags & ShaderStage::Compute)
        {
            vk_flags |= vk::ShaderStageFlagBits::eCompute;
        }
        if (flags & ShaderStage::Vertex)
        {
            vk_flags |= vk::ShaderStageFlagBits::eVertex;
        }
        if (flags & ShaderStage::Fragment)
        {
            vk_flags |= vk::ShaderStageFlagBits::eFragment;
        }
        if (flags & ShaderStage::Geometry)
        {
            vk_flags |= vk::ShaderStageFlagBits::eGeometry;
        }
        if (flags & ShaderStage::TessellationEvaluation)
        {
            vk_flags |= vk::ShaderStageFlagBits::eTessellationEvaluation;
        }
        if (flags & ShaderStage::TessellationControl)
        {
            vk_flags |= vk::ShaderStageFlagBits::eTessellationControl;
        }
        return vk_flags;
    }

    vk::AttachmentLoadOp ToVkAttachmentLoadOp(AttachmentLoadOp value)
    {
        switch (value)
        {
        case AttachmentLoadOp::Clear:
            return vk::AttachmentLoadOp::eClear;
        case AttachmentLoadOp::DontCare:
            return vk::AttachmentLoadOp::eDontCare;
        case AttachmentLoadOp::Load:
            return vk::AttachmentLoadOp::eLoad;
        default:
            ToVkConversionError("AttachmentLoadOp", "AttachmentLoadOp", value);
            return {};
        }
    }

    vk::AttachmentStoreOp ToVkAttachmentStoreOp(AttachmentStoreOp value)
    {
        switch (value)
        {
        case AttachmentStoreOp::DontCare:
            return vk::AttachmentStoreOp::eDontCare;
        case AttachmentStoreOp::Store:
            return vk::AttachmentStoreOp::eStore;
        default:
            ToVkConversionError("AttachmentStoreOp", "AttachmentStoreOp", value);
            return {};
        }
    }

    vk::BufferUsageFlags ToVkBufferUsageFlags(BufferUsageFlags flags)
    {
        auto vk_flags = vk::BufferUsageFlags{0};
        if (flags & BufferUsage::TransferDst)
        {
            vk_flags |= vk::BufferUsageFlagBits::eTransferDst;
        }
        if (flags & BufferUsage::TransferSrc)
        {
            vk_flags |= vk::BufferUsageFlagBits::eTransferSrc;
        }
        if (flags & BufferUsage::Index)
        {
            vk_flags |= vk::BufferUsageFlagBits::eIndexBuffer;
        }
        if (flags & BufferUsage::Indirect)
        {
            vk_flags |= vk::BufferUsageFlagBits::eIndirectBuffer;
        }
        if (flags & BufferUsage::Storage)
        {
            vk_flags |= vk::BufferUsageFlagBits::eStorageBuffer;
        }
        if (flags & BufferUsage::Uniform)
        {
            vk_flags |= vk::BufferUsageFlagBits::eUniformBuffer;
        }
        if (flags & BufferUsage::Vertex)
        {
            vk_flags |= vk::BufferUsageFlagBits::eVertexBuffer;
        }
        return vk_flags;
    }

    vk::ImageUsageFlags ToVkImageUsageFlags(ImageUsageFlags flags)
    {
        auto vk_flags = vk::ImageUsageFlags{0};
        if (flags & ImageUsage::TransferSrc)
        {
            vk_flags |= vk::ImageUsageFlagBits::eTransferSrc;
        }
        if (flags & ImageUsage::TransferDst)
        {
            vk_flags |= vk::ImageUsageFlagBits::eTransferDst;
        }
        if (flags & ImageUsage::ColorAttachment)
        {
            vk_flags |= vk::ImageUsageFlagBits::eColorAttachment;
        }
        if (flags & ImageUsage::InputAttachment)
        {
            vk_flags |= vk::ImageUsageFlagBits::eInputAttachment;
        }
        if (flags & ImageUsage::DepthStencilAttachment)
        {
            vk_flags |= vk::ImageUsageFlagBits::eDepthStencilAttachment;
        }
        if (flags & ImageUsage::Sampled)
        {
            vk_flags |= vk::ImageUsageFlagBits::eSampled;
        }
        if (flags & ImageUsage::Storage)
        {
            vk_flags |= vk::ImageUsageFlagBits::eStorage;
        }
        return vk_flags;
    }

    vk::ImageType ToVkImageType(ImageDimensions value)
    {
        switch (value)
        {
        case ImageDimensions::One:
            return vk::ImageType::e1D;
        case ImageDimensions::Two:
        case ImageDimensions::Cube:
        case ImageDimensions::Array1D:
            return vk::ImageType::e2D;
        case ImageDimensions::Three:
        case ImageDimensions::Array2D:
        case ImageDimensions::ArrayCube:
            return vk::ImageType::e3D;
        default:
            ToVkConversionError("ImageDimensions", "ImageType", value);
            return {};
        }
    }

    vk::ShaderStageFlagBits ToVkShaderStageFlagBits(ShaderStage value)
    {
        switch (value)
        {
        case ShaderStage::Compute:
            return vk::ShaderStageFlagBits::eCompute;
        case ShaderStage::Fragment:
            return vk::ShaderStageFlagBits::eFragment;
        case ShaderStage::Geometry:
            return vk::ShaderStageFlagBits::eGeometry;
        case ShaderStage::TessellationControl:
            return vk::ShaderStageFlagBits::eTessellationControl;
        case ShaderStage::TessellationEvaluation:
            return vk::ShaderStageFlagBits::eTessellationEvaluation;
        case ShaderStage::Vertex:
            return vk::ShaderStageFlagBits::eVertex;
        case ShaderStage::None:
        default:
            ToVkConversionError("ShaderStageFlags", "ShaderStage", value);
            return {};
        }
    }

    Format FromVkFormat(vk::Format value)
    {
        return static_cast<Format>(value);
    }

    vk::Format ToVkFormat(Format value)
    {
        return static_cast<vk::Format>(value);
    }

    vk::ImageAspectFlags ToVkAspectFlags(Format value)
    {
        if (IsDepthStencil(value))
        {
            return vk::ImageAspectFlagBits::eDepth;
        }
        return vk::ImageAspectFlagBits::eColor;
    }

    vk::DescriptorType ToVkDescriptorType(DescriptorType value)
    {
        switch (value)
        {
        case DescriptorType::CombinedImageSampler:
            return vk::DescriptorType::eCombinedImageSampler;
        case DescriptorType::SampledImage:
            return vk::DescriptorType::eSampledImage;
        case DescriptorType::Sampler:
            return vk::DescriptorType::eSampler;
        case DescriptorType::StorageBuffer:
            return vk::DescriptorType::eStorageBuffer;
        case DescriptorType::StorageImage:
            return vk::DescriptorType::eStorageImage;
        case DescriptorType::StorageBufferDynamic:
            return vk::DescriptorType::eStorageBufferDynamic;
        case DescriptorType::UniformBuffer:
            return vk::DescriptorType::eUniformBuffer;
        case DescriptorType::UniformBufferDynamic:
            return vk::DescriptorType::eUniformBufferDynamic;
        default:
            ToVkConversionError("DescriptorType", "DescriptorType", value);
            return {};
        }
    }

    vk::ImageViewType ToVkImageViewType(ImageDimensions value)
    {
        switch (value)
        {
        case ImageDimensions::One:
            return vk::ImageViewType::e1D;
        case ImageDimensions::Two:
            return vk::ImageViewType::e2D;
        case ImageDimensions::Three:
            return vk::ImageViewType::e3D;
        case ImageDimensions::Cube:
            return vk::ImageViewType::eCube;
        case ImageDimensions::Array1D:
            return vk::ImageViewType::e1DArray;
        case ImageDimensions::Array2D:
            return vk::ImageViewType::e2DArray;
        case ImageDimensions::ArrayCube:
            return vk::ImageViewType::eCubeArray;
        default:
            ToVkConversionError("ImageDimensions", "ImageViewType", value);
            return {};
        }
    }

    vk::IndexType ToVkIndexType(IndexFormat value)
    {
        switch (value)
        {
        case IndexFormat::UInt16:
            return vk::IndexType::eUint16;
        case IndexFormat::UInt32:
            return vk::IndexType::eUint32;
        default:
            ToVkConversionError("IndexFormat", "IndexType", value);
            return {};
        }
    }

    vk::PrimitiveTopology ToVkPrimitiveTopology(PrimitiveTopology value)
    {
        switch (value)
        {
        case PrimitiveTopology::PointList:
            return vk::PrimitiveTopology::ePointList;
        case PrimitiveTopology::LineList:
            return vk::PrimitiveTopology::eLineList;
        case PrimitiveTopology::TriangleList:
            return vk::PrimitiveTopology::eTriangleList;
        default:
            ToVkConversionError("PrimitiveTopology", "PrimitiveTopology", value);
            return {};
        }
    }

    vk::PolygonMode ToVkPolygonMode(PolygonMode value)
    {
        switch (value)
        {
        case PolygonMode::Fill:
            return vk::PolygonMode::eFill;
        case PolygonMode::Line:
            return vk::PolygonMode::eLine;
        case PolygonMode::Point:
            return vk::PolygonMode::ePoint;
        default:
            ToVkConversionError("PolygonMode", "PolgyonMode", value);
            return {};
        }
    }

    vk::CullModeFlagBits ToVkCullMode(CullMode value)
    {
        switch (value)
        {
        case CullMode::None:
            return vk::CullModeFlagBits::eNone;
        case CullMode::Front:
            return vk::CullModeFlagBits::eFront;
        case CullMode::Back:
            return vk::CullModeFlagBits::eBack;
        case CullMode::FrontAndBack:
            return vk::CullModeFlagBits::eFrontAndBack;
        default:
            ToVkConversionError("CullMode", "CullModeFlagBits", value);
            return {};
        }
    }

    vk::FrontFace ToVkFrontFace(Winding value)
    {
        switch (value)
        {
        case Winding::Clockwise:
            return vk::FrontFace::eClockwise;
        case Winding::CounterClockwise:
            return vk::FrontFace::eCounterClockwise;
        default:
            ToVkConversionError("Winding", "FrontFace", value);
            return {};
        }
    }

    vk::DynamicState ToVkDynamicState(DynamicState value)
    {
        switch (value)
        {
        case DynamicState::Scissors:
            return vk::DynamicState::eScissor;
        case DynamicState::Viewport:
            return vk::DynamicState::eViewport;
        default:
            ToVkConversionError("DynamicState", "DynamicState", value);
            return {};
        }
    }

    vk::CompareOp ToVkCompareOp(CompareOp value)
    {
        switch (value)
        {
        case CompareOp::Always:
            return vk::CompareOp::eAlways;
        case CompareOp::Equal:
            return vk::CompareOp::eEqual;
        case CompareOp::Greater:
            return vk::CompareOp::eGreater;
        case CompareOp::GreaterOrEqual:
            return vk::CompareOp::eGreaterOrEqual;
        case CompareOp::Less:
            return vk::CompareOp::eLess;
        case CompareOp::LessOrEqual:
            return vk::CompareOp::eLessOrEqual;
        case CompareOp::Never:
            return vk::CompareOp::eNever;
        case CompareOp::NotEqual:
            return vk::CompareOp::eNotEqual;
        default:
            ToVkConversionError("CompareOp", "CompareOp", value);
            return {};
        }
    }

    vk::LogicOp ToVkLogicOp(LogicOp value)
    {
        switch (value)
        {
        case LogicOp::And:
            return vk::LogicOp::eAnd;
        case LogicOp::AndInverted:
            return vk::LogicOp::eAndInverted;
        case LogicOp::AndReverse:
            return vk::LogicOp::eAndReverse;
        case LogicOp::Clear:
            return vk::LogicOp::eClear;
        case LogicOp::Copy:
            return vk::LogicOp::eCopy;
        case LogicOp::CopyInverted:
            return vk::LogicOp::eCopyInverted;
        case LogicOp::Equivalent:
            return vk::LogicOp::eEquivalent;
        case LogicOp::Invert:
            return vk::LogicOp::eInvert;
        case LogicOp::Nand:
            return vk::LogicOp::eNand;
        case LogicOp::NoOp:
            return vk::LogicOp::eNoOp;
        case LogicOp::Nor:
            return vk::LogicOp::eNor;
        case LogicOp::Or:
            return vk::LogicOp::eOr;
        case LogicOp::OrInverted:
            return vk::LogicOp::eOrInverted;
        case LogicOp::OrReverse:
            return vk::LogicOp::eOrReverse;
        case LogicOp::Set:
            return vk::LogicOp::eSet;
        case LogicOp::Xor:
            return vk::LogicOp::eXor;
        default:
            ToVkConversionError("LogicOp", "LogicOp", value);
            return {};
        }
    }

    vk::BlendOp ToVkBlendOp(BlendOp value)
    {
        switch (value)
        {
        case BlendOp::Add:
            return vk::BlendOp::eAdd;
        case BlendOp::Max:
            return vk::BlendOp::eMax;
        case BlendOp::Min:
            return vk::BlendOp::eMin;
        case BlendOp::ReverseSubtract:
            return vk::BlendOp::eReverseSubtract;
        case BlendOp::Subtract:
            return vk::BlendOp::eSubtract;
        default:
            ToVkConversionError("BlendOp", "BlendOp", value);
            return {};
        }
    }

    vk::BlendFactor ToVkBlendFactor(BlendFactor value)
    {
        switch (value)
        {
        case BlendFactor::One:
            return vk::BlendFactor::eOne;
        case BlendFactor::ConstantAlpha:
            return vk::BlendFactor::eConstantAlpha;
        case BlendFactor::ConstantColor:
            return vk::BlendFactor::eConstantColor;
        case BlendFactor::DstAlpha:
            return vk::BlendFactor::eDstAlpha;
        case BlendFactor::DstColor:
            return vk::BlendFactor::eDstColor;
        case BlendFactor::OneMinusConstantAlpha:
            return vk::BlendFactor::eOneMinusConstantAlpha;
        case BlendFactor::OneMinusConstantColor:
            return vk::BlendFactor::eOneMinusConstantColor;
        case BlendFactor::OneMinusDstAlpha:
            return vk::BlendFactor::eOneMinusDstAlpha;
        case BlendFactor::OneMinusDstColor:
            return vk::BlendFactor::eOneMinusDstColor;
        case BlendFactor::OneMinusSrc1Alpha:
            return vk::BlendFactor::eOneMinusSrc1Alpha;
        case BlendFactor::OneMinusSrc1Color:
            return vk::BlendFactor::eOneMinusSrc1Color;
        case BlendFactor::OneMinusSrcAlpha:
            return vk::BlendFactor::eOneMinusSrcAlpha;
        case BlendFactor::OneMinusSrcColor:
            return vk::BlendFactor::eOneMinusSrcColor;
        case BlendFactor::Src1Alpha:
            return vk::BlendFactor::eSrc1Alpha;
        case BlendFactor::Src1Color:
            return vk::BlendFactor::eSrc1Color;
        case BlendFactor::SrcAlpha:
            return vk::BlendFactor::eSrcAlpha;
        case BlendFactor::SrcAlphaSaturate:
            return vk::BlendFactor::eSrcAlphaSaturate;
        case BlendFactor::SrcColor:
            return vk::BlendFactor::eSrcColor;
        case BlendFactor::Zero:
            return vk::BlendFactor::eZero;
        default:
            ToVkConversionError("BlendFactor", "BlendFactor", value);
            return {};
        }
    }

    vk::Filter ToVkFilter(SamplerFilter value)
    {
        switch (value)
        {
        case SamplerFilter::Linear:
            return vk::Filter::eLinear;
        case SamplerFilter::Nearest:
            return vk::Filter::eNearest;
        case SamplerFilter::Cubic:
            return vk::Filter::eCubicEXT;
        default:
            ToVkConversionError("Filter", "SamplerFilter", value);
            return {};
        }
    }

    vk::SamplerMipmapMode ToVkSamplerMipmapMode(SamplerMipmapMode value)
    {
        switch (value)
        {
        case SamplerMipmapMode::Linear:
            return vk::SamplerMipmapMode::eLinear;
        case SamplerMipmapMode::Nearest:
            return vk::SamplerMipmapMode::eNearest;
        default:
            ToVkConversionError("SamplerMipmapMode", "SamplerMipmapMode", value);
            return {};
        }
    }

    vk::SamplerAddressMode ToVkSamplerAddressMode(SamplerAddressMode value)
    {
        switch (value)
        {
        case SamplerAddressMode::ClampToBorder:
            return vk::SamplerAddressMode::eClampToBorder;
        case SamplerAddressMode::ClampToEdge:
            return vk::SamplerAddressMode::eClampToEdge;
        case SamplerAddressMode::MirroredRepeat:
            return vk::SamplerAddressMode::eMirroredRepeat;
        case SamplerAddressMode::Repeat:
            return vk::SamplerAddressMode::eRepeat;
        default:
            ToVkConversionError("SamplerAddressMode", "SamplerAddressMode", value);
            return {};
        }
    }

    vk::Viewport ToVkViewport(const Viewport& value)
    {
        auto viewport     = vk::Viewport{};
        viewport.x        = value.x;
        viewport.y        = value.y;
        viewport.width    = value.width;
        viewport.height   = value.height;
        viewport.minDepth = value.min_depth;
        viewport.maxDepth = value.max_depth;
        return viewport;
    }

    vk::Rect2D ToVkScissor(const Scissor& value)
    {
        auto scissor          = vk::Rect2D{};
        scissor.offset.x      = static_cast<Int32>(value.x);
        scissor.offset.y      = static_cast<Int32>(value.y);
        scissor.extent.width  = value.width;
        scissor.extent.height = value.height;
        return scissor;
    }

    vk::PipelineStageFlags2 ToVkPipelineStageFlags(ShaderStageFlags stage_flags, vk::AccessFlags2 access_flags)
    {
        auto vk_flags = vk::PipelineStageFlags2{};

        if (access_flags & vk::AccessFlagBits2::eColorAttachmentRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eColorAttachmentOutput;
        }

        if (access_flags & vk::AccessFlagBits2::eColorAttachmentWrite)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eColorAttachmentOutput;
        }

        if (access_flags & vk::AccessFlagBits2::eDepthStencilAttachmentRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eEarlyFragmentTests
                | vk::PipelineStageFlagBits2::eLateFragmentTests;
        }

        if (access_flags & vk::AccessFlagBits2::eDepthStencilAttachmentWrite)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eEarlyFragmentTests
                | vk::PipelineStageFlagBits2::eLateFragmentTests;
        }

        if (access_flags & vk::AccessFlagBits2::eIndexRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eIndexInput;
        }

        if (access_flags & vk::AccessFlagBits2::eIndirectCommandRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eDrawIndirect;
        }

        if (access_flags & vk::AccessFlagBits2::eInputAttachmentRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eFragmentShader;
        }

        if (access_flags & vk::AccessFlagBits2::eTransferRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eTransfer;
        }

        if (access_flags & vk::AccessFlagBits2::eTransferWrite)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eTransfer;
        }

        if (access_flags & vk::AccessFlagBits2::eVertexAttributeRead)
        {
            vk_flags |= vk::PipelineStageFlagBits2::eVertexAttributeInput;
        }

        // Shader stage is only required for certain access flags. Most have singular stage mapped to them.
        if (access_flags
            & (vk::AccessFlagBits2::eUniformRead | vk::AccessFlagBits2::eShaderSampledRead
               | vk::AccessFlagBits2::eShaderRead | vk::AccessFlagBits2::eShaderWrite
               | vk::AccessFlagBits2::eShaderStorageRead | vk::AccessFlagBits2::eShaderStorageWrite))
        {
            if (stage_flags & ShaderStage::Compute)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eComputeShader;
            }

            if (stage_flags & ShaderStage::Fragment)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eFragmentShader;
            }

            if (stage_flags & ShaderStage::Geometry)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eGeometryShader;
            }

            if (stage_flags & ShaderStage::TessellationControl)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eTessellationControlShader;
            }

            if (stage_flags & ShaderStage::TessellationEvaluation)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eTessellationEvaluationShader;
            }

            if (stage_flags & ShaderStage::Vertex)
            {
                vk_flags |= vk::PipelineStageFlagBits2::eVertexShader;
            }

            VALO_ASSERT(vk_flags);
        }

        return vk_flags;
    }

    vk::AccessFlags2 ToVkAccessFlags(BufferUsageFlags usage_flags, AccessFlags access_flags)
    {
        auto vk_flags = vk::AccessFlags2{};

        if (usage_flags & BufferUsage::Storage)
        {
            VALO_ASSERT(usage_flags == BufferUsage::Storage);
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eShaderStorageRead;
            }
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eShaderStorageWrite;
            }
        }

        if (usage_flags & BufferUsage::Uniform)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eUniformRead;
            }
        }

        if (usage_flags & BufferUsage::Index)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eIndexRead;
            }
        }

        if (usage_flags & BufferUsage::Vertex)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eVertexAttributeRead;
            }
        }

        if (usage_flags & BufferUsage::Indirect)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eIndirectCommandRead;
            }
        }

        if (usage_flags & BufferUsage::TransferDst)
        {
            VALO_ASSERT(usage_flags == BufferUsage::TransferDst);
            VALO_ASSERT(!(access_flags & AccessFlagBits::Read));
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eTransferWrite;
            }
        }

        if (usage_flags & BufferUsage::TransferSrc)
        {
            VALO_ASSERT(usage_flags == BufferUsage::TransferSrc);
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eTransferRead;
            }
        }

        return vk_flags;
    }

    vk::AccessFlags2 ToVkAccessFlags(ImageUsageFlags usage_flags, AccessFlags access_flags)
    {
        auto vk_flags = vk::AccessFlags2{};

        if (usage_flags & ImageUsage::Storage)
        {
            VALO_ASSERT(usage_flags == ImageUsage::Storage);
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eShaderStorageRead;
            }
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eShaderStorageWrite;
            }
        }

        if (usage_flags & ImageUsage::ColorAttachment)
        {
            VALO_ASSERT(usage_flags == ImageUsage::ColorAttachment);
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eColorAttachmentRead;
            }
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eColorAttachmentWrite;
            }
        }

        if (usage_flags & ImageUsage::DepthStencilAttachment)
        {
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eDepthStencilAttachmentRead;
            }
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eDepthStencilAttachmentWrite;
            }
        }

        if (usage_flags & ImageUsage::InputAttachment)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eInputAttachmentRead;
            }
        }

        if (usage_flags & ImageUsage::Sampled)
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eShaderSampledRead;
            }
        }

        if (usage_flags & ImageUsage::TransferDst)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferDst);
            VALO_ASSERT(!(access_flags & AccessFlagBits::Read));
            if (access_flags & AccessFlagBits::Write)
            {
                vk_flags |= vk::AccessFlagBits2::eTransferWrite;
            }
        }

        if (usage_flags & ImageUsage::TransferSrc)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferSrc);
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            if (access_flags & AccessFlagBits::Read)
            {
                vk_flags |= vk::AccessFlagBits2::eTransferRead;
            }
        }

        return vk_flags;
    }

    vk::ImageLayout ToVkImageLayout(DescriptorType descriptor_type)
    {
        switch (descriptor_type)
        {
        case DescriptorType::CombinedImageSampler:
        case DescriptorType::SampledImage:
            return vk::ImageLayout::eReadOnlyOptimal;
        case DescriptorType::StorageImage:
            return vk::ImageLayout::eGeneral;
        default:
            ToVkConversionError("DescriptorType", "ImageLayout", descriptor_type);
            return vk::ImageLayout::eUndefined;
        }
    }

    vk::ImageLayout ToVkImageLayout(ImageUsageFlags usage_flags, AccessFlags access_flags)
    {
        if ((usage_flags & ImageUsage::ColorAttachment) && (access_flags & AccessFlagBits::Write))
        {
            VALO_ASSERT(usage_flags == ImageUsage::ColorAttachment);
            return vk::ImageLayout::eColorAttachmentOptimal;
        }

        if ((usage_flags & ImageUsage::DepthStencilAttachment) && (access_flags & AccessFlagBits::Write))
        {
            VALO_ASSERT(usage_flags == ImageUsage::DepthStencilAttachment);
            return vk::ImageLayout::eDepthStencilAttachmentOptimal;
        }

        if (usage_flags & ImageUsage::Storage)
        {
            VALO_ASSERT(usage_flags == ImageUsage::Storage);
            return vk::ImageLayout::eGeneral;
        }

        if (usage_flags & ImageUsage::TransferDst)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferDst);
            return vk::ImageLayout::eTransferDstOptimal;
        }

        if (usage_flags & ImageUsage::TransferSrc)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferSrc);
            return vk::ImageLayout::eTransferSrcOptimal;
        }

        if (usage_flags & (ImageUsage::DepthStencilAttachment | ImageUsage::InputAttachment | ImageUsage::Sampled))
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            return vk::ImageLayout::eShaderReadOnlyOptimal;
        }

        return vk::ImageLayout::eUndefined;
    }

    vk::ImageLayout ToVkImageLayout(ImageUsageFlags usage_flags, AccessFlags access_flags, Format format)
    {
        if ((usage_flags & ImageUsage::ColorAttachment) && (access_flags & AccessFlagBits::Write))
        {
            VALO_ASSERT(usage_flags == ImageUsage::ColorAttachment);
            return vk::ImageLayout::eColorAttachmentOptimal;
        }

        if ((usage_flags & ImageUsage::DepthStencilAttachment) && (access_flags & AccessFlagBits::Write))
        {
            VALO_ASSERT(usage_flags == ImageUsage::DepthStencilAttachment);
            return vk::ImageLayout::eDepthStencilAttachmentOptimal;
        }

        if (usage_flags & ImageUsage::Storage)
        {
            VALO_ASSERT(usage_flags == ImageUsage::Storage);
            return vk::ImageLayout::eGeneral;
        }

        if (usage_flags & ImageUsage::TransferDst)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferDst);
            return vk::ImageLayout::eTransferDstOptimal;
        }

        if (usage_flags & ImageUsage::TransferSrc)
        {
            VALO_ASSERT(usage_flags == ImageUsage::TransferSrc);
            return vk::ImageLayout::eTransferSrcOptimal;
        }

        if (usage_flags & (ImageUsage::DepthStencilAttachment | ImageUsage::InputAttachment | ImageUsage::Sampled))
        {
            VALO_ASSERT(!(access_flags & AccessFlagBits::Write));
            return IsDepthStencil(format) ? vk::ImageLayout::eDepthStencilReadOnlyOptimal
                                          : vk::ImageLayout::eShaderReadOnlyOptimal;
        }

        return vk::ImageLayout::eUndefined;
    }

    vk::BufferMemoryBarrier2 ToVkBufferMemoryBarrier(const BufferMemoryBarrier& barrier)
    {
        const auto src_access_mask = ToVkAccessFlags(barrier.src_usage_flags, barrier.src_access_flags);
        const auto src_stage_mask  = ToVkPipelineStageFlags(barrier.src_stage_flags, src_access_mask);

        const auto dst_access_mask = ToVkAccessFlags(barrier.dst_usage_flags, barrier.dst_access_flags);
        const auto dst_stage_mask  = ToVkPipelineStageFlags(barrier.dst_stage_flags, dst_access_mask);

        const auto vk_barrier = vk::BufferMemoryBarrier2{
            .srcStageMask  = src_stage_mask,
            .srcAccessMask = src_access_mask,
            .dstStageMask  = dst_stage_mask,
            .dstAccessMask = dst_access_mask,
            .offset        = barrier.offset,
            .size          = barrier.range,
        };
        return vk_barrier;
    }

    vk::ImageMemoryBarrier2 ToVkImageMemoryBarrier(const ImageMemoryBarrier& barrier)
    {
        const auto src_access_mask = ToVkAccessFlags(barrier.src_usage_flags, barrier.src_access_flags);
        const auto src_stage_mask  = ToVkPipelineStageFlags(barrier.src_stage_flags, src_access_mask);

        const auto dst_access_mask = ToVkAccessFlags(barrier.dst_usage_flags, barrier.dst_access_flags);
        const auto dst_stage_mask  = ToVkPipelineStageFlags(barrier.dst_stage_flags, dst_access_mask);

        const auto vk_subresource_range = vk::ImageSubresourceRange{
            .aspectMask     = vk::ImageAspectFlagBits::eColor,
            .baseMipLevel   = barrier.base_mip_level,
            .levelCount     = barrier.level_count,
            .baseArrayLayer = barrier.base_array_layer,
            .layerCount     = barrier.layer_count,
        };

        const auto vk_barrier = vk::ImageMemoryBarrier2{
            .srcStageMask     = src_stage_mask,
            .srcAccessMask    = src_access_mask,
            .dstStageMask     = dst_stage_mask,
            .dstAccessMask    = dst_access_mask,
            .oldLayout        = ToVkImageLayout(barrier.src_usage_flags, barrier.src_access_flags),
            .newLayout        = ToVkImageLayout(barrier.dst_usage_flags, barrier.dst_access_flags),
            .subresourceRange = vk_subresource_range,
        };
        return vk_barrier;
    }
}
