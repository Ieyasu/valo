module;

#include <Valo/Vulkan.hpp>

#include <vk_mem_alloc.h>

export module Valo.VulkanRHI:Image;

export namespace Valo::RHI
{
    struct VulkanImage final
    {
        vk::Image     vk_image{nullptr};
        vk::ImageView vk_image_view{nullptr};
        VmaAllocation vma_allocation{nullptr};
    };
}
