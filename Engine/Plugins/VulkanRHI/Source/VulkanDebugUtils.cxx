module;

#include <Valo/Macros.hpp>
#include <Valo/Vulkan.hpp>

#include <sstream>

module Valo.VulkanRHI;

import Valo.Logger;
import Valo.Std;

import :Conversions;

namespace Valo::RHI::VulkanDebugUtils
{
    vk::DebugUtilsMessengerEXT CreateMessenger(vk::Instance instance)
    {
        const auto [result, messenger] = instance.createDebugUtilsMessengerEXT(GetMessengerDescriptor(), nullptr);
        if (result != vk::Result::eSuccess)
        {
            Log::Error(
                "Failed to create Vulkan debug messenger: vk::Instance::createDebugUtilsMessengerEXT() failed ({})",
                result);
            return nullptr;
        }
        return messenger;
    }

    vk::DebugUtilsMessengerCreateInfoEXT GetMessengerDescriptor()
    {
        auto descriptor            = vk::DebugUtilsMessengerCreateInfoEXT{};
        descriptor.pfnUserCallback = DebugCallback;
        descriptor.setMessageType(
            vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation
            | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance);
        descriptor.setMessageSeverity(
            vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning
            | vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo);
        return descriptor;
    }

    VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT      message_severity,
        VkDebugUtilsMessageTypeFlagsEXT             message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        void* /*user_data*/)
    {
        std::ostringstream stream;
        switch (message_type)
        {
        case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
            ComposeSimpleDebugMessage(callback_data, stream);
            break;
        case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
        case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
            ComposeDetailedDebugMessage(message_type, callback_data, stream);
            break;
        default:
            Log::Fatal("Failed to log Vulkan validation message: unknown type {}", message_type);
            return VK_TRUE;
        }

        switch (message_severity)
        {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            Log::Trace(stream.str());
            return VK_FALSE;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            Log::Warning(stream.str());
            return VK_FALSE;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
            Log::Error(stream.str());
            return VK_FALSE;
        default:
            Log::Error("Failed to log Vulkan validation message: unknown severity {}", message_severity);
            return VK_FALSE;
        }
    }

    void ComposeSimpleDebugMessage(const VkDebugUtilsMessengerCallbackDataEXT* callback_data, std::ostringstream& stream)
    {
        stream << callback_data->pMessage;
    }

    void ComposeDetailedDebugMessage(
        VkDebugUtilsMessageTypeFlagsEXT             message_type,
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        std::ostringstream&                         stream)
    {
        // Compose header
        stream << callback_data->pMessageIdName;
        stream << " (";
        switch (message_type)
        {
        case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT:
            stream << "GENERAL";
            break;
        case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT:
            stream << "PERFORMANCE";
            break;
        case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT:
            stream << "VALIDATION";
            break;
        default:
            Log::Fatal("Failed to compose Vulkan validation message: unknown type {}", message_type);
            return;
        }
        stream << " " << callback_data->messageIdNumber;
        stream << ")" << std::endl;

        // Compose message
        stream << callback_data->pMessage;

        // Compose objects
        if (callback_data->objectCount > 0)
        {
            stream << std::endl;
            stream << "    Objects: " << callback_data->objectCount;
        }

        for (UInt32 i = 0; i < callback_data->objectCount; ++i)
        {
            const auto& obj = callback_data->pObjects[i];
            stream << std::endl;
            stream << "        ";
            stream << "[" << i << "] " << std::hex << obj.objectHandle;
            stream << ", type: " << static_cast<int>(obj.objectType);
            stream << ", name: ";
            if (obj.pObjectName == nullptr)
            {
                stream << "NULL";
            }
            else
            {
                stream << obj.pObjectName;
            }
        }
    }
}
