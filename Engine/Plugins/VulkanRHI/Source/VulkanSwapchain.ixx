module;

#include <Valo/Vulkan.hpp>

export module Valo.VulkanRHI:Swapchain;

import Valo.RHI;
import Valo.Std;
import Valo.Window;

import :Image;

export namespace Valo::RHI
{
    class VulkanInstance;
    class VulkanDevice;

    class VulkanSwapchain;
    using UniqueVulkanSwapchain = UniquePtr<VulkanSwapchain>;

    class VulkanSwapchain final
    {
    public:
        VulkanSwapchain() = default;

        VulkanSwapchain(const VulkanSwapchain& other)            = delete;
        VulkanSwapchain& operator=(const VulkanSwapchain& other) = delete;

        VulkanSwapchain(VulkanSwapchain&& other)            = delete;
        VulkanSwapchain& operator=(VulkanSwapchain&& other) = delete;

        ~VulkanSwapchain();

        static UniqueResult<vk::Result, VulkanSwapchain> Create(
            const VulkanInstance& instance,
            VulkanDevice&         device,
            WindowHandle          window);

        vk::Result Recreate();

        void CleanUp();

        const SwapchainInfo& GetInfo() const;

        vk::SwapchainKHR GetVkSwapchain() const;

        UInt32 GetCurrentImageIndex() const;

        PoolHandle<VulkanImage> GetImage(UInt32 index) const;

        ResultValue<vk::Result, UInt32> AquireNextImageIndex(vk::Semaphore semaphore);

    private:
        static ResultValue<vk::Result, vk::SurfaceFormatKHR> SelectSurfaceFormat(
            vk::PhysicalDevice physical_device,
            vk::SurfaceKHR     surface);

        static ResultValue<vk::Result, vk::PresentModeKHR> SelectPresentMode(
            vk::PhysicalDevice physical_device,
            vk::SurfaceKHR     surface,
            bool               vsync);

        ObserverPtr<const VulkanInstance> m_instance{nullptr};
        ObserverPtr<VulkanDevice>         m_device{nullptr};
        WindowHandle                      m_window{};

        vk::SurfaceKHR       m_vk_surface{nullptr};
        vk::SwapchainKHR     m_vk_swapchain{nullptr};
        vk::SurfaceFormatKHR m_vk_surface_format{};
        vk::PresentModeKHR   m_vk_present_mode{};

        SwapchainInfo                   m_info{};
        Vector<PoolHandle<VulkanImage>> m_image_handles{};
        UInt32                          m_current_image_index{0};
    };
}
