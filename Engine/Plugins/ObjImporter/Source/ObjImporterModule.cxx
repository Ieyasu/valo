module;

#include <filesystem>

module Valo.ObjImporter;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Renderer;
import Valo.Std;

import :ObjImporterModule;
import :TinyObjImporter;

namespace Valo
{
    ObjImporterModule::~ObjImporterModule() = default;

    bool ObjImporterModule::Initialize(Engine& engine)
    {
        m_asset_database = engine.Require<AssetDatabase>();
        m_asset_database->RegisterImporter(*this);

        auto renderer = engine.Require<Renderer>();
        m_tinyobj_importer = MakeUnique<TinyObjImporter>(renderer->m_material_store.get(), renderer->m_mesh_store.get());

        return true;
    }

    void ObjImporterModule::Deinitialize()
    {
        m_asset_database->DeregisterImporter(*this);
        m_tinyobj_importer.reset();
    }

    bool ObjImporterModule::CanImport(const Path& AssetPath) const
    {
        const auto Extension = AssetPath.GetExtension();
        return Extension == ".obj";
    }

    void ObjImporterModule::Import(AssetImportContext& context)
    {
        m_tinyobj_importer->Import(context);
    }
}
