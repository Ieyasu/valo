module;

#include <tiny_obj_loader.h>

export module Valo.ObjImporter:TinyObjImporter;

import Valo.AssetDatabase;
import Valo.Math;
import Valo.Renderer;
import Valo.Std;

import :VertexIndexCache;

export namespace Valo
{
    class TinyObjImporter final
    {
    public:
        TinyObjImporter(MaterialStoreHandle material_store, MeshStoreHandle mesh_store);

        void Import(AssetImportContext& context);

    private:
        void ImportMesh(const tinyobj::mesh_t& mesh, const tinyobj::attrib_t& attrib);
        void ImportLines(const tinyobj::lines_t& lines, const tinyobj::attrib_t& attrib);
        void ImportPoints(const tinyobj::points_t& points, const tinyobj::attrib_t& attrib);

        AssetDatabaseHandle m_asset_database{};

        MaterialStoreHandle m_material_store{};
        MeshStoreHandle     m_mesh_store{};
        MeshHandle          m_mesh{};

        Vector<UInt32> m_index_buffer{};
        Vector<Vec3>   m_position_buffer{};
        Vector<Vec3>   m_normal_buffer{};
        Vector<Vec4>   m_tangent_buffer{};
        Vector<Vec2>   m_uv_buffer{};

        VertexIndexCache m_vertex_index_cache{};
    };
}
