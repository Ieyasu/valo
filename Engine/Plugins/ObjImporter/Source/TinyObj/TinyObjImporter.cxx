module;

#include <tiny_obj_loader.h>

#include <filesystem>

module Valo.ObjImporter;

import Valo.AssetDatabase;
import Valo.Logger;
import Valo.Math;
import Valo.Renderer;
import Valo.Std;

import :TinyObjImporter;

namespace Valo
{
    TinyObjImporter::TinyObjImporter(MaterialStoreHandle material_store, MeshStoreHandle mesh_store) :
        m_material_store(material_store),
        m_mesh_store(mesh_store)
    {
    }

    void TinyObjImporter::Import(AssetImportContext& context)
    {
        auto path                     = context.GetAssetPath();
        auto reader_config            = tinyobj::ObjReaderConfig{};
        reader_config.triangulate     = true;
        reader_config.mtl_search_path = path.RemoveFilename().GetString();

        auto reader = tinyobj::ObjReader{};
        if (!reader.ParseFromFile(context.GetAssetPath(), reader_config))
        {
            context.AddErrorMessage(reader.Error());
            return;
        }

        if (!reader.Warning().empty())
        {
            context.AddWarningMessage(reader.Warning());
        }

        m_vertex_index_cache.Clear();

        m_mesh = m_mesh_store->CreateMesh();
        // context.add_asset(m_mesh);

        const auto& shapes = reader.GetShapes();
        const auto& attrib = reader.GetAttrib();
        for (const auto& shape : shapes)
        {
            ImportMesh(shape.mesh, attrib);
            ImportLines(shape.lines, attrib);
            ImportPoints(shape.points, attrib);
        }
    }

    void TinyObjImporter::ImportMesh(const tinyobj::mesh_t& mesh, const tinyobj::attrib_t& attrib)
    {
        for (size_t i = 0; i < mesh.indices.size(); i += 3)
        {
            auto triangle = UVec3{};
            for (UInt32 j = 0; j < 3; ++j)
            {
                // Get indices for attributes. If normals shouldn't be exported,
                // ignore the normal index
                const auto obj_attr_indices = mesh.indices[i + j];
                const auto attr_indices     = IVec3(
                    obj_attr_indices.vertex_index,
                    obj_attr_indices.normal_index,
                    obj_attr_indices.texcoord_index);

                // If vertex is already in the cache, don't add to their buffers again
                const auto cached_tri_index = m_vertex_index_cache.TryGet(attr_indices);
                if (cached_tri_index.has_value())
                {
                    triangle[j] = *cached_tri_index;
                    continue;
                }

                // If the vertex is not in the cache, create a new vertex and add
                // the index to the cache
                triangle[j] = static_cast<UInt32>(m_position_buffer.size());
                m_vertex_index_cache.Set(attr_indices, triangle[j]);

                // Add vertex position
                const auto vertex_index = static_cast<size_t>(obj_attr_indices.vertex_index);
                const auto vx           = attrib.vertices[3 * vertex_index];
                const auto vy           = attrib.vertices[3 * vertex_index + 1];
                const auto vz           = attrib.vertices[3 * vertex_index + 2];
                m_position_buffer.emplace_back(vx, vy, vz);

                // Add (optional) vertex normal
                if (obj_attr_indices.normal_index >= 0)
                {
                    const auto normal_index = static_cast<size_t>(obj_attr_indices.normal_index);
                    const auto nx           = attrib.normals[3 * normal_index];
                    const auto ny           = attrib.normals[3 * normal_index + 1];
                    const auto nz           = attrib.normals[3 * normal_index + 2];
                    m_normal_buffer.emplace_back(nx, ny, nz);
                }

                // Add (optional) vertex texture coordinate
                if (obj_attr_indices.texcoord_index >= 0)
                {
                    const auto texcoord_index = static_cast<size_t>(obj_attr_indices.texcoord_index);
                    const auto u              = attrib.texcoords[2 * texcoord_index];
                    const auto v              = attrib.texcoords[2 * texcoord_index + 1];
                    m_uv_buffer.emplace_back(u, v);
                }
            }
            m_index_buffer.emplace_back(triangle[0]);
            m_index_buffer.emplace_back(triangle[1]);
            m_index_buffer.emplace_back(triangle[2]);
        }
    }

    void TinyObjImporter::ImportLines(const tinyobj::lines_t& lines, const tinyobj::attrib_t& attrib)
    {
        (void)lines;
        (void)attrib;
        Log::Fatal("Not implemented");
    }

    void TinyObjImporter::ImportPoints(const tinyobj::points_t& points, const tinyobj::attrib_t& attrib)
    {
        (void)points;
        (void)attrib;
        Log::Fatal("Not implemented");
    }
}
