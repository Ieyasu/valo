export module Valo.ObjImporter:VertexIndexCache;

import Valo.Std;
import Valo.Math;

export namespace Valo
{
    class VertexIndexCache final
    {
    public:
        void Clear();

        Optional<UInt32> TryGet(const IVec3& key) const;

        void Set(IVec3 key, UInt32 value);

    private:
        UnorderedMap<IVec3, UInt32> m_cache{};
    };
}
