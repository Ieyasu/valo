export module Valo.ObjImporter:ObjImporterModule;

import Valo.AssetDatabase;
import Valo.Engine;
import Valo.Std;

import :TinyObjImporter;

export namespace Valo
{
    class ObjImporterModule final : public AssetImporterModule
    {
    public:
        ObjImporterModule()                                    = default;
        ObjImporterModule(const ObjImporterModule&)            = delete;
        ObjImporterModule(ObjImporterModule&&)                 = delete;
        ObjImporterModule& operator=(const ObjImporterModule&) = delete;
        ObjImporterModule& operator=(ObjImporterModule&&)      = delete;
        ~ObjImporterModule() override;

        bool CanImport(const String& asset_path) const override;

        void Import(AssetImportContext& context) override;

    private:
        bool Initialize(Engine& engine) override;

        void Deinitialize() override;

        AssetDatabaseHandle        m_asset_database{};
        UniquePtr<TinyObjImporter> m_tinyobj_importer{};
    };
}
