module Valo.ObjImporter;

import Valo.Std;
import Valo.Math;

import :VertexIndexCache;

namespace Valo
{
    void VertexIndexCache::Clear()
    {
        m_cache.clear();
    }

    Optional<UInt32> VertexIndexCache::TryGet(const IVec3& key) const
    {
        const auto& it = m_cache.find(key);
        if (it == m_cache.end())
        {
            return nullopt;
        }
        return it->second;
    }

    void VertexIndexCache::Set(IVec3 key, UInt32 value)
    {
        m_cache[key] = value;
    }
}
