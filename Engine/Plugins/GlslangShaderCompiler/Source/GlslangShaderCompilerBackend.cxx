module;

#include "DirStackIncluder.hpp"

#include <glslang/Public/ResourceLimits.h>
#include <glslang/Public/ShaderLang.h>
#include <glslang/SPIRV/GlslangToSpv.h>

#include <filesystem>

module Valo.GlslangShaderCompiler;

import Valo.Logger;
import Valo.ShaderCompiler;
import Valo.Std;

namespace Valo
{
    bool GlslangShaderCompilerBackend::IsShaderLanguageSupported(ShaderLanguage Language) const
    {
        return Language == ShaderLanguage::GLSL;
    }

    bool GlslangShaderCompilerBackend::IsShaderIRSupported(ShaderIR IR) const
    {
        return IR == ShaderIR::SPIRV;
    }

    Optional<BinaryBlob> GlslangShaderCompilerBackend::Compile(const ShaderCompilerOptions& Options)
    {
        // Load the shader source from file
        auto IsLoaded = LoadShaderSource(Options, StringBuffer);
        if (!IsLoaded)
        {
            return nullopt;
        }

        // Deduce the shader stage of the shader
        auto ShaderStage = GetStage(Options.Filepath);
        if (!ShaderStage.has_value())
        {
            AddCompilerError("Failed to deduce shader stage. Make sure the file extension is valid.");
            return nullopt;
        }

        EShLanguage ShaderType{};
        switch (ShaderStage.value())
        {
        case ShaderStageReflection::Fragment:
            ShaderType = EShLangFragment;
            break;
        case ShaderStageReflection::Vertex:
            ShaderType = EShLangVertex;
            break;
        case ShaderStageReflection::Geometry:
            ShaderType = EShLangGeometry;
            break;
        case ShaderStageReflection::Compute:
            ShaderType = EShLangCompute;
            break;
        case ShaderStageReflection::TessellationControl:
            ShaderType = EShLangTessControl;
            break;
        case ShaderStageReflection::TessellationEvaluation:
            ShaderType = EShLangTessEvaluation;
            break;
        case ShaderStageReflection::None:
        default:
            AddCompilerError("Unknown shader stage.");
            return nullopt;
        }

        const auto ShaderVersion    = 100;
        const auto SemanticsVersion = 100;
        const auto VulkanVersion    = glslang::EShTargetVulkan_1_2;
        const auto SpirvVersion     = glslang::EShTargetSpv_1_3;

        // Initialize glslang just once
        static bool GlslangInitialized = false;
        if (!GlslangInitialized)
        {
            glslang::InitializeProcess();
            GlslangInitialized = true;
        }

        // Initialize the glsl shader object to compile
        const auto* SourcePtr  = StringBuffer.c_str();
        auto        GlslShader = glslang::TShader(ShaderType);
        GlslShader.setStrings(&SourcePtr, 1);
        GlslShader.setEnvInput(glslang::EShSourceGlsl, ShaderType, glslang::EShClientVulkan, SemanticsVersion);
        GlslShader.setEnvClient(glslang::EShClientVulkan, VulkanVersion);
        GlslShader.setEnvTarget(glslang::EShTargetSpv, SpirvVersion);

        // Initialize file includer to resolve #include directives
        const auto* Resources = GetDefaultResources();
        const auto  Messages  = static_cast<EShMessages>(EShMsgSpvRules | EShMsgVulkanRules);
        const auto  Directory = Options.Filepath.GetParentDirectory();
        auto        Includer  = DirStackFileIncluder();
        Includer.pushExternalLocalDirectory(Directory);

        // Preprocess (resolve macros and includes)
        auto Preprocessed = String();
        if (!GlslShader
                 .preprocess(Resources, ShaderVersion, ENoProfile, false, false, Messages, &Preprocessed, Includer))
        {
            AddCompilerError("Failed to preprocess shader.");
            AddCompilerError(GlslShader.getInfoLog());
            AddCompilerError(GlslShader.getInfoDebugLog());
            return nullopt;
        }

        const auto* PreprocessedStr = Preprocessed.c_str();
        GlslShader.setStrings(&PreprocessedStr, 1);
        if (!GlslShader.parse(Resources, ShaderVersion, false, Messages))
        {
            AddCompilerError("Failed to parse shader.");
            AddCompilerError(GlslShader.getInfoLog());
            AddCompilerError(GlslShader.getInfoDebugLog());
            return nullopt;
        }

        auto Program = glslang::TProgram();
        Program.addShader(&GlslShader);
        if (!Program.link(Messages))
        {
            AddCompilerError("Failed to link shader.");
            AddCompilerError(Program.getInfoLog());
            AddCompilerError(Program.getInfoDebugLog());
            return nullopt;
        }

        // Translate to spriv
        auto Logger                 = spv::SpvBuildLogger();
        auto SpvOptions             = glslang::SpvOptions();
        SpvOptions.stripDebugInfo   = true;
        SpvOptions.disableOptimizer = true;
        SpvOptions.validate         = true;

        SpirvBuffer.clear();
        glslang::GlslangToSpv(*Program.getIntermediate(ShaderType), SpirvBuffer, &Logger, &SpvOptions);
        if (Logger.getAllMessages().length() > 0)
        {
            AddCompilerWarning(Logger.getAllMessages());
        }

        // Convert to binary blob
        auto Binary = BinaryBlob(SpirvBuffer.size() * sizeof(UInt32));
        Memory::Memcpy(Binary.data(), SpirvBuffer.data(), Binary.size());
        return Binary;
    }

    bool GlslangShaderCompilerBackend::LoadShaderSource(const ShaderCompilerOptions& Options, String& OutSource)
    {
        Log::Debug("Loading shader: '{}'", Options.Filepath);

        File SourceFile;
        if (!SourceFile.Open(Options.Filepath, FileMode::Text, FileAccess::Read))
        {
            AddCompilerError("File not found.");
            return false;
        }

        auto Line = String();

        // Clear the buffer
        OutSource.clear();

        // Add defines to the file
        while (SourceFile.ReadLine(Line))
        {
            StringFunc::LTrim(Line);
            if (Line.find("#version") == String::npos)
            {
                OutSource += Line;
                OutSource += "\n";
                for (const auto& Define : Options.Defines)
                {
                    OutSource += "#define ";
                    OutSource += Define;
                    OutSource += "\n";
                }
                break;
            }

            if (!Line.empty())
            {
                for (const auto& Define : Options.Defines)
                {
                    OutSource += "#define ";
                    OutSource += Define;
                    OutSource += "\n";
                }
                OutSource += Line;
                OutSource += "\n";
                break;
            }
        }

        while (SourceFile.ReadLine(Line))
        {
            OutSource += Line + "\n";
        }

        return true;
    }

    Optional<ShaderStageReflection> GlslangShaderCompilerBackend::GetStage(StringView Filepath)
    {
        if (Filepath.ends_with(".frag"))
        {
            return ShaderStageReflection::Fragment;
        }

        if (Filepath.ends_with(".vert"))
        {
            return ShaderStageReflection::Vertex;
        }

        if (Filepath.ends_with(".geom"))
        {
            return ShaderStageReflection::Geometry;
        }

        if (Filepath.ends_with(".comp"))
        {
            return ShaderStageReflection::Compute;
        }

        return nullopt;
    }
}
