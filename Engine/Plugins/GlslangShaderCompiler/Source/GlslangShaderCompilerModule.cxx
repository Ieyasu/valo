module Valo.GlslangShaderCompiler;

import Valo.Engine;
import Valo.ShaderCompiler;

namespace Valo
{
    bool GlslangShaderCompilerModule::Initialize(Engine& Engine)
    {
        Compiler = Engine.Require<ShaderCompilerModule>()->GetShaderCompiler();
        Compiler->RegisterBackend(CompilerBackend);
        return true;
    }

    void GlslangShaderCompilerModule::Deinitialize()
    {
        Compiler->DeregisterBackend(CompilerBackend);
    }
}
