export module Valo.GlslangShaderCompiler:GlslangShaderCompilerModule;

import Valo.Engine;
import Valo.ShaderCompiler;

import :GlslangShaderCompilerBackend;

export namespace Valo
{
    class GlslangShaderCompilerModule final : public EngineModule
    {
    public:
        bool Initialize(Engine& engine) override;

        void Deinitialize() override;

    private:
        ShaderCompilerHandle Compiler{};

        GlslangShaderCompilerBackend CompilerBackend{};
    };
}
