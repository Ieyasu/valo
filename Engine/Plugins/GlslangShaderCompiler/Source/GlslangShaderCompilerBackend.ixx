export module Valo.GlslangShaderCompiler:GlslangShaderCompilerBackend;

import Valo.ShaderCompiler;
import Valo.Std;

export namespace Valo
{
    class GlslangShaderCompilerBackend final : public ShaderCompilerBackend
    {
    protected:
        bool IsShaderLanguageSupported(ShaderLanguage Language) const override;

        bool IsShaderIRSupported(ShaderIR IR) const override;

        Optional<BinaryBlob> Compile(const ShaderCompilerOptions& Options) override;

    private:
        bool LoadShaderSource(const ShaderCompilerOptions& Options, String& OutSource);

        static Optional<ShaderStageReflection> GetStage(StringView Filepath);

        String         StringBuffer{};
        Vector<UInt32> SpirvBuffer{};
    };
}
