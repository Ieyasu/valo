#include <valo/spirv/result.hpp>
#include <valo/spirv/shader_compiler.hpp>

#include <catch2/catch_test_macros.hpp>
#include <catch2/generators/catch_generators.hpp>

namespace Valo
{
    using ShaderCompiler;
    using ShaderCompileResult;

    struct ShaderCompilerTestRun final
    {
        String              filepath{};
        ShaderCompileResult error{0};

        explicit ShaderCompilerTestRun(const String& p_filepath) :
            filepath("Shaders/Valid/" + p_filepath)
        {
        }

        ShaderCompilerTestRun(const String& p_filepath, ShaderCompileResult p_error) :
            filepath("Shaders/Invalid/" + p_filepath),
            error(p_error)
        {
        }
    };

    TEST_CASE("Valo::GlslangShaderCompiler (valid shaders)", "[spirv]")
    {
        auto compiler = ShaderCompiler{};

        auto shader_test = GENERATE(ShaderCompilerTestRun{"Trivial.vert"}, ShaderCompilerTestRun{"Trivial.frag"});

        SECTION("ShaderCompiler::compile() compiles without errors for valid shaders")
        {
            auto [result, shader_data] = compiler.Compile(shader_test.filepath);
            REQUIRE(result == ShaderCompileResult::Success);
        }
    }

    TEST_CASE("Valo::GlslangShaderCompiler (invalid shaders)", "[spirv]")
    {
        auto compiler = ShaderCompiler{};

        auto shader_test = GENERATE(
            ShaderCompilerTestRun{"file_not_found", ShaderCompileResult::ErrorFileNotFound},
            ShaderCompilerTestRun{"Shader.unknown", ShaderCompileResult::ErrorUnknownShaderStage});

        SECTION("ShaderCompiler::compile() emits correct errors for invalid shaders")
        {
            auto [result, shader_data] = compiler.Compile(shader_test.filepath);
            REQUIRE(result == shader_test.error);
        }
    }

    // TEST_CASE("Valo::ShaderCompiler (reflection)", "[spirv]")
    // {
    //     auto compiler = ShaderCompiler{};

    // SECTION("ShaderCompiler::compile() generates reflection data for 'trivial.vert'")
    // {
    //     const auto& shader_data = compiler.compile("shaders/valid/trivial.vert");
    //     REQUIRE(shader_data.has_value());

    // const auto& shader_reflection = shader_data.value()->reflection();
    // REQUIRE(shader_reflection.descriptor_set_bindings().size() == 0);
    // REQUIRE(shader_reflection.stage_inputs().size() == 2);
    // REQUIRE(shader_reflection.stage_outputs().size() == 1);

    // const auto in_pos_property = shader_reflection.property_id("position");
    // REQUIRE(in_pos_property.has_value());
    // REQUIRE(shader_reflection.stage_input(in_pos_property.value()).name == "position");
    // REQUIRE(shader_reflection.stage_input(in_pos_property.value()).location == 0);
    // REQUIRE(shader_reflection.stage_input(in_pos_property.value()).type.baseType == BaseTypeReflection::Float);
    // REQUIRE(shader_reflection.stage_input(in_pos_property.value()).type.columns == 3);
    // REQUIRE(shader_reflection.stage_input(in_pos_property.value()).type.rows == 1);

    // const auto in_uv_property = shader_reflection.property_id("uv");
    // REQUIRE(in_uv_property.has_value());
    // REQUIRE(shader_reflection.stage_input(in_uv_property.value()).name == "uv");
    // REQUIRE(shader_reflection.stage_input(in_uv_property.value()).location == 0);
    // REQUIRE(shader_reflection.stage_input(in_uv_property.value()).type.baseType == BaseTypeReflection::Float);
    // REQUIRE(shader_reflection.stage_input(in_uv_property.value()).type.columns == 2);
    // REQUIRE(shader_reflection.stage_input(in_uv_property.value()).type.rows == 1);

    // const auto out_uv_property = shader_reflection.property_id("out_uv");
    // REQUIRE(out_uv_property.has_value());
    // REQUIRE(shader_reflection.stage_input(out_uv_property.value()).location == 0);
    // REQUIRE(shader_reflection.stage_input(out_uv_property.value()).type.baseType == BaseTypeReflection::Float);
    // REQUIRE(shader_reflection.stage_input(out_uv_property.value()).type.columns == 2);
    // REQUIRE(shader_reflection.stage_input(out_uv_property.value()).type.rows == 1);}

    // SECTION("ShaderCompiler::compile() generates reflection data for 'trivial.frag'")
    // {
    //     const auto& shader_data = compiler.compile("shaders/valid/trivial.frag");
    //     REQUIRE(shader_data.has_value());

    // const auto& shader_reflection = shader_data.value()->reflection();
    // REQUIRE(shader_reflection.descriptor_set_bindings().size() == 1);
    // REQUIRE(shader_reflection.stage_inputs().size() == 1);
    // REQUIRE(shader_reflection.stage_outputs().size() == 1);

    // const auto img_property = shader_reflection.property_id("image");
    // REQUIRE(img_property.has_value());
    // REQUIRE(shader_reflection.descriptor_set_binding(img_property.value()).name == "image");
    // REQUIRE(shader_reflection.descriptor_set_binding(img_property.value()).set == 0);
    // REQUIRE(shader_reflection.descriptor_set_binding(img_property.value()).binding == 0);
    // REQUIRE(shader_reflection.descriptor_set_binding(img_property.value()).array_size == 0);

    // const auto uv_property = shader_reflection.property_id("uv");
    // REQUIRE(uv_property.has_value());
    // REQUIRE(shader_reflection.stage_input(uv_property.value()).name == "uv");
    // REQUIRE(shader_reflection.stage_input(uv_property.value()).location == 0);
    // REQUIRE(shader_reflection.stage_input(uv_property.value()).type.baseType == BaseTypeReflection::Float);
    // REQUIRE(shader_reflection.stage_input(uv_property.value()).type.columns == 2);
    // REQUIRE(shader_reflection.stage_input(uv_property.value()).type.rows == 1);

    // const auto col_property = shader_reflection.property_id("out_color");
    // REQUIRE(col_property.has_value());
    // REQUIRE(shader_reflection.stage_input(col_property.value()).name == "uv");
    // REQUIRE(shader_reflection.stage_input(col_property.value()).location == 0);
    // REQUIRE(shader_reflection.stage_input(col_property.value()).type.baseType == BaseTypeReflection::Float);
    // REQUIRE(shader_reflection.stage_input(col_property.value()).type.columns == 4);
    // REQUIRE(shader_reflection.stage_input(col_property.value()).type.rows == 1);}
}
