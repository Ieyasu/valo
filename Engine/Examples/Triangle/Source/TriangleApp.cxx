module Valo.Examples.TriangleApp;

import Valo.GlslangShaderCompiler;
import Valo.Math;

namespace Valo
{
    TriangleApp::TriangleApp()
    {
        m_renderer = GetEngine().Require<Renderer>();
        GetEngine().Require<GlslangShaderCompilerModule>();
        CreateTriangle();
    }

    void TriangleApp::CreateTriangle()
    {
        // Define some vertex data for our triangle
        const auto indices   = Vector<UInt32>{0, 1, 2};
        const auto positions = Vector<Vec3>{
            { 1,  1, 0},
            {-1,  1, 0},
            { 0, -1, 0}
        };
        const auto colors = Vector<Vec4>{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {0, 0, 1, 0}
        };

        // Create a new mesh and material to render the triangle
        auto mesh     = m_renderer->CreateMesh();
        auto material = m_renderer->CreateMaterial("Valo/Lit");

        // Assign vertex data to the mesh
        mesh->SetIndices(0, indices);
        mesh->SetPositions(positions);
        // mesh->SetColors(colors);

        // Create a render element containing all info required to render the mesh
        auto render_element      = RenderElement{};
        render_element.material  = material.Get();
        render_element.mesh      = mesh.Get();
        render_element.transform = Mat4(1);

        // Get render set for opaque geometry and insert the element there
        const auto opaque_set_descriptor = RenderSetDescriptor{
            .tags = {RenderTag::Opaque},
        };

        auto opaque_render_set = m_renderer->GetOrCreateRenderSet(opaque_set_descriptor);

        opaque_render_set->Insert(render_element);
    }
}
