import Valo.Examples.TriangleApp;

int main()
{
    auto app = Valo::TriangleApp();
    app.Run();

    return 0;
}
