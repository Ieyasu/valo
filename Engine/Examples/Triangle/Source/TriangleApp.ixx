export module Valo.Examples.TriangleApp;

import Valo.Renderer;
import Valo.Examples;
import Valo.Std;

export namespace Valo
{
    class TriangleApp final : public ExampleApp
    {
    public:
        TriangleApp();

    private:
        void CreateTriangle();

        ObserverPtr<Renderer> m_renderer{};
    };
}
