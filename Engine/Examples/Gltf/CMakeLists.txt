cmake_minimum_required(VERSION 3.30)

valo_add_example(Gltf)

target_sources(${target}
    PUBLIC FILE_SET cxx_modules TYPE CXX_MODULES FILES
        Source/GltfApp.ixx
    PRIVATE
        Source/Main.cpp
        Source/GltfApp.cxx
)
