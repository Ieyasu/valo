import Valo.Examples.GltfApp;

int main()
{
    auto app = Valo::GltfApp();
    app.Run();

    return 0;
}
