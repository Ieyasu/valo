export module Valo.Examples.GltfApp;

import Valo.AssetDatabase;
import Valo.Renderer;
import Valo.Examples;
import Valo.Std;

export namespace Valo
{
    class GltfApp final : public ExampleApp
    {
    public:
        GltfApp();

    private:
        void CreateMesh();

        ObserverPtr<AssetDatabase> m_asset_db{};
        ObserverPtr<Renderer>      m_renderer{};
    };
}
