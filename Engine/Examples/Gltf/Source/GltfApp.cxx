module;

#include <vector>

module Valo.Examples.GltfApp;

import Valo.GlslangShaderCompiler;
import Valo.GltfImporter;
import Valo.Math;

namespace Valo
{
    GltfApp::GltfApp()
    {
        m_asset_db = GetEngine().Require<AssetDatabase>();
        m_renderer = GetEngine().Require<Renderer>();
        GetEngine().Require<GltfImporterModule>();
        GetEngine().Require<GlslangShaderCompilerModule>();
        CreateMesh();
    }

    void GltfApp::CreateMesh()
    {
        const auto& assets    = m_asset_db->Import("glTFSampleAssets/SciFiHelmet/glTF/SciFiHelmet.gltf");
        const auto& meshes    = assets.GetAssets<Mesh>();
        const auto& materials = assets.GetAssets<Material>();

        // Get render set for opaque geometry and insert the element there
        const auto opaque_set_descriptor = RenderSetDescriptor{
            .tags = {RenderTag::Opaque},
        };

        auto opaque_render_set = m_renderer->GetOrCreateRenderSet(opaque_set_descriptor);
        for (auto mesh : meshes)
        {
            // Create a render element containing all info required to render the mesh
            auto render_element      = RenderElement{};
            render_element.material  = materials.front().Get();
            render_element.mesh      = mesh.Get();
            render_element.transform = Mat4(1);

            opaque_render_set->Insert(render_element);
        }
    }
}
