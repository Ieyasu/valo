export module Valo.Examples:ExampleApp;

import Valo.Engine;
import Valo.Input;
import Valo.Platform;
import Valo.Std;

export namespace Valo
{
    class ExampleApp
    {
    public:
        ExampleApp();
        ExampleApp(const ExampleApp&)            = delete;
        ExampleApp(ExampleApp&&)                 = delete;
        ExampleApp& operator=(const ExampleApp&) = delete;
        ExampleApp& operator=(ExampleApp&&)      = delete;
        virtual ~ExampleApp()                    = default;

        Engine& GetEngine();
        Input&  GetInput();

        void Run();

    private:
        Engine                      m_engine{};
        ObserverPtr<Input>          m_input{};
        ObserverPtr<PlatformModule> m_platform{};
    };
}
