module Valo.Examples;

import Valo.Engine;
import Valo.Logger;
import Valo.Input;
import Valo.Platform;

namespace Valo
{
    ExampleApp::ExampleApp() :
        m_engine(),
        m_input(m_engine.Require<Input>()),
        m_platform(m_engine.Require<PlatformModule>())
    {
        Log::Configure()
            .AddFormatter<Log::BasicFormatter>()
            .AddLogger<Log::ConsoleLogger>()
            .AddLogger<Log::FileLogger>("valo.log")
            .Finish();
    }

    Engine& ExampleApp::GetEngine()
    {
        return m_engine;
    }

    Input& ExampleApp::GetInput()
    {
        return *m_input;
    }

    void ExampleApp::Run()
    {
        m_engine.Start();

        while (m_engine.IsRunning())
        {
            m_engine.Update();
        }
    }
}
