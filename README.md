[![pipeline status](https://gitlab.com/Ieyasu/valo/badges/master/pipeline.svg)](https://gitlab.com/Ieyasu/valo/-/commits/master)
[![coverage report](https://gitlab.com/Ieyasu/valo/badges/master/coverage.svg)](https://gitlab.com/Ieyasu/valo/-/commits/master)

# Valo

A Vulkan graphics engine

## Build requirements

- glfw
- vulkan

## TODO

- Feature: QTangents
- Feature: Transforms and camera


- Optimization: Pass old swapchain to new spawchain pNext, might make resizing smoother
- Optimization: Meshes should be added to single vertex/index buffer
- Optimization: Only store shader binary and reflection data as long as needed
- Optimization: Move parts of GraphicsPipelineLayout descriptors to Material/ShaderEffect
- Optimization: Move parts of GraphicsPipelineLayout descriptors to ShaderData
- Optimization: Move PipelineLayout to shader data https://vkguide.dev/docs/gpudriven/material_system/
- Optimization: Save common GraphicsPipelineLayout objects as members

- Refactor: Split rendering core and rendering pipeline to different libraries. Pipeline should rely on core.

- Quality Control: Write tests for pool

## TODO far future

- Optimization: Animation compression with ACL
